# iBChatiOS and EMqttiOS
iBChatiOS is a pubsub based MQTT library wrote in swift using [Aws SDK IoT][aws-ios-sdk-iot] . It supports,

* SSL Connection (default port 8883) with Certificates, Username, and Password based authentication
* Web Socket Connection (default port 443) with AWS Cognito Pool Id based authentication. This works only for aws iot server end point.

iBChatiOS is a pubsub based MQTT library wrote using [CocoaMQTT][cocoa-mqtt] . It supports,

* SSL Connection (default port 8883) with Certificates, Username, and Password based authentication
* TCP Connection (default port 1883) with Username and Password based authentication

# Installation
We can use any type protocol based on authentication mechanism. Client and backend team needs to discuss and decide on a connection mechanism. Based on your use case authentication mechanism you can use either iBChatiOS or EMqttiOS. Both available as cocoapods.
If you have not initialized cocoapods previously, follow [Cocoapods][cocoapods] documentation.

#### Adding pods
Add below lines in the pod file. Make sure you use one of the pod only according your use case and do **pod install**
**For iBChatiOS**
```
source 'https://github.com/CocoaPods/Specs.git'
source 'https://bitbucket.org/rahulsccl/ibchat-ios-podspec.git'
target 'Your App' do
    ...
    pod 'iBChatiOS','1.0.8'
    ...
end
```

**For EmqttiOS**
```
source 'https://github.com/CocoaPods/Specs.git'
source 'https://bitbucket.org/rahulsccl/emqttios-podspec.git'
target 'Your App' do
    ...
    pod 'EMqttiOS','2.0.7'
    ...
end
```

# Overview
We need to configure certain parameters and need to start chat service. Once the connection establishes, It subscribes for new messages from the server and publishes messages from the user. When the user publishes a message backend processes it based **event_type** in the message [You will know more about it later] and sends an acknowledgment.

# Configuration
Now you need to create an object for Mqtt class in order to connect, publish or subscribe to it. To make sure that object won't get deallocate until the user closes the app you need to keep either in AppDelegate or MainUIViewController.

**For iBChatiOS**
```
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:
    [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    ...
    _ = MqttIoT()
    ...
}    
```
**For EMqttiOS**
```
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:
    [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    ...
    _ = EMqttIoT()
    ...
}    
```

There are some scenarios you need to start a service to connect with MQTT server.

* When you are initializing service for the first time. Here you need to set connection parameters required for connecting to the server, initializes service with type **ChatConstants.SERVICE_FROM_USER_ACTION**.
* When the service is already initialized, and you want to restart the service with some other connection parameters. Then you need set latest connection parameters and needs to start service with type **ChatConstants.SERVICE_FROM_RESTART_SESSION**.
* Handling messages when the app is not opened or offline using fcm.

### 1. Initializing service for the first time
All the below parameters will be communicated by backend team.

```
let connectionParameters = ConnectionParameters(
        caCertificate ,
        certificate ,
        clientEndPoint ,
        clientId ,
        cognitoPoolId,
        isSSLEnabled,
        mqttPort,
        password,
        pkcs12Data,
        privateKey,
        publishTopic,
        region,
        source,
        subscribeTopic,
        senderId,
        username
    );
String connectionParametersJson = (connectionParameters?.toJSONString()!)!;
MqttMethods.startChatService(connectionParametersJson: connectionParametersJson, serviceFrom: ChatConstants.SERVICE_FROM_USER_ACTION)
```

Here,

| Parameter | Description | Default
| ------ | ------ | -----
| caCertificate | Useful in android only, Send this as empty | Empty
| certificate | Useful in android only, Send this as empty | Empty
| clientEndPoint | MQTT server end point url, URL should not have any prefixes like **http, https, tcp, ssl** and post fixes like / , .  | Empty
| clientId | Unique id to connect with server | Empty
| cognitoPoolId | Configure this for cognito pool id based authentication in Web sockets connection| Empty
| isSSLEnabled | To enable SSL connection, Generally it will be true for certificate based authentication | false
| mqttPort | Port of MQTT server, send -1 for predefined ports TCP - 1883, SSL - 8883, Web Socket- 443 | -1
| password | Configure this to set password for TCP or ssl connection | Empty
| pkcs12Data | pkcs12Data for certificate authentication | Empty
| privateKey | Useful in android only, Send this as empty | Empty
| publishTopic | The topic to which our messages will be published | Empty
| region | One of the "USEast1", "USEast2", "USWest1", "USWest2", "EUWest1", "EUWest2", "EUCentral1", "APSoutheast1", "APNortheast1", "APNortheast2", "APSoutheast2", "APSouth1", "SAEast1", "CNNorth1", "CACentral1", "USGovWest1" | Empty
| source | Souce to identify the app uniquely by our server, we need to send this in every event we publish | Empty
| subscribeTopic | The topic from which we will receive messages published by server | Empty
| senderId | Unique user Id to identify sender in the particular source | Empty
| username | Configure this to set username for TCP or SSL connection | Empty

### 2. Restarting service with different connection parameters
It is similar to initializing service for the first time, except that you need to start service with type.

```
String connectionParametersJson = (connectionParameters?.toJSONString()!)!;
MqttMethods.startChatService(connectionParametersJson: connectionParametersJson, serviceFrom: ChatConstants.SERVICE_FROM_RESTART_SESSION);
```

### Publishing messages
Every message you publish should be in JSON format and should have event_type, sender_id, source, and cli_event_id.

* The event_type tells about the type of the event. To know more about different event types look into [Event Types][event-types].
* The sender_id is same as userId you gave while initializing in start service in connection parameters. You can get it anywhere by,
    **let sender_id=MqttMethods.getCurrentSenderId();**
* The source is same as the source you gave while initializing in start service in connection parameters. You can get it anywhere by,
    **let source=MqttMethods.getSource();**
* The cli_event_id will be useful to identify that event is initiated by the user currently using the app. If you keep some id to cli_event_id key in a publish event, the server will send same back in acknowledgment events. For example **cliEventId_<userid>_<ephoctime>**.

You can publish messages using MqttConstants.PUBLISH and [PublishEvent][publish-event-ios] through Notification center. For example,

```
let sampleEvent = SampleEvent(...)
let publishEvent = PublishEvent(eventJson: sampleEvent.toJSONString()!)
NotificationCenter.default.post(name: Notification.Name(rawValue: MqttConstants.PUBLISH), object: nil, userInfo: [MqttConstants.PUBLISH: publishEvent])
```

### Receiving messages when user is online
You can listen to notification center for MqttConstants.SUBSCRIBE_EVENT type. [SubscribeEvent][subscribe-event-ios] data will be in userInfo with key MqttConstants.SUBSCRIBE_EVENT. For Example,

```
var observer: Any?
override func viewDidLoad() {
   super.viewDidLoad()
   observer = notificationCenter.addObserver(forName: Notification.Name(rawValue: MqttConstants.SUBSCRIBE_EVENT), object: nil, queue: nil) { notification in
    guard let userInfo = notification.userInfo,
        let subscribeEvent = userInfo[MqttConstants.SUBSCRIBE_EVENT] as? SubscribeEvent else {
            print("No subscribe event found in notification")
            return
        }
        let eventType = EventType.deserialize(from: subscribeEvent.getIotMessage())
        if(eventType?.getEventType() == ChatConstants.EVENT_TYPE_MESSAGE) {
            let message = MessageItem.deserialize(from: subscribeEvent.getIotMessage())
            ChatCommonMethods.printD(message: "Message \(message?.content ?? "")")
        }
    }
}
```

And don't forget to remove the observer, when the user is leaving that UIViewController, Other wise you may receive messages as many times as you visit that class. For example,
```
    @IBAction func onBackPressed(_ sender: Any) {
        NotificationCenter.default.removeObserver(observer ?? self)
        NotificationCenter.default.removeObserver(observer ?? self, name: NSNotification.Name(rawValue: MqttConstants.SUBSCRIBE_EVENT), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
```

### Receiving messages when user is offline
Our backend server sends a message through fcm notification when the user is offline so that they will get notified when they are back to online. Follow [FCM iOS][fcm-ios] and [FCM iOS Configuration][fcm-configuration-ios] references to integrate fcm. You can see an example at [FCM iOS Example][fcm-ios-example]. Then change AppDelegate.swift as shown in [Example App Delegate][app-delegate]. Now you need to send fcm_token to server whenever a new token generate as shown in [Example App Delegate][fcm-token-refresh] and you can get latest fcm token where ever you want by,
```
let fcmToken = Messaging.messaging().fcmToken
```

You can see a sample to send fcm token to backend server at [FCM Server Registration Sample][fcm-server-registration-sample
]

### Listening for MQTT connection status
For receiving any messages from service we need to subscribe to SubscribeEvent through [Event Bus][event-bus]. The message is connection callback if the event type is **ChatConstants.EVENT_TYPE_CONNECTION_CALL_BACK**. Then you can get the status using,

```
message:String = subscribeEvent.getIotMessage();
let connectionCallbackEvent = ConnectionCallbackEvent.deserialize(from: subscribeEvent.getIotMessage())
String connectionStatus=connectionCallbackEvent.connectionStatus;
```

Apart from that you can get the current connection status anywhere by,

```
connectionStatus:String = MqttMethods.getConnectionStatus();
```

The connectionStatus value will be one of the following,

* **MqttConstants.CONNECTING** - Client is connecting to the server
* **MqttConstants.CONNECTED** - Client is connected to the server
* **MqttConstants.CONNECTION_LOST** - Client lost the connection with the server because of some network error or wrong configuration. You check logs for information.
* **MqttConstants.RECONNECTING** - Client is reconnecting to the server. This happens when connection lost is because of some network error and trying to reconnect.
* **MqttConstants.DISCONNECTED** - Client is disconnected with server when we invoke disconnect method()

### Debugging
To enable debugging keep below statements in AppDelegate. Make sure you set them properly or removing them in prod environment.

**For iBChatiOS**
```
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:
    [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    ...
    ChatConstants.isDebug = true
    //To Enable AWS Logs
    AWSDDLog.sharedInstance.logLevel = .warning
    AWSDDLog.add(AWSDDTTYLogger.sharedInstance)
    ...
}
```

**For EMqttiOS**
```
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:
    [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    ...
    ChatConstants.isDebug = true
    ...
}    
```


### References
To use User defaults or Shared preferences
https://stackoverflow.com/questions/19206762/equivalent-to-shared-preferences-in-ios

Notification center for communication between components
http://dev.iachieved.it/iachievedit/notifications-and-userinfo-with-swift-3-0/

To create a custom cocoapod
https://guides.cocoapods.org/making/using-pod-lib-create.html

To change objective-c Appdelegate to swift
https://stackoverflow.com/questions/40420755/integrate-react-native-app-to-swift-3

To disintegrate cocoapods
https://github.com/CocoaPods/cocoapods-deintegrate

To fix errors when upgrading from react-native version 0.3** to 0.4**
http://imgur.com/a/KUOQC

To convert hex string to Data - Useful when importing p12 data from hex string instead of file
https://stackoverflow.com/questions/32231926/nsdata-from-hex-string

To convert Json to ios model and vice versa conversions
https://github.com/alibaba/HandyJSON

To Integrate FCM in iOS swift
http://shubhank101.github.io/iOSAndroidChaosOverFlow/2016/07/Push-Notification-in-iOS-using-FCM-(Swift)
https://firebase.google.com/docs/cloud-messaging/ios/clientfcm-ios

Know the difference between ‘?’ and ‘!’ before using
https://stackoverflow.com/questions/24496830/difference-between-and-in-swift-language

To skip updating all local repos while doing 'pod update'
https://github.com/CocoaPods/CocoaPods/issues/1023

# License

MIT

   [aws-ios-sdk-iot]: <https://github.com/aws/aws-sdk-ios/tree/master/AWSIoT>
   [cocoa-mqtt]:<https://github.com/emqtt/CocoaMQTT>
   [cocoapods]:<https://guides.cocoapods.org/using/using-cocoapods.html>
   [fcm-ios]:<https://firebase.google.com/docs/cloud-messaging/ios/clientfcm-ios>
   [fcm-ios-example]:<https://github.com/firebase/quickstart-ios/tree/master/messaging>
   [fcm-configuration-ios]:<http://shubhank101.github.io/iOSAndroidChaosOverFlow/2016/07/Push-Notification-in-iOS-using-FCM-(Swift)>
   [publish-event-ios]: <https://bitbucket.org/rahulsccl/ibchat-ios/src/c7f9f39a690740caff57875ea43e9c9d774b36f4/iBChatiOS/Classes/Models/PublishEvent.swift?at=master&fileviewer=file-view-default>
   [subscribe-event-ios]: <https://bitbucket.org/rahulsccl/ibchat-ios/src/c7f9f39a690740caff57875ea43e9c9d774b36f4/iBChatiOS/Classes/Models/SubscribeEvent.swift?at=master&fileviewer=file-view-default>
   [event-types]: <http://keepeventtypesaddresshere>
   [app-delegate]:<https://bitbucket.org/rahulsccl/ibchat-ios/src/21244d4067b1ed7dc2e6f37863e82548d9d19c7c/Example/iBChatiOS/AppDelegate.swift?at=master&fileviewer=file-view-default>
   [fcm-token-refresh]:<https://bitbucket.org/rahulsccl/ibchat-ios/src/21244d4067b1ed7dc2e6f37863e82548d9d19c7c/Example/iBChatiOS/AppDelegate.swift?at=master&fileviewer=file-view-default#AppDelegate.swift-191:193>
   [fcm-server-registration-sample]:<https://bitbucket.org/rahulsccl/ibchat-ios/src/21244d4067b1ed7dc2e6f37863e82548d9d19c7c/Example/iBChatiOS/PublishViewControllder.swift?at=master&fileviewer=file-view-default#PublishViewControllder.swift-64:112>