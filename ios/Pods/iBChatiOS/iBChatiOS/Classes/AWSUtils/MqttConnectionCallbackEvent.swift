//
//  MqttConnectionCallbackEvent.swift
//  Pods
//
//  Created by iB Hubs Mini Mac on 07/08/17.
//
//

import AWSIoT
import HandyJSON

public class MqttConnectionCallbackEvent: HandyJSON {
    public var connectionStatus: String?
    public var event_type: String = ChatConstants.EVENT_TYPE_CONNECTION_CALL_BACK
    public required init() {}

    public init(connectionStatus: AWSIoTMQTTStatus) {
        self.connectionStatus = MqttConnectionCallbackEvent.getStatus(connectionStatus: connectionStatus)
    }

    public static func getStatus(connectionStatus: AWSIoTMQTTStatus) -> String {
        switch connectionStatus {
        case .connecting:
            return MqttConstants.CONNECTING
        case .connected:
            return MqttConstants.CONNECTED 
        case .disconnected:
            return MqttConstants.DISCONNECTED
        default:
            return MqttConstants.CONNECTION_LOST
        }
    }
}
