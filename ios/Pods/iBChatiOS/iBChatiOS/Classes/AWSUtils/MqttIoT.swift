//
//  MqttIoT.swift
//  iBChatiOS
//
//  Created by iB Hubs Mini Mac on 31/07/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import AWSIoT
import ReachabilitySwift
import UIKit

public class MqttIoT {

    public static let singleInstance: MqttIoT = MqttIoT()

    var connectionStatus: AWSIoTMQTTStatus = .disconnected

    var iotDataManager: AWSIoTDataManager?
    var iotData: AWSIoTData!
    var iotManager: AWSIoTManager!
    var iot: AWSIoT!
    var credentialProvider: AWSCredentialsProvider?

    let TAG: String = "MqttIoT"
    private var reachability = Reachability()
    var hasInternet: Bool = false
    var unPublishedEvents: Array<PublishEvent> = Array()
    var connectionStatusTAG: String = "Disconnected"

    var isLogoutInitiated: Bool = false
    var isRestartInitiated: Bool = false
    var isConnectionInitiated: Bool = false
    
    let notificationCenter = NotificationCenter.default
    var connectObserver: Any?
    var startObserver: Any?
    var publishObserver: Any?
    var restartServiceObserver: Any?
    var logoutServiceObserver: Any?

    func mqttEventCallback(_ status: AWSIoTMQTTStatus) {
        DispatchQueue.main.async {
            self.connectionStatus = status
            switch status {
            case .connecting:
                self.connectionStatusTAG = "Connecting"
            case .connected:
                self.connectionStatusTAG = "Connected"
                self.iotDataManager?.subscribe(toTopic: MqttMethods.getSubscribeKey(), qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: {
                    (payload) -> Void in
                    let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
                    ChatCommonMethods.printD(tag: self.TAG, message: "New event\(stringValue)")
                    let payloadJson = stringValue as String
                    let eventType = EventType.deserialize(from: payloadJson)
                    if eventType != nil && eventType?.getEventType() != nil && eventType?.getEventType() != ChatConstants.EVENT_TYPE_ALIVE_MESSAGE_ACK {
                        let subscribeEvent = SubscribeEvent(iotMessage: payloadJson)
                        self.notificationCenter.post(name: Notification.Name(rawValue: MqttConstants.SUBSCRIBE_EVENT), object: nil, userInfo: [MqttConstants.SUBSCRIBE_EVENT: subscribeEvent])
                    }
                })
                var publishEvents: Array<PublishEvent> = Array()
                for publishEvent in self.unPublishedEvents {
                    publishEvents.append(publishEvent)
                }
                self.unPublishedEvents.removeAll()
                for publishEvent in publishEvents {
                    self.notificationCenter.post(name: Notification.Name(rawValue: MqttConstants.PUBLISH_EVENT), object: nil, userInfo: [MqttConstants.PUBLISH_EVENT: publishEvent])
                }
            case .disconnected:
                self.connectionStatusTAG = "Disconnected"
                self.iotDataManager = nil
                if(self.isLogoutInitiated==false){
                    self.notificationCenter.post(name: Notification.Name(rawValue: MqttConstants.START_SERVICE), object: nil)
                }
            case .connectionError:
                self.connectionStatusTAG = "Connection error"
            case .connectionRefused:
                self.connectionStatusTAG = "Connection fefused"
            case .protocolError:
                self.connectionStatusTAG = "Protocol error"
            default:
                self.connectionStatusTAG = "Unknown"
            }
            MqttMethods.setConnectionStatus(connectionStatus: MqttConnectionCallbackEvent.getStatus(connectionStatus: self.connectionStatus))
            let connectionCallbackEvent = MqttConnectionCallbackEvent(connectionStatus: self.connectionStatus)
            let subscribeEvent = SubscribeEvent(iotMessage: connectionCallbackEvent.toJSONString()!)
            self.notificationCenter.post(name: Notification.Name(rawValue: MqttConstants.SUBSCRIBE_EVENT), object: nil, userInfo: [MqttConstants.SUBSCRIBE_EVENT: subscribeEvent])
            ChatCommonMethods.printD(tag: self.TAG, message: "connection status = \(self.connectionStatusTAG)  \(status.rawValue)")
        }
    }

    private init() {

        connectObserver = notificationCenter.addObserver(forName: Notification.Name(rawValue: MqttConstants.CONNECT), object: nil, queue: nil) { _ in
            if MqttMethods.getPkcs12Data() != "" || MqttMethods.getCognitoIdentityPoolId() != "" {
                ChatCommonMethods.printD(message: "getPkcs12Data or cognito pool id is not empty")
                if self.connectionStatus != .connecting && self.connectionStatus != .connected && self.isRestartInitiated == false {
                    if self.iotDataManager == nil {
                        ChatCommonMethods.printD(message: "iotDataManager is null")
                        DispatchQueue.main.async {
                            self.initializeConfig()
                        }
                    } else {
                        ChatCommonMethods.printD(message: "iotDataManager not null")
                        if MqttMethods.getCognitoIdentityPoolId() != "" {
                            DispatchQueue.main.async {
                                self.iotDataManager?.connectUsingWebSocket(withClientId: MqttMethods.getMqttClientId(), cleanSession: true, statusCallback: self.mqttEventCallback)
                            }
                        } else {
                            let defaults = UserDefaults.standard
                            let certificateId = defaults.string(forKey: ChatConstants.CERTIFICATE_ID)
                            ChatCommonMethods.printD(tag: self.TAG, message: "Certificate Id \(certificateId ?? "")")
                            if certificateId == nil {
                                if AWSIoTManager.importIdentity(fromPKCS12Data: Data(hex: MqttMethods.getPkcs12Data().replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "\n", with: "")), passPhrase: "", certificateId: ChatConstants.CERTIFICATE_ID) {
                                    defaults.set(ChatConstants.CERTIFICATE_ID, forKey: ChatConstants.CERTIFICATE_ID)
                                    defaults.set("from-bundle", forKey: "certificateArn")
                                }
                            }
                            DispatchQueue.main.async {
                                self.iotDataManager?.connect(withClientId: MqttMethods.getMqttClientId(), cleanSession: true, certificateId: ChatConstants.CERTIFICATE_ID, statusCallback: self.mqttEventCallback)
                            }
                        }
                    }
                } else {
                    ChatCommonMethods.printD(tag: self.TAG, message: "Connection in progress with status \(self.connectionStatus.rawValue)")
                }
            } else {
                ChatCommonMethods.printD(message: "getPkcs12Data and cognito poolid are empty")
            }
        }

        publishObserver = notificationCenter.addObserver(forName: Notification.Name(rawValue: MqttConstants.PUBLISH_EVENT), object: nil, queue: nil) { notification in
            guard let userInfo = notification.userInfo,
                let publishEvent = userInfo[MqttConstants.PUBLISH_EVENT] as? PublishEvent else {
                print("No publish event found in notification")
                return
            }
            ChatCommonMethods.printD(tag: "\(self.TAG) Publish", message: "\(self.connectionStatus.rawValue) \(publishEvent.eventJson!)")
            if self.connectionStatus == .connected {
                DispatchQueue.main.async {
                    self.iotDataManager?.publishString(publishEvent.eventJson!, onTopic: MqttMethods.getPublishKey(), qoS: .messageDeliveryAttemptedAtMostOnce)
                }
            } else {
                let eventType = EventType.deserialize(from: publishEvent.eventJson)
                if eventType?.getEventType() != ChatConstants.EVENT_TYPE_ALIVE_MESSAGE {
                    self.unPublishedEvents.append(publishEvent)
                }
                if self.hasInternet {
                    self.notificationCenter.post(name: Notification.Name(rawValue: MqttConstants.CONNECT), object: nil)
                }
            }
        }

        startObserver = notificationCenter.addObserver(forName: Notification.Name(rawValue: MqttConstants.START_SERVICE), object: nil, queue: nil) { _ in
            self.notificationCenter.post(name: Notification.Name(rawValue: MqttConstants.CONNECT), object: nil)
            DispatchQueue.main.async {
                self.internetCallbacks()
            }
        }

        restartServiceObserver = notificationCenter.addObserver(forName: Notification.Name(rawValue: MqttConstants.RESTART_SERVICE), object: nil, queue: nil) { _ in
            ChatCommonMethods.printD(message: "Restarting service here")
            if self.iotDataManager != nil {
                DispatchQueue.main.async {
                    self.connectionStatus = .disconnected
                    self.isRestartInitiated = true
                    self.iotDataManager?.disconnect()
                }
            }
        }
        
        logoutServiceObserver = notificationCenter.addObserver(forName: Notification.Name(rawValue: MqttConstants.LOGOUT_SERVICE), object: nil, queue: nil) { _ in
            ChatCommonMethods.printD(message: "Logout here")
            if self.iotDataManager != nil {
                DispatchQueue.main.async {
                    self.connectionStatus = .disconnected
                    self.isLogoutInitiated = true;
                    self.iotDataManager?.disconnect()
                }
            }
        }
        notificationCenter.post(name: Notification.Name(rawValue: MqttConstants.START_SERVICE), object: nil)
    }

    func initializeConfig() {

        let clientEndPoint = "https://" + MqttMethods.getClientEndPoint()
        let iotEndPoint = AWSEndpoint(urlString: clientEndPoint)
        // configuration for AWSIoT control plane APIs
        if MqttMethods.getCognitoIdentityPoolId() != "" {
            credentialProvider = AWSCognitoCredentialsProvider(regionType: AWSUtils.getAWSiOSRegion(), identityPoolId: MqttMethods.getCognitoIdentityPoolId())
        }
        let iotConfiguration = AWSServiceConfiguration(region: AWSUtils.getAWSiOSRegion(), credentialsProvider: credentialProvider)
        // configuration for AWSIoT Data plane APIs
        let iotDataConfiguration = AWSServiceConfiguration(region: AWSUtils.getAWSiOSRegion(), endpoint: iotEndPoint, credentialsProvider: credentialProvider)
        AWSServiceManager.default().defaultServiceConfiguration = iotConfiguration

        let lastWillAndTestament = LastWillAndTestament(sender_id: MqttMethods.getCurrentSenderId())
        let lwtTopic: NSString = MqttMethods.getPublishKey() as NSString
        let lwtMessage: NSString = lastWillAndTestament.toJSONString()! as NSString
        iotDataManager?.mqttConfiguration.lastWillAndTestament.topic = lwtTopic as String
        iotDataManager?.mqttConfiguration.lastWillAndTestament.message = lwtMessage as String
        iotDataManager?.mqttConfiguration.lastWillAndTestament.qos = .messageDeliveryAttemptedAtMostOnce

        let lwtConfig = AWSIoTMQTTLastWillAndTestament()
        lwtConfig.message = lwtMessage as String
        lwtConfig.topic = lwtTopic as String
        lwtConfig.qos = .messageDeliveryAttemptedAtMostOnce

        let mqttConfig = AWSIoTMQTTConfiguration(keepAliveTimeInterval: Double(MqttMethods.getKeepAliveTime()),
                                                 baseReconnectTimeInterval: 1.0,
                                                 minimumConnectionTimeInterval: 20.0,
                                                 maximumReconnectTimeInterval: 128.0,
                                                 runLoop: RunLoop.current,
                                                 runLoopMode: RunLoopMode.defaultRunLoopMode.rawValue,
                                                 autoResubscribe: true,
                                                 lastWillAndTestament: lwtConfig)

        iotManager = AWSIoTManager.default()
        iot = AWSIoT.default()

        AWSIoTDataManager.register(with: iotDataConfiguration!, with: mqttConfig, forKey: "MyIotDataManager")
        iotDataManager = AWSIoTDataManager(forKey: "MyIotDataManager")

        AWSIoTData.register(with: iotDataConfiguration!, forKey: "MyIotData")
        iotData = AWSIoTData(forKey: "MyIotData")

        notificationCenter.post(name: Notification.Name(rawValue: MqttConstants.CONNECT), object: nil)
    }

    func internetCallbacks() {
        hasInternet = ChatCommonMethods.isNetworkAvailable()
        reachability?.whenReachable = { _ in
            ChatCommonMethods.printD(tag: self.TAG, message: "Got internet connection")
            self.hasInternet = true
        }
        reachability?.whenUnreachable = { _ in
            ChatCommonMethods.printD(tag: self.TAG, message: "Lost internet connection")
            self.hasInternet = false
        }
        do {
            try reachability?.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }

    deinit {
        ChatCommonMethods.printD(tag: TAG, message: "de init called")
        notificationCenter.removeObserver(self)
        notificationCenter.removeObserver(connectObserver ?? self)
        notificationCenter.removeObserver(connectObserver ?? self, name: NSNotification.Name(rawValue: MqttConstants.CONNECT), object: nil)
        notificationCenter.removeObserver(startObserver ?? self)
        notificationCenter.removeObserver(startObserver ?? self, name: NSNotification.Name(rawValue: MqttConstants.START_SERVICE), object: nil)
        notificationCenter.removeObserver(publishObserver ?? self)
        notificationCenter.removeObserver(publishObserver ?? self, name: NSNotification.Name(rawValue: MqttConstants.PUBLISH_EVENT), object: nil)
        notificationCenter.removeObserver(restartServiceObserver ?? self)
        notificationCenter.removeObserver(restartServiceObserver ?? self, name: NSNotification.Name(rawValue: MqttConstants.RESTART_SERVICE), object: nil)
    }
}
