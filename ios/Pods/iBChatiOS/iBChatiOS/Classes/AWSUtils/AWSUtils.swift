//
//  AWSUtils.swift
//  Pods
//
//  Created by iB Hubs Mini Mac on 07/08/17.
//
//
import AWSIoT
import UIKit

public class AWSUtils {
    public static func getAWSiOSRegion() -> AWSRegionType {
        switch MqttMethods.getAWSRegion() {
        case "USEast1":
            return AWSRegionType.USEast1
        case "USEast2":
            return AWSRegionType.USEast2
        case "USWest1":
            return AWSRegionType.USWest1
        case "USWest2":
            return AWSRegionType.USWest2
        case "EUWest1":
            return AWSRegionType.EUWest1
        case "EUWest2":
            return AWSRegionType.EUWest2
        case "EUCentral1":
            return AWSRegionType.EUCentral1
        case "APSoutheast1":
            return AWSRegionType.APSoutheast1
        case "APNortheast1":
            return AWSRegionType.APNortheast1
        case "APNortheast2":
            return AWSRegionType.APNortheast2
        case "APSoutheast2":
            return AWSRegionType.APSoutheast2
        case "APSouth1":
            return AWSRegionType.APSouth1
        case "SAEast1":
            return AWSRegionType.SAEast1
        case "CNNorth1":
            return AWSRegionType.CNNorth1
        case "CACentral1":
            return AWSRegionType.CACentral1
        case "USGovWest1":
            return AWSRegionType.USGovWest1
        default:
            return AWSRegionType.Unknown
        }
    }
}
