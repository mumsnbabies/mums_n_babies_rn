//
//  KeepAlive.swift
//  sample
//
//  Created by iB Hubs Mini Mac on 01/07/17.
//  Copyright © 2017 iB Hubs. All rights reserved.
//

import HandyJSON

public class KeepAlive: HandyJSON {
    public var event_type: String?
    public var source: String?
    public var sender_id: String?

    public required init() {}

    public init(sender_id: String) {
        source = MqttMethods.getSource()
        self.sender_id = sender_id
        event_type = ChatConstants.EVENT_TYPE_ALIVE_MESSAGE
    }

    public func getEvent_type() -> String {
        return event_type!
    }
}
