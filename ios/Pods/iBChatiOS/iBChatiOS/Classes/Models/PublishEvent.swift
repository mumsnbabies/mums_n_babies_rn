//
//  ScheduleEvent.swift
//  sample
//
//  Created by iB Hubs Mini Mac on 01/07/17.
//  Copyright © 2017 iB Hubs. All rights reserved.
//

public class PublishEvent {
    public var eventJson: String?

    public required init() {}

    public init(eventJson: String) {
        self.eventJson = eventJson
    }
}
