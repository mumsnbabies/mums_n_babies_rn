//
//  LastWillAndTestament.swift
//  sample
//
//  Created by iB Hubs Mini Mac on 17/07/17.
//  Copyright © 2017 iB Hubs. All rights reserved.
//

import HandyJSON
import UIKit

public class LastWillAndTestament: HandyJSON {
    public var cli_event_id: String?
    public var sender_id: String?
    public var source: String?
    public var event_type: String?

    public required init() {}

    public init(sender_id: String) {
        source = MqttMethods.getSource()
        self.sender_id = sender_id
        event_type = ChatConstants.EVENT_TYPE_LAST_WILL_MESSAGE
        cli_event_id = MqttMethods.getClientEventId()
    }
}
