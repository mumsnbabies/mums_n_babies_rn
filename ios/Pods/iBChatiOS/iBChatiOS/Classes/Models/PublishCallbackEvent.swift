//
//  PublishCallbackEvent.swift
//  sample
//
//  Created by iB Hubs Mini Mac on 01/07/17.
//  Copyright © 2017 iB Hubs. All rights reserved.
//

import HandyJSON

public class PublishCallbackEvent: HandyJSON {
    public var status: String?
    public var messageJson: String?
    public var event_type: String = ChatConstants.EVENT_TYPE_PUBLISH_CALL_BACK
    public required init() {}

    public init(status: String, messageJson: String) {
        self.status = status
        self.messageJson = messageJson
    }
}
