//
//  CustomMqttError.swift
//  sample
//
//  Created by iB Hubs Mini Mac on 01/07/17.
//  Copyright © 2017 iB Hubs. All rights reserved.
//

import HandyJSON

public class CustomMqttError {
    public var code: String?
    public var message: String?

    public required init() {}

    public init(code: String, message: String) {
        self.code = code
        self.message = message
    }
}
