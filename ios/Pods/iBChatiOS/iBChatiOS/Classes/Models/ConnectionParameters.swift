//
//  ConnectionParameters.swift
//  sample
//
//  Created by iB Hubs Mini Mac on 01/07/17.
//  Copyright © 2017 iB Hubs. All rights reserved.
//

import HandyJSON
import UIKit

public class ConnectionParameters: HandyJSON {
    public var caCertificate: String? = ""
    public var certificate: String? = ""
    public var clientEndPoint: String? = ""
    public var clientId: String? = ""
    public var cognitoPoolId: String? = ""
    public var isSSLEnabled: Bool? = false
    public var keepAliveTime: Int? = ChatConstants.DEFAULT_KEEP_ALIVE_TIME
    public var mqttPort: Int? = -1
    public var password: String? = ""
    public var pkcs12Data: String? = ""
    public var privateKey: String? = ""
    public var publishTopic: String? = ""
    public var region: String? = ""
    public var source: String? = ""
    public var subscribeTopic: String? = ""
    public var senderId: String? = ""
    public var username: String? = ""
    public required init() {}

    public init?(caCertificate: String, certificate: String, clientEndPoint: String, clientId: String, cognitoPoolId: String, isSSLEnabled: Bool = false, keepAliveTime: Int = ChatConstants.DEFAULT_KEEP_ALIVE_TIME, mqttPort: Int = -1, password: String, pkcs12Data: String, privateKey: String, publishTopic: String, region: String, source: String, subscribeTopic: String, senderId: String, username: String) {
        self.caCertificate = caCertificate
        self.certificate = certificate
        self.clientEndPoint = clientEndPoint
        self.clientId = clientId
        self.cognitoPoolId = cognitoPoolId
        self.isSSLEnabled = isSSLEnabled
        self.keepAliveTime = keepAliveTime
        self.password = password
        self.pkcs12Data = pkcs12Data
        self.mqttPort = mqttPort
        self.privateKey = privateKey
        self.publishTopic = publishTopic
        self.region = region
        self.source = source
        self.subscribeTopic = subscribeTopic
        self.senderId = senderId
        self.username = username
    }
}
