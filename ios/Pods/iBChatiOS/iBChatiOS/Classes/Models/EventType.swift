//
//  EventType.swift
//  sample
//
//  Created by iB Hubs Mini Mac on 01/07/17.
//  Copyright © 2017 iB Hubs. All rights reserved.
//

import HandyJSON
import UIKit

public class EventType: HandyJSON {
    public var event_type: String?
    public var sender_id: String?
    public var cli_event_id: String?
    public var error: CustomMqttError?

    public required init() {}

    public func getEventType() -> String {
        if event_type == nil {
            return ""
        }
        return event_type!
    }

    public func setEventType(eventType: String) {
        event_type = eventType
    }

    public func getSender_id() -> String {
        return sender_id!
    }

    public func getErrorMessage() -> String {
        if error == nil || error?.message == nil || error?.message == "" {
            return "ERROR_MESSAGE_NOT_FOUND"
        }
        return error!.message!
    }

    public func getCli_event_id() -> String {
        return cli_event_id!
    }
}
