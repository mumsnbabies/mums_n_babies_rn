//
//  SubscribeEvent.swift
//  sample
//
//  Created by iB Hubs Mini Mac on 01/07/17.
//  Copyright © 2017 iB Hubs. All rights reserved.
//

public class SubscribeEvent {
    public var iotMessage: String?

    public required init() {}

    public init(iotMessage: String) {
        self.iotMessage = iotMessage
    }

    public func getIotMessage() -> String {
        return iotMessage!
    }
}
