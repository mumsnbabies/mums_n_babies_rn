//
//  ChatCommonMethods.swift
//  sample
//
//  Created by iB Hubs Mini Mac on 30/06/17.
//  Copyright © 2017 iB Hubs. All rights reserved.
//

import SystemConfiguration
import UIKit

public class ChatCommonMethods {

    public static func printD(tag: String, message: String) {
        if ChatConstants.isDebug == true {
            NSLog(tag + ": " + message)
        }
    }

    public static func printD(message: String) {
        if ChatConstants.isDebug == true {
            NSLog("Chat :" + message)
        }
    }

    public static func printE(tag: String, message: String) {
        if ChatConstants.isDebug == true {
            NSLog(tag + " error: " + message)
        }
    }

    public static func printE(message: String) {
        if ChatConstants.isDebug == true {
            NSLog("Chat error:" + message)
        }
    }

    public static func setFCMToken(fcmToken: String) {
        let preferences = UserDefaults.standard
        preferences.set(fcmToken, forKey: ChatConstants.FCM_TOKEN)
        preferences.synchronize()
    }

    public static func getFCMToken() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: ChatConstants.FCM_TOKEN) != nil {
            return preferences.string(forKey: ChatConstants.FCM_TOKEN)!
        }
        return ""
    }

    public static func isNetworkAvailable() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }

        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }

        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)

        return ret
    }

    public static func isServiceRunning() -> Bool {
        // TODO:
        return false
    }

    public static func BG(_ block: @escaping () -> Void) {
        DispatchQueue.global(qos: .background).async(execute: block)
    }
}
