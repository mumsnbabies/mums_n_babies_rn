//
//  MqttConstants.swift
//  sample
//
//  Created by iB Hubs Mini Mac on 30/06/17.
//  Copyright © 2017 iB Hubs. All rights reserved.
//

public class MqttConstants {
    public static let MQTT_DETAILS: String = "MQTT_DETAILS"
    public static let CA_CERIFICATE: String = "CA_CERIFICATE"
    public static let CERIFICATE: String = "CERIFICATE"
    public static let REGION: String = "REGION"
    public static let PRIVATE_KEY: String = "PRIVATE_KEY"
    public static let MQTT_CLIENT_ID: String = "MQTT_CLIENT_ID"
    public static let MQTT_PUBLISH_KEY: String = "MQTT_PUBLISH_KEY"
    public static let PKCS12_KEY: String = "PKCS12_KEY"
    public static let MQTT_SUBSCRIBE_KEY: String = "MQTT_SUBSCRIBE_KEY"
    public static let CLIENT_SERVICE: String = "CLIENT_SERVICE"
    public static let CLIENT_END_POINT: String = "CLIENT_END_POINT"
    public static let SENDER_ID: String = "SENDER_ID"
    public static let SOURCE: String = "SOURCE"
    public static let COGNITO_IDENTITY_POOL_ID: String = "COGNITO_IDENTITY_POOL_ID"
    public static let IS_SSL_ENABLED: String = "IS_SSL_ENABLED"
    public static let KEEP_ALIVE_TIME: String = "KEEP_ALIVE_TIME"
    public static let MQTT_USERNAME = "MQTT_USERNAME"
    public static let MQTT_PASSWORD = "MQTT_PASSWORD"
    public static let MQTT_PORT = "MQTT_PORT"
    public static let CONNECTION_STATUS: String = "CONNECTION_STATUS"
    public static let IS_IN_APPLICATION: String = "IS_IN_APPLICATION"
    public static let APP_PACKAGE: String = "APP_PACKAGE"
    
    public static let CONNECT: String = "CONNECT"
    public static let PUBLISH_EVENT: String = "PUBLISH_EVENT"
    public static let START_SERVICE: String = "START_SERVICE"
    public static let SUBSCRIBE_EVENT: String = "SUBSCRIBE_EVENT"
    public static let RESTART_SERVICE: String = "RESTART_SERVICE"
    public static let LOGOUT_SERVICE: String = "LOGOUT_SERVICE"
    
    public static let CONNECTING: String = "CONNECTING"
    public static let CONNECTED: String = "CONNECTED"
    public static let RECONNECTING: String = "RECONNECTING"
    public static let DISCONNECTED: String = "DISCONNECTED"
    public static let CONNECTION_LOST: String = "CONNECTION_LOST"
}
