//
//  ChatCommonMethods.swift
//  sample
//
//  Created by iB Hubs Mini Mac on 30/06/17.
//  Copyright © 2017 iB Hubs. All rights reserved.
//

import UIKit

public class MqttMethods {

    public static func getCaCertificate() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.CA_CERIFICATE) != nil {
            return preferences.string(forKey: MqttConstants.CA_CERIFICATE)!
        }
        return ""
    }

    public static func setCaCertificate(caCertificate: String) {
        let preferences = UserDefaults.standard
        preferences.set(caCertificate, forKey: MqttConstants.CA_CERIFICATE)
        preferences.synchronize()
    }

    public static func getCertificate() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.CERIFICATE) != nil {
            return preferences.string(forKey: MqttConstants.CERIFICATE)!
        }
        return ""
    }

    public static func setCertificate(certificate: String) {
        let preferences = UserDefaults.standard
        preferences.set(certificate, forKey: MqttConstants.CERIFICATE)
        preferences.synchronize()
    }

    public static func getPrivateKey() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.PRIVATE_KEY) != nil {
            return preferences.string(forKey: MqttConstants.PRIVATE_KEY)!
        }
        return ""
    }

    public static func setPrivateKey(privateKey: String) {
        let preferences = UserDefaults.standard
        preferences.set(privateKey, forKey: MqttConstants.PRIVATE_KEY)
        preferences.synchronize()
    }

    public static func getClientEndPoint() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.CLIENT_END_POINT) != nil {
            var clientEndPoint = preferences.string(forKey: MqttConstants.CLIENT_END_POINT)!.replacingOccurrences(of: "https://", with: "").replacingOccurrences(of: "http://", with: "")
            if clientEndPoint.characters.last == "/" {
                clientEndPoint = String(clientEndPoint.characters.dropLast())
            }
            return clientEndPoint
        }
        return ""
    }

    public static func setClientEndPoint(clientEndPoint: String) {
        let preferences = UserDefaults.standard
        preferences.set(clientEndPoint, forKey: MqttConstants.CLIENT_END_POINT)
        preferences.synchronize()
    }

    public static func getMqttClientId() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.MQTT_CLIENT_ID) != nil {
            return preferences.string(forKey: MqttConstants.MQTT_CLIENT_ID)!
        }
        return ""
    }

    public static func setMqttClientId(mqttClientId: String) {
        let preferences = UserDefaults.standard
        preferences.set(mqttClientId, forKey: MqttConstants.MQTT_CLIENT_ID)
        preferences.synchronize()
    }

    public static func setKeepAliveTime(keepAliveTime: Int) {
        let preferences = UserDefaults.standard
        preferences.set(keepAliveTime, forKey: MqttConstants.KEEP_ALIVE_TIME)
        preferences.synchronize()
    }
    
    public static func getKeepAliveTime() -> Int {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.KEEP_ALIVE_TIME) != nil {
            return preferences.integer(forKey: MqttConstants.KEEP_ALIVE_TIME)
        }
        return ChatConstants.DEFAULT_KEEP_ALIVE_TIME
    }
    
    public static func setMqttPort(mqttPort: Int) {
        let preferences = UserDefaults.standard
        preferences.set(mqttPort, forKey: MqttConstants.MQTT_PORT)
        preferences.synchronize()
    }

    public static func getMqttPort() -> Int {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.MQTT_PORT) != nil {
            return preferences.integer(forKey: MqttConstants.MQTT_PORT)
        }
        return -1
    }

    public static func getPkcs12Data() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.PKCS12_KEY) != nil {
            return preferences.string(forKey: MqttConstants.PKCS12_KEY)!
        }
        return ""
    }

    public static func setPkcs12Data(pkcs12Data: String) {
        let preferences = UserDefaults.standard
        preferences.set(pkcs12Data.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "\n", with: ""), forKey: MqttConstants.PKCS12_KEY)
        preferences.synchronize()
    }

    public static func getCognitoIdentityPoolId() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.COGNITO_IDENTITY_POOL_ID) != nil {
            return preferences.string(forKey: MqttConstants.COGNITO_IDENTITY_POOL_ID)!
        }
        return ""
    }

    public static func setCognitoIdentityPoolId(cognitoIdentityPoolId: String) {
        let preferences = UserDefaults.standard
        preferences.set(cognitoIdentityPoolId, forKey: MqttConstants.COGNITO_IDENTITY_POOL_ID)
        preferences.synchronize()
    }

    public static func getPublishKey() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.MQTT_PUBLISH_KEY) != nil {
            return preferences.string(forKey: MqttConstants.MQTT_PUBLISH_KEY)!
        }
        return ""
    }

    public static func setPublishKey(publishKey: String) {
        let preferences = UserDefaults.standard
        preferences.set(publishKey, forKey: MqttConstants.MQTT_PUBLISH_KEY)
        preferences.synchronize()
    }

    public static func getSubscribeKey() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.MQTT_SUBSCRIBE_KEY) != nil {
            return preferences.string(forKey: MqttConstants.MQTT_SUBSCRIBE_KEY)!
        }
        return ""
    }

    public static func setSubscribeKey(subscribeKey: String) {
        let preferences = UserDefaults.standard
        preferences.set(subscribeKey, forKey: MqttConstants.MQTT_SUBSCRIBE_KEY)
        preferences.synchronize()
    }

    public static func setUserName(username: String) {
        let preferences = UserDefaults.standard
        preferences.set(username, forKey: MqttConstants.MQTT_USERNAME)
        preferences.synchronize()
    }

    public static func getUserName() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.MQTT_USERNAME) != nil {
            return preferences.string(forKey: MqttConstants.MQTT_USERNAME)!
        }
        return ""
    }

    public static func setPassword(password: String) {
        let preferences = UserDefaults.standard
        preferences.set(password, forKey: MqttConstants.MQTT_PASSWORD)
        preferences.synchronize()
    }

    public static func getPassword() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.MQTT_PASSWORD) != nil {
            return preferences.string(forKey: MqttConstants.MQTT_PASSWORD)!
        }
        return ""
    }

    public static func setAWSRegion(region: String) {
        let preferences = UserDefaults.standard
        preferences.set(region, forKey: MqttConstants.REGION)
        preferences.synchronize()
    }

    public static func getAWSRegion() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.REGION) != nil {
            return preferences.string(forKey: MqttConstants.REGION)!
        }
        return ""
    }

    public static func getSource() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.SOURCE) != nil {
            return preferences.string(forKey: MqttConstants.SOURCE)!
        }
        return ""
    }

    public static func setSource(source: String) {
        let preferences = UserDefaults.standard
        preferences.set(source, forKey: MqttConstants.SOURCE)
        preferences.synchronize()
    }

    public static func getCurrentSenderId() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.SENDER_ID) != nil {
            return preferences.string(forKey: MqttConstants.SENDER_ID)!
        }
        return ""
    }

    public static func setCurrentSenderId(sessionId: String) {
        let preferences = UserDefaults.standard
        preferences.set(sessionId, forKey: MqttConstants.SENDER_ID)
        preferences.synchronize()
    }

    public static func getConnectionStatus() -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.CONNECTION_STATUS) != nil {
            return preferences.string(forKey: MqttConstants.CONNECTION_STATUS)!
        }
        return ""
    }

    public static func setConnectionStatus(connectionStatus: String) {
        let preferences = UserDefaults.standard
        preferences.set(connectionStatus, forKey: MqttConstants.CONNECTION_STATUS)
        preferences.synchronize()
    }

    public static func getClientEventId() -> String {
        let ephocTime = NSDate().timeIntervalSince1970
        return "cli_event_" + getCurrentSenderId() + "_\(ephocTime)"
    }

    public static func getClientChannelId() -> String {
        let ephocTime = NSDate().timeIntervalSince1970
        return "cli_channel_" + getCurrentSenderId() + "_\(ephocTime)"
    }

    public static func saveConnectionParameters(connectionParametersJson: String) {
        if let connectionParameters = ConnectionParameters.deserialize(from: connectionParametersJson) {
            setCaCertificate(caCertificate: (connectionParameters.caCertificate)!)
            setCertificate(certificate: (connectionParameters.certificate)!)
            setClientEndPoint(clientEndPoint: (connectionParameters.clientEndPoint)!)
            setCognitoIdentityPoolId(cognitoIdentityPoolId: (connectionParameters.cognitoPoolId)!)
            setSSLEnabled(sslEnabled: (connectionParameters.isSSLEnabled)!)
            setKeepAliveTime(keepAliveTime: (connectionParameters.keepAliveTime)!)
            setMqttClientId(mqttClientId: (connectionParameters.clientId)!)
            setPassword(password: (connectionParameters.password)!)
            setMqttPort(mqttPort: (connectionParameters.mqttPort)!)
            setPrivateKey(privateKey: (connectionParameters.privateKey)!)
            setPkcs12Data(pkcs12Data: (connectionParameters.pkcs12Data)!)
            setPublishKey(publishKey: (connectionParameters.publishTopic)!)
            setAWSRegion(region: (connectionParameters.region)!)
            setSubscribeKey(subscribeKey: (connectionParameters.subscribeTopic)!)
            setSource(source: (connectionParameters.source)!)
            setCurrentSenderId(sessionId: (connectionParameters.senderId)!)
            setUserName(username: (connectionParameters.username)!)
        } else {
            ChatCommonMethods.printD(message: "saving connection parameters failed")
        }
    }

    public static func clearChatPrefsData() {
        let preferences = UserDefaults.standard
        preferences.removeObject(forKey: MqttConstants.CA_CERIFICATE)
        preferences.removeObject(forKey: MqttConstants.CERIFICATE)
        preferences.removeObject(forKey: MqttConstants.PRIVATE_KEY)
        preferences.removeObject(forKey: MqttConstants.PKCS12_KEY)
        preferences.removeObject(forKey: MqttConstants.MQTT_CLIENT_ID)
        preferences.removeObject(forKey: MqttConstants.COGNITO_IDENTITY_POOL_ID)
        preferences.removeObject(forKey: MqttConstants.IS_SSL_ENABLED)
        preferences.removeObject(forKey: MqttConstants.MQTT_PORT)
        preferences.removeObject(forKey: MqttConstants.KEEP_ALIVE_TIME)
        preferences.removeObject(forKey: MqttConstants.MQTT_PUBLISH_KEY)
        preferences.removeObject(forKey: MqttConstants.MQTT_SUBSCRIBE_KEY)
        preferences.removeObject(forKey: MqttConstants.CLIENT_END_POINT)
        preferences.removeObject(forKey: MqttConstants.MQTT_PASSWORD)
        preferences.removeObject(forKey: MqttConstants.SOURCE)
        preferences.removeObject(forKey: MqttConstants.SENDER_ID)
        preferences.removeObject(forKey: MqttConstants.MQTT_USERNAME)
        preferences.removeObject(forKey: MqttConstants.REGION)
    }

    public static func getIsInApplication() -> Bool {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.IS_IN_APPLICATION) != nil {
            return preferences.bool(forKey: MqttConstants.IS_IN_APPLICATION)
        }
        return false
    }

    public static func setIsInApplication(isInApplication: Bool) {
        let preferences = UserDefaults.standard
        preferences.set(isInApplication, forKey: MqttConstants.IS_IN_APPLICATION)
        preferences.synchronize()
    }

    public static func isSSLEnabled() -> Bool {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: MqttConstants.IS_SSL_ENABLED) != nil {
            return preferences.bool(forKey: MqttConstants.IS_SSL_ENABLED)
        }
        return false
    }

    public static func setSSLEnabled(sslEnabled: Bool) {
        let preferences = UserDefaults.standard
        preferences.set(sslEnabled, forKey: MqttConstants.IS_SSL_ENABLED)
        preferences.synchronize()
    }

    public static func setIsDebug(isDebug: Bool) {
        ChatConstants.isDebug = isDebug
    }

    public static func startChatService(serviceFrom: String) {
        if serviceFrom == ChatConstants.SERVICE_FROM_RESTART_SESSION {
            NotificationCenter.default.post(name: Notification.Name(rawValue: MqttConstants.RESTART_SERVICE), object: nil)
        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: MqttConstants.START_SERVICE), object: nil)
        }
    }

    public static func startChatService(connectionParametersJson: String, serviceFrom: String) {
        MqttMethods.saveConnectionParameters(connectionParametersJson: connectionParametersJson)
        MqttMethods.startChatService(serviceFrom: serviceFrom)
    }
    
    public static func logoutChatService() {
        MqttMethods.clearChatPrefsData()
        NotificationCenter.default.post(name: Notification.Name(rawValue: MqttConstants.LOGOUT_SERVICE), object: nil)
    }
}

public extension UnicodeScalar {
    var hexNibble: UInt8 {
        let value = self.value
        if 48 <= value && value <= 57 {
            return UInt8(value - 48)
        } else if 65 <= value && value <= 70 {
            return UInt8(value - 55)
        } else if 97 <= value && value <= 102 {
            return UInt8(value - 87)
        }
        fatalError("\(self) not a legal hex nibble")
    }
}

public extension Data {
    init(hex: String) {
        let scalars = hex.unicodeScalars
        var bytes = Array<UInt8>(repeating: 0, count: (scalars.count + 1) >> 1)
        for (index, scalar) in scalars.enumerated() {
            var nibble = scalar.hexNibble
            if index & 1 == 0 {
                nibble <<= 4
            }
            bytes[index >> 1] |= nibble
        }
        self = Data(bytes: bytes)
    }
}
