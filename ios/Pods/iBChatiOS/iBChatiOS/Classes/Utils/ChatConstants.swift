//
//  ChatConstants.swift
//  sample
//
//  Created by iB Hubs Mini Mac on 30/06/17.
//  Copyright © 2017 iB Hubs. All rights reserved.
//

import UIKit

public class ChatConstants {

    public static var isDebug: Bool = false

    public static let CERTIFICATE_ID: String = "default"
    public static let DEFAULT_KEEP_ALIVE_TIME: Int = 300

    // Message status
    public static let MESSAGE_STATUS_WAITING: String = "WAITING"
    public static let MESSAGE_STATUS_SENT: String = "SENT"
    public static let MESSAGE_STATUS_READ: String = "READ"
    public static let MESSAGE_STATUS_DELIVERED: String = "DELIVERED"
    public static let MESSAGE_STATUS_FAILED: String = "FAILED"
    public static let MESSAGE_STATUS_DELETED: String = "DELETED"

    // Message type
    public static let DATE: String = "DATE"
    public static let IMAGE: String = "IMAGE"
    public static let MESSAGE: String = "MESSAGE"

    // Event Type
    public static let EVENT_TYPE_ALIVE_MESSAGE: String = "ALIVE_MESSAGE"
    public static let EVENT_TYPE_ALIVE_MESSAGE_ACK: String = "ALIVE_MESSAGE_ACK"
    public static let EVENT_TYPE_MESSAGE: String = "MESSAGE"
    public static let EVENT_TYPE_SENT: String = "SENT"
    public static let EVENT_TYPE_DELIVERED: String = "DELIVERED"
    public static let EVENT_TYPE_DELIVERY_REPORT: String = "DELIVERY_REPORT"
    public static let EVENT_TYPE_DELIVERY_REPORT_ACK: String = "DELIVERY_REPORT_ACK"
    public static let EVENT_TYPE_READ: String = "READ"
    public static let EVENT_TYPE_READ_REPORT: String = "READ_REPORT"
    public static let EVENT_TYPE_READ_REPORT_ACK: String = "READ_REPORT_ACK"
    public static let EVENT_TYPE_DELETE_MESSAGE: String = "DELETE_MESSAGE"
    public static let EVENT_TYPE_DELETE_MESSAGE_ACK: String = "DELETE_MESSAGE_ACK"
    public static let EVENT_TYPE_BLOCK_USER: String = "BLOCK_USER"
    public static let EVENT_TYPE_BLOCK_USER_ACK: String = "BLOCK_USER_ACK"

    public static let EVENT_TYPE_CREATE_GROUP: String = "CREATE_GROUP"
    public static let EVENT_TYPE_NEW_GROUP: String = "NEW_GROUP"
    public static let EVENT_TYPE_LEAVE_GROUP: String = "LEAVE_GROUP"
    public static let EVENT_TYPE_REMOVE_FROM_GROUP: String = "REMOVE_FROM_GROUP"
    public static let EVENT_TYPE_ADD_USERS_TO_GROUP: String = "ADD_USERS_TO_GROUP"
    public static let EVENT_TYPE_MAKE_ADMIN: String = "MAKE_ADMIN"
    public static let EVENT_TYPE_DELETE_GROUP: String = "DELETE_GROUP"
    public static let EVENT_TYPE_LEAVE_GROUP_MESSAGE: String = "LEAVE_GROUP_MESSAGE"
    public static let EVENT_TYPE_REMOVE_FROM_GROUP_MESSAGE: String = "REMOVE_FROM_GROUP_MESSAGE"
    public static let EVENT_TYPE_ADD_USERS_TO_GROUP_MESSAGE: String = "ADD_USERS_TO_GROUP_MESSAGE"
    public static let EVENT_TYPE_MAKE_ADMIN_MESSAGE: String = "MAKE_ADMIN_MESSAGE"
    public static let EVENT_TYPE_DELETE_GROUP_MESSAGE: String = "DELETE_GROUP_MESSAGE"
    public static let EVENT_TYPE_GET_CHAT_ROOM_DETAILS: String = "GET_CHAT_ROOM_DETAILS"
    public static let EVENT_TYPE_GET_CHAT_ROOM_DETAILS_MESSAGE: String = "GET_CHAT_ROOM_DETAILS_MESSAGE"
    public static let EVENT_TYPE_UPDATE_ROOM_DETAILS: String = "UPDATE_ROOM_DETAILS"
    public static let EVENT_TYPE_UPDATE_ROOM_DETAILS_MESSAGE: String = "UPDATE_ROOM_DETAILS_MESSAGE"
    public static let EVENT_TYPE_LAST_WILL_MESSAGE: String = "LAST_WILL_MESSAGE"
    public static let EVENT_TYPE_USER_OFFLINE_MESSAGE: String = "USER_OFFLINE_MESSAGE"

    public static let EVENT_TYPE_ERROR: String = "ERROR"

    // Room type
    public static let ROOM_TYPE_GROUP: String = "GROUP"
    public static let ROOM_TYPE_USER: String = "USER"

    // User type
    public static let USER_TYPE_MEMBER: String = "MEMBER"
    public static let USER_TYPE_ADMIN: String = "ADMIN"

    // Room details change type
    public static let CHANGE_ROOM_MULTIMEDIA: String = "MULTIMEDIA"
    public static let CHANGE_ROOM_NAME: String = "NAME"

    // Message publish call back
    public static let MESSAGE_SUCCESS: String = "MESSAGE_SUCCESS"
    public static let MESSAGE_FAILED: String = "MESSAGE_FAILED"
    public static let MESSAGE_TIMEOUT: String = "MESSAGE_TIMEOUT"

    // Intent data from service
    public static let EVENT_TYPE_CONNECTION_CALL_BACK: String = "EVENT_TYPE_CONNECTION_CALL_BACK"
    public static let EVENT_TYPE_PUBLISH_CALL_BACK: String = "EVENT_TYPE_PUBLISH_CALL_BACK"
    public static let IOT_MESSAGE: String = "IOT_MESSAGE"
    public static let SINGLE_CHANNEL_ITEM: String = "SINGLE_CHAT_ITEM"

    // Service starting point
    public static let SERVICE_FROM: String = "SERVICE_FROM"
    public static let SERVICE_FROM_FCM: String = "SERVICE_FROM_FCM"
    public static let SERVICE_FROM_RESCHEDULE: String = "SERVICE_FROM_RESCHEDULE"
    public static let SERVICE_FROM_USER_ACTION: String = "SERVICE_FROM_USER_ACTION"
    public static let SERVICE_FROM_RESTART_SESSION: String = "SERVICE_FROM_RESTART_SESSION"

    public static let FCM_TOKEN = "FCM_TOKEN"
}
