import { createStore, applyMiddleware, compose } from "redux";
import { autoRehydrate } from "redux-persist";
import Config from "../Config/DebugConfig";
import createSagaMiddleware from "redux-saga";
import ReduxPersist from "../Config/ReduxPersist";
import { CreateJumpstateMiddleware } from "jumpstate";

// creates the store
export default (rootReducer, rootSaga) => {
  /* ------------- Redux Configuration ------------- */

  const middleware = [];
  const enhancers = [];

  /* ------------- Saga Middleware ------------- */

  const sagaMonitor = __DEV__ ? console.tron.createSagaMonitor() : null;
  const sagaMiddleware = createSagaMiddleware({ sagaMonitor });
  middleware.push(sagaMiddleware);

  middleware.push(CreateJumpstateMiddleware());
  /* ------------- Assemble Middleware ------------- */

  enhancers.push(applyMiddleware(...middleware));
  if (__DEV__) {
    const { logger } = require("redux-logger");
    enhancers.push(applyMiddleware(logger));
  }

  /* ------------- AutoRehydrate Enhancer ------------- */

  // add the autoRehydrate enhancer
  if (ReduxPersist.active) {
    enhancers.push(autoRehydrate());
  }

  // if Reactotron is enabled (default for __DEV__), we'll create the store through Reactotron
  const createAppropriateStore = Config.useReactotron
    ? console.tron.createStore
    : createStore;
  const store = createAppropriateStore(rootReducer, compose(...enhancers));

  // configure persistStore and check reducer version number
  if (ReduxPersist.active) {
  }

  // kick off root saga
  sagaMiddleware.run(rootSaga);

  return store;
};
