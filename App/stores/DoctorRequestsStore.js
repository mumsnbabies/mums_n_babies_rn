import { observable, computed } from "mobx";
import { genericApiCall } from "../Utils/AppApi";
import { BASE_URL } from "../Utils/URL";

class DoctorRequests {
  @observable errors: Object;
  @observable getPendingRequestsStatus: Number;
  @observable getAcceptedRequestsStatus: Number;
  @observable pendingRequests: Object;
  @observable acceptedRequests: Object;
  @observable acceptRequestStatus: Number;
  constructor() {
    // initialization
    this.errors = {};
    this.getPendingRequestsStatus = 0;
    this.getAcceptedRequestsStatus = 0;
    this.pendingRequests = {};
    this.acceptedRequests = {};
    this.acceptRequestStatus = 0;
  }
  pendingRequestsProcess = () => {
    this.getPendingRequestsStatus = 100;
  };
  storePendingRequests = response => {
    this.getPendingRequestsStatus = 200;
    this.pendingRequests = response;
  };
  getPendingRequestsFailure = response => {
    this.errors = response;
    this.getPendingRequestsStatus = response.errors.http_status_code;
  };
  resetPendingRequestsState = () => {
    this.getPendingRequestsStatus = 0;
    this.errors = {};
  };
  acceptedRequestsProcess = () => {
    this.getAcceptedRequestsStatus = 100;
  };
  storeAcceptedRequests = response => {
    this.getAcceptedRequestsStatus = 200;
    this.acceptedRequests = response;
  };
  getAcceptedRequestsFailure = response => {
    this.errors = response;
    this.getAcceptedRequestsStatus = response.errors.http_status_code;
  };
  resetAcceptedRequestsState = () => {
    this.getAcceptedRequestsStatus = 0;
    this.errors = {};
  };
  acceptRequestProcess = () => {
    this.acceptRequestStatus = 100;
  };
  removeAcceptedPatient = patient_id => {
    this.pendingRequests.patients = this.pendingRequests.patients.filter(
      patient => {
        if (patient.patient_id !== patient_id) {
          return patient;
        }
      }
    );
    this.pendingRequests.total = this.pendingRequests.total - 1;
    if (this.acceptedRequests.total) {
      this.acceptedRequests.total = this.acceptedRequests.total + 1;
    }
    this.acceptRequestStatus = 200;
  };
  acceptRequestFailure = response => {
    this.errors = response;
    this.acceptRequestStatus = response.errors.http_status_code;
  };
  resetAcceptRequestStatus = () => {
    this.acceptRequestStatus = 0;
    this.errors = {};
  };
  getDoctorPendingRequests = (requestObject = {}) => {
    this.pendingRequestsProcess();
    let apiEndpoint = BASE_URL + "doctor/relations/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.storePendingRequests(response);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
          this.getPendingRequestsFailure(response);
        }
      }
    );
  };
  getDoctorAcceptedRequests = (requestObject = {}) => {
    this.acceptedRequestsProcess();
    let apiEndpoint = BASE_URL + "doctor/relations/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.storeAcceptedRequests(response);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
          this.getAcceptedRequestsFailure(response);
        }
      }
    );
  };
  acceptPatientRequest = (requestObject = {}) => {
    this.acceptRequestProcess();
    let apiEndpoint = BASE_URL + "doctor/patient/approve/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.removeAcceptedPatient(requestObject.patient_id);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.acceptRequestFailure(response);
      }
    );
  };
}
const DoctorRequestsStore = new DoctorRequests();

export default DoctorRequestsStore;
