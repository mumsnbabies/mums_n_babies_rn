import { observable, computed } from "mobx"
class IdentityModel {
  @observable loginResponse: Object
}

export default IdentityModel
