import { observable, computed, action } from "mobx"

import AuthStore from "../AuthStore"

class Document {
  patientProfileStore
  @observable documentId = -1
  @observable entityId = -1
  @observable description = ""
  @observable entityType = ""
  @observable doctorAccessIds = []
  @observable documentUrl = "NOIMAGE"
  @observable documentType = ""
  @observable documentDate = ""
  constructor(store, documentObject) {
    this.patientProfileStore = store
    this.documentId = documentObject.document_id
    this.entityId = documentObject.entity_id
    this.description = documentObject.description || "No description"
    this.entityType = documentObject.entity_type
    this.doctorAccessIds = documentObject.doctor_access_ids
    this.documentUrl = documentObject.document_url
    this.documentType = documentObject.document_type
    this.documentDate = documentObject.document_date
  }

  @action("UPDATE_DOCUMENT")
  updateDocument(docObject) {
    this.documentId = docObject.document_id
    this.entityId = docObject.entity_id
    this.description = docObject.description || "No description"
    this.entityType = docObject.entity_type
    this.doctorAccessIds = docObject.doctor_access_ids
    this.documentUrl = docObject.document_url
    this.documentType = docObject.document_type
    this.documentDate = docObject.document_date
  }
  @computed get getDocumentAuthorName() {
    //TODO: Names are hardcoded here, need to uhpdate with login response
    if (this.entityType === "PATIENT") {
      return AuthStore.loginResponse.name
    }
    if (this.entityType === "CHILD") {
      let childName = ""
      AuthStore.loginResponse.children.map(child => {
        if (child.child_id === this.entityId) {
          childName = child.name
        }
      })
      return childName
    }
  }
}

export default Document
