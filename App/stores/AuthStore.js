import { observable, computed, action } from "mobx";
import { genericApiCall } from "../Utils/AppApi";
import {
  BASE_URL,
  BASE_URL_AUTH,
  BASE_URL_PATIENT,
  BASE_URL_AGENDA
} from "../Utils/URL";

import {
  API_INITIAL,
  API_FETCHING,
  API_SUCCESS,
  INIT,
  API_PAGINATION_FINISHED
} from "../Utils/AppAPIConstants";

import { AsyncStorage } from "react-native";
import moment from "moment";
import IdentityModel from "./models/IdentityModel";
class Auth extends IdentityModel {
  @observable accessToken: string;

  @observable error: Object;
  @observable registerStatus: Number;
  @observable mode: Number;

  @observable verifyRegisterStatus: Number;

  @observable loginStatus: Number;

  @observable recoverPasswordStatus: Number;

  @observable resetPasswordStatus: Number;

  @observable updatePasswordStatus: Number;

  @observable doctorProfileStatus: Number;
  @observable updatetimingStatus: Number;
  @observable event_id: Number;
  @observable timings: Array;
  @observable getProfileStatus: Number;
  constructor() {
    super({});
    this.timings = [];
    this.event_id = 0;
    this.updatetimingStatus = 0;
    this.mode = 0;
    this.error = {};
    this.registerStatus = 0;
    this.verifyRegisterStatus = 0;
    this.loginStatus = 0;
    this.recoverPasswordStatus = 0;
    this.resetPasswordStatus = 0;
    this.updatePasswordStatus = 0;
    this.doctorProfileStatus = 0;
    this.getProfileStatus = 0;
  }

  @action.bound
  setAccessToken(accessToken: string) {
    this.accessToken = accessToken;
  }

  updateAuthStoreScreenResponse = response => {
    //TODO: Bad code - need to remove in future
    this.loginResponse.address = response.address;
    this.loginResponse.age = response.age;
    this.loginResponse.category = response.category;
    this.loginResponse.children = response.children;
    this.loginResponse.expected_date = response.expected_date;
    this.loginResponse.name = response.name;
    this.loginResponse.patient_attributes = response.patient_attributes;
    this.loginResponse.pic = response.pic;
    this.loginResponse.relation_stats = response.relation_stats;
  };

  // actions to update state
  //updatetimingStatus
  updatetimingProgress = () => {
    this.updatetimingStatus = 100;
  };
  updatetimingSuccess = response => {
    this.event_id = response.event_id;
    this.updatetimingStatus = 200;
    this.timings = [];
    this.resetUpdatetimingState();
  };
  updatetimingSuccess1 = () => {
    this.updatetimingStatus = 200;
    this.timings = [];
    this.resetUpdatetimingState();
  };
  updatetimingFailure = response => {
    this.updatetimingStatus = response.errors.http_status_code;
    this.error = response;
  };
  resetUpdatetimingState = () => {
    this.updatetimingStatus = 0;
    this.error = {};
  };
  //Update profile Validation
  updateProfileProgress = () => {
    this.doctorProfileStatus = 100;
  };
  updateProfileSuccess = () => {
    this.doctorProfileStatus = 200;
  };
  updateProfileFailure = response => {
    this.doctorProfileStatus = response.errors.http_status_code;
    this.error = response;
  };
  resetUpdateProfileState = () => {
    this.doctorProfileStatus = 0;
    this.error = {};
  };
  //Update Password Validation
  updatePasswordProgress = () => {
    this.updatePasswordStatus = 100;
  };
  updatePasswordSuccess = () => {
    this.updatePasswordStatus = 200;
  };
  updatePasswordFailure = response => {
    this.updatePasswordStatus = response.errors.http_status_code;
    this.error = response;
  };
  resetUpdatePasswordState = () => {
    this.updatePasswordStatus = 0;
    this.error = {};
  };
  //reset Password Validation
  resetPasswordProgress = () => {
    this.resetPasswordStatus = 100;
  };
  resetPasswordSuccess = () => {
    this.resetPasswordStatus = 200;
  };
  resetPasswordFailure = response => {
    this.resetPasswordStatus = response.errors.http_status_code;
    this.error = response;
  };
  resetResetPasswordState = () => {
    this.resetPasswordStatus = 0;
    this.error = {};
  };
  //Recover Password Validation
  recoverPwdProgress = () => {
    this.recoverPasswordStatus = 100;
  };
  recoverPwdSuccess = () => {
    this.recoverPasswordStatus = 200;
  };
  recoverPwdFailure = response => {
    this.recoverPasswordStatus = response.errors.http_status_code;
    this.error = response;
  };
  resetRecoverPwdState = () => {
    this.recoverPasswordStatus = 0;
    this.error = {};
  };
  //Verify resigter Validation
  verifyRegisterProgress = () => {
    this.verifyRegisterStatus = 100;
  };
  verifyRegisterSuccess = response => {
    this.loginResponse = response;
    this.verifyRegisterStatus = 200;
  };
  verifyRegisterFailure = response => {
    this.verifyRegisterStatus = response.errors.http_status_code;
    this.error = response;
  };
  resetVerifyRegisterState = () => {
    this.verifyRegisterStatus = 0;
    this.error = {};
  };
  //resigter Validation
  registerProgress = () => {
    this.registerStatus = 100;
  };
  registerSuccess = response => {
    this.registerStatus = 200;
  };
  registerFailure = response => {
    this.error = response;
    this.registerStatus = response.errors.http_status_code;
  };
  resetRegisterState = () => {
    this.registerStatus = 0;
    this.error = {};
  };
  //login Validation
  loginProgress = () => {
    this.loginStatus = 100;
  };
  loginSuccess = response => {
    this.loginResponse = response;
    this.loginStatus = 200;

    // let requestObject1 = {

    //   limit: 1,

    //   filters: {

    //     entity_id: response.doctor_id,

    //     entity_type: "DOCTOR_TIMINGS"

    //   },

    //   offset: 0

    // }

    // this.getEventId(requestObject1)
  };
  loginPatientSuccess = response => {
    this.loginResponse = response;
    this.loginStatus = 200;
  };
  loginFailure = response => {
    this.error = response;
    this.loginStatus = response.errors.http_status_code;
  };
  resetLoginState = () => {
    this.loginStatus = 0;
    this.error = {};
    this.loginResponse = {};
  };
  resetUpdatePasswordStatus = () => {
    this.updatePasswordStatus = false;
  };
  resetState = () => {
    this.loginStatus = 0;
    this.mode = 0;
    this.error = {};
    this.registerStatus = 0;
    this.verifyRegisterStatus = 0;
    this.loginResponse = {};
    this.recoverPasswordStatus = 0;
    this.resetPasswordStatus = 0;
    this.updatePasswordStatus = 0;
    this.doctorProfileStatus = false;
    this.getProfileStatus = 0;
  };
  // Api Calls / Network Calls
  registerDoctor = (requestObject = {}) => {
    this.registerProgress();
    let apiEndpoint = BASE_URL + "doctor/register/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.registerSuccess(response);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.registerFailure(response);
      }
    );
  };
  getEventId = (requestObject = {}) => {
    let apiEndpoint = BASE_URL_AGENDA + "agenda/events/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        if (response.events.length > 0) {
          this.event_id = response.events[0].event_id;
        } else {
          this.event_id = 0;
        }
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
      }
    );
  };
  loginDoctor = (requestObject = {}) => {
    this.loginProgress();
    let apiEndpoint = BASE_URL + "doctor/login/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.setAccessToken(response.access_token);
        let storingValues = [];
        storingValues.push(["AccessToken", response.access_token]);
        if (response.patient_id) {
          storingValues.push(["patient_id", "" + response.patient_id]);
        } else if (response.doctor_id) {
          storingValues.push(["doctor_id", "" + response.doctor_id]);
        }
        AsyncStorage.multiSet(
          storingValues,
          function() {
            this.loginSuccess(response);
          }.bind(this)
        );

        // AsyncStorage.setItem(

        //   "AccessToken",

        //   response.access_token,

        //   function() {

        //     this.loginSuccess(response)

        //   }.bind(this)

        // )
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.loginFailure(response);
      }
    );
  };
  verifyRegisteredDoctor = (requestObject = {}) => {
    this.verifyRegisterProgress();
    let apiEndpoint = BASE_URL + "doctor/register/verify/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.setAccessToken(response.access_token);
        let storingValues = [];
        storingValues.push(["AccessToken", response.access_token]);
        if (response.patient_id) {
          storingValues.push(["patient_id", "" + response.patient_id]);
        } else if (response.doctor_id) {
          storingValues.push(["doctor_id", "" + response.doctor_id]);
        }
        AsyncStorage.multiSet(
          storingValues,
          function() {
            this.verifyRegisterSuccess(response);
          }.bind(this)
        );

        // AsyncStorage.setItem(

        //   "AccessToken",

        //   response.access_token,

        //   function() {

        //     this.verifyRegisterSuccess(response)

        //   }.bind(this)

        // )
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.verifyRegisterFailure(response);
      }
    );
  };
  recoverPassword = (requestObject = {}) => {
    let apiEndpoint = BASE_URL_AUTH + "user/recover_password/v2/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.recoverPwdSuccess();
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.recoverPwdFailure(response);
      }
    );
  };
  resetPassword = (requestObject = {}) => {
    this.resetPasswordProgress();
    let apiEndpoint = BASE_URL_AUTH + "user/reset_password/v2/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.resetPasswordSuccess();
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.resetPasswordFailure(response);
      }
    );
  };
  updatePassword = (requestObject = {}) => {
    this.updatePasswordProgress();
    let apiEndpoint = BASE_URL_AUTH + "user/access_credentials/pwd/update/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.updatePasswordSuccess();
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.updatePasswordFailure(response);
      }
    );
  };
  DoctorUpdateProfileReq = (requestObject = {}) => {
    this.updateProfileProgress();
    let apiEndpoint = BASE_URL + "doctor/profile/update/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.updateProfileSuccess();
        this.loginResponse = { ...this.loginResponse, ...requestObject };
        if (__DEV__) {
          console.log("Login After Update: ", this.loginResponse);
        }
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.updateProfileFailure(response);
      }
    );
  };

  //Patient
  registerPatient = (requestObject = {}) => {
    this.registerProgress();
    let apiEndpoint = BASE_URL_PATIENT + "patient/register/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.registerSuccess(response);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.registerFailure(response);
      }
    );
  };

  loginPatient = (requestObject = {}) => {
    this.loginProgress();
    let apiEndpoint = BASE_URL_PATIENT + "patient/login/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.setAccessToken(response.access_token);
        let storingValues = [];
        storingValues.push(["AccessToken", response.access_token]);
        if (response.patient_id) {
          storingValues.push(["patient_id", "" + response.patient_id]);
        } else if (response.doctor_id) {
          storingValues.push(["doctor_id", "" + response.doctor_id]);
        }
        AsyncStorage.multiSet(
          storingValues,
          function() {
            this.loginPatientSuccess(response);
          }.bind(this)
        );

        // AsyncStorage.setItem(

        //   "AccessToken",

        //   response.access_token,

        //   function() {

        //     this.loginPatientSuccess(response)

        //   }.bind(this)

        // )
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.loginFailure(response);
      }
    );
  };
  verifyRegisteredPatient = (requestObject = {}) => {
    this.verifyRegisterProgress();
    let apiEndpoint = BASE_URL_PATIENT + "patient/register/verify/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.setAccessToken(response.access_token);
        let storingValues = [];
        storingValues.push(["AccessToken", response.access_token]);
        if (response.patient_id) {
          storingValues.push(["patient_id", "" + response.patient_id]);
        } else if (response.doctor_id) {
          storingValues.push(["doctor_id", "" + response.doctor_id]);
        }
        AsyncStorage.multiSet(
          storingValues,
          function() {
            this.verifyRegisterSuccess(response);
          }.bind(this)
        );

        // AsyncStorage.setItem(

        //   "AccessToken",

        //   response.access_token,

        //   function() {

        //     this.verifyRegisterSuccess(response)

        //   }.bind(this)

        // )
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.verifyRegisterFailure(response);
      }
    );
  };
  updatetiming = (requestObject = {}) => {
    this.updatetimingProgress();
    let apiEndpoint = BASE_URL_AGENDA + "agenda/events/create/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.updatetimingSuccess(response);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.updatetimingFailure(response);
      }
    );
  };
  updatetiming1 = (requestObject = {}) => {
    this.updatetimingProgress();
    let apiEndpoint = BASE_URL_AGENDA + `agenda/event/${this.event_id}/`;
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.updatetimingSuccess1();
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.updatetimingFailure(response);
      }
    );
  };
  setTimings = daytimings => {
    let timings = [];
    daytimings.map((eachDay, index) => {
      let times = {
        start: moment(eachDay.start).format("HH:mm"),
        end: moment(eachDay.end).format("HH:mm"),
        title: eachDay.title
      };
      timings.push(times);
    });
    this.timings = timings;
  };
  getEventDetail = (requestObject = {}) => {
    // this.updatetimingProgress();
    let apiEndpoint = BASE_URL_AGENDA + `agenda/event/${this.event_id}/`;
    let requestType = "GET";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.setTimings(response.occurrences);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
      }
    );
  };
  DoctorProfileStatusUpdate = () => {
    this.doctorProfileStatus = 0;
  };
  @action
  getPatientProfileSuccess(response) {
    this.mode = 1;
    this.getProfileStatus = 200;
    this.loginResponse = response;
  }
  @action
  getPatientProfileFailure(response) {
    this.mode = 1;
    this.getProfileStatus = response.errors.http_status_code;
    this.error = response;
  }
  @action
  getDoctorProfileSuccess(response) {
    this.mode = 0;
    this.getProfileStatus = 200;
    this.loginResponse = response;
    let requestObject1 = {
      limit: 1,
      filters: {
        entity_id: response.doctor_id,
        entity_type: "DOCTOR_TIMINGS"
      },
      offset: 0
    };
    this.getEventId(requestObject1);
  }
  @action
  getDoctorProfileFailure(response) {
    this.mode = 0;
    this.getProfileStatus = response.errors.http_status_code;
    this.error = response;
  }
  @action
  getPatientProfileAPI(requestObject) {
    let apiEndpoint = BASE_URL_PATIENT + `patient/${requestObject.patient_id}/`;
    let requestType = "GET";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.getPatientProfileSuccess(response);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
          this.getPatientProfileFailure(response);
        }
      }
    );
  }
  @action
  getDoctorProfileAPI(requestObject) {
    let apiEndpoint = BASE_URL + `doctor/${requestObject.doctor_id}/`;
    let requestType = "GET";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.getDoctorProfileSuccess(response);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
          this.getDoctorProfileFailure(response);
        }
      }
    );
  }

  @action("RESEND_OTP")
  resendOTP = (requestObject = {}) => {
    let apiEndpoint = BASE_URL_AUTH + "user/access_credentials/otp/resend/v2/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
      }
    );
  };
}
const AuthStore = new Auth();

export default AuthStore;
