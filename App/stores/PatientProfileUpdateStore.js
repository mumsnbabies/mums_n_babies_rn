import { observable, computed, action } from "mobx"
import { genericApiCall } from "../Utils/AppApi"
import { BASE_URL, BASE_URL_PATIENT } from "../Utils/URL"
import { AsyncStorage } from "react-native"
import moment from "moment"
import IdentityModel from "./models/IdentityModel"
import AuthStore from "./AuthStore"

class PatientUpdateStore extends IdentityModel {
  @observable error: Object
  @observable detailProgressStatus: Number
  @observable getQuickRecordsStatus: Number
  @observable quickRecords: Array
  @observable updateRecordsStatus: Number
  @observable getPeriodicalAttributesStatus: Number
  @observable periodicalAttributes: Array
  @observable updateThresholdValues: Number
  @observable selectedRecord: String
  @observable selectedChild: Number
  @observable selectedPerson: String
  constructor() {
    super()
    this.error = {}
    this.detailProgressStatus = 0
    this.getQuickRecordsStatus = 0
    this.quickRecords = []
    this.updateRecordsStatus = 0
    this.getPeriodicalAttributesStatus = 0
    this.periodicalAttributes = []
    this.updateThresholdValues = 0
    this.selectedRecord = ""
    this.selectedChild = null
    this.selectedPerson = "Parent"
  }

  // actions to update state
  //updatetimingStatus
  @action
  updatePatientProProgress() {
    this.detailProgressStatus = 100
  }
  @action
  updatePatientProSuccess() {
    this.detailProgressStatus = 200
  }
  @action
  updatePatientProFailure(response) {
    this.detailProgressStatus = response.errors.http_status_code
    this.error = response
  }
  @action
  resetUpdatePatientProState() {
    this.detailProgressStatus = 0
    this.error = {}
  }
  @action
  updateGetRecordsProgress() {
    this.getQuickRecordsStatus = 100
  }
  @action
  updateQuickRecord(records) {
    this.quickRecords = records
    this.getQuickRecordsStatus = 200
  }
  @action
  storeSelectedTab(child_id, type) {
    this.selectedChild = child_id
    this.selectedPerson = type
    this.selectedRecord = ""
  }
  @action
  clearSelectedTab() {
    this.selectedChild = null
    this.selectedPerson = "Parent"
    this.selectedRecord = ""
  }
  @action
  storeRecord(record) {
    this.selectedRecord = record
  }
  @action
  clearRecord() {
    this.selectedRecord = ""
  }
  @action
  updateValues(value, key) {
    let records = this.quickRecords
    let isRecord = false
    records.map((eachRecord, index) => {
      if (eachRecord.attr_key === key) {
        eachRecord.attr_value = value
        isRecord = true
      }
    })
    if (!isRecord) {
      records.push({
        description: "string",
        update_time: records[0].update_time,
        attr_id: -1,
        attr_value: value,
        attr_key: key
      })
    }
    this.quickRecords = records
  }
  @action
  updateQuickRecordFailure(response) {
    this.error = response
    this.getQuickRecordsStatus = response.errors.http_status_code
  }
  @action
  updateRecordsUpdateProgress() {
    this.updateRecordsStatus = 100
  }
  @action
  updateRecordsSuccess() {
    this.updateRecordsStatus = 200
  }
  @action
  updateRecordsFailure(response) {
    this.updateRecordsStatus = response.errors.http_status_code
    this.error = response
  }
  @action
  resetQuickRecordsState() {
    this.getQuickRecordsStatus = 0
    this.updateRecordsStatus = 0
    this.quickRecords = []
  }
  @action
  getPeriodicalAttributesProcess() {
    this.getPeriodicalAttributesStatus = 100
  }
  @action
  getPeriodicalAttributesSuccess(response) {
    this.getPeriodicalAttributesStatus = 200
    this.periodicalAttributes = response
  }
  @action
  getPeriodicalAttributesFailure(response) {
    this.getPeriodicalAttributesFailure = response.errors.http_status_code
    this.error = response
  }
  @action
  resetPeriodicalAttributes() {
    this.getPeriodicalAttributesStatus = 0
    this.periodicalAttributes = []
  }
  @action
  updateThresholdValuesProgress() {
    this.updateThresholdValues = 100
  }
  @action
  updateThresholdValuesSuccess() {
    this.updateThresholdValues = 200
  }
  @action
  updateThresholdValuesFailure(response) {
    this.updateThresholdValues = response.errors.http_status_code
    this.error = response
  }
  @action
  resetThresholdValues() {
    this.updateThresholdValues = 0
  }
  savePatientAttributesRequest = requestObject => {
    let apiEndpoint = BASE_URL + "mnb_app/attributes/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
      }
    )
  }
  @action
  getPatientProfile = (requestObject = {}) => {
    let apiEndpoint =
      BASE_URL_PATIENT + "patient/" + requestObject.patient_id + "/"
    let requestType = "GET"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.loginResponse = response
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
      }
    )
  }
  @action
  updatePatientDetails(requestObject, callback = () => {}) {
    this.updatePatientProProgress()
    let apiEndpoint = BASE_URL_PATIENT + "patient/details/update/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.updatePatientProSuccess()
        this.loginResponse = response
        callback()
        AuthStore.updateAuthStoreScreenResponse(response)
        alert("Patient Details Updated")
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
        this.updatePatientProFailure(response)
      }
    )
  }
  @action
  getQuickRecords(requestObject) {
    this.updateGetRecordsProgress()
    let apiEndpoint = BASE_URL_PATIENT + "user/analytics/v2/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.updateQuickRecord(response)
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
        this.updateQuickRecordFailure(response)
      }
    )
  }
  @action
  createRecord(requestObject) {
    this.updateRecordsUpdateProgress()
    let apiEndpoint = BASE_URL + "mnb_app/attributes/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.updateRecordsSuccess()
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
        this.updateRecordsFailure(response)
      }
    )
  }
  @action
  updateRecordAnalytics(requestObject) {
    this.updateRecordsUpdateProgress()
    let apiEndpoint = BASE_URL_PATIENT + "patient/analytics/update/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.updateRecordsSuccess()
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
        this.updateRecordsFailure(response)
      }
    )
  }
  @action
  getPeriodicalAttributes(requestObject, callback) {
    this.getPeriodicalAttributesProcess()
    let apiEndpoint = BASE_URL_PATIENT + "attribute/periodical/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.getPeriodicalAttributesSuccess(response)
        if (callback) {
          callback()
        }
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
        this.getPeriodicalAttributesFailure(response)
      }
    )
  }
  @action
  updateThresholdValuesAPI(requestObject, callback = () => {}) {
    this.updateThresholdValuesProgress()
    let apiEndpoint = BASE_URL_PATIENT + "attribute/threshold/update/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.updateThresholdValuesSuccess()
        callback()
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
        this.updateThresholdValuesFailure(response)
      }
    )
  }
}
const PatientProfileUpdateStore = new PatientUpdateStore()

export default PatientProfileUpdateStore
