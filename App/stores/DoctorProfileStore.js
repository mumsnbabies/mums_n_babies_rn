import { observable, computed } from "mobx";
import { genericApiCall } from "../Utils/AppApi";
import { BASE_URL, BASE_URL_AGENDA } from "../Utils/URL";
class DoctorProfile {
  @observable doctorProfileResponse: Object;
  @observable getDoctorProfileStatus: Number;
  @observable doctorTimingsResponse: Object;
  @observable error: Object;
  constructor() {
    this.doctorProfileResponse = {};
    this.doctorTimingsResponse = {};
    this.getDoctorProfileStatus = 0;
    this.error = {};
  }

  storeDoctorProfileDetails = response => {
    this.doctorProfileResponse = response;
  };
  storeDoctorTimings = response => {
    this.doctorTimingsResponse = response;
    if (__DEV__) {
      console.log("storeDoctorTimings: ", this.doctorTimingsResponse);
    }
  };
  getDoctorProfile = (requestObject = {}, doctor_id) => {
    let apiEndpoint = BASE_URL + `doctor/${doctor_id}/`;
    let requestType = "GET";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.storeDoctorProfileDetails(response);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
      }
    );
  };
  getDoctorTiming = (requestObject = {}) => {
    let apiEndpoint = BASE_URL_AGENDA + "agenda/events/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.storeDoctorTimings(response);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
      }
    );
  };

  updateDoctorProfile=(requestObject={}) => {
      let apiEndpoint = BASE_URL + "doctor/profile/update/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        //this.storeDoctorTimings(response);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
      }
    );
  };

}


const DoctorProfileStore = new DoctorProfile();

export default DoctorProfileStore;
