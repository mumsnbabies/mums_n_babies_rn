import { Alert } from "react-native";
import { observable, computed, action } from "mobx";
import { genericApiCall, networkCallWithApisauce } from "../Utils/AppApi";
import { BASE_URL_PATIENT } from "../Utils/URL";
import { BASE_URL } from "../Utils/URL";

class PatientRequests {
  @observable getAcceptedRequestsStatus: Number;
  @observable getPendingRequestsStatus: Number;
  @observable acceptedRequests: Object;
  @observable error: Object;
  @observable search_doctors: Object;
  @observable connected_doctors: Object;
  @observable searchDoctorStatus: Number;
  @observable detailProgressStatus: Number;
  @observable doctor_requests: Object;
  @observable connectToDoctorAPIStatus: Number;
  constructor() {
    this.getPendingRequestsStatus = 0;
    this.detailProgressStatus = 0;
    this.errors = {};
    this.acceptedRequests = {};
    this.getAcceptedRequestsStatus = 0;
    this.searchDoctorStatus = 0;
    this.search_doctors = {};
    this.connected_doctors = {};
    this.doctor_requests = {};
    this.connectToDoctorAPIStatus = 0;
  }
  @action("TO GET DOCTORS PROCESS")
  acceptedDoctorsProgress = () => {
    this.getAcceptedRequestsStatus = 100;
  };
  @action
  storeAcceptedDoctors = response => {
    this.getAcceptedRequestsStatus = 200;
    this.acceptedRequests = response;
  };
  @action
  getAcceptedDoctorsFailure = response => {
    this.errors = response;
    this.getAcceptedRequestsStatus = response.errors.http_status_code;
  };
  @action
  getPatientAcceptedRequests = (requestObject = {}) => {
    this.acceptedDoctorsProgress();
    let apiEndpoint = BASE_URL_PATIENT + "patient/relations/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.storeAcceptedDoctors(response);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.getAcceptedDoctorsFailure(response);
      }
    );
  };
  @action
  SearchDoctorsProgress = () => {
    this.searchDoctorStatus = 100;
  };
  @action
  storeSearchDoctors = response => {
    this.search_doctors = response;
    if (__DEV__) {
      console.log("get doctors", this.search_doctors);
    }
    this.searchDoctorStatus = 200;
  };
  @action
  getDoctorsList = (requestObject = {}) => {
    this.SearchDoctorsProgress();
    let apiEndpoint = BASE_URL + "doctors/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.storeSearchDoctors(response);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
      }
    );
  };
  @action
  getConnectedDoctorsList = (requestObject = {}) => {
    let apiEndpoint = BASE_URL_PATIENT + "patient/doctor/connect/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success  requestConnectDoctor Response", response);
        }
        this.connected_doctors = response;
      },
      response => {
        if (__DEV__) {
          console.log("Failure  requestConnectDoctor Responce", response);
        }
      }
    );
  };
  DetailsProgress = () => {
    this.detailProgressStatus = 100;
  };
  DetailsSuccess = () => {
    this.detailProgressStatus = 200;
  };
  DetailsFailure = response => {
    this.detailProgressStatus = response.errors.http_status_code;
  };
  resetPatientDetailsStatus = () => {
    this.detailProgressStatus = 0;
  };
  patientDetailsRequest = (requestObject = {}) => {
    this.DetailsProgress();
    let apiEndpoint = BASE_URL_PATIENT + "patient/details/update/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.DetailsSuccess();
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.DetailsFailure(response);
      }
    );
  };
  getDoctorRequests = response => {
    this.doctor_requests = response;
    this.getPendingRequestsStatus = 200;
  };
  pendingRequestsProcess = () => {
    this.getPendingRequestsStatus = 100;
  };
  getDoctorRequestsFailure = response => {
    this.getAcceptedRequestsStatus = response.errors.http_status_code;
    this.errors = response;
  };
  requestGetPendingDoctorRequest = (requestObject = {}) => {
    this.pendingRequestsProcess();
    let apiEndpoint = BASE_URL_PATIENT + "patient/relations/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.getDoctorRequests(response);
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.getDoctorRequestsFailure(response);
      }
    );
  };

  //Patient --> Doctor Relations

  @action.bound
  setConnectToDoctorsAPIStatus(status: Number) {
    this.connectToDoctorAPIStatus = status;
  }

  @action("RequestToConnectDoctor - relation API")
  requestToConnectDoctor = (requestObject = {}, callBack = () => {}) => {
    this.setConnectToDoctorsAPIStatus(100);
    let apiEndpoint = BASE_URL_PATIENT + "patient/doctor/connect/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        callBack();
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
        this.setConnectToDoctorsAPIStatus(response.errors.http_status_code);
        alert(response.statusText);
      }
    );
  };
  @action("updateDoctorList  - Remove Doctor from Doctor List, showToast")
  updateDoctorList = pendingDoctorId => {
    //TODO: Show Toast
    this.search_doctors.doctors = this.search_doctors.doctors.filter(doctor => {
      return doctor.doctor_id != pendingDoctorId;
    });
    this.search_doctors.total = this.search_doctors.total - 1;
    if (this.doctor_requests.total) {
      this.doctor_requests.total = this.doctor_requests.total + 1;
    }
  };

  @action("setPatientAccessDetails initially - While connecting to doctor")
  setPatientAccessDetails = (requestObject = {}, pendingDoctorId) => {
    let apiEndpoint = BASE_URL_PATIENT + "patient/data/access/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        this.updateDoctorList(pendingDoctorId);
        Alert.alert("", "Connect request sent to doctor", [
          {
            text: "OK",
            onPress: () => {
              this.setConnectToDoctorsAPIStatus(200);
            }
          }
        ]);
      },
      response => {
        if (__DEV__) {
          // console.log("Patient Access Details Set Failure", response)
        }
        this.setConnectToDoctorsAPIStatus(response.errors.http_status_code);
        alert("Failed to request doctor");
      }
    );
  };

  @action("getDoctorAccessProfilesAPI")
  getDoctorAccessProfilesAPI = (requestObject = {}, callBack = () => {}) => {
    let apiEndpoint = BASE_URL_PATIENT + "patient/data/access/list/";
    let requestType = "POST";
    networkCallWithApisauce(
      apiEndpoint,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
      }
    ).then(response => {
      callBack(response);
      return response;
    });
  };
  @action("updateDoctorAccessProfilesAPI")
  updateDoctorAccessPatientProfilesAPI = (requestObject = {}, callBack) => {
    let apiEndpoint = BASE_URL_PATIENT + "patient/data/access/";
    let requestType = "POST";
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response);
        }
        callBack();
        alert("doctor access details updated");
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response);
        }
      }
    );
  };
}
const PatientRequestsStore = new PatientRequests();

export default PatientRequestsStore;
