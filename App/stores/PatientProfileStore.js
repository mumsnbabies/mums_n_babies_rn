import { observable, computed, action } from "mobx"
import { genericApiCall } from "../Utils/AppApi"
import { BASE_URL_PATIENT, BASE_URL, BASE_URL_DOCTOR_GAMMA } from "../Utils/URL"

import {
  API_INITIAL,
  API_FETCHING,
  API_SUCCESS,
  INIT,
  API_PAGINATION_FINISHED
} from "../Utils/AppAPIConstants"

import type { IObservableArray, IMap } from "mobx"

import Document from "./models/Document"

class PatientProfile {
  @observable errors: Object
  @observable getProfileStatus: Number
  @observable getAnalyticsStatus: Number
  @observable patientDetails: Object
  @observable analytics: Object
  @observable patientDocuments: Object
  @observable getDocumentsStatus: Number
  @observable getPeriodicalAttributesStatus: Number
  @observable periodicalAttributes: Array

  //Being used by Patient only
  @observable getAllDocumentsListLoadingStatus: Number
  @observable allChildrenDocuments: IObservableArray<Document>
  @observable allPatientDocuments: IObservableArray<Document>
  @observable allDocumentsLength: Number
  @observable allDocuments: IObservableArray<Document>
  @observable getDefaultAttributesFormAPIStatus: Number
  @observable formAPIResponse: Array
  @observable selectedPerson: String
  @observable selectedChild: Number

  constructor() {
    // initialization
    this.errors = {}
    this.getProfileStatus = 0
    this.getAnalyticsStatus = 0
    this.patientDetails = {}
    this.analytics = {}
    this.patientDocuments = {}
    this.getDocumentsStatus = 0
    this.getPeriodicalAttributesStatus = 0
    this.periodicalAttributes = []

    this.getAllDocumentsListLoadingStatus = API_INITIAL
    this.allDocumentsLength = 0
    this.allChildrenDocuments = observable([])
    this.allPatientDocuments = observable([])
    this.allDocuments = observable([])
    this.getDefaultAttributesFormAPIStatus = API_INITIAL
    this.formAPIResponse = []
    this.selectedPerson = "Parent"
    this.selectedChild = null
  }

  @action
  storeSelectedValues(child_id, type) {
    this.selectedPerson = type
    this.selectedChild = child_id
  }

  @action
  resetSelectedValues() {
    this.selectedChild = null
    this.selectedPerson = "Parent"
  }

  @computed
  get getProfileDetailsStatus() {
    return this.getProfileStatus
  }

  @computed
  get getUpdatedAllDocuments() {
    return this.allDocuments
  }

  @computed
  get defaultAttributesArray() {
    let defaultAttributesArray = []
    this.formAPIResponse.map(formObject => {
      if (formObject.name === "ALL_FIELDS") {
        let unstringifyObject = JSON.parse(formObject.form_json)
        unstringifyObject.fields.map(fieldObject => {
          if (fieldObject.type === "PARAMETER") {
            fieldObject.fields.map(parameterField => {
              defaultAttributesArray.push(parameterField.name)
            })
          }
          if (fieldObject.type === "FIELD") {
            defaultAttributesArray.push(fieldObject.name)
          }
        })
      }
      if (__DEV__) console.log("defaultAttributesArray", defaultAttributesArray)
    })
    return defaultAttributesArray
  }

  @action("GET_DOCUMENT_INSTANCE_WITH_ID")
  getSelectedDocument = id => {
    let documentObject = this.allDocuments.filter(
      documentObject => documentObject.documentId === id
    )
    return documentObject[0]
  }

  @action
  getProfileProcess = () => {
    this.getProfileStatus = 100
  }

  @action
  storePatientDetails = patientDetails => {
    this.patientDetails = patientDetails
    this.getProfileStatus = 200
  }

  @action
  getProfileFailure = response => {
    this.getProfileStatus = response.errors.http_status_code
    this.errors = {}
  }
  @action
  getAnalyticsProcess = () => {
    this.getAnalyticsStatus = 100
  }
  @action
  storeAnalytics = analytics => {
    if (__DEV__) {
      console.log(analytics)
    }
    let analyticObject = {}
    analytics.map((eachAnalyticObject, index) => {
      if (
        Object.keys(analyticObject).indexOf(eachAnalyticObject.attr_key) < 0
      ) {
        analyticObject[eachAnalyticObject.attr_key] = []
        analyticObject[eachAnalyticObject.attr_key].push(eachAnalyticObject)
      } else {
        analyticObject[eachAnalyticObject.attr_key].push(eachAnalyticObject)
      }
    })
    if (__DEV__) {
      console.log(analyticObject)
    }
    this.analytics = analyticObject
    this.getAnalyticsStatus = 200
  }

  @action
  getAnalyticsFailure = response => {
    this.errors = response
    this.getAnalyticsStatus = response.errors.http_status_code
  }
  @action
  resetAnalyticsState = () => {
    this.getAnalyticsStatus = 0
    this.errors = {}
  }
  getDocumentsProcess = () => {
    this.getDocumentsStatus = 100
  }
  storePatientDocuments = response => {
    this.patientDocuments = response
    this.getDocumentsStatus = 200
  }
  getDocumentsFailure = response => {
    this.errors = response
    this.getDocumentsStatus = response.errors.http_status_code
  }
  resetDocumentsState = () => {
    this.getDocumentsStatus = 0
    this.errors = {}
  }
  @action
  getPeriodicalAttributesProcess() {
    this.getPeriodicalAttributesStatus = 100
  }
  @action
  getPeriodicalAttributesSuccess(response) {
    this.getPeriodicalAttributesStatus = 200
    this.periodicalAttributes = response
  }
  @action
  getPeriodicalAttributesFailure(response) {
    this.getPeriodicalAttributesFailure = response.errors.http_status_code
    this.error = response
  }
  @action
  resetPeriodicalAttributes() {
    this.getPeriodicalAttributesStatus = 0
    this.periodicalAttributes = []
  }
  @action
  getPatientProfile = (requestObject = {}) => {
    this.getProfileProcess()
    let apiEndpoint =
      BASE_URL_PATIENT + "patient/" + requestObject.patient_id + "/"
    let requestType = "GET"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.storePatientDetails(response)
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
        this.getProfileFailure()
      }
    )
  }
  @action
  getPatientAnalytics = (requestObject = {}) => {
    this.getAnalyticsProcess()
    let apiEndpoint = BASE_URL_PATIENT + "user/analytics/v2/"
    // let apiEndpoint = BASE_URL_PATIENT + "patient/analytics/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.storeAnalytics(response)
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
        this.getAnalyticsFailure(response)
      }
    )
  }
  @action
  doctorGettingPatientAnalytics = (requestObject = {}) => {
    this.getAnalyticsProcess()
    let apiEndpoint = BASE_URL_PATIENT + "patient/analytics/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.storeAnalytics(response)
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
        this.getAnalyticsFailure(response)
      }
    )
  }
  @action
  doctorGettingChildAnalytics = (requestObject = {}) => {
    this.getAnalyticsProcess()
    let apiEndpoint = BASE_URL_PATIENT + "patient/child/analytics/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.storeAnalytics(response)
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
        this.getAnalyticsFailure(response)
      }
    )
  }
  @action
  getChildAnalytics = (requestObject = {}) => {
    this.getAnalyticsProcess()
    // let apiEndpoint = BASE_URL_PATIENT + "patient/child/analytics/"
    let apiEndpoint = BASE_URL_PATIENT + "user/analytics/v2/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.storeAnalytics(response)
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
        this.getAnalyticsFailure(response)
      }
    )
  }
  @action
  getPeriodicalAttributes(requestObject) {
    this.getPeriodicalAttributesProcess()
    let apiEndpoint = BASE_URL_PATIENT + "attribute/periodical/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.getPeriodicalAttributesSuccess(response)
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
        this.getPeriodicalAttributesFailure(response)
      }
    )
  }
  @action
  getPatientDocuments = (requestObject = {}) => {
    this.getDocumentsProcess()
    let apiEndpoint = BASE_URL_PATIENT + "patient/documents/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.storePatientDocuments(response)
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
        this.getDocumentsFailure(response)
      }
    )
  }
  @action("SET_GET_ALL_DOCUMENTS")
  setAllDocumentsList(patientDocuments, childrenDocuments, totalLength) {
    this.allChildrenDocuments = childrenDocuments.map(
      childDocument => new Document(this, childDocument)
    )
    this.allPatientDocuments = patientDocuments.map(
      patientDocument => new Document(this, patientDocument)
    )
    this.allDocumentsLength = totalLength
    this.allDocuments = this.allChildrenDocuments
      .slice()
      .concat(this.allPatientDocuments.slice())
  }

  @action("SET_GETALL_DOCUMENTS_LOADING_STATUS")
  setAllDocumentsListLoadingStatus(status) {
    if (__DEV__) {
      console.log("BIND VALUE: ", status)
    }
    this.getAllDocumentsListLoadingStatus = status
  }

  @action("SET_GET_DEFAULT_ATTRIBUTES_LOADING_STATUS")
  setGetDefaultAttributesFormAPIStatus(status) {
    this.getDefaultAttributesFormAPIStatus = status
  }

  @action("GET_ALL_DOCUMENTS_LIST")
  getAllDocumentsList = (requestObject = {}) => {
    let apiEndpoint = BASE_URL_PATIENT + "user/patient/documents/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.setAllDocumentsList(
          response.patient_documents,
          response.children_documents,
          response.total
        )
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
      },
      this.setAllDocumentsListLoadingStatus.bind(this)
    )
  }

  @action("ADD_NEW_DOCUMENT")
  addNewDocument(newDocument) {
    if (newDocument.type === "CHILD") {
      this.allChildrenDocuments = this.allChildrenDocuments.concat(
        new Document(this, newDocument)
      )
    }
    if (newDocument.type === "PATIENT") {
      this.allPatientDocuments = this.allPatientDocuments.concat(
        new Document(this, newDocument)
      )
    }
    this.allDocuments = this.allDocuments.concat(
      new Document(this, newDocument)
    )
    this.allDocumentsLength = this.allDocumentsLength + 1
    //TODO:TOASTER
    alert("New Document Created")
  }

  @action("CREATE_NEW_PATIENT_DOCUMENT")
  createPatientDocumentAPI = (requestObject = {}, resetDocumentDetails) => {
    let apiEndpoint = BASE_URL_PATIENT + "patient/document/add/"
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.addNewDocument(response)
        resetDocumentDetails()
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
      }
    )
  }

  @action("UPDATE_PATIENT_DOCUMENT")
  updatePatientDocument(updatedDocument) {
    //TODO: Also updatePatient Details in allChildDocuments, allPatientDocuments Variables
    //Just incase if fiter functionality is implemented

    // if (updatedDocument.type === "CHILD") {
    //   let toBeUpdatedDocument = this.getSelectedDocument(
    //     updatedDocument.documentId
    //   )
    //   toBeUpdatedDocument.updateDocument(updatedDocument)
    //   this.allChildrenDocuments = this.allChildrenDocuments.concat(
    //     new Document(this, newDocument)
    //   )
    // }
    // if (updatedDocument.type === "PATIENT") {
    //   this.allPatientDocuments = this.allPatientDocuments.concat(
    //     new Document(this, newDocument)
    //   )
    // }

    let toBeUpdatedDocument = this.getSelectedDocument(
      updatedDocument.document_id
    )
    toBeUpdatedDocument.updateDocument(updatedDocument)

    //TODO:TOASTER

    alert("Document Updated")
  }

  @action("UPDATE_PATIENT_DOCUMENT")
  updatePatientDocumentAPI = (
    requestObject = {},
    documentId,
    resetDocumentDetails
  ) => {
    let apiEndpoint =
      BASE_URL_PATIENT + `patient/document/${documentId}/update/`
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        this.updatePatientDocument(response)
        resetDocumentDetails()
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
      }
    )
  }

  @action("SHARE_DOCUMENT")
  setDocumentShareAccessAPI = (requestObject = {}, changeScreenCallback) => {
    let apiEndpoint =
      BASE_URL_PATIENT + `patient/document/${documentId}/update/`
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        alert("Document details Shared")
        changeScreenCallback()
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
      }
    )
  }

  @action("DELETE_DOCUMENT_STORE")
  deleteDocumentStore = documentId => {
    this.allDocuments = this.allDocuments.filter(obj => {
      return obj.documentId !== documentId
    })
    this.allDocumentsLength = this.allDocumentsLength - 1
  }

  @action("DELETE_DOCUMENT API")
  deleteDocumentAPI = (requestObject = {}, documentId) => {
    let apiEndpoint =
      BASE_URL_PATIENT + `patient/document/${documentId}/update/`
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }

        this.deleteDocumentStore(documentId)
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
      }
    )
  }

  @action("CREATE_PATIENT_ATTRIBUTE_API")
  createPatientAttributeAPI = (requestObject = {}, callback) => {
    let apiEndpoint = BASE_URL_DOCTOR_GAMMA + `mnb_app/attributes/`
    let requestType = "POST"
    if (__DEV__) console.log("requestObject", requestObject)
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        callback()
        alert("Patient Attributes created")
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
      }
    )
  }

  @action("SET_MIN_MAX_PATIENT_ATTRIBUTE_API")
  setMaxMinPatientAttributeAPI = (requestObject = {}, callback) => {
    let apiEndpoint = BASE_URL_PATIENT + `attribute/threshold/update/`
    let requestType = "POST"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }
        callback()
        alert("Max Min Values submitted")
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
      }
    )
  }

  @action("SET_GET_DEFAULT_ATTRIBUTES")
  setGetDefaultAttributesFormAPIResponse = response => {
    this.formAPIResponse = response
  }
  @action("GET_DEFAULT_ATTRIBUTES_FORM_API")
  getDefaultAttributesFormAPI = (requestObject = {}) => {
    let apiEndpoint = BASE_URL_DOCTOR_GAMMA + `medical_attributes/`
    let requestType = "GET"
    genericApiCall(
      apiEndpoint,
      requestType,
      requestObject,
      response => {
        if (__DEV__) {
          console.log("Success Response", response)
        }

        this.setGetDefaultAttributesFormAPIResponse(response)
      },
      response => {
        if (__DEV__) {
          console.log("Failure Responce", response)
        }
      },
      this.setGetDefaultAttributesFormAPIStatus.bind(this)
    )
  }
}
const PatientProfileStore = new PatientProfile()

export default PatientProfileStore
