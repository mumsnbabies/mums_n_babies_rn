import { StyleSheet, Platform } from "react-native";
import { Fonts, Colors } from "../../Themes/";

export default StyleSheet.create({
  DoctorPendingCard: {
    flexDirection: "row",
    backgroundColor: Colors.whiteColor,
    elevation: 1,
    borderRadius: 10,
    margin: 10,
    padding: 10,
    shadowColor: Colors.blackColor,
    shadowOffset: { width: 3, height: 3 },
    shadowOpacity: 0.3,
    shadowRadius: 1
  },
  ProfilePic: {
    height: 60,
    width: 60,
    borderWidth: 0.3,
    borderRadius: 30
  },
  DoctorPendingCardContent: {
    marginLeft: 10
  },
  DoctorPendingPatientName: {
    fontSize: Fonts.size.medium,
    color: Colors.blackColor,
    marginBottom: 10,
    fontFamily: Fonts.family.fontfamily
  },
  DoctorPendingPatientDetails: {
    fontSize: Fonts.size.small,
    marginBottom: 15,
    color: Colors.lightblack,
    fontFamily: Fonts.family.fontfamily
  },
  DoctorPendingCardContentBtns: {
    flexDirection: "row",
    marginBottom: 10
  },
  DoctorPendingCardAcceptBtn: {
    justifyContent: "center",
    alignItems: "center",
    width: 100,
    borderRadius: 40,
    backgroundColor: Colors.requestButtom
  },
  DoctorPendingCardAcceptBtnText: {
    color: Colors.whiteColor,
    paddingTop: 6,
    paddingBottom: 6,
    fontSize: Fonts.sizes.mediumsmall,
    fontFamily: Fonts.family.fontfamily
  },
  DoctorPendingCardOpenBtn: {
    width: 100,
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 10,
    borderRadius: 40,
    borderWidth: 0.7,
    borderColor: Colors.requestButtom
  },
  DoctorPendingCardOpenBtnText: {
    paddingTop: 6,
    paddingBottom: 6,
    color: Colors.requestButtom,
    fontSize: Fonts.sizes.mediumsmall,
    fontFamily: Fonts.family.fontfamily
  }
});
