import { StyleSheet, Platform } from "react-native"
import { Metrics } from "../../Themes/"

export default StyleSheet.create({
  container: {
    flex: 1,
    // paddingTop: Metrics.navBarHeight,
    justifyContent: "center",
    alignItems: "center"
  }
})
