import { StyleSheet,Platform } from 'react-native'
import { Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
 card:{
  flex:1,
  flexDirection:'row',
  height:40,
  backgroundColor:'white',
  elevation:1,
  borderRadius:5,
  alignItems:'center',
  justifyContent:'center',
  marginLeft:10,
  marginRight:10,
  marginBottom:5
 }
})
