import { StyleSheet, Platform } from "react-native";
import { Fonts, Colors } from "../../Themes/";

export default StyleSheet.create({
  card: {
    height: 260,
    width: 355,
    backgroundColor: Colors.bluecolor,
    elevation: 2,
    borderRadius: 3,
    alignItems: "flex-start",
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5
  },
  inputField: {
    flex: 2,
    marginTop: 10,
    height: 20,
    width: 100,
    color: Colors.lightblack
  },
  daycount2: {
    justifyContent: "center",
    alignItems: "center",
    height: 24,
    width: 130,
    backgroundColor: Colors.whiteColor,
    borderRadius: 3,
    elevation: 2,
    left: 202,
    marginTop: 10,
    marginBottom: 10
  }
});
