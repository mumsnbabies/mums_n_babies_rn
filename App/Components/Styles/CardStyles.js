import { StyleSheet, Platform } from "react-native";
import { Fonts, Colors } from "../../Themes/";

export default StyleSheet.create({
  Card: {
    flex: 1,
    flexDirection: "row",
    height: 100,
    backgroundColor: Colors.whiteColor,
    elevation: 2,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    margin: 10,
    padding: 10,
    shadowColor: Colors.lightblack,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 1
  },
  CardContent: {
    flex: 2,
    marginLeft: 25
  },
  DoctorName: {
    fontSize: Fonts.size.medium,
    color: Colors.blackColor
  },
  DoctorDetails: {
    fontSize: Fonts.sizes.normal,
    marginBottom: 15
  },
  ChatContainerBtns: {
    flexDirection: "row",
    marginBottom: 10
  },
  ChatContainerAcceptBtn: {
    justifyContent: "center",
    alignItems: "center",
    width: 100,
    height: 25,
    borderRadius: 3,
    backgroundColor: Colors.requestButtom,
    marginRight: 20
  },
  ChatContainerAcceptBtnText: {
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.mediumsmall
  },
  ChatContainerOpenBtn: {
    justifyContent: "center",
    alignItems: "center",
    width: 100,
    height: 25,
    borderRadius: 3,
    backgroundColor: Colors.whiteColor,
    borderWidth: 1.2,
    borderColor: Colors.requestButtom
  },
  ChatContainerOpenBtnText: {
    color: Colors.requestButtom,
    fontSize: Fonts.sizes.mediumsmall
  }
});
