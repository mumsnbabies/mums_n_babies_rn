import { StyleSheet, Platform } from "react-native"
import { Fonts, Colors } from "../../Themes/"

export default StyleSheet.create({
  RequestMainView: {
    alignItems: "flex-end",
    marginRight: 10
  },
  Daycount: {
    justifyContent: "center",
    alignItems: "center",
    height: 46,
    width: 200,
    backgroundColor: Colors.secondaryColor,
    borderRadius: 30,
    elevation: 2,
    marginBottom: 10
  },
  CalendarText: {
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.smallheading,
    marginLeft: 30,
    marginBottom: 10,
    fontFamily: Fonts.family.fontFamilyBold
  },
  RequestText: {
    color: Colors.whiteColor,
    fontWeight: "600"
  },
  AppointmentsView: {
    marginTop: 40,
    paddingBottom: 40
  }
})
