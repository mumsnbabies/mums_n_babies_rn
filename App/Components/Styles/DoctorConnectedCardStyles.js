import { StyleSheet, Platform } from "react-native";
import { Fonts, Colors } from "../../Themes/";

export default StyleSheet.create({
  DoctorConnectedCard: {
    flexDirection: "row",
    backgroundColor: Colors.whiteColor,
    elevation: 1,
    borderRadius: 10,
    margin: 10,
    padding: 10,
    shadowColor: Colors.blackColor,
    shadowOffset: { width: 3, height: 3 },
    shadowOpacity: 0.3,
    shadowRadius: 1
  },
  DoctorConnectedCard2: {
    flexDirection: "row",
    height: 90,
    backgroundColor: Colors.whiteColor,
    elevation: 1,
    borderRadius: 5,
    alignItems: "center",
    margin: 10,
    padding: 10,
    shadowColor: Colors.blackColor,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 1
  },
  DoctorProfilePicContainer2: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center"
  },
  ProfilePic: {
    height: 60,
    width: 60,
    borderWidth: 0.3,
    borderRadius: 30
  },
  DoctorConnectedCardContent: {
    marginLeft: 10,
    flex: 2
  },
  bodycontent: {
    flex: 4,
    flexDirection: "column"
  },
  patientname: {
    flex: 1
  },
  patientaddress: {
    flex: 1
  },
  DoctorConnectedCardContent2: {
    flex: 3,
    marginBottom: 20,
    justifyContent: "flex-start"
  },
  DoctorConnectedPatientName: {
    color: Colors.blackColor,
    marginBottom: 5,
    fontFamily: Fonts.family.fontfamily
  },
  DoctorConnectedPatientName2: {
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  DoctorConnectedPatientDetails: {
    fontSize: Fonts.size.small,
    marginBottom: 15,
    color: Colors.lightblack,
    fontFamily: Fonts.family.fontfamily
  },
  DoctorConnectedPatientDetails2: {
    fontSize: Fonts.sizes.mediumsmall,
    marginTop: 10,
    color: Colors.lightblack,
    fontFamily: Fonts.family.fontfamily
  },
  DoctorConnectedCardContentBtns: {
    flexDirection: "row",
    flex: 2,
    marginRight: 30
  },
  DoctorConnectedCardAcceptBtn: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    height: 30,
    borderRadius: 40,
    backgroundColor: Colors.requestButtom
  },
  DoctorConnectedCardAcceptBtnText: {
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.mediumsmall,
    fontFamily: Fonts.family.fontfamily
  },
  DoctorConnectedCardOpenBtn: {
    flex: 1,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 10,
    borderRadius: 40,
    borderWidth: 0.7,
    borderColor: Colors.requestButtom
  },
  DoctorConnectedCardOpenBtnText: {
    color: Colors.requestButtom,
    fontSize: Fonts.sizes.mediumsmall,
    fontFamily: Fonts.family.fontFamilyBold
  }
});
