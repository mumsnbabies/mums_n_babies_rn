import { StyleSheet, Platform } from "react-native";
import { Fonts, Colors } from "../../Themes/";

export default StyleSheet.create({
  card: {
    flex: 1,
    flexDirection: "row",
    height: 100,
    backgroundColor: Colors.whiteColor,
    elevation: 2,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    margin: 10,
    padding: 10,
    shadowColor: Colors.lightblack,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 1
  }
});
