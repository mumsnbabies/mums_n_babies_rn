import { StyleSheet, Platform } from "react-native";
import { Fonts, Colors } from "../../Themes/";

export default StyleSheet.create({
  daycount: {
    justifyContent: "center",
    alignItems: "center",
    height: 22,
    width: 230,
    backgroundColor: Colors.secondaryColor,
    borderRadius: 30,
    elevation: 2,
    marginBottom: 10
  },
  daycount2: {
    justifyContent: "center",
    alignItems: "center",
    height: 22,
    width: 130,
    backgroundColor: Colors.secondaryColor,
    borderRadius: 30,
    elevation: 2,
    marginBottom: 10
  },
  inputField: {
    height: 45,
    width: 355,
    color: Colors.lightblack,
    marginLeft: 20
  },
  input: {
    marginLeft: 10,
    borderColor: Colors.lightblack,
    borderWidth: 0.5,
    borderRadius: 5,
    marginBottom: 10,
    backgroundColor: Colors.whiteColor
  }
});
