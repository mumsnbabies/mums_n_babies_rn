import { StyleSheet, Platform } from "react-native";
import { Fonts, Colors } from "../../Themes/";

export default StyleSheet.create({
  card: {
    height: 100,
    width: 355,
    backgroundColor: Colors.bluecolor,
    elevation: 2,
    borderRadius: 3,
    alignItems: "flex-start",
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5
  }
});
