import React from "react"
import { TouchableOpacity, View, Text, Image, ScrollView } from "react-native"
import styles from "./styles.js"
export default class NoDataView extends React.Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        <Text style={styles.textStyles}>NO DATA </Text>
      </View>
    )
  }
}
