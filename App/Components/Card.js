import React from "react"
import { TouchableOpacity, View, Text, Image, ScrollView } from "react-native"
import styles from "./Styles/CardStyles"
import { Actions as NavigationActions } from "react-native-router-flux"
export default class Card extends React.Component {
  componentWillMount() {}
  render() {
    if (!this.props.item) {
      return null
    }
    let { name, details, profilePic } = this.props.item
    return (
      <View style={styles.Card}>
        {/* <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <Image
            style={{ width: 50, height: 50 }}
            source={{ uri: profilePic }}
          />
        </View> */}
        <View style={styles.CardContent}>
          <Text style={styles.DoctorName}>
            {name}
          </Text>
          {/* <Text style={{ fontSize: 13 }}>
            Gynecologist
          </Text> */}
          <Text style={styles.DoctorDetails}>
            {details}
          </Text>
          <View style={styles.ChatContainerBtns}>
            <TouchableOpacity
              onPress={() => {
                NavigationActions.ChatConversation({ item: this.props.item })
              }}
            >
              <View style={styles.ChatContainerAcceptBtn}>
                <Text style={styles.ChatContainerAcceptBtnText}>Accept</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                NavigationActions.PatientProfile({ item: this.props.item })
              }}
            >
              <View style={styles.ChatContainerOpenBtn}>
                <Text style={styles.ChatContainerOpenBtnText}>Open</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}
