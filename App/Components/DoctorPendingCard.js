import React from "react"
import { TouchableOpacity, View, Text, Image, ScrollView } from "react-native"
import styles from "./Styles/DoctorPendingCardStyles"
import Images from "../Themes/Images"
import {DOCTORPENDINGCARDCONSTANTS} from "../Constants/Doctor";
export default class DoctorPendingCard extends React.Component {
  acceptRequest = () => {
    let { patient_id } = this.props.details
    let requestObject = {
      is_accept: true,
      patient_id: patient_id
    }
    if(__DEV__)
    {
      console.log("Accept Request:", requestObject);
    }
    this.props.acceptPatientRequest(requestObject)
  }
  displayPatientDetails = () => {
    let { patient_id, patient_name } = this.props.details
    let requestObject = {
      patient_id: patient_id
    }
    this.props.displayPatientProfile(requestObject, patient_name)
  }
  render() {
    let { patient_name, patient_address } = this.props.details
    return (
      <View style={styles.DoctorPendingCard}>
        <View style={styles.DoctorProfilePicContainer}>
          <Image
            style={styles.ProfilePic}
            resizemode="contain"
            source={Images.patientProfilePic}
          />
        </View>
        <View style={styles.DoctorPendingCardContent}>
          <Text style={styles.DoctorPendingPatientName}>
            {patient_name ? patient_name.toUpperCase() : null}
          </Text>
          <Text style={styles.DoctorPendingPatientDetails}>
            {patient_address ? patient_address.toUpperCase() : null}
          </Text>
          <View style={styles.DoctorPendingCardContentBtns}>
            <TouchableOpacity onPress={this.acceptRequest}>
              <View style={styles.DoctorPendingCardAcceptBtn}>
                <Text style={styles.DoctorPendingCardAcceptBtnText}>
                  {DOCTORPENDINGCARDCONSTANTS.ACCEPT_BUTTON_TEXT}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.displayPatientDetails}>
              <View style={styles.DoctorPendingCardOpenBtn}>
                <Text style={styles.DoctorPendingCardOpenBtnText}>{DOCTORPENDINGCARDCONSTANTS.OPEN_BUTTON_TEXT}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}
