import React from "react"
import {
  TouchableOpacity,
  View,
  Text,
  Image,
  ScrollView,
  TextInput
} from "react-native"
import styles from "./Styles/MedicalStyles"
import Record from "./Record"

export default class Medical extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      text: "  Medical Record Name"
    }
  }
  render() {
    return (
      <View>
        <Text
          style={{
            color: "white",
            fontSize: 24,
            marginLeft: 30,
            marginBottom: 10,
            fontWeight: "600"
          }}
        >
          Add Medical Record
        </Text>
        <View style={styles.input}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.inputField}
            placeholder="Medical Record Name"
            autoCorrect={false}
            autoCapitalize="none"
            underlineColorAndroid="transparent"
          />
        </View>
        <TouchableOpacity>
          <View style={styles.daycount}>
            <Text style={{ color: "white", fontWeight: "600", fontSize: 10 }}>
              Attach Files
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.daycount}>
            <Text style={{ color: "white", fontWeight: "600", fontSize: 10 }}>
              Add record
            </Text>
          </View>
        </TouchableOpacity>
        <Text
          style={{
            color: "white",
            fontSize: 24,
            marginLeft: 30,
            marginBottom: 20,
            fontWeight: "600"
          }}
        >
          Your Records
        </Text>
        <Record />
        <Record />
        <Record />
        <Record />
      </View>
    )
  }
}
