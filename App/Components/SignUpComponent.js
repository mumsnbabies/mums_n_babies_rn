import React from "react"
import { TouchableOpacity, View, Text, Image, ScrollView } from "react-native"
import styles from "./Styles/SignUpComponentStyles"
export default class SignUpComponent extends React.Component {
  render() {
    return (
      <View style={styles.card}>
        <Text
          style={{
            marginLeft: 20,
            color: "white",
            marginTop: 10,
            fontWeight: "600",
            fontSize: 14
          }}
        >
          Expecting Mother Details
        </Text>
        <View
          style={{
            justifyContent: "center",
            marginLeft: 20,
            backgroundColor: "white",
            marginTop: 15,
            height: 40,
            width: 320,
            borderRadius: 1
          }}
        >
          <TouchableOpacity>
            <Text style={{ marginLeft: 10, fontWeight: "500" }}>
              Expected Date
            </Text>
          </TouchableOpacity>
        </View>
        {/*date picker*/}
      </View>
    )
  }
}
