import React, { Component } from "react";
import {
  View,
  Image,
  UIManager,
  findNodeHandle,
  TouchableOpacity
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

const ICON_SIZE = 24;

export default class PopupMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      icon: null
    };
  }

  onError() {
    if (__DEV__) {
      console.log("Popup Error");
    }
  }

  onPress = () => {
    if (this.state.icon) {
      UIManager.showPopupMenu(
        findNodeHandle(this.state.icon),
        this.props.actions,
        this.onError,
        this.props.onPress
      );
    }
  };

  render() {
    return (
      <TouchableOpacity onPress={this.onPress}>
        <Image
          style={{ marginTop: -3 }}
          source={require("../Images/more.png")}
          size={ICON_SIZE}
          ref={this.onRef}
        />
      </TouchableOpacity>
    );
  }

  onRef = icon => {
    if (!this.state.icon) {
      this.setState({ icon });
    }
  };
}
