import React from "react";
import { View, Text, ActivityIndicator } from "react-native";
import styles from "./Styles/LoaderStyle";

export default class Loader extends React.Component {
  static defaultProps = {
    color: "#ff69b4"
  };
  render() {
    if (this.props.animating) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            animating={this.props.animating}
            color={this.props.color}
            size="small"
          />
        </View>
      );
    }
    return null;
  }
}
