import { StyleSheet, Platform } from "react-native"
import { Fonts, Colors } from "../../Themes/"

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 2,
    backgroundColor: Colors.primaryColor
  },
  headerContainer: {
    flex: 1,
    backgroundColor: Colors.whiteColor,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: 40,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5
  },
  bodyContentContainer: {
    flex: 1
  },
  headerTextStyles: {
    marginLeft: 10,
    fontFamily: Fonts.family.fontfamily,
    fontSize: 18,
    color: Colors.blackColor
  },
  headerContentContainer: {
    flex: 10,
    justifyContent: "center"
  },
  popupMenuContainer: {
    flex: 1,
    marginRight: 7
  },

  footerContainer: {
    backgroundColor: Colors.whiteColor,
    height: 90,
    justifyContent: "center",
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    flex: 1,
    flexDirection: "row",
    alignItems: "center"
  },
  flexContainer: {
    flex: 1,
    height: 90,
    justifyContent: "center"
  },
  authorNameStyles: {
    marginLeft: 10,
    fontFamily: Fonts.family.fontfamily,
    fontSize: 18,
    color: Colors.blackColor
  },
  recordDateStyles: {
    marginLeft: 10,
    fontFamily: Fonts.family.fontfamily,
    fontSize: 16,
    color: Colors.blackColor
  },
  imageContainer: {
    //alignItems: "center",
    elevation: 2,
    flex: 1
  }
})
