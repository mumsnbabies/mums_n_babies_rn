import React from "react"
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  Text,
  Image,
  Platform,
  Dimensions
} from "react-native"
import styles from "./styles.js"
import PopupMenu from "../PopupMenu"
import PopupMenuIOS from "../PopupMenuIOS"
import moment from "moment"

export default class Document extends React.Component {
  onPopupEvent = (eventName, index, documentObject) => {
    this.props.handlePopupMenu(eventName, index, documentObject)
  }
  renderPopupMenu = documentObject => {
    if (Platform.OS === "ios") {
      return (
        <PopupMenuIOS
          actions={["Update Document", "Delete", "Cancel"]}
          onSelect={this.onPopupEvent}
          documentObject={documentObject}
          onPress={(eventName, index) =>
            this.onPopupEvent(eventName, index, documentObject)}
        />
      )
    }
    return (
      <PopupMenu
        actions={["Update", "Delete"]}
        onPress={(eventName, index) =>
          this.onPopupEvent(eventName, index, documentObject)}
      />
    )
  }
  render() {
    const { height, width } = Dimensions.get("window")
    const {
      patientProfileStore,
      documentId,
      entityId,
      description,
      entityType,
      doctorAccessIds,
      documentUrl,
      documentType,
      documentDate
    } = this.props.documentObject
    if (__DEV__) console.log(documentUrl, "documentUrldocumentUrl")
    return (
      <TouchableWithoutFeedback
        onPress={() => this.props.updateDocument(documentId)}
      >
        <View style={styles.mainContainer}>
          <View style={styles.headerContainer}>
            <View style={styles.headerContentContainer}>
              <Text style={styles.headerTextStyles}>{description}</Text>
            </View>
            <View style={styles.popupMenuContainer}>
              {this.renderPopupMenu(this.props.documentObject)}
            </View>
          </View>
          <View style={styles.bodyContentContainer}>
            {documentUrl != "NOIMAGE" ? (
              <View style={[styles.imageContainer]}>
                <Image
                  source={{ uri: documentUrl }}
                  style={[
                    styles.attachedImage,
                    { width: width - 60, height: 250 }
                  ]}
                  resizeMode="cover"
                />
              </View>
            ) : null}
          </View>

          <View style={styles.footerContainer}>
            <View style={styles.flexContainer}>
              <Text style={styles.authorNameStyles}>
                {this.props.documentObject.getDocumentAuthorName}
              </Text>
              <Text style={styles.recordDateStyles}>
                {moment(documentDate).format("ddd, MMM Do YYYY")}
              </Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}
