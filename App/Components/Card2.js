import React from "react"
import { TouchableOpacity, View, Text, Image, ScrollView } from "react-native"
import styles from "./Styles/CardStyles"
import { Actions as NavigationActions } from "react-native-router-flux"
export default class Card2 extends React.Component {
  componentWillMount() {}
  render() {
    if (!this.props.item) {
      return null
    }
    let { name, details, profilePic } = this.props.item
    return (
      <View style={styles.card}>
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <Image
            style={{ width: 50, height: 50 }}
            source={{ uri: profilePic }}
          />
        </View>
        <View style={{ flex: 2 }}>
          <Text style={{ fontSize: 15 }}>
            {name}
          </Text>
          <Text style={{ fontSize: 13 }}>
            Gynecologist
          </Text>
          <Text style={{ fontSize: 12 }}>
            {details}
          </Text>
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity
              onPress={() => {
                NavigationActions.ChatConversation({ item: this.props.item })
              }}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  width: 70,
                  height: 25,
                  borderRadius: 3,
                  backgroundColor: "#387ef5",
                  marginTop: 5,
                  marginRight: 20
                }}
              >
                <Text style={{ color: "white", fontSize: 13 }}>Connect</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                NavigationActions.PatientProfile({ item: this.props.item })
              }}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  width: 70,
                  height: 25,
                  borderRadius: 3,
                  backgroundColor: "white",
                  marginTop: 5,
                  borderWidth: 1.2,
                  borderColor: "#387ef5"
                }}
              >
                <Text style={{ color: "#387ef5", fontSize: 13 }}>Open</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}
