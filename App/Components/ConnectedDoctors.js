import React from "react";
import {
  TouchableOpacity,
  View,
  Text,
  Image,
  ScrollView,
  TextInput
} from "react-native";
import styles from "../Containers/Styles/ChatsListStyles";
import Card from "./Card";
import ChatList from "../Data/ChatList";
import { Actions as NavigationActions } from "react-native-router-flux";

export default class ConnectedDoctors extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "  Search Doctor"
    };
  }

  displayChatList = () => {
    return ChatList.map((item, index) => {
      return <Card index={index} item={item} />;
    });
  };
  render() {
    let chats = this.displayChatList();
    return (
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        {chats}
      </ScrollView>
    );
  }
}
