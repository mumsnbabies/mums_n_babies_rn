import React, { Component } from "react";
import {
  View,
  Image,
  ActionSheetIOS,
  findNodeHandle,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import Mqtt from "@rn/react-native-mqtt-iot";
import AuthStore from "../stores/AuthStore";
import { Actions as NavigationActions } from "react-native-router-flux";
import Images from "../Themes/Images";

const ICON_SIZE = 24;
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;
var CANCEL_INDEX2 = 3;
export default class PopupMenuIOS extends Component {
  constructor(props) {
    super(props);
    this.state = {
      icon: null,
      clicked: "none"
    };
  }

  onError() {
    if (__DEV__) {
      console.log("Popup Error");
    }
  }

  onPress = () => {
    if (this.state.icon) {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          options: this.props.actions,
          cancelButtonIndex: this.props.onSelect ? CANCEL_INDEX2 : CANCEL_INDEX,
          destructiveButtonIndex:
            AuthStore.mode === 0
              ? DESTRUCTIVE_INDEX + 1
              : this.props.onSelect ? DESTRUCTIVE_INDEX - 1 : DESTRUCTIVE_INDEX
        },
        buttonIndex => {
          this.setState({ clicked: this.props.actions[buttonIndex] });
          if (this.props.onSelect) {
            this.props.onSelect(
              this.props.actions[buttonIndex],
              buttonIndex,
              this.props.documentObject
            );
          }
        }
      );
    }
  };
  render() {
    if (AuthStore.mode === 0) {
      if (this.state.clicked === "Change Password") {
        NavigationActions.ChangePassword();
      } else if (this.state.clicked === "Update Profile") {
        NavigationActions.DoctorUpdateProfile();
      } else if (this.state.clicked === "Log Out") {
        AsyncStorage.multiRemove(
          ["AccessToken", "doctor_id"],
          async function() {
            AuthStore.resetState();
            await Mqtt.logoutChatService();
            NavigationActions.Initial({ type: "reset" });
          }.bind(this)
        );
        // AsyncStorage.removeItem(
        //   "AccessToken",
        //   function() {
        //     AuthStore.resetState()
        //     NavigationActions.Initial({ type: "reset" })
        //   }.bind(this)
        // )
      } else if (this.state.clicked === "Update Timings") {
        NavigationActions.DoctorTiming();
      }
    } else {
      if (this.state.clicked === "Change Password") {
        NavigationActions.ChangePassword();
      } else if (this.state.clicked === "Update Profile") {
        NavigationActions.PatientProfileUpdate();
      } else if (this.state.clicked === "Log Out") {
        AsyncStorage.multiRemove(
          ["AccessToken", "patient_id"],
          async function() {
            AuthStore.resetState();
            await Mqtt.logoutChatService();
            NavigationActions.Initial({ type: "reset" });
          }.bind(this)
        );
        // AsyncStorage.removeItem(
        //   "AccessToken",
        //   function() {
        //     AuthStore.resetState()
        //     NavigationActions.Initial({ type: "reset" })
        //   }.bind(this)
        // )
      }
    }
    return (
      <TouchableOpacity onPress={this.onPress}>
        <View hitSlop={{ top: 30, left: 30, bottom: 30, right: 30 }}>
          <Image
            style={{ marginTop: -3 }}
            source={Images.menuHorizontal}
            size={ICON_SIZE}
            ref={this.onRef}
          />
        </View>
      </TouchableOpacity>
    );
  }

  onRef = icon => {
    if (!this.state.icon) {
      this.setState({ icon });
    }
  };
}
