import React from "react";
import { TouchableOpacity, View, Text, Image } from "react-native";
import Images from "../Themes/Images";
import styles from "../Containers/Styles/ChatsListStyles";
export default class PatientChat extends React.Component {
  render() {
    let { item, gotoChatList, chatRoom } = this.props;
    let { name, onlineStatus, id, metadata, unreadMessagesCount } = item;
    let { detail } = JSON.parse(metadata);
    return (
      <TouchableOpacity
        underlayColor="#eee"
        onPress={() => {
          gotoChatList(chatRoom);
        }}
      >
        <View style={styles.ChatListSection}>
          <View style={styles.ChatLeftTopSection}>
            {detail.pic ? (
              <Image
                style={styles.PersonImage}
                resizeMode="contain"
                source={{ uri: detail.pic }}
              />
            ) : (
              <Image
                style={styles.PersonImage}
                resizeMode="contain"
                source={Images.patientProfilePic}
              />
            )}
          </View>
          <View style={styles.ChatRightSection}>
            <View style={styles.ChatRightTopSection}>
              <View style={styles.ChatDetails}>
                <Text numberOfLines={1} style={styles.PersonName}>
                  {name}
                </Text>
                <Text numberOfLines={1} style={styles.PersonDetails}>
                  {detail.city}
                </Text>
              </View>
              <View style={styles.StatusView}>
                {chatRoom.unreadMessagesCount ? (
                  <Text>[{chatRoom.unreadMessagesCount}]</Text>
                ) : null}
                {onlineStatus === "ONLINE" ? (
                  <View style={styles.ActiveDot} />
                ) : (
                  <View style={styles.InactiveDot} />
                )}
              </View>
            </View>
            {/* <View style={styles.ChatRightBottomSection}>
              <View style={styles.LatestMsgView}>
                <Text numberOfLines={1} style={styles.LatestMsgViewText}>
                  {latestMsg}kjkljkljkljlkjjljjkljkljlkjhgyyuvvhvhhgghhghhggghvgggvgvghhghg
                </Text>
              </View>
            </View> */}
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
