import React from "react";
import {
  TouchableOpacity,
  View,
  Text,
  Image,
  ScrollView,
  TextInput
} from "react-native";
import styles from "../Containers/Styles/ChatsListStyles";
import Card2 from "./Card2";
import ChatList from "../Data/ChatList";
import { Actions as NavigationActions } from "react-native-router-flux";
export default class SearchDoctors extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "  Search Doctor"
    };
  }

  displayChatList = () => {
    return ChatList.map((item, index) => {
      return <Card2 index={index} item={item} />;
    });
  };
  render() {
    let chats = this.displayChatList();
    return (
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <View style={styles.input}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            returnKeyType="search"
            style={styles.inputField}
            placeholder="Search Doctors"
            autoCorrect={false}
            autoCapitalize="none"
            underlineColorAndroid="transparent"
          />
        </View>
        <TouchableOpacity>
          <View style={styles.daycount2}>
            <Text style={{ color: "white", fontWeight: "600", fontSize: 12 }}>
              Search
            </Text>
          </View>
        </TouchableOpacity>
        <Text
          style={{
            fontWeight: "600",
            fontSize: 17,
            alignSelf: "center",
            marginBottom: 10
          }}
        >
          Search Results
        </Text>
        {chats}
      </ScrollView>
    );
  }
}
