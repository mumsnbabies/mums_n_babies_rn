import React from "react";
import { TouchableOpacity, View, Text, Image, ScrollView } from "react-native";
import styles from "./Styles/DoctorConnectedCardStyles";
import Images from "../Themes/Images";
import { DOCTORCONNECTEDCARDCONSTANTS } from "../Constants/Doctor";
export default class DoctorConnectedCard extends React.Component {
  displayPatientDetails = () => {
    let { patient_id, patient_name } = this.props.details;
    let requestObject = {
      patient_id: patient_id
    };
    this.props.displayPatientProfile(requestObject, patient_name);
  };
  render() {
    let { patient_name, patient_address } = this.props.details;
    return (
      <View style={styles.DoctorConnectedCard}>
        <View style={styles.DoctorProfilePicContainer}>
          <Image
            style={styles.ProfilePic}
            resizemode="contain"
            source={Images.patientProfilePic}
          />
        </View>
        <View style={styles.bodycontent}>
          <View style={styles.DoctorConnectedCardContent}>
            <View style={styles.patientname}>
              <Text style={styles.DoctorConnectedPatientName}>
                {patient_name ? patient_name.toUpperCase() : null}
              </Text>
            </View>
            <View style={styles.patientaddress}>
              <Text style={styles.DoctorConnectedPatientDetails}>
                {patient_address ? patient_address.toUpperCase() : null}
              </Text>
            </View>
            <View style={styles.DoctorConnectedCardContentBtns}>
              <View style={styles.DoctorConnectedCardAcceptBtn}>
                <Text style={styles.DoctorConnectedCardAcceptBtnText}>
                  {DOCTORCONNECTEDCARDCONSTANTS.ACCEPTED_BUTTON_TEXT}
                </Text>
              </View>
              <TouchableOpacity
                onPress={this.displayPatientDetails}
                style={styles.DoctorConnectedCardOpenBtn}
              >
                <Text style={styles.DoctorConnectedCardOpenBtnText}>
                  {DOCTORCONNECTEDCARDCONSTANTS.OPEN_BUTTON_TEXT}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
