import React, { Component } from "react";
// import { SmoothLine } from "react-native-pathjs-charts";
import PatientProfileStore from "../stores/PatientProfileStore";
import AuthStore from "../stores/AuthStore";
import { observer } from "mobx-react";
import moment from "moment";
// import Chart from "react-native-chart";
import {
  Dimensions,
  ActivityIndicator,
  Alert,
  View,
  StyleSheet,
  processColor
} from "react-native";
import Loader from "./Loader";
import { LineChart } from "react-native-charts-wrapper";
// import {
//   VictoryChart,
//   VictoryTheme,
//   VictoryArea,
//   VictoryLine
// } from "victory-native";

const styles = StyleSheet.create({
  loaderContainer: {
    width: 280,
    height: 200,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 30
  }
});
@observer
export default class Graph extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    // componentWillReceiveProps(props) {
    let date = new Date();
    let old_date = new Date().setMonth(new Date().getMonth() - 6);
    let requestObject = {};
    if (AuthStore.mode === 0) {
      requestObject = {
        attr_keys: this.props.recordValueKeys,
        from_date: moment(old_date).format("YYYY-MM-DD ") + "00:00",
        to_date: moment(date).format("YYYY-MM-DD ") + "23:59"
      };
      if (this.props.selectedPerson === "Parent") {
        requestObject["patient_id"] = this.props.selectedParent;
      } else {
        requestObject["child_id"] = this.props.selectedChild;
      }
    } else {
      requestObject = {
        attr_keys: this.props.recordValueKeys,
        from_date: moment(old_date).format("YYYY-MM-DD ") + "00:00",
        to_date: moment(date).format("YYYY-MM-DD ") + "23:59",
        child_id: -1,
        is_child_data: false
      };
    }
    if (this.props.selectedParent || this.props.selectedChild) {
      if (this.props.selectedPerson === "Parent") {
        if (AuthStore.mode === 0) {
          PatientProfileStore.doctorGettingPatientAnalytics(requestObject);
        } else {
          PatientProfileStore.getPatientAnalytics(requestObject);
        }
      } else {
        if (AuthStore.mode === 0) {
          PatientProfileStore.doctorGettingChildAnalytics(requestObject);
        } else {
          requestObject["child_id"] = this.props.selectedChild;
          requestObject["is_child_data"] = true;
          PatientProfileStore.getChildAnalytics(requestObject);
        }
      }
      if (__DEV__) {
        console.log(requestObject);
      }
    }
  }
  componentWillReceiveProps(nextProps) {
    let date = new Date();
    let old_date = new Date().setMonth(new Date().getMonth() - 6);
    let requestObject = {};
    if (AuthStore.mode === 0) {
      requestObject = {
        attr_keys: nextProps.recordValueKeys,
        from_date: moment(old_date).format("YYYY-MM-DD ") + "00:00",
        to_date: moment(date).format("YYYY-MM-DD ") + "23:59"
      };
      if (nextProps.selectedPerson === "Parent") {
        requestObject["patient_id"] = nextProps.selectedParent;
      } else {
        requestObject["child_id"] = nextProps.selectedChild;
      }
    } else {
      requestObject = {
        attr_keys: nextProps.recordValueKeys,
        from_date: moment(old_date).format("YYYY-MM-DD ") + "00:00",
        to_date: moment(date).format("YYYY-MM-DD ") + "23:59",
        child_id: -1,
        is_child_data: false
      };
    }
    if (nextProps.selectedParent || nextProps.selectedChild) {
      if (nextProps.selectedPerson === "Parent") {
        if (AuthStore.mode === 0) {
          PatientProfileStore.doctorGettingPatientAnalytics(requestObject);
        } else {
          PatientProfileStore.getPatientAnalytics(requestObject);
        }
      } else {
        if (AuthStore.mode === 0) {
          PatientProfileStore.doctorGettingChildAnalytics(requestObject);
        } else {
          requestObject["child_id"] = nextProps.selectedChild;
          requestObject["is_child_data"] = true;
          PatientProfileStore.getChildAnalytics(requestObject);
        }
      }
      if (__DEV__) {
        console.log(requestObject);
      }
    }
  }
  render() {
    let count = 0;
    if (__DEV__) {
      console.log(PatientProfileStore.analytics);
    }
    let data = [];
    if (PatientProfileStore.getAnalyticsStatus === 100) {
      return (
        <View style={styles.loaderContainer}>
          <Loader animating={true} color={"white"} />
        </View>
      );
    } else if (PatientProfileStore.getAnalyticsStatus === 200) {
      if (Object.keys(PatientProfileStore.analytics).length) {
        let analytics = Object.keys(PatientProfileStore.analytics);
        let graph_values = [];
        // let initial_value = {
        //   time: moment(new Date().setMonth(new Date().getMonth() - 1)).unix(),
        //   attr_value: 0
        // };
        // graph_values.push(initial_value);
        data = {
          dataSets: []
        };
        let xAxisValues = [];
        let colorCodes = [
          processColor("white"),
          processColor("red"),
          processColor("blue"),
          processColor("green")
        ];
        analytics.map((eachAnalytic, index) => {
          let yAxisValues = [];
          PatientProfileStore.analytics[eachAnalytic].map(
            (eachValue, index) => {
              let analytic = {};
              let chart_value = [
                moment(eachValue.update_time).unix(),
                parseInt(eachValue.attr_value)
              ];
              if (__DEV__) {
                console.log(eachValue.update_time, eachValue.attr_value);
              }
              xAxisValues.push(moment(eachValue.update_time).format("MMM DD"));
              yAxisValues.push({
                y: parseInt(eachValue.attr_value),
                marker:
                  "( " +
                  moment(eachValue.update_time).format("MMM DD") +
                  ", " +
                  parseInt(eachValue.attr_value) +
                  " )"
              });
              // analytic["attr_value"] = parseInt(eachValue.attr_value);
              // analytic["time"] = moment(eachValue.update_time).unix();
              // analytic["y"] = parseInt(eachValue.attr_value);
              // analytic["x"] = moment(eachValue.update_time).unix();
              // analytic["x"] = index;
              // graph_values.push(analytic);
              graph_values.push(chart_value);
            }
          );
          data.dataSets.push({
            values: yAxisValues,
            label: eachAnalytic,
            config: {
              lineWidth: 2,
              drawCircles: true,
              highlightColor: colorCodes[index],
              color: colorCodes[index],
              drawFilled: false,
              fillColor: colorCodes[index],
              fillAlpha: 60,
              circleColor: colorCodes[index],
              drawHighlightIndicators: false,
              drawValues: false
              // valueTextSize: 15
            }
          });
        });
        // let xAxis = {
        //   $set: {
        //     valueFormatter: xAxisValues
        //   }
        // }
        if (__DEV__) {
          console.log(graph_values);
        }
        // data = graph_values

        if (__DEV__) {
          console.log(xAxisValues);
        }
        let xAxis = {
          valueFormatter: xAxisValues,
          position: "BOTTOM",
          drawGridLines: false,
          drawAxisLine: true,
          axisLineColor: processColor("black"),
          gridLineWidth: 0,
          drawValues: false
        };
        let yAxis = {
          left: {
            drawGridLines: false,
            drawAxisLine: true,
            axisLineColor: processColor("black")
          },
          right: { enabled: false },
          drawGridLines: false,

          drawZeroLine: false,
          gridLineWidth: 0
        };
        let legend = {
          enabled: true,
          textColor: processColor("black"),
          textSize: 12,
          position: "BELOW_CHART_RIGHT",
          form: "SQUARE",
          formSize: 14,
          xEntrySpace: 10,
          yEntrySpace: 5,
          formToTextSpace: 5,
          wordWrapEnabled: true,
          maxSizePercent: 0.5
        };
        let marker = {
          enabled: true,
          backgroundTint: processColor("teal"),
          markerColor: processColor("#F0C0FF8C"),
          textColor: processColor("white")
        };
        return (
          <LineChart
            style={{ height: 250 }}
            data={data}
            chartDescription={{ text: "" }}
            description={{ text: "" }}
            legend={legend}
            marker={marker}
            xAxis={xAxis}
            yAxis={yAxis}
            drawGridBackground={false}
            drawGridLines={false}
            drawAxisLine={false}
            borderColor={processColor("black")}
            borderWidth={1}
            drawBorders={false}
            touchEnabled={true}
            dragEnabled={true}
            scaleEnabled={true}
            scaleXEnabled={true}
            scaleYEnabled={true}
            pinchZoom={true}
            doubleTapToZoomEnabled={true}
            dragDecelerationEnabled={true}
            dragDecelerationFrictionCoef={0.99}
            keepPositionOnRotation={false}
            drawValues={true}
            maxVisibleValueCount={12}
          />
        );
      }
      // let options = {
      //   width: 280,
      //   height: 200,
      //   color: "#fffaf0",
      //   margin: {
      //     top: 10,
      //     left: 30,
      //     bottom: 20,
      //     right: 20
      //   },
      //   animate: {
      //     type: "delayed",
      //     duration: 200
      //   },
      //   axisX: {
      //     showAxis: true,
      //     showLines: false,
      //     showLabels: true,
      //     showTicks: true,
      //     zeroAxis: false,
      //     orient: "bottom",
      //     label: {
      //       fontFamily: "Arial",
      //       fontSize: 14,
      //       fontWeight: true,
      //       fill: "#ffffff"
      //     }
      //   },
      //   axisY: {
      //     showAxis: true,
      //     showLines: false,
      //     showLabels: true,
      //     showTicks: true,
      //     zeroAxis: false,
      //     orient: "left",
      //     label: {
      //       fontFamily: "Arial",
      //       fontSize: 14,
      //       fontWeight: true,
      //       fill: "#ffffff"
      //     }
      //   }
      // };

      // if (data.length > 0) {
      //   // return (
      //   //   <VictoryChart theme={VictoryTheme.material}>
      //   //     <VictoryArea interpolation="natural" data={data} />
      //   //   </VictoryChart>
      //   // );
      //   // return (
      //   //   <SmoothLine
      //   //     data={data}
      //   //     options={options}
      //   //     xKey="time"
      //   //     yKey="attr_value"
      //   //   />
      //   // );
      //   return (
      //     <Chart
      //       style={{
      //         height: 200,
      //         width: Dimensions.get("window").width - 20
      //       }}
      //       data={data}
      //       type="line"
      //       showDataPoint={true}
      //       xAxisTransform={value => {
      //         let date = moment.unix(value).format("MMM DD");
      //         count = count + 1;
      //         if (data.length < 8) {
      //           return date;
      //         }
      //         if (data.length > 7 && data.length < 14) {
      //           if (count % Math.ceil(data.length / 7) === 0) {
      //             return date;
      //           }
      //         }
      //         if (
      //           count % Math.ceil(data.length / 7) ===
      //           (data.length % 7) % 7
      //         ) {
      //           return date;
      //         }
      //         return null;

      //         // if (count % 5 === 4) {

      //         //   return date

      //         // } else {

      //         //   return null

      //         // }
      //       }}
      //       color="#fff"
      //       fillColor="rgba(255,255,255,0.4)"
      //       axisColor="#662a48"
      //       axisLabelColor="#ffffff"
      //       dataPointColor="#fff"
      //       dataPointFillColor="#fff"
      //       showGrid={false}
      //       tightBounds
      //       verticalGridStep={10}

      //       // xAxisHeight={30}
      //     />
      //   );
      // }
    } else if (PatientProfileStore.getAnalyticsStatus > 200) {
      Alert.alert("Error", PatientProfileStore.errors.statusText, [
        {
          text: "OK",
          onPress: () => PatientProfileStore.resetAnalyticsState()
        }
      ]);
    }
    return null;
  }
}
