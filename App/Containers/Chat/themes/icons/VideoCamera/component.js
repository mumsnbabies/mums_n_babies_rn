/*
* @flow
*/

import React from 'react'
import Svg, { Defs, G, Path, Rect } from 'react-native-svg'

type Props = {
  width?: number,
  height?: number,
  color?: string
}

const VideoCamera = (
  props: Props = { width: 24, height: 24, color: '#40506D' }
) => {
  const { width = 24, height = 24, color = '#40506D' } = props // eslint-disable-line
  return (
    <Svg height={height} version="1.1" viewBox="0 0 24 24" width={width}>
      <Defs />
      <G
        fill="none"
        fillRule="evenodd"
        id="Symbols"
        stroke="none"
        strokeWidth="1"
      >
        <G id="video_dark">
          <G id="VideoCamera">
            <Rect height="24" id="Rectangle-7" width="24" x="0" y="0" />

            <Path
              d="M21.5505317,7.00407765 C22.1572444,6.57071139 23,7.00440849 23,7.75 L23,16.9166667 C23,17.6622582 22.1572444,18.0959553 21.5505317,17.662589 L15.133865,13.0792557 C14.622045,12.71367 14.622045,11.9529967 15.133865,11.587411 L21.5505317,7.00407765 Z M17.2437596,12.3333333 L21.1666667,15.1354098 L21.1666667,9.53125688 L17.2437596,12.3333333 Z"
              fill={color}
              fillRule="nonzero"
              id="Shape"
            />

            <Path
              d="M3.75,6.83333333 C3.24373898,6.83333333 2.83333333,7.24373898 2.83333333,7.75 L2.83333333,16.9166667 C2.83333333,17.4229277 3.24373898,17.8333333 3.75,17.8333333 L13.8333333,17.8333333 C14.3395944,17.8333333 14.75,17.4229277 14.75,16.9166667 L14.75,7.75 C14.75,7.24373898 14.3395944,6.83333333 13.8333333,6.83333333 L3.75,6.83333333 Z M3.75,5 L13.8333333,5 C15.3521164,5 16.5833333,6.23121694 16.5833333,7.75 L16.5833333,16.9166667 C16.5833333,18.4354497 15.3521164,19.6666667 13.8333333,19.6666667 L3.75,19.6666667 C2.23121694,19.6666667 1,18.4354497 1,16.9166667 L1,7.75 C1,6.23121694 2.23121694,5 3.75,5 Z"
              fill={color}
              fillRule="nonzero"
              id="Rectangle-path"
            />
          </G>
        </G>
      </G>
    </Svg>
  )
}

export default VideoCamera
