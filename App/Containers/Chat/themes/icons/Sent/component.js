/*
* @flow
*/

import React from 'react'
import Svg, { Defs, G, Polygon } from 'react-native-svg'

type Props = {
  width?: number,
  height?: number,
  color?: string
}

const Sent = (props: Props = { width: 18, height: 18, color: '#798499' }) => {
  const { width = 18, height = 18, color = '#798499' } = props // eslint-disable-line
  return (
    <Svg height={height} version="1.1" viewBox="0 0 18 18" width={width}>
      <Defs />
      <G
        fill="none"
        fillRule="evenodd"
        id="Page-1"
        stroke="none"
        strokeWidth="1"
      >
        <G id="Icons" transform={{ translate: '-287.000000, -973.000000' }}>
          <G id="sent" transform={{ translate: '287.000000, 973.000000' }}>
            <Polygon id="Shape" points="0 0 18 0 18 18 0 18" />

            <Polygon
              fill={color}
              fillRule="nonzero"
              id="Shape"
              points="6.75 12.15 3.6 9 2.55 10.05 6.75 14.25 15.75 5.25 14.7 4.2"
            />
          </G>
        </G>
      </G>
    </Svg>
  )
}

export default Sent
