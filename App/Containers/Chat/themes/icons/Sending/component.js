/*
* @flow
*/

import React from 'react'
import Svg, { Defs, G, Path, Polygon } from 'react-native-svg'

type Props = {
  width?: number,
  height?: number,
  color?: string
}

const Sending = (
  props: Props = { width: 18, height: 18, color: '#798499' }
) => {
  const { width = 18, height = 18, color = '#798499' } = props // eslint-disable-line
  return (
    <Svg height={height} version="1.1" viewBox="0 0 18 18" width={width}>
      <Defs />
      <G
        fill="none"
        fillRule="evenodd"
        id="Page-1"
        stroke="none"
        strokeWidth="1"
      >
        <G id="Icons" transform={{ translate: '-322.000000, -973.000000' }}>
          <G id="sending" transform={{ translate: '322.000000, 973.000000' }}>
            <Path
              d="M8.9925,1.5 C4.8525,1.5 1.5,4.86 1.5,9 C1.5,13.14 4.8525,16.5 8.9925,16.5 C13.14,16.5 16.5,13.14 16.5,9 C16.5,4.86 13.14,1.5 8.9925,1.5 Z M9,15 C5.685,15 3,12.315 3,9 C3,5.685 5.685,3 9,3 C12.315,3 15,5.685 15,9 C15,12.315 12.315,15 9,15 Z"
              fill={color}
              fillRule="nonzero"
              id="Shape"
            />

            <Polygon id="Shape" points="0 0 18 0 18 18 0 18" />

            <Polygon
              fill={color}
              fillRule="nonzero"
              id="Shape"
              points="9.375 5.25 8.25 5.25 8.25 9.75 12.1875 12.1125 12.75 11.19 9.375 9.1875"
            />
          </G>
        </G>
      </G>
    </Svg>
  )
}

export default Sending
