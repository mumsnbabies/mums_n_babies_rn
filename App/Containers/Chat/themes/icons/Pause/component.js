/*
* @flow
*/

import React from 'react'
import Svg, { Defs, G, Path } from 'react-native-svg'

type Props = {
  width?: number,
  height?: number,
  color?: string
}

const Pause = (props: Props) => {
  const { width = 15, height = 18, color = '#40506D' } = props
  return (
    <Svg height={height} version="1.1" viewBox="0 0 15 18" width={width}>
      <Defs />
      <G
        fill="none"
        fillRule="evenodd"
        id="Page-1"
        stroke="none"
        strokeWidth="1"
      >
        <G
          fill={color}
          fillRule="nonzero"
          id="Icons"
          transform="translate(-53.000000, -1075.000000)"
        >
          <G id="pause" transform="translate(53.000000, 1075.000000)">
            <Path
              d="M5.246625,15.366 C5.246625,16.73925 4.13325,17.852625 2.76,17.852625 L2.76,17.852625 C1.38675,17.852625 0.273375,16.73925 0.273375,15.366 L0.273375,2.486625 C0.273375,1.113375 1.38675,0 2.76,0 L2.76,0 C4.13325,0 5.246625,1.113375 5.246625,2.486625 L5.246625,15.366 Z"
              id="Shape"
            />

            <Path
              d="M14.578875,15.366 C14.578875,16.73925 13.4655,17.852625 12.09225,17.852625 L12.09225,17.852625 C10.719,17.852625 9.605625,16.73925 9.605625,15.366 L9.605625,2.486625 C9.606,1.113375 10.719375,0 12.09225,0 L12.09225,0 C13.4655,0 14.578875,1.113375 14.578875,2.486625 L14.578875,15.366 Z"
              id="Shape"
            />
          </G>
        </G>
      </G>
    </Svg>
  )
}

Pause.defaultProps = {
  width: 15,
  height: 18,
  color: '#40506D'
}

export default Pause
