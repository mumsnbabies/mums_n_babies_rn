/*
* @flow
*/

import React from 'react'
import Svg, { Defs, G, Path, Rect } from 'react-native-svg'

type Props = {
  width?: number,
  height?: number,
  color?: string
}

const Headphones = (
  props: Props = { width: 24, height: 24, color: '#40506D' }
) => {
  const { width = 24, height = 24, color = '#40506D' } = props // eslint-disable-line
  return (
    <Svg height={height} version="1.1" viewBox="0 0 24 24" width={width}>
      <Defs />
      <G
        fill="none"
        fillRule="evenodd"
        id="Symbols"
        stroke="none"
        strokeWidth="1"
      >
        <G id="audio_dark">
          <G id="Headphones">
            <Rect height="24" id="Rectangle-7" width="24" x="0" y="0" />

            <Path
              d="M4,18 L4,12 C4.00000007,7.58172206 7.58172205,4.00000013 12,4.00000013 C16.418278,4.00000013 19.9999999,7.58172206 20,12 L20,18 C20,18.5522847 20.4477153,19 21,19 C21.5522847,19 22,18.5522847 22,18 L22,12 C21.9999999,6.47715254 17.5228474,2.00000013 12,2.00000013 C6.47715256,2.00000013 2.00000008,6.47715254 2,12 L2,18 C2,18.5522847 2.44771525,19 3,19 C3.55228475,19 4,18.5522847 4,18 Z"
              fill={color}
              fillRule="nonzero"
              id="Shape"
            />

            <Path
              d="M20,15 L18,15 C17.4477153,15 17,15.4477153 17,16 L17,19 C17,19.5522847 17.4477153,20 18,20 L19,20 C19.5522847,20 20,19.5522847 20,19 L20,15 Z M22,19 C22,20.6568542 20.6568542,22 19,22 L18,22 C16.3431458,22 15,20.6568542 15,19 L15,16 C15,14.3431458 16.3431458,13 18,13 L21,13 C21.5522847,13 22,13.4477153 22,14 L22,19 Z M2,19 L2,14 C2,13.4477153 2.44771525,13 3,13 L6,13 C7.65685425,13 9,14.3431458 9,16 L9,19 C9,20.6568542 7.65685425,22 6,22 L5,22 C3.34314575,22 2,20.6568542 2,19 Z M4,15 L4,19 C4,19.5522847 4.44771525,20 5,20 L6,20 C6.55228475,20 7,19.5522847 7,19 L7,16 C7,15.4477153 6.55228475,15 6,15 L4,15 Z"
              fill={color}
              fillRule="nonzero"
              id="Shape"
            />
          </G>
        </G>
      </G>
    </Svg>
  )
}

export default Headphones
