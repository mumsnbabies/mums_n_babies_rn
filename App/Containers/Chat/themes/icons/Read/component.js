/*
* @flow
*/

import React from 'react'
import Svg, { Defs, G, Path, Polygon } from 'react-native-svg'

type Props = {
  width?: number,
  height?: number,
  color?: string
}
const Read = (props: Props = { width: 18, height: 18, color: '#0064FF' }) => {
  const { width = 18, height = 18, color = '#0064FF' } = props // eslint-disable-line
  // FIXME: width and height from props and color
  return (
    <Svg height={height} version="1.1" viewBox="0 0 18 18" width={width}>
      <Defs />
      <G
        fill="none"
        fillRule="evenodd"
        id="Page-1"
        stroke="none"
        strokeWidth="1"
      >
        <G id="Icons" transform={{ translate: '-50.000000, -973.000000' }}>
          <G id="read" transform={{ translate: '50.000000, 973.000000' }}>
            <Polygon id="Shape" points="0 0 18 0 18 18 0 18" />

            <Path
              d="M13.5,5.25 L12.4425,4.1925 L7.6875,8.9475 L8.745,10.005 L13.5,5.25 Z M16.68,4.1925 L8.745,12.1275 L5.61,9 L4.5525,10.0575 L8.745,14.25 L17.745,5.25 L16.68,4.1925 Z M0.3075,10.0575 L4.5,14.25 L5.5575,13.1925 L1.3725,9 L0.3075,10.0575 Z"
              fill={color}
              fillRule="nonzero"
              id="Shape"
            />
          </G>
        </G>
      </G>
    </Svg>
  )
}

export default Read
