/*
* @flow
*/

import React from 'react'
import Svg, { Defs, G, Path } from 'react-native-svg'

type Props = {
  width?: number,
  height?: number,
  color?: string
}

const SearchLight = (
  props: Props = { width: 18, height: 18, color: '#8892A4' }
) => {
  const { width = 18, height = 18, color = '#8892A4' } = props // eslint-disable-line
  return (
    <Svg height={height} version="1.1" viewBox="0 0 18 18" width={width}>
      <Defs />
      <G
        fill="none"
        fillRule="evenodd"
        id="Page-1"
        stroke="none"
        strokeWidth="1"
      >
        <G
          fill={color}
          fillRule="nonzero"
          id="Icons"
          transform={{ translate: '-121.000000, -973.000000' }}
        >
          <G
            id="search_light"
            transform={{ translate: '121.000000, 973.000000' }}
          >
            <Path
              d="M8.25,14.25 C4.9362915,14.25 2.25,11.5637085 2.25,8.25 C2.25,4.9362915 4.9362915,2.25 8.25,2.25 C11.5637085,2.25 14.25,4.9362915 14.25,8.25 C14.25,11.5637085 11.5637085,14.25 8.25,14.25 Z M8.25,12.75 C10.7352814,12.75 12.75,10.7352814 12.75,8.25 C12.75,5.76471863 10.7352814,3.75 8.25,3.75 C5.76471863,3.75 3.75,5.76471863 3.75,8.25 C3.75,10.7352814 5.76471863,12.75 8.25,12.75 Z"
              id="Oval-8"
            />

            <Path
              d="M15.5303301,14.4696699 L12.8786797,11.8180195 C12.5857864,11.5251263 12.1109127,11.5251263 11.8180195,11.8180195 C11.5251263,12.1109127 11.5251263,12.5857864 11.8180195,12.8786797 L14.4696699,15.5303301 C14.7625631,15.8232233 15.2374369,15.8232233 15.5303301,15.5303301 C15.8232233,15.2374369 15.8232233,14.7625631 15.5303301,14.4696699 Z"
              id="Line"
            />
          </G>
        </G>
      </G>
    </Svg>
  )
}

export default SearchLight
