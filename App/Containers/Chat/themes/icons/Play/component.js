/*
* @flow
*/

import React from 'react'
import Svg, { G, Path } from 'react-native-svg'

type Props = {
  width?: number,
  height?: number,
  color?: string
}

const Play = (props: Props) => {
  const { width = 16, height = 18, color = '#40506D' } = props
  return (
    <Svg height={height} version="1.1" viewBox="0 0 16 18" width={width}>
      <G
        id="Page-1"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
      >
        <G
          id="Icons"
          transform={{ translate: '-89.000000, -1075.000000' }}
          fillRule="nonzero"
          fill={color}
        >
          <G id="play" transform={{ translate: '89.000000, 1075.000000' }}>
            <Path
              d="M14.430206,7.69658369 L2.49581974,0.177218884 C2.14300429,-0.032832618 1.70490129,0.00216309013 1.37402575,0.00216309013 C0.0505236052,0.00216309013 0.0563948498,1.02399142 0.0563948498,1.28286695 L0.0563948498,16.6516223 C0.0563948498,16.8704807 0.0506008584,17.9324034 1.37402575,17.9324034 C1.70490129,17.9324034 2.14308155,17.9672446 2.49581974,17.7572704 L14.4301288,10.2379828 C15.4096996,9.6551073 15.2404378,8.96724464 15.2404378,8.96724464 C15.2404378,8.96724464 15.4097768,8.27938197 14.430206,7.69658369 Z"
              id="Shape"
            />
          </G>
        </G>
      </G>
    </Svg>
  )
}

Play.defaultProps = {
  width: 16,
  height: 18,
  color: '#40506D'
}

export default Play
