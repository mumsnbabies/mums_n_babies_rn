/*
* @flow
*/

import React from 'react'
import Svg, { Defs, G, Path, Rect } from 'react-native-svg'

type Props = {
  width?: number,
  height?: number,
  color?: string
}

const ImageAttachment = (
  props: Props = { width: 24, height: 24, color: '#40506D' }
) => {
  const { width = 24, height = 24, color = '#40506D' } = props // eslint-disable-line
  return (
    <Svg height={height} version="1.1" viewBox="0 0 24 24" width={width}>
      <Defs />
      <G
        fill="none"
        fillRule="evenodd"
        id="Symbols"
        stroke="none"
        strokeWidth="1"
      >
        <G id="image_dark">
          <G id="image_attachment">
            <Rect height="24" id="Rectangle-7" width="24" x="0" y="0" />

            <Path
              d="M4,4 C3.44771525,4 3,4.44771525 3,5 L3,19 C3,19.5522847 3.44771525,20 4,20 L20,20 C20.5522847,20 21,19.5522847 21,19 L21,5 C21,4.44771525 20.5522847,4 20,4 L4,4 Z M4,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L4,22 C2.34314575,22 1,20.6568542 1,19 L1,5 C1,3.34314575 2.34314575,2 4,2 Z"
              fill={color}
              fillRule="nonzero"
              id="Rectangle-path"
            />

            <Path
              d="M7.5,11 C6.11928813,11 5,9.88071187 5,8.5 C5,7.11928813 6.11928813,6 7.5,6 C8.88071187,6 10,7.11928813 10,8.5 C10,9.88071187 8.88071187,11 7.5,11 Z M7.5,9 C7.77614237,9 8,8.77614237 8,8.5 C8,8.22385763 7.77614237,8 7.5,8 C7.22385763,8 7,8.22385763 7,8.5 C7,8.77614237 7.22385763,9 7.5,9 Z"
              fill={color}
              fillRule="nonzero"
              id="Oval"
            />

            <Path
              d="M21.3356362,15.7474093 C21.7484189,16.1143273 22.3804913,16.0771466 22.7474093,15.6643638 C23.1143273,15.2515811 23.0771466,14.6195087 22.6643638,14.2525907 L17.0393638,9.25259068 C16.6604778,8.91580311 16.0895222,8.91580311 15.7106362,9.25259068 L3.33563616,20.2525907 C2.92285339,20.6195087 2.88567266,21.2515811 3.25259068,21.6643638 C3.6195087,22.0771466 4.25158107,22.1143273 4.66436384,21.7474093 L16.375,11.337955 L21.3356362,15.7474093 Z"
              fill={color}
              fillRule="nonzero"
              id="Shape"
            />
          </G>
        </G>
      </G>
    </Svg>
  )
}

export default ImageAttachment
