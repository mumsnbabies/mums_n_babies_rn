/*
* @flow
*/

import React from 'react'
import Svg, { Defs, G, Path } from 'react-native-svg'

type Props = {
  width?: number,
  height?: number,
  color?: string
}

const Smiley = (props: Props = { width: 18, height: 18, color: '#4F5E79' }) => {
  const { width, height, color } = props // eslint-disable-line
  return (
    <Svg height={height} version="1.1" viewBox="0 0 18 18" width={width}>
      <Defs />
      <G
        fill="none"
        fillRule="evenodd"
        id="Page-1"
        stroke="none"
        strokeWidth="1"
      >
        <G
          fill={color}
          fillRule="nonzero"
          id="Icons"
          transform={{ translate: '-199.000000, -973.000000' }}
        >
          <G id="smiley" transform={{ translate: '199.000000, 973.000000' }}>
            <Path
              d="M12.15,8.1 C12.87,8.1 13.5,7.47 13.5,6.75 C13.5,6.03 12.87,5.4 12.15,5.4 C11.43,5.4 10.8,6.03 10.8,6.75 C10.8,7.47 11.43,8.1 12.15,8.1 Z M5.85,8.1 C6.57,8.1 7.2,7.47 7.2,6.75 C7.2,6.03 6.57,5.4 5.85,5.4 C5.13,5.4 4.5,6.03 4.5,6.75 C4.5,7.47 5.13,8.1 5.85,8.1 Z M9,14.4 C11.34,14.4 13.32,12.87 14.13,10.8 L3.87,10.8 C4.68,12.87 6.66,14.4 9,14.4 Z M9,0 C4.05,0 0,4.05 0,9 C0,13.95 4.05,18 9,18 C13.95,18 18,13.95 18,9 C18,4.05 13.95,0 9,0 Z M9,16.2 C5.04,16.2 1.8,12.96 1.8,9 C1.8,5.04 5.04,1.8 9,1.8 C12.96,1.8 16.2,5.04 16.2,9 C16.2,12.96 12.96,16.2 9,16.2 Z"
              id="Shape"
            />
          </G>
        </G>
      </G>
    </Svg>
  )
}

export default Smiley
