/**
 * @flow
 */
import React from 'react'
import { observer } from 'mobx-react/native'
import styles from './styles' // eslint-disable-line
import SvgComponent from './component'

@observer
class SvgComponentWrapper extends React.Component {
  render() {
    return <SvgComponent {...this.props} />
  }
}

export default SvgComponentWrapper
