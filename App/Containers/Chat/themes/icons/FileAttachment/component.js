/*
* @flow
*/

import React from 'react'
import Svg, { Defs, G, Path, Rect } from 'react-native-svg'

type Props = {
  width?: number,
  height?: number,
  color?: string
}
const FileAttachment = (
  props: Props = { width: 24, height: 24, color: '#40506D' }
) => {
  const { width = 24, height = 24, color = '#40506D' } = props // eslint-disable-line
  return (
    <Svg height={height} version="1.1" viewBox="0 0 24 24" width={width}>
      <Defs />
      <G
        fill="none"
        fillRule="evenodd"
        id="Symbols"
        stroke="none"
        strokeWidth="1"
      >
        <G id="file_dark">
          <G id="file_attachment">
            <Rect height="24" id="Rectangle-7" width="24" x="0" y="0" />

            <Path
              d="M13,1 C13.2652165,1 13.5195704,1.10535684 13.7071068,1.29289322 L20.7071068,8.29289322 C20.8946432,8.4804296 21,8.73478351 21,9 L21,20 C21,21.6568542 19.6568542,23 18,23 L6,23 C4.34314575,23 3,21.6568542 3,20 L3,4 C3,2.34314575 4.34314575,1 6,1 L13,1 Z M12.5857864,3 L6,3 C5.44771525,3 5,3.44771525 5,4 L5,20 C5,20.5522847 5.44771525,21 6,21 L18,21 C18.5522847,21 19,20.5522847 19,20 L19,9.41421356 L12.5857864,3 Z"
              fill={color}
              fillRule="nonzero"
              id="Shape"
            />

            <Path
              d="M14,2 C14,1.44771525 13.5522847,1 13,1 C12.4477153,1 12,1.44771525 12,2 L12,9 C12,9.55228475 12.4477153,10 13,10 L20,10 C20.5522847,10 21,9.55228475 21,9 C21,8.44771525 20.5522847,8 20,8 L14,8 L14,2 Z"
              fill={color}
              fillRule="nonzero"
              id="Shape"
            />
          </G>
        </G>
      </G>
    </Svg>
  )
}

export default FileAttachment
