import { Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");

const TOOLBAR_HEIGHT = 58;

// Used via Metrics.baseMargin
const metrics = {
  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
  navBarHeight: Platform.OS === "ios" ? 64 : 54,
  customNavBarHeight: TOOLBAR_HEIGHT,
  statusBarHeight: 20,
  layoutHeight: height - TOOLBAR_HEIGHT,
  segmentHorizontalPadding: 18,
  segmentVerticalPadding: 8,
  segmentTabBorderWidth: 1,
  radioButtonTextMargin: 6,
  radioButtonVerticalPadding: 6,
  radioButtonImageSize: 20,
  checkboxVerticalPadding: 6,
  checkboxImageSize: 20,
  checkboxTextMargin: 6,
  paginationNavigateButtonHorizontalPadding: 10,
  paginationNavigateButtonVerticalPadding: 8,
  paginationNavigateButtonBorderWidth: 1,
  issuesMediumThumbnailSize: 36,
  issuesSmallThumbnailSize: 24,
  chatSidebarWidth: 260,
  sideBarWidth: 260,
  sideBarLeftMargin: 30,
  sideBarRightMargin: 30,
  chatChannelItemHeight: 56,
  chatMemberItemHeight: 56,
  chatIssueItemHeight: 36,
  chatSmallThumbnailSize: 36,
  chatMediumThumbnailSize: 42,
  chatLargeThumbnailSize: 60
};

export default metrics;
