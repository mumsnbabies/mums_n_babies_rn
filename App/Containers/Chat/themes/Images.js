/**
 * @flow
 * @author: Anesh Parvatha
 */

const sending = require('../images/sending/sending.png')
const sent = require('../images/sent/sent.png')
const receieved = require('../images/received/received.png')
const read = require('../images/read/read.png')

export default {
  sending,
  sent,
  receieved,
  read
}
