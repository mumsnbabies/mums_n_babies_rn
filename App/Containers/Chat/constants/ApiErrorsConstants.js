import I18n from 'react-native-i18n'

export const INVALID_DATE_RANGE = I18n.t('invalidDateRange')
