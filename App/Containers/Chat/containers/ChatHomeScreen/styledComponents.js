/*
 * @flow
 * @Author: Chinmaya
 */

import styled from "styled-components/native";
import BaseChatContainer from "@rn/react-native-mqtt-iot/app/components/ChatContainer";

export const ChatContainer = styled(BaseChatContainer)`
  flex: 1;
`;
