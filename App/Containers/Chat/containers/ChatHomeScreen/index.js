/*
 * @flow
 * @Author: Chinmaya
 */

import React, { Component } from "react";
import { observer, inject } from "mobx-react/native";
import chatConfig from "@rn/react-native-mqtt-iot/app/stores/ChatConfig";
import ChatMessageContainer from "../../components/ChatMessageContainer";
import KeyboardHeightListener from "../../components/KeyboardHeightListener";
import Metrics from "../../themes/Metrics";

import ChatHeader from "../../components/ChatHeader";
import ChatConversation from "../../components/ChatConversation";
import ChatFooter from "../../components/ChatFooter";
import { chatFlow } from "../../stores/ChatStore";

import { ScreenContainer, ChatContainer } from "./styledComponents";
import chatStore from "../../stores/ChatStore";
import type { ChatStore } from "../../stores/ChatStore"; // eslint-disable-line

type Props = {};

import { StyleSheet, Text, View } from "react-native";

const styles = StyleSheet.create({
  container: {}
});

@observer
export class ChatHomeScreen extends Component<Props> {
  onRetry = () => {
    // TODO: Implement retry
    chatFlow(this.props.chatStore);
  };

  render() {
    const { ...other } = this.props;

    return (
      <KeyboardHeightListener
        topOffset={Metrics.customNavBarHeight + Metrics.statusBarHeight}
      >
        <ChatContainer
          chatRoom={chatStore.selectedChatRoom}
          renderHeader={ChatHeader}
          renderMessages={ChatConversation}
          renderMessage={ChatMessageContainer}
          renderFooter={ChatFooter}
          {...other}
        />
      </KeyboardHeightListener>
    );
  }
}

export default ChatHomeScreen;
