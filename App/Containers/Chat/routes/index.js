/*
 * @flow
 * Author: Sunny
 */

import React from 'react'
import { Scene } from 'react-native-router-flux'
import I18n from 'react-native-i18n'

import ChatHomeScreen from 'Chat/containers/ChatHomeScreen'
import NavigationConstants from 'Chat/constants/NavigationConstants'

const scenes = [
  <Scene
    key={NavigationConstants.CHAT_HOME_SCREEN}
    component={ChatHomeScreen}
    title={I18n.t('chatModule.chat')}
  />
  /* <Scene
    key={NavigationConstants.VIDEOS}
    component={VideosList}
    title="Videos"
    hideNavBar
  />,
  <Scene
    key={NavigationConstants.COURSES}
    component={Courses}
    coursesStore={coursesStore}
    hideNavBar
    title="Courses"
  /> */
]

export default scenes
