/*
 * @flow
 * Author: Chinmaya
 */

import "../utils/config";
/*
  chatConfig should be configured before import ChatStore from '@rn/react-native-mqtt-iot'
 */
/* eslint-disable import/first, import/order */
import { action, observable, reaction, computed } from "mobx";
import { isNetworkConnected } from "@rn/react-native-mqtt-iot/packages/app-api";
import compareDesc from "date-fns/compare_desc";
import { Actions } from "react-native-router-flux";
import Mqtt from "@rn/react-native-mqtt-iot"; // eslint-disable-line
import {
  ROOM_TYPE_USER,
  ROOM_TYPE_GROUP,
  CONNECTED
} from "@rn/react-native-mqtt-iot/app/constants/ChatConstants";

import { ChatStore as BaseChatStore } from "@rn/react-native-mqtt-iot/app/stores/ChatStore";
import ConnectionParameters from "@rn/react-native-mqtt-iot/app/stores/models/ConnectionParameters";
// import CommonMethods from 'Common/utils/CommonMethods'

import NavigationConstants from "../constants/NavigationConstants";

import MemberChatRoom from "./models/MemberChatRoom";

import type { ObservableMap } from "mobx"; // eslint-disable-line
import type { SocketConnectionParams } from "@rn/react-native-mqtt-iot/app/stores/types/index"; // eslint-disable-line
import type { Source } from "@rn/react-native-mqtt-iot/app/stores/types/MqttMessage"; // eslint-disable-line
import type { ConnectionStatus } from "@rn/react-native-mqtt-iot/app/constants/ChatConstants"; // eslint-disable-line
import type BaseChatMember from "@rn/react-native-mqtt-iot/app/stores/models/ChatMember"; // eslint-disable-line
import type BaseChatRoom from "./models/BaseChatRoom";
import type { ErrorType } from "Common/stores/types";
import type {
  APIGetAllChatRooms,
  APIGetChatRoomMessages // eslint-disable-line
} from "./types"; // eslint-disable-line

function sortRooms(roomA: BaseChatRoom, roomB: BaseChatRoom) {
  return compareDesc(roomA.lastUpdatedTimeStamp, roomB.lastUpdatedTimeStamp);
}

class ChatStore extends BaseChatStore {
  @observable roomsListStatus: 0;
  @observable memberRooms: ObservableMap<MemberChatRoom> = new Map();

  apiError: ErrorType;

  @action.bound
  setRoomsListStatus(status: APIStatus) {
    this.roomsListStatus = status;
  }

  onError(error) {
    console.log("Chat onError ", error.message);
  }

  @computed
  get latestMemberRooms() {
    return Array.from(this.memberRooms.values()).sort(sortRooms);
  }

  async onMqttConnectionConnected() {
    const request = {
      offset: 0,
      limit: 100,
      room_type: ROOM_TYPE_USER
    };
    try {
      this.setRoomsListStatus(100);
      await Promise.all([this.getAllChatRooms(request)]);
      this.setRoomsListStatus(200);
    } catch (e) {
      console.log("Error ", e);
      this.apiError = e;
      this.setRoomsListStatus(400);
    }
  }

  setInitialSelectedChatRoom() {
    if (
      Actions.currentParams.chatRoomId !== undefined &&
      this.rooms.has(Actions.currentParams.chatRoomId)
    ) {
      this.setSelectedChatRoom(
        this.rooms.get(Actions.currentParams.chatRoomId)
      );
    } else if (this.rooms.size > 0 && this.selectedChatRoom === undefined) {
      this.setSelectedChatRoom(Array.from(this.rooms.values())[0]);
    } else if (this.selectedChatRoom) this.selectedChatRoom.setIsFocused(true);
  }

  @action.bound
  setAPIError(error: ErrorType) {
    this.apiError = error;
  }

  // @action.bound
  // updateSessionResponse(response: APISession) {
  //   this.connectionParameters = new ConnectionParameters()
  //   this.connectionParameters.source = response.source
  //   this.connectionParameters.subscribeTopic = response.read_topic
  //   this.connectionParameters.publishTopic = response.write_topic
  //   this.connectionParameters.clientId = response.session_id
  //   // this.connectionParameters.subscribeTopic = 'topic/read/session1'
  //   // this.connectionParameters.publishTopic = 'topic/read/session1'
  //   // this.connectionParameters.clientId = 'sesion1'
  //   this.connectionParameters.senderId = response.user_id
  //   console.log('udpate session response', this.connectionParameters)
  //   this.connect()
  // }
  // updateRoomsFromApi(response: APIGetAllChatRooms) {
  //   console.log("update rooms ****");
  //   this.setTotalRooms(response.total);
  //   if (Array.isArray(response.rooms)) {
  //     response.rooms.forEach(room => {
  //       const roomType = room.room_type;
  //       let chatRoom;
  //       switch (roomType) {
  //         case ROOM_TYPE_USER:
  //           try {
  //             const { metadata } = room;
  //             const metadataObject = metadata ? JSON.parse(metadata) : {};
  //             chatRoom = new MemberChatRoom(room, this);
  //             chatRoom.franchiseLocation = metadataObject.franchise_location;
  //             chatRoom.designation = metadataObject.designation;
  //             this.memberRooms.set(chatRoom.id, chatRoom);
  //           } catch (e) {
  //             console.log(e.message);
  //           } //eslint-disable-line
  //           break;
  //         case ROOM_TYPE_GROUP:
  //           try {
  //             if (room.chat_room_id.includes("ISSUE")) {
  //               chatRoom = new IssueChatRoom(room, this);
  //               chatRoom.issueId = room.chat_room_id.substr(6);
  //               chatRoom.issueTitle = room.name;
  //               this.issueRooms.set(chatRoom.id, chatRoom);
  //             } else {
  //               chatRoom = new GroupChatRoom(room, this);
  //               this.groupRooms.set(chatRoom.id, chatRoom);
  //             }
  //           } catch (e) {
  //             console.error("Unable to create chat room", e.message);
  //           } // eslint-disable-line
  //           break;
  //         default:
  //       }
  //       if (chatRoom !== undefined) this.rooms.set(chatRoom.id, chatRoom);
  //     });
  //   }
  // }
}

const chatStore = new ChatStore();
//

export async function chatFlow(store: ChatStore = chatStore) {
  if (store !== undefined) {
    store.setRoomsListStatus(100);
    const chatSenderId = await Mqtt.getCurrentSenderId();
    if (!chatSenderId) {
      try {
        store.createNewChatSession();
      } catch (e) {
        store.setAPIError(e);
      }
    } else {
      const status = await Mqtt.getConnectionStatus();
      if (status === CONNECTED) {
        store
          .updateConnectionParametersFromLocal()
          .then(store.onMqttConnectionConnected.bind(store));
      } else {
        try {
          const isConnected = await isNetworkConnected();
          if (isConnected === false) {
            throw Error(
              JSON.stringify({
                res_status: "",
                response: "",
                http_status_code: 503
              })
            );
          }
        } catch (e) {
          store.setRoomsListStatus(400);
          store.setAPIError(e);
        }
      }
    }
  }
}

// eslint-disable-next-line
// const dispose = reaction(
//   () => Actions.currentScene,
//   currentScene => {
//     let store: ChatStore;
//     switch (currentScene) {
//       case NavigationConstants.CHAT_HOME_SCREEN:
//         store = Actions.currentParams.chatStore;
//         chatFlow(store);

//         // store.getAllChatRooms()

//         break;
//       default:
//         /**
//          * When navigating to another screen from chat screen we are removing focus of selected chat
//          * @param  {[type]} chatStore [description]
//          * @return {[type]}           [description]
//          */
//         if (chatStore && chatStore.selectedChatRoom) {
//           chatStore.selectedChatRoom.setIsFocused(false);
//         }
//     }
//   }
// );

export { chatStore as default, ChatStore };
