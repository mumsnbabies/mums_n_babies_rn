/*
 * @flow
 * @Author: Chinmaya
 */

import type {
  ClientId,
  UserId,
  TimeStamp,
  Source
} from "@rn/react-native-mqtt-iot/app/stores/types/MqttMessage";

import type { APIChatRoom as BaseAPIChatRoom } from "@rn/react-native-mqtt-iot/app/stores/types/index";
import type { RoomType } from "@rn/react-native-mqtt-iot/app/constants/ChatConstants";

import type { APIChatMessage as BaseAPIChatMessage } from "@rn/react-native-mqtt-iot/app/stores/types/ChatMessage";

export type Pagination = {
  offset: number,
  limit: number
};

export type APIGetAllChatRoomsRequest = Pagination & {
  room_type: RoomType
};

type DeviceId = string;
type DeviceType = string;

export type APIDeviceInfo = {
  device_type: DeviceType,
  device_id: DeviceId
};

export type APISession = {
  start_timestamp: TimeStamp,
  read_topic: string,
  user_id: UserId,
  app_id: Source,
  session_id: ClientId,
  device_type: DeviceType,
  end_timestamp: TimeStamp,
  write_topic: string,
  device_id: DeviceId
};

export type APIChatRoom = BaseAPIChatRoom & {
  metadata: string
};

export type APIGetAllChatRooms = {
  total: number,
  rooms: Array<APIChatRoom>
};

export type APIGetChatRoomMessagesRequest = {
  chat_room_id: ChatRoomId,
  limit: number,
  offset_time_stamp: string
};

export type APIChatMessage = BaseAPIChatMessage;

export type APIGetChatRoomMessages = Array<APIChatMessage>;

export interface API {
  getAllChatRooms(
    request: APIGetAllChatRoomsRequest
  ): Promise<APIGetAllChatRooms>;

  createNewChatSession(request: APIDeviceInfo): Promise<APISession>;

  getRoomMessages(
    request: APIGetChatRoomMessagesRequest
  ): Promise<APIGetChatRoomMessages>;
}
