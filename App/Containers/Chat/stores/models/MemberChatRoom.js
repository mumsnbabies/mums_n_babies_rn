/*
 * @flow
 * Author: Chinmaya
 */

import { observable, action, computed } from 'mobx' // eslint-disable-line

import BaseChatRoom from './BaseChatRoom'

class MemberChatRoom extends BaseChatRoom {
  @observable designation: string
  @observable franchiseLocation: string
  @observable memberType: string = 'MEMBER'

  get chatRoomType() {
    return 'MEMBER'
  }
}

export default MemberChatRoom
