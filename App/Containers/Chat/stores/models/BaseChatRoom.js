/*
 * @flow
 * @Author: Chinmaya
 */
import ChatRoom from "@rn/react-native-mqtt-iot/app/stores/models/ChatRoom";

export default class BaseChatRoom extends ChatRoom {}
