/*
 * @flow
 * @Author: Chinmaya
 */
import BaseChatMember from "@rn/react-native-mqtt-iot/app/stores/models/ChatMember";

import type { APIChatMember } from "@rn/react-native-mqtt-iot/app/stores/types";

class ChatMember extends BaseChatMember {
  metadata: string;

  constructor(member: APIChatMember) {
    super(member);
    try {
      this.metadata = member.metadata;
    } catch (e) {} // eslint-disable-line
  }
}

export default ChatMember;
