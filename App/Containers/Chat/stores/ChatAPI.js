/*
 * @flow
 * @Author: Chinmaya
 */

import networkCallWithApisauce from 'Common/utils/AppAPI'

import Urls from '../utils/Urls'
import type {
  APISession,
  APIDeviceInfo,
  APIGetAllChatRooms,
  APIGetChatRoomMessages,
  APIGetAllChatRoomsRequest,
  APIGetChatRoomMessagesRequest,
  API
} from './types'

export default class ChatAPI implements API {
  createNewChatSession(request: APIDeviceInfo): Promise<APIGetAllChatRooms> {
    return networkCallWithApisauce(Urls.createNewSession, request)
  }

  getAllChatRooms(request: APIGetAllChatRoomsRequest): Promise<APISession> {
    return networkCallWithApisauce(Urls.getAllRooms, request)
  }

  getRoomMessages(
    request: APIGetChatRoomMessagesRequest
  ): Promise<APIGetChatRoomMessages> {
    return networkCallWithApisauce(Urls.getRoomMessages, request)
  }
}
