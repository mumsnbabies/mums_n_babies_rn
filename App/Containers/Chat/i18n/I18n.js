// @flow
/* eslint-disable */
import I18n from 'react-native-i18n'

// Enable fallbacks if you want `en-US` and `en-GB` to fallback to `en`
I18n.fallbacks = true

// English language is the main language for fall back:
const en = require('./languages/english.json')

I18n.translations.en = {
  ...I18n.translations.en,
  ...en
}

const hi = require('./languages/hi.json')
I18n.translations.hi = {
  ...I18n.translations.hi,
  ...hi
}

const te = require('./languages/te.json')
I18n.translations.te = {
  ...I18n.translations.te,
  ...te
}

const languageCode = I18n.locale.substr(0, 2)

// All other translations for the app goes to the respective language file:
switch (languageCode) {
  case 'af': {
    const af = require('./languages/af.json')
    I18n.translations.af = {
      ...I18n.translations.af,
      af
    }
    break
  }
  case 'am': {
    const am = require('./languages/am.json')
    I18n.translations.am = {
      ...I18n.translations.am,
      ...am
    }
    break
  }
  case 'ar': {
    const ar = require('./languages/ar.json')
    I18n.translations.ar = {
      ...I18n.translations.ar,
      ...ar
    }
    break
  }
  case 'bg': {
    const bg = require('./languages/bg.json')
    I18n.translations.bg = {
      ...I18n.translations.bg,
      ...bg
    }
    break
  }
  case 'ca': {
    const ca = require('./languages/ca.json')
    I18n.translations.ca = {
      ...I18n.translations.ca,
      ...ca
    }
    break
  }
  case 'cs': {
    const cs = require('./languages/cs.json')
    I18n.translations.cs = {
      ...I18n.translations.cs,
      ...cs
    }
    break
  }
  case 'da': {
    const da = require('./languages/da.json')
    I18n.translations.da = {
      ...I18n.translations.da,
      ...da
    }
    break
  }
  case 'de': {
    const de = require('./languages/de.json')
    I18n.translations.de = {
      ...I18n.translations.de,
      ...de
    }
    break
  }
  case 'el': {
    const el = require('./languages/el.json')
    I18n.translations.el = {
      ...I18n.translations.el,
      ...el
    }
    break
  }
  case 'es': {
    const es = require('./languages/es.json')
    I18n.translations.es = {
      ...I18n.translations.es,
      ...es
    }
    break
  }
  case 'et': {
    const et = require('./languages/et.json')
    I18n.translations.et = {
      ...I18n.translations.et,
      ...et
    }
    break
  }
  case 'fi': {
    const addCode = I18n.locale.substr(0, 3)
    if (addCode === 'fil') {
      const fil = require('./languages/fil.json')
      I18n.translations.fil = {
        ...I18n.translations.fil,
        ...fil
      }
    } else {
      const fi = require('./languages/fi.json')
      I18n.translations.fi = {
        ...I18n.translations.fi,
        ...fi
      }
    }
    break
  }
  case 'fr': {
    const fr = require('./languages/fr.json')
    I18n.translations.fr = {
      ...I18n.translations.fr,
      ...fr
    }
    break
  }
  case 'he': {
    const he = require('./languages/he.json')
    I18n.translations.he = {
      ...I18n.translations.he,
      ...he
    }
    break
  }
  case 'hi': {
    const hi = require('./languages/hi.json')
    I18n.translations.hi = {
      ...I18n.translations.hi,
      ...hi
    }
    break
  }
  case 'hr': {
    const hr = require('./languages/hr.json')
    I18n.translations.hr = {
      ...I18n.translations.hr,
      ...hr
    }
    break
  }
  case 'hu': {
    const hu = require('./languages/hu.json')
    I18n.translations.hu = {
      ...I18n.translations.hu,
      ...hu
    }
    break
  }
  case 'in': {
    const id = require('./languages/id.json')
    I18n.translations.in = {
      ...I18n.translations.in,
      ...id
    }
    break
  }
  case 'id': {
    const idInternlize = require('./languages/id.json')
    I18n.translations.id = {
      ...I18n.translations.id,
      ...idInternlize
    }
    break
  }
  case 'it': {
    const it = require('./languages/it.json')
    I18n.translations.it = {
      ...I18n.translations.it,
      ...it
    }
    break
  }
  case 'ja': {
    const ja = require('./languages/ja.json')
    I18n.translations.ja = {
      ...I18n.translations.ja,
      ...ja
    }
    break
  }
  case 'ko': {
    const ko = require('./languages/ko.json')
    I18n.translations.ko = {
      ...I18n.translations.ko,
      ...ko
    }
    break
  }
  case 'lt': {
    const lt = require('./languages/lt.json')
    I18n.translations.lt = {
      ...I18n.translations.lt,
      ...lt
    }
    break
  }
  case 'lv': {
    const lv = require('./languages/lv.json')
    I18n.translations.lv = {
      ...I18n.translations.lv,
      ...lv
    }
    break
  }
  case 'ms': {
    const ms = require('./languages/ms.json')
    I18n.translations.ms = {
      ...I18n.translations.ms,
      ...ms
    }
    break
  }
  case 'nb': {
    const nb = require('./languages/nb.json')
    I18n.translations.nb = {
      ...I18n.translations.nb,
      ...nb
    }
    break
  }
  case 'nl': {
    const nl = require('./languages/nl.json')
    I18n.translations.nl = {
      ...I18n.translations.nl,
      ...nl
    }
    break
  }
  case 'no': {
    const no = require('./languages/no.json')
    I18n.translations.no = {
      ...I18n.translations.no,
      ...no
    }
    break
  }
  case 'pl': {
    const pl = require('./languages/pl.json')
    I18n.translations.pl = {
      ...I18n.translations.pl,
      ...pl
    }
    break
  }
  case 'pt': {
    const pt = require('./languages/pt.json')
    I18n.translations.pt = {
      ...I18n.translations.pt,
      ...pt
    }
    break
  }
  case 'ro': {
    const ro = require('./languages/ro.json')
    I18n.translations.ro = {
      ...I18n.translations.ro,
      ...ro
    }
    break
  }
  case 'ru': {
    const ru = require('./languages/ru.json')
    I18n.translations.ru = {
      ...I18n.translations.ru,
      ...ru
    }
    break
  }
  case 'sl': {
    const sl = require('./languages/sl.json')
    I18n.translations.sl = {
      ...I18n.translations.sl,
      ...sl
    }
    break
  }
  case 'sk': {
    const sk = require('./languages/sk.json')
    I18n.translations.sk = {
      ...I18n.translations.sk,
      ...sk
    }
    break
  }
  case 'sr': {
    const sr = require('./languages/sr.json')
    I18n.translations.sr = {
      ...I18n.translations.sr,
      ...sr
    }
    break
  }
  case 'sv': {
    const sv = require('./languages/sv.json')
    I18n.translations.sv = {
      ...I18n.translations.sv,
      ...sv
    }
    break
  }
  case 'sw': {
    const sw = require('./languages/sw.json')
    I18n.translations.sw = {
      ...I18n.translations.sw,
      ...sw
    }
    break
  }
  case 'te': {
    const te = require('./languages/te.json')
    I18n.translations.te = {
      ...I18n.translations.te,
      ...te
    }
    break
  }
  case 'th': {
    const th = require('./languages/th.json')
    I18n.translations.th = {
      ...I18n.translations.th,
      ...th
    }
    break
  }
  case 'tr': {
    const tr = require('./languages/tr.json')
    I18n.translations.tr = {
      ...I18n.translations.tr,
      ...tr
    }
    break
  }
  case 'uk': {
    const uk = require('./languages/uk.json')
    I18n.translations.uk = {
      ...I18n.translations.uk,
      ...uk
    }
    break
  }
  case 'vi': {
    const vi = require('./languages/vi.json')
    I18n.translations.vi = {
      ...I18n.translations.vi,
      ...vi
    }
    break
  }
  case 'zh': {
    const zh = require('./languages/zh.json')
    I18n.translations.zh = {
      ...I18n.translations.zh,
      ...zh
    }
    break
  }
  case 'zu': {
    const zu = require('./languages/zu.json')
    I18n.translations.zu = {
      ...I18n.translations.zu,
      ...zu
    }
    break
  }
}
