import { Actions as NavigationActions } from "react-native-router-flux";

import NavigationConstants from "../constants/NavigationConstants";

export function goToChatScreen(props) {
  NavigationActions[NavigationConstants.CHAT_HOME_SCREEN](props);
}
