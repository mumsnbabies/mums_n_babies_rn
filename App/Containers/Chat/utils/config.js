import { Region } from "@rn/react-native-mqtt-iot";
import chatConfig from "@rn/react-native-mqtt-iot/app/stores/ChatConfig";
import { getAuthorizationHeaders } from "@rn/react-native-mqtt-iot/packages/app-api";

import AuthStore from "../../../stores/AuthStore";

import ChatMember from "../stores/models/ChatMember";

const COGNITO_POOL_ID = "ap-southeast-1:60e90e9b-cbba-4a08-9438-06206ba39ddd";
const CHAT_CLIENT_ENDPOINT = "atnpc1vra79m2.iot.ap-southeast-1.amazonaws.com";
const BASE_CHAT_URL =
  "https://mnb-backend-prod.mums-n-babies.com/api/ib_chat_wrapper/";

async function getToken() {
  let token = await getAccessToken();
  return token;
}

function getHeaders() {
  let accessToken = AuthStore.accessToken;
  return getAuthorizationHeaders(accessToken, "mnb_app");
}

function updateChatConfig() {
  chatConfig.setRegion(Region.APSoutheast1);
  chatConfig.setCognitoPoolId(COGNITO_POOL_ID);
  chatConfig.setBaseURL(BASE_CHAT_URL);
  chatConfig.setClientEndPoint(CHAT_CLIENT_ENDPOINT);
  chatConfig.setGetHeaders(getHeaders);
  chatConfig.setChatMember(ChatMember);
  chatConfig.setSource("7f3873f6-7b6b-4fd7-ab4b-34580972b4f4");
  chatConfig.setClientAndroidService("co.ibhubs.ibchatrn.SampleService");
}

updateChatConfig();
