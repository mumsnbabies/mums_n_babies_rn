const BASE_CHAT_URL =
  "https://mnb-backend-prod.mums-n-babies.com/api/ib_chat_wrapper/";

const endpoints = {
  createNewSession: `${BASE_CHAT_URL}session/new/v1/`,
  getAllRooms: `${BASE_CHAT_URL}rooms/v1/`,
  getRoomMessages: `${BASE_CHAT_URL}room/messages/`
};

export default endpoints;
