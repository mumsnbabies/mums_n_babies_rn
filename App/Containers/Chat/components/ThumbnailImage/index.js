/**
 * @flow
 */

import React, { Component } from "react";
import { Image, Text } from "react-native";
import { observer } from "mobx-react/native";
import Metrics from "../../themes/Metrics";
import { RoundedImage, RoundedImagePlaceholder } from "./styledComponents";

type Props = {
  thumbnailUrl?: string,
  name?: string,
  size?: number
};

@observer
export class ThumbnailImage extends Component<Props> {
  static defaultProps: Props = {
    thumbnailUrl: "",
    name: "Anonymous",
    size: Metrics.chatMediumThumbnailSize
  };

  get thumbnailContent(): string {
    const { name } = this.props;
    let firstLetter;
    try {
      firstLetter = name.charAt(0).toUpperCase();
    } catch (err) {
      firstLetter = "#";
    }
    if (firstLetter === "") {
      firstLetter = "#";
    }
    return firstLetter;
  }

  props: Props;

  thumbnailThemes = [
    { backgroundColor: "#004acd", color: "#13274b" },
    { backgroundColor: "#ef5350", color: "white" },
    { backgroundColor: "#00b8d4", color: "#13274b" },
    { backgroundColor: "#ffab00", color: "#13274b" },
    { backgroundColor: "#7e57c2", color: "white" },
    { backgroundColor: "#3bad6a", color: "#13274b" },
    { backgroundColor: "#795548", color: "white" }
  ];

  nameHashCode = () => {
    const { name } = this.props;
    /* eslint-disable */
    return Math.abs(
      name
        .split("")
        .reduce(
          (prevHash, currVal) =>
            (prevHash << 5) - prevHash + currVal.charCodeAt(0),
          0
        )
    );
    /* eslint-enable  */
  };

  themeStyle = () => {
    const themeIndex = this.nameHashCode() % this.thumbnailThemes.length;
    return this.thumbnailThemes[themeIndex];
  };

  render() {
    const { thumbnailUrl, size, ...imageProps } = this.props;
    if (thumbnailUrl) {
      return (
        <RoundedImage
          size={size}
          source={{ uri: thumbnailUrl }}
          {...imageProps}
        />
      );
    }
    return (
      <RoundedImagePlaceholder {...this.themeStyle()} size={size}>
        <Text>{this.thumbnailContent}</Text>
      </RoundedImagePlaceholder>
    );
  }
}

type ThumbnailImageProps = $PropertyType<Image, "props">;

export const LargeThumbnailImage = observer((props: ThumbnailImageProps) => (
  <ThumbnailImage size={Metrics.chatLargeThumbnailSize} {...props} />
));

export const MediumThumbnailImage = observer((props: ThumbnailImageProps) => (
  <ThumbnailImage size={Metrics.chatMediumThumbnailSize} {...props} />
));

export const SmallThumbnailImage = observer((props: ThumbnailImageProps) => (
  <ThumbnailImage size={Metrics.chatSmallThumbnailSize} {...props} />
));

export default ThumbnailImage;
