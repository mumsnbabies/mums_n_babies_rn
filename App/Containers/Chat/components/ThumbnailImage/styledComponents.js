/**
 * @flow
 */

import styled from 'styled-components/native'

export const RoundedImagePlaceholder = styled.View`
  width: ${(props) => props.size};
  height: ${(props) => props.size};
  border-radius: ${(props) => props.size / 2};
  align-items: center;
  justify-content: center;
  background-color: ${(props) => props.backgroundColor};
`

export const RoundedImage = styled.Image`
  width: ${(props) => props.size};
  height: ${(props) => props.size};
  border-radius: ${(props) => props.size / 2};
  align-items: center;
  justify-content: center;
`
