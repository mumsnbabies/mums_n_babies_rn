/*
 * @flow
 * @Author: Chinmaya
 */

import * as React from "react";
import { observer } from "mobx-react/native";
import styled from "styled-components/native";

import type { StyleObj } from "react-native/Libraries/StyleSheet/StyleSheetTypes"; // eslint-disable-line

type Props = {
  style?: StyleObj,
  children?: React.Node
};

// eslint-disable-next-line
const ChatHeaderOuterContainer = styled.View`
  background-color: white;
  margin-bottom: 3px;
  shadow-offset: 0px -1px;
  shadow-radius: 1px;
  shadow-color: black;

  shadow-opacity: 1;
`;

const ChatHeaderInnerContainer = styled.View`
  shadow-color: white;
  shadow-offset: 0px 1px;
  shadow-radius: 2px;
  flex-direction: row;
  shadow-opacity: 1;
  padding-bottom: 15;
  margin-bottom: 3;
  padding-top: 15;
  padding-left: 20;
  padding-right: 20;
  align-items: center;
  justify-content: flex-start;
  background-color: white;
`;

/* <ChatHeaderOuterContainer style={props.style}>
</ChatHeaderOuterContainer> */

const ChatHeaderCard = observer((props: Props) => (
  <ChatHeaderInnerContainer>{props.children}</ChatHeaderInnerContainer>
));

export default ChatHeaderCard;
