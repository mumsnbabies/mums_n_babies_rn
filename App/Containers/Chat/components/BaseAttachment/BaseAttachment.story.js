/*
 * @flow
 * @Author: Chinmaya
 */

import * as React from 'react'
import { storiesOf } from '@storybook/react-native'
import { View } from 'react-native'
import Headphones from 'Chat/themes/icons/Headphones/component'

import BaseAttachment from './index'

storiesOf('BaseAttachment', module).add('default', () => (
  <BaseAttachment
    renderIcon={() => <Headphones color="#FFF" width={32} height={32} />}
    renderAttachmentDetails={View}
  />
))
