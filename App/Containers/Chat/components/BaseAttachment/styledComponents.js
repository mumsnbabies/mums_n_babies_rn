/*
 * @flow
 * @Author: Chinmaya
 */

import styled from 'styled-components/native'
import Colors from 'themes/Colors' // eslint-disable-line

export const BaseAttachmentView = styled.View`
  flex-direction: row;
  align-items: stretch;
  border-radius: 4;
  border-width: 1;
  border-color: ${Colors.argh};
  width: 40%;
  overflow: hidden;
  background-color: ${Colors.white};
`

export const IconWithBackgroundView = styled.View`
  background-color: ${Colors.iris};
  justify-content: center;
  align-items: center;
  width: 60;
  height: 60;
`
