/*
 * @flow
 * @Author: Chinmaya
 */

import * as React from 'react'
import { observer } from 'mobx-react/native'

import { BaseAttachmentView, IconWithBackgroundView } from './styledComponents'

type Props = {
  renderIcon: React.Node,
  renderAttachmentDetails: React.Node,
  attachmentDetailsProps?: Object
}

const IconWithBackground = observer(({ icon: Icon }) => (
  <IconWithBackgroundView>
    <Icon />
  </IconWithBackgroundView>
))

@observer
export class BaseAttachment extends React.Component<Props> {
  render() {
    const {
      renderIcon: Icon,
      renderAttachmentDetails: AttachmentDetails,
      attachmentDetailsProps
    } = this.props
    return (
      <BaseAttachmentView>
        <IconWithBackground icon={Icon} />
        <AttachmentDetails {...attachmentDetailsProps} />
      </BaseAttachmentView>
    )
  }
}

export default BaseAttachment
