/**
 * @flow
 */

import styled from 'styled-components/native'
import Colors from '../../../Common/themes/Colors' // eslint-disable-line
import Metrics from '../../themes/Metrics'
import { TextStyle } from '../../../Common/styledComponents/Text/StyledTexts'

export const BodyContainer = styled.View`
  flex-direction: column;
  align-items: flex-start;
  background-color: ${Colors.toby};
  flex: 1;
`

export const RoomContainer = styled.View``

export const HeaderContainer = styled.View`
  margin-top: 20;
`

export const HeaderText = TextStyle.extend`
  margin-left: ${Metrics.sideBarLeftMargin};
  margin-bottom: 5;
`
export const IconContainer = styled.View`
  justify-content: center;
  align-items: center;
  width: 18;
  height: 18;
`

export const MoreTouchableContainer = styled.TouchableWithoutFeedback`
  padding-top: 7;
  padding-bottom: 7;
`

export const MoreContainer = styled.View`
  flex-direction: row;
  margin-left: ${Metrics.sideBarLeftMargin};
`
