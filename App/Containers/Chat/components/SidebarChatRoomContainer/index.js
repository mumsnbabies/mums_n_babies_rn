/*
 * @flow
 */

import * as React from "react";
import { observable, action, computed } from "mobx";
import I18n from "react-native-i18n";
import { observer } from "mobx-react/native";
import { ChatRoomItem } from "../ChatRoomItem";
import { Caption13White } from "../../../Common/styledComponents/Text/StyledTexts";
import ChevronDown from "../../../Common/themes/icons/ChevronDown/component";
import GroupChatRoom from "../../stores/models/GroupChatRoom";
import MemberChatRoom from "../../stores/models/MemberChatRoom";
import IssueChatRoom from "../../stores/models/IssueChatRoom";

import {
  RoomContainer,
  BodyContainer,
  HeaderText,
  MoreContainer,
  HeaderContainer,
  IconContainer,
  MoreTouchableContainer
} from "./styledComponents";

import type ChatRoom from "@rn/react-native-mqtt-iot/app/stores/models/ChatRoom"; // eslint-disable-line
type ChatRoomType = GroupChatRoom | MemberChatRoom | IssueChatRoom;

type Props = {
  chatRooms: Array<ChatRoomType>,
  title: string,
  renderItem: React$Component<{ chatRoom: ChatRoomType }>,
  chatRoomItemProps: { handleOnPress: (chatRoom: ?ChatRoomType) => mixed },
  paginationOffset: number
};

type MoreProps = {
  onMorePress: () => mixed
};
export const MoreComponent = ({ onMorePress }: MoreProps) => (
  <MoreTouchableContainer onPress={onMorePress}>
    <MoreContainer>
      <Caption13White>{I18n.t("chatModule.more")}</Caption13White>
      <IconContainer>
        <ChevronDown width="8.1" height="5" />
      </IconContainer>
    </MoreContainer>
  </MoreTouchableContainer>
);

@observer
export class SidebarChatRoomContainer extends React.Component<Props> {
  static defaultProps = {
    paginationOffset: 3
  };

  @observable currentPage: number;

  constructor(props: Props) {
    super(props);
    this.currentPage = 1;
  }

  @computed
  get isMoreVisible(): boolean {
    return (
      this.props.chatRooms.length >
      this.currentPage * this.props.paginationOffset
    );
  }

  @computed
  get visibleChatRooms(): Array<ChatRoomType> {
    return this.props.chatRooms.slice(
      0,
      this.props.paginationOffset * this.currentPage
    );
  }

  @action.bound
  onMorePress() {
    this.currentPage += 1;
  }
  render() {
    const { title, renderItem, chatRoomItemProps } = this.props;
    if (this.visibleChatRooms.length === 0) {
      return null;
    }
    return (
      <BodyContainer>
        <HeaderContainer>
          <HeaderText numberOfLines={1}>{title}</HeaderText>
        </HeaderContainer>
        <RoomContainer>
          {this.visibleChatRooms.map(chatRoom => (
            <ChatRoomItem
              key={chatRoom.id}
              {...chatRoomItemProps}
              chatRoom={chatRoom}
              renderItem={renderItem}
            />
          ))}
          {this.isMoreVisible ? (
            <MoreComponent onMorePress={this.onMorePress} />
          ) : null}
        </RoomContainer>
      </BodyContainer>
    );
  }
}

export default SidebarChatRoomContainer;
