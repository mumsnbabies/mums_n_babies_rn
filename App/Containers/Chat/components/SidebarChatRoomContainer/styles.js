/**
 * @flow
 */
import StyleSheet from '@rn/commons/app/components/GenericComponents/GenericStyleSheet'
import Colors from '../../../Common/themes/Colors'

export const styles = StyleSheet.create({
  flatListStyle: {
    backgroundColor: Colors.toby
  }
})
