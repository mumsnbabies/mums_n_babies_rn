/**
 * @flow
 * @author: Anesh Parvatha
 */

import React from "react";
import { observer } from "mobx-react/native";
import ChatMember from "@rn/react-native-mqtt-iot/app/stores/models/ChatMember";

import { Text } from "react-native";

type Props = {
  sender: ChatMember
};

const ChatMessageSenderName = (props: Props) => {
  const { sender } = props;
  return <Text>{sender ? sender.name : ""}</Text>;
};

export default observer(ChatMessageSenderName);
