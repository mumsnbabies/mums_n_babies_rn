/*
 * @flow
 * @Author: Chinmaya
 */

import * as React from "react";
import { storiesOf } from "@storybook/react-native";

// import ChatCircularProgress, { LargeChatCircularProgress } from './index'

storiesOf("ChatCircularProgress", module)
  .add("default", () => (
    <ChatCircularProgress progress={0.2} onPress={() => {}} />
  ))
  .add("large", () => (
    <LargeChatCircularProgress progress={0.5} onPress={() => {}} />
  ));
