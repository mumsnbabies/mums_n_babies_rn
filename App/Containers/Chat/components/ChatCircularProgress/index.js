/*
 * @flow
 * @Author: Chinmaya
 */

import * as React from "react";
import { TouchableWithoutFeedback } from "react-native";
import { observer } from "mobx-react/native";
// import CircularProgress from 'react-native-progress/Circle'
// import CloseIcon from "Common/themes/icons/Close/component";
// import Colors from "themes/Colors";

import type { StyleObj } from "react-native/Libraries/StyleSheet/StyleSheetTypes"; // eslint-disable-line

type Props = {
  progress: number,
  closeIconSize?: number,
  size?: number,
  progressColor?: string,
  progressWidth?: number,
  style?: StyleObj,
  onPress: () => mixed
};

@observer
export class ChatCircularProgress extends React.Component<Props> {
  static defaultProps = {
    closeIconSize: 9,
    size: 28,
    progressColor: "blue",
    progressWidth: 2
  };

  render() {
    const {
      progress,
      closeIconSize,
      size,
      progressColor,
      progressWidth,
      style,
      onPress
    } = this.props;
    return (
      <TouchableWithoutFeedback style={style} onPress={onPress}>
        <CircularProgress
          progress={progress}
          animated
          size={size}
          indeterminate={false}
          thickness={progressWidth}
          borderWidth={0}
          showsText
          color={progressColor}
          formatText={() => (
            <CloseIcon height={closeIconSize} width={closeIconSize} />
          )}
        />
      </TouchableWithoutFeedback>
    );
  }
}

export const LargeChatCircularProgress = observer((props: Props) => (
  <ChatCircularProgress
    size={48}
    progressWidth={3}
    closeIconSize={17}
    {...props}
  />
));

export default ChatCircularProgress;
