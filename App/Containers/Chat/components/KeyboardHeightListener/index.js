import * as React from "react";
import { observable, action } from "mobx";
import { observer } from "mobx-react/native";
import { View, Keyboard, Dimensions, Platform } from "react-native";

type Props = {
  topOffset: number,
  children: React.Node
};

const { height: deviceHeight } = Dimensions.get("window");

@observer
export class KeyboardHeightListener extends React.Component<Props> {
  @observable visibleHeight: number;

  constructor(props) {
    super(props);
    this.setHeight(deviceHeight - props.topOffset);
  }

  componentWillMount() {
    Keyboard.addListener("keyboardWillShow", this.keyboardWillShow.bind(this));
    Keyboard.addListener("keyboardWillHide", this.keyboardWillHide.bind(this));
    if (Platform.OS === "android") {
      Keyboard.addListener("keyboardDidShow", this.keyboardWillShow.bind(this));
      Keyboard.addListener("keyboardDidHide", this.keyboardWillHide.bind(this));
    }
  }

  componentWillUnmount() {
    Keyboard.removeListener(
      "keyboardWillShow",
      this.keyboardWillShow.bind(this)
    );
    Keyboard.removeListener(
      "keyboardWillHide",
      this.keyboardWillHide.bind(this)
    );
    if (Platform.OS === "android") {
      Keyboard.removeListener(
        "keyboardDidShow",
        this.keyboardWillShow.bind(this)
      );
      Keyboard.removeListener(
        "keyboardDidHide",
        this.keyboardWillHide.bind(this)
      );
    }
  }
  @action.bound
  setHeight(height: number) {
    this.visibleHeight = height;
  }

  keyboardWillShow(e) {
    const newSize =
      deviceHeight - this.props.topOffset - e.endCoordinates.height;
    this.setHeight(newSize);
  }

  keyboardWillHide() {
    this.setHeight(deviceHeight - this.props.topOffset);
  }

  render() {
    return (
      <View style={{ height: this.visibleHeight }}>{this.props.children}</View>
    );
  }
}

export default KeyboardHeightListener;
