/**
 * @flow
 * @author: Anesh Parvatha
 */

import * as React from "react";
import { storiesOf } from "@storybook/react-native";
import ChatMessageModel from "@rn/react-native-mqtt-iot/app/stores/models/ChatMessage";
import ChatRoom from "@rn/react-native-mqtt-iot/app/stores/models/ChatRoom";

import ChatMessage from "./index";

const selfChatMessage = {
  status: "READ",
  event_type: "READ",
  multimedia_urls: [],
  chat_room_id: "090a08-820a1c-deadbeef-0xcafe",
  msg_id: "0xdead-0xbeef-0xcafe-1",
  sender_id: 1,
  server_log_time_stamp: new Date("2017.11.11").getTime() / 1000,
  content:
    "Differentiate Yourself And Attract More Attention Sales And Profits. Differentiate Yourself And Attract More Attention Sales And Profits. Differentiate Yourself And Attract More Attention Sales And Profits. ",
  recipient_id: 2
};

const otherChatMessage = {
  status: "READ",
  event_type: "READ",
  multimedia_urls: [],
  chat_room_id: "090a08-820a1c-deadbeef-0xcafe",
  msg_id: "0xdead-0xbeef-0xcafe-2",
  sender_id: 2,
  server_log_time_stamp: new Date("2017.11.11").getTime() / 1000,
  content:
    "Differentiate Yourself And Attract More Attention Sales And Profits. Differentiate Yourself",
  recipient_id: 1
};

const apiChatRoom = {
  chat_room_id: "79382452452",
  name: "Lokesh Dokara",
  room_type: "USER",
  members: [],
  removed_members: [],
  room_online_status: "ONLINE",
  last_msg: {},
  unread_msgs_count: 2,
  multimedia_urls: [],
  created_time_stamp: "29/05/17 12:00AM",
  last_updated_time_stamp: "29/05/17 12:00AM"
};

const chatRoom = new ChatRoom(apiChatRoom, { connectionParams: {} });
const selfChatMessageModel = new ChatMessageModel(selfChatMessage, chatRoom);
const otherChatMessageModel = new ChatMessageModel(otherChatMessage, chatRoom);

storiesOf("ChatMessage ", module)
  .add("self", () => <ChatMessage message={selfChatMessageModel} />)
  .add("other", () => <ChatMessage message={otherChatMessageModel} />);
