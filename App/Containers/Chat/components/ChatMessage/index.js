/**
 * @flow
 * @author: Anesh Parvatha
 */
import React, { Component } from "react";
import { observer } from "mobx-react";
import type ChatMessageModel from "@rn/react-native-mqtt-iot/app/stores/models/ChatMessage";
import { SELF } from "@rn/react-native-mqtt-iot/app/constants/ChatConstants";

// import ChatMessageSenderThumbnail from '../ChatMessageSenderThumbnail'
import ChatMessageBodyContainer from "../ChatMessageBodyContainer";

import { SelfChatMessage, OtherChatMessage } from "./styledComponents";

type Props = {
  message: ChatMessageModel
};

@observer
class ChatMessage extends Component<Props> {
  props: Props;
  render() {
    const { message } = this.props;
    if (message.type === SELF) {
      return (
        <SelfChatMessage>
          {/* <ChatMessageSenderThumbnail sender={message.sender} /> */}
          <ChatMessageBodyContainer message={message} />
        </SelfChatMessage>
      );
    }
    return (
      <OtherChatMessage>
        {/* <ChatMessageSenderThumbnail sender={message.sender} /> */}
        <ChatMessageBodyContainer message={message} />
      </OtherChatMessage>
    );
  }
}
export default ChatMessage;
