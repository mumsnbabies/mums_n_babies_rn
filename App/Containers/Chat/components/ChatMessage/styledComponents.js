/**
 * @flow
 * @author: Anesh Parvatha
 */

import styled from 'styled-components/native'

export const ChatMessageView = styled.View`
  flex-direction: row;

  margin-left: 17;
  margin-bottom: 20;
`

export const SelfChatMessage = ChatMessageView.extend`
  flex-direction: row-reverse;
`

export const OtherChatMessage = ChatMessageView.extend``

export const ChatLogMessage = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
`
