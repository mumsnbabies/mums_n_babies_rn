/**
 * @flow
 */

import styled from 'styled-components/native'
import Colors from '../../../Common/themes/Colors'
import Metrics from '../../themes/Metrics'
import {
  ButtonTextWhite,
  BodyWhite
} from '../../../Common/styledComponents/Text/StyledTexts'

export const BodyContainer = styled.View`
  width: ${Metrics.sideBarWidth};
  flex-direction: row;
  align-items: center;
  background-color: ${(props) =>
    (props.backgroundColor ? props.backgroundColor : Colors.white)};
  padding-left: ${Metrics.sideBarLeftMargin};
  padding-right: ${Metrics.sideBarRightMargin};
  padding-top: 10;
  padding-bottom: 10;
`

export const ChatRoomNameBoldText = ButtonTextWhite.extend`
  text-align: left;
  padding-right: 12;
  flex: 1;
`

export const ChatRoomNameText = BodyWhite.extend`
  padding-right: 12;
  flex: 1;
`
export const ChatRoomNameContainer = styled.View`
  padding-right: 12;
  align-items: flex-start;
  flex-direction: column;
  flex: 1;
`

export const ImageContainer = styled.View`
  margin-right: 20;
`
export const OnlineStatusView = styled.View`
  width: 11;
  height: 11;
  border-radius: 6.5;
  align-items: center;
  justify-content: center;
  background-color: ${(props) => props.backgroundColor};
  position: absolute;
  right: 0;
  bottom: 0;
`
