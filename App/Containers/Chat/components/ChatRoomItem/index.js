/**
 * @flow
 */

import React, { Component } from "react";
import { computed } from "mobx";
import { observer } from "mobx-react/native";
import type { OnlineStatus } from "@rn/react-native-mqtt-iot/app/constants/ChatConstants";
import Touchable from "@rn/commons/app/components/GenericComponents/Touchable";

import Colors from "../../../Common/themes/Colors";
import { SmallThumbnailImage } from "../ThumbnailImage";
import GroupChatRoom from "../../stores/models/GroupChatRoom";
import MemberChatRoom from "../../stores/models/MemberChatRoom";
import IssueChatRoom from "../../stores/models/IssueChatRoom";
import WarningBadge from "../../../Common/styledComponents/Badges/WarningBadge";
import {
  ButtonTextWhite,
  BodyWhite,
  Caption13White
} from "../../../Common/styledComponents/Text/StyledTexts";

import {
  BodyContainer,
  ImageContainer,
  ChatRoomNameBoldText,
  ChatRoomNameText,
  ChatRoomNameContainer,
  OnlineStatusView
} from "./styledComponents";

type ChatRoomType = GroupChatRoom | MemberChatRoom | IssueChatRoom;

type Props = {
  handleOnPress: (chatRoom: ?ChatRoomType) => mixed,
  chatRoom: ChatRoomType,
  renderItem: React$Component<{ chatRoom: ChatRoomType }>
};

type ChatRoomProps = {
  chatRoom: ChatRoomType
};
type OnlineStatusIconProps = {
  roomOnlineStatus: OnlineStatus
};

type ChatRoomItemProps = {
  chatRoom: ChatRoomType,
  handleOnPress: () => mixed,
  backgroundColor: string
};

export const MemberTextContainer = observer(({ chatRoom }: ChatRoomProps) => {
  const { isFocused } = chatRoom;
  // TODO: handle for relation ship manager
  const { designation, franchiseName, name } = chatRoom.friends[0];
  const details = `${designation}, ${franchiseName}`;
  // switch (memberType) {
  //   case 'CREW':
  //     details = `${designation}, ${franchiseLocation}`
  //     break
  //   case 'RELATIONSHIP_MANAGER':
  //     details = `${designation}`
  //     break
  //   default:
  //     details = `${designation}, ${franchiseLocation}`
  // }
  if (isFocused) {
    return (
      <ChatRoomNameContainer>
        <ButtonTextWhite numberOfLines={1}>{name}</ButtonTextWhite>
        <Caption13White numberOfLines={1}>{details}</Caption13White>
      </ChatRoomNameContainer>
    );
  }
  return (
    <ChatRoomNameContainer>
      <BodyWhite numberOfLines={1}>{name}</BodyWhite>
      <Caption13White numberOfLines={1}>{details}</Caption13White>
    </ChatRoomNameContainer>
  );
});

export const IssueTextContainer = observer(({ chatRoom }: ChatRoomProps) => {
  const { isFocused, issueId, issueTitle } = chatRoom;
  const title = `#${issueId} - ${issueTitle}`;
  if (isFocused) {
    return (
      <ChatRoomNameBoldText numberOfLines={1}>{title}</ChatRoomNameBoldText>
    );
  }
  return <ChatRoomNameText numberOfLines={1}>{title}</ChatRoomNameText>;
});

export const ChannelTextContainer = observer(({ chatRoom }: ChatRoomProps) => {
  const { isFocused, name } = chatRoom;
  if (isFocused) {
    return (
      <ChatRoomNameBoldText numberOfLines={1}>{name}</ChatRoomNameBoldText>
    );
  }
  return <ChatRoomNameText numberOfLines={1}>{name}</ChatRoomNameText>;
});

export const UnreadCountIcon = observer(({ unreadMessagesCount = 0 }) => {
  if (unreadMessagesCount > 0) {
    return <WarningBadge>{unreadMessagesCount.toString()}</WarningBadge>;
  }
  return null;
});

export const OnlineStatusIcon = observer(
  ({ roomOnlineStatus }: OnlineStatusIconProps) => {
    let backgroundColor;
    switch (roomOnlineStatus) {
      case "ONLINE":
        backgroundColor = Colors.kuvira;
        break;
      case "OFFLINE":
        backgroundColor = Colors.steel;
        break;
      case "IDLE":
        backgroundColor = Colors.vandel;
        break;
      default:
        backgroundColor = Colors.steel;
    }
    return <OnlineStatusView backgroundColor={backgroundColor} />;
  }
);

export const ChannelImageComponent = observer(({ chatRoom }: ChatRoomProps) => {
  const { name } = chatRoom;
  return (
    <ImageContainer>
      <SmallThumbnailImage name={name} />
    </ImageContainer>
  );
});

export const MemberImageComponent = observer(({ chatRoom }: ChatRoomProps) => {
  const { name, roomOnlineStatus } = chatRoom;
  return (
    <ImageContainer>
      <SmallThumbnailImage name={name} />
      <OnlineStatusIcon roomOnlineStatus={roomOnlineStatus} />
    </ImageContainer>
  );
});

export const ChatMemberItem = observer(
  ({ chatRoom, backgroundColor, onPress }: ChatRoomItemProps) => (
    <Touchable onPress={onPress}>
      <BodyContainer backgroundColor={backgroundColor}>
        <MemberImageComponent chatRoom={chatRoom} />
        <MemberTextContainer chatRoom={chatRoom} />
        <UnreadCountIcon unreadMessagesCount={chatRoom.unreadMessagesCount} />
      </BodyContainer>
    </Touchable>
  )
);

export const ChatIssueItem = observer(
  ({ chatRoom, backgroundColor, onPress }: ChatRoomItemProps) => (
    <Touchable onPress={onPress}>
      <BodyContainer backgroundColor={backgroundColor}>
        <IssueTextContainer chatRoom={chatRoom} />
        <UnreadCountIcon unreadMessagesCount={chatRoom.unreadMessagesCount} />
      </BodyContainer>
    </Touchable>
  )
);

export const ChatChannelItem = observer(
  ({ chatRoom, backgroundColor, onPress }: ChatRoomItemProps) => (
    <Touchable onPress={onPress}>
      <BodyContainer backgroundColor={backgroundColor}>
        <ChannelImageComponent chatRoom={chatRoom} />
        <ChannelTextContainer chatRoom={chatRoom} />
        <UnreadCountIcon unreadMessagesCount={chatRoom.unreadMessagesCount} />
      </BodyContainer>
    </Touchable>
  )
);

@observer
export class ChatRoomItem extends Component<Props> {
  @computed
  get backgroundColor() {
    const { chatRoom } = this.props;
    if (chatRoom.isFocused === true) {
      return Colors.blinky;
    }
    return Colors.toby;
  }

  props: Props;

  handleOnPress = () => {
    this.props.handleOnPress(this.props.chatRoom);
  };

  render() {
    const { chatRoom, renderItem: RenderItem } = this.props;
    return (
      <RenderItem
        chatRoom={chatRoom}
        backgroundColor={this.backgroundColor}
        onPress={this.handleOnPress}
      />
    );
  }
}
