/**
 * @flow
 * @author: Anesh Parvatha
 */

import styled from "styled-components/native";

export const DataMessage = styled.View`
  width: 120;
  height: 30;
  border-radius: 4;
  background-color: white;
  justify-content: center;
  align-items: center;
`;
export const DateText = styled.Text`
  text-align: center;
  color: black;
`;
