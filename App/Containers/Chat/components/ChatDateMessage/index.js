/**
 * @flow
 * @author: Anesh Parvatha
 */
import React from "react";
import { observer } from "mobx-react/native";
import isToday from "date-fns/is_today";
import format from "date-fns/format";
import isYesterday from "date-fns/is_yesterday";

import { DataMessage, DateText } from "./styledComponents";

type Props = {
  dateString: string
};

const ChatDateMessage = (props: Props) => {
  const { dateString } = props;
  let displayString = dateString;
  // FIXME: isToday is not accepting local time
  // console.log(
  //   'Is today ',
  //   dateString,
  //   isToday(dateString),
  //   isYesterday(dateString)
  // )
  if (isToday(dateString)) {
    displayString = "Today";
  } else if (isYesterday(dateString)) {
    displayString = "Yesterday";
  } else {
    displayString = format(dateString, "DD MMM YYYY");
  }
  return (
    <DataMessage>
      <DateText>{displayString}</DateText>
    </DataMessage>
  );
};

export default observer(ChatDateMessage);
