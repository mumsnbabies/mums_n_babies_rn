/**
 * @flow
 * @author: Anesh Parvatha
 */

import styled from "styled-components/native";

export const MessageTextContainer = styled.View`
  padding-top: 5;
  padding-right: 13;
  padding-bottom: 10;
  padding-left: 10;

  background-color: white;
  border-style: solid;
  border-width: 1;
  border-color: black;

  max-width: 90%;
`;

export const SelfMessageTextContainer = MessageTextContainer.extend`
  border-top-left-radius: 20;
  border-top-right-radius: 0;
  border-bottom-left-radius: 20;
  border-bottom-right-radius: 20;
`;

export const OtherMessageTextContainer = MessageTextContainer.extend`
  border-top-left-radius: 0;
  border-top-right-radius: 20;
  border-bottom-left-radius: 20;
  border-bottom-right-radius: 20;
`;

export const CombinedView = styled.View`
  flex-direction: row;
`;

export const MessageContent = styled.View`
  max-width: 85%;
`;

export const TimeContent = styled.View`
  align-self: flex-end;
`;
