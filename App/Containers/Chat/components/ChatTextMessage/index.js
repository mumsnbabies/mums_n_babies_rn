/**
 * @flow
 * @author: Anesh Parvatha
 */
import React, { Component } from "react";
import { Text } from "react-native";
import { observer } from "mobx-react/native";

import type ChatMessage from "@rn/react-native-mqtt-iot/app/stores/models/ChatMessage";
import { SELF } from "@rn/react-native-mqtt-iot/app/constants/ChatConstants";

import ChatMessageTime from "../ChatMessageTime";

import {
  SelfMessageTextContainer,
  OtherMessageTextContainer,
  CombinedView,
  MessageContent,
  TimeContent
} from "./styledComponents";

type Props = {
  message: ChatMessage
};

@observer
class ChatTextMessage extends Component<Props> {
  props: Props;
  render() {
    const { message } = this.props;
    if (!message.content) {
      return null;
    }
    if (message.type === SELF) {
      return (
        <SelfMessageTextContainer>
          <CombinedView>
            <MessageContent>
              <Text>{message.content}</Text>
            </MessageContent>
            <TimeContent>
              <ChatMessageTime timestamp={message.serverLogTimeStamp} />
            </TimeContent>
          </CombinedView>
        </SelfMessageTextContainer>
      );
    }
    return (
      <OtherMessageTextContainer>
        <CombinedView>
          <MessageContent>
            <Text>{message.content}</Text>
          </MessageContent>
          <TimeContent>
            <ChatMessageTime timestamp={message.serverLogTimeStamp} />
          </TimeContent>
        </CombinedView>
      </OtherMessageTextContainer>
    );
  }
}

export default ChatTextMessage;
