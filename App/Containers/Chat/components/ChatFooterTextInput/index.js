/*
 * @flow
 * @Author: Chinmaya
 */

import React, { Component } from "react";
import { observer } from "mobx-react/native";

import { StyledInput } from "./styledComponents";

type Props = {
  message: string,
  onChange: (message: string) => mixed
};

@observer
export class ChatFooterTextInput extends Component<Props> {
  onChange = event => {
    const message = event.nativeEvent.text;
    this.props.onChange(message);
  };

  resetTextInput = () => {
    this.textInput.clear();
    this.textInput.resetHeightToMin();
  };
  textInput: ?StyledInput;
  render() {
    return (
      <StyledInput
        placeholder="Type message"
        value={this.props.message}
        onChange={this.onChange}
        maxHeight={130}
        minHeight={48}
        ref={r => {
          this.textInput = r;
        }}
        underlineColorAndroid="transparent"
        multiline={true}
        autoGrow={true}
      />
    );
  }
}

export default ChatFooterTextInput;
