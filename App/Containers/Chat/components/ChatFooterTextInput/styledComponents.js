/*
 * @flow
 * @Author: Chinmaya
 */

import styled from "styled-components/native";

// TODO: explore how to extend styles of Body for AutoGrowingTextInput

export const StyledInput = styled.TextInput`
  border-radius: 4;
  border-color: black;
  border-width: 2;
  padding-top: 14;
  padding-bottom: 14;
  padding-left: 20;
  padding-right: 20;
  margin-left: 7;
  margin-right: 7;
  font-size: 14;
  line-height: 20;
  text-align: left;
  color: black;
  flex: 1;
  height: 30;
`;
