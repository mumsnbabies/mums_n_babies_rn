/*
 * @flow
 * @Author: Chinmaya
 */

import * as React from "react";
import { observer } from "mobx-react/native";
import styled from "styled-components/native";

import type { StyleObj } from "react-native/Libraries/StyleSheet/StyleSheetTypes"; // eslint-disable-line

type Props = {
  style?: StyleObj,
  children?: React.Node
};

// eslint-disable-next-line
const ChatFooterOuterContainer = styled.View`
  shadow-color: black;
  shadow-offset: 0px 1px;
  shadow-radius: 1px;
  shadow-opacity: 1;
  flex-direction: column;
  align-items: stretch;
`;

const ChatFooterInnerContainer = styled.View`
  background-color: white;
  shadow-color: white;
  shadow-offset: 0px -1px;
  shadow-radius: 2px;
  shadow-opacity: 1;
  margin-top: 3;
  padding-top: 15;
  padding-left: 20;
  padding-right: 20;
  padding-bottom: 15;
  flex-direction: row;
  align-items: center;
`;

/* <ChatFooterOuterContainer style={props.style}>
</ChatFooterOuterContainer> */

const ChatFooterCard = observer((props: Props = { style: 0 }) => (
  <ChatFooterInnerContainer>{props.children}</ChatFooterInnerContainer>
));

export default ChatFooterCard;
