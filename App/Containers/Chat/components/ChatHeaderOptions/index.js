/*
 * @flow
 * @Author: Chinmaya
 */

/* eslint-disable */

import React, { Component } from "react";
import { observer } from "mobx-react/native";
import { View } from "react-native";

import type { StyleObj } from "react-native/Libraries/StyleSheet/StyleSheetTypes";
import type ChatRoom from "@rn/react-native-mqtt-iot/app/stores/models/ChatRoom";

type Props = {
  style?: StyleObj,
  chatRoom: ChatRoom
};

@observer
export class ChatHeaderOptions extends Component<Props> {
  static defaultProps = { style: 0 };

  render() {
    return <View />;
  }
}

export default ChatHeaderOptions;
