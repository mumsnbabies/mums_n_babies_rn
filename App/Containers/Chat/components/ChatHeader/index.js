/*
 * @flow
 * @Author: Chinmaya
 */

import React, { Component } from "react";
import { View } from "react-native";
import { observer } from "mobx-react/native";

// import ChatHeaderCard from "../ChatHeaderCard";
// import ChatHeaderThumbnail from "../ChatHeaderThumbnail";
// import ChatHeaderName from "../ChatHeaderName";
// import ChatHeaderOptions from '../ChatHeaderOptions'

import type { StyleObj } from "react-native/Libraries/StyleSheet/StyleSheetTypes"; // eslint-disable-line
import type ChatRoom from "@rn/react-native-mqtt-iot/app/stores/models/ChatRoom"; // eslint-disable-line

type Props = {
  style: StyleObj,
  chatRoom: ChatRoom
};

@observer
export class ChatHeader extends Component<Props> {
  render() {
    const { style, ...other } = this.props;
    return <View />;
    {
      /* <ChatHeaderCard style={style}>
        <ChatHeaderThumbnail {...other} />
        <ChatHeaderName {...other} />
        <ChatHeaderOptions {...other} />
      </ChatHeaderCard>
    ); */
    }
  }
}

export default ChatHeader;
