/**
 * @flow
 * @author: Anesh Parvatha
 */
import React from "react";
import ChatMember from "@rn/react-native-mqtt-iot/app/stores/models/ChatMember";

import { MediumThumbnailImage } from "../ThumbnailImage";

type Props = {
  sender: ChatMember
};

const ChatMessageSenderThumbnail = (props: Props) => {
  const { sender } = props;
  return (
    <MediumThumbnailImage
      thumbnailUrl={sender ? sender.thumbnail : ""}
      name={sender ? sender.name : ""}
    />
  );
};

export default ChatMessageSenderThumbnail;
