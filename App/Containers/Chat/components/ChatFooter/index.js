/*
 * @flow
 * @Author: Chinmaya
 */

import React, { Component } from "react";
import { observable } from "mobx";
import { observer } from "mobx-react/native";
import { Keyboard } from "react-native";
import ChatMessage from "@rn/react-native-mqtt-iot/app/stores/models/ChatMessage";

import ChatFooterCard from "../ChatFooterCard";
import ChatFooterTextInput from "../ChatFooterTextInput";
import ChatFooterSend from "../ChatFooterSend";
// import ChatFooterAttachments from '../ChatFooterAttachments'

import type { StyleObj } from "react-native/Libraries/StyleSheet/StyleSheetTypes"; // eslint-disable-line
import type ChatRoom from "@rn/react-native-mqtt-iot/app/stores/models/ChatRoom"; // eslint-disable-line

type Props = {
  style: StyleObj,
  chatRoom: ChatRoom,
  onSend?: (message: ?ChatMessage) => mixed
};

@observer
export class ChatFooter extends Component<Props> {
  static defaulProps = {
    onSend: () => {},
    style: 0
  };

  @observable message: ChatMessage;

  constructor(props: Props) {
    super(props);
    this.message = ChatMessage.createDummyMessage(props.chatRoom);
  }

  onContentChange = (content: string) => {
    this.message.content = content;
  };

  onSend = () => {
    Keyboard.dismiss();
    if (this.message.content.trim()) {
      if (this.props.onSend) this.props.onSend(this.message);
      this.props.chatRoom.addNewMessage(this.message);
      this.props.chatRoom.publishMessage(this.message.getPublishObject());
      this.message = ChatMessage.createDummyMessage(this.props.chatRoom);
    }
  };

  render() {
    const { style, chatRoom } = this.props;
    return (
      <ChatFooterCard style={style}>
        {/* <ChatFooterAttachments message={this.message} chatRoom={chatRoom} /> */}
        <ChatFooterTextInput
          message={this.message.content}
          onChange={this.onContentChange}
        />
        <ChatFooterSend chatRoom={chatRoom} onSend={this.onSend} />
        {/* <AttachmentIcon />
          <EmojiIcon />
          <ChatTextInput />
        <SendButton /> */}
      </ChatFooterCard>
    );
  }
}

export default ChatFooter;
