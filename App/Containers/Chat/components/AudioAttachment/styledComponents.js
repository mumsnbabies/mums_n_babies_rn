/*
 * @flow
 * @Author: Chinmaya
 */

import styled from "styled-components/native";

export const AudioDetailsView = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
`;

export const AudioDetailsIcon = styled.TouchableWithoutFeedback`
  justify-content: center;
  align-items: center;
  align-self: center;
`;

export const AudioDetailsIconContainer = styled.View`
  width: 44;
  height: 44;
  justify-content: center;
  align-items: center;
`;
