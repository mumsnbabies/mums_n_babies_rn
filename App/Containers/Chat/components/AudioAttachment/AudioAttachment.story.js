/*
 * @flow
 * @Author: Chinmaya
 */

import * as React from 'react'
import { storiesOf } from '@storybook/react-native'

import AudioAttachment from './index'

storiesOf('AudioAttachment', module).add('default', () => (
  <AudioAttachment url="https://s3.amazonaws.com/hanselminutes/hanselminutes_0001.mp3" />
))
