/*
 * @flow
 * @Author: Chinmaya
 */

import React, { Component } from 'react'
import { observable, action, computed } from 'mobx'
import { observer } from 'mobx-react/native'
import Sound from 'react-native-sound'
import Headphones from 'Chat/themes/icons/Headphones/component'
import Mic from 'Chat/themes/icons/Mic/component'
import Play from 'Chat/themes/icons/Play/component'
import Pause from 'Chat/themes/icons/Pause/component'
import Colors from 'themes/Colors'

import BaseAttachment from '../BaseAttachment'
import {
  AudioDetailsView,
  AudioDetailsIcon,
  AudioDetailsIconContainer
} from './styledComponents'

type Props = {
  url: string
}

type BasePlayProps = {
  isPlaying: boolean,
  isLoaded: boolean
}

type IconProps = BasePlayProps & {
  onPress: () => mixed
}

type DetailsProps = BasePlayProps & {
  sound: Sound,
  onIconPress: () => mixed
}

const CurrentIcon = observer((props: BasePlayProps) => {
  const { isPlaying, isLoaded } = props
  if (isLoaded === true) {
    if (isPlaying) {
      return <Pause color={Colors.dusk} />
    }
    return <Play color={Colors.dusk} width={16} height={18} />
  }
  return <Play color={Colors.coolGrey} width={16} height={18} />
})

const AudioDetailsIconView = observer((props: IconProps) => {
  const { onPress, ...other } = props
  return (
    <AudioDetailsIcon onPress={onPress}>
      <AudioDetailsIconContainer>
        <CurrentIcon {...other} />
      </AudioDetailsIconContainer>
    </AudioDetailsIcon>
  )
})

const AudioDetails = observer((props: DetailsProps) => {
  const { onIconPress } = props
  return (
    <AudioDetailsView>
      <AudioDetailsIconView {...props} onPress={onIconPress} />
    </AudioDetailsView>
  )
})

@observer
export class AudioAttachment extends Component<Props> {
  @observable isAudioLoaded: boolean = false
  @observable isPlaying: boolean = false
  sound: ?Sound

  componentDidMount() {
    this.loadAudio()
  }

  componentWillUnmount() {
    if (this.sound !== undefined && this.sound !== null) {
      this.sound.release()
    }
  }

  loadAudio = () => {
    this.sound = new Sound(
      this.props.url,
      undefined,
      action((error) => {
        if (error) {
          console.log(error)
        } else {
          console.log('Playing sound')
          this.isAudioLoaded = true
        }
      })
    )
  }

  @action
  togglePlay = () => {
    console.log('toggle play')
    if (
      this.isAudioLoaded === true &&
      this.sound !== undefined &&
      this.sound !== null &&
      this.sound.isLoaded()
    ) {
      if (this.isPlaying === true) {
        this.sound.pause()
        this.isPlaying = false
      } else {
        this.sound.play()
        this.isPlaying = true
      }
    }
  }

  @computed
  get renderAttachmentIcon() {
    if (this.isPlaying === true) {
      return Mic
    }
    return Headphones
  }

  render() {
    const attachmentDetailsProps = {
      isPlaying: this.isPlaying,
      isLoaded: this.isAudioLoaded,
      onIconPress: this.togglePlay
    }
    const AttachmentIcon = this.renderAttachmentIcon
    return (
      <BaseAttachment
        renderIcon={() => <AttachmentIcon color={Colors.white} />}
        renderAttachmentDetails={AudioDetails}
        attachmentDetailsProps={attachmentDetailsProps}
      />
    )
  }
}

export default AudioAttachment
