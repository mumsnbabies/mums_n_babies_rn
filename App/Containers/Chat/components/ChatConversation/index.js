/**
 * @flow
 * @author: Anesh Parvatha
 */
import * as React from "react";
import { observer } from "mobx-react/native";

import type ChatMessage from "@rn/react-native-mqtt-iot/app/stores/models/ChatMessage";
import FlatListContainer from "@rn/react-native-mqtt-iot/app/components/FlatListPaginatedContainer/FlatListPaginatedContainer";

import type BaseChatRoom from "../../stores/models/BaseChatRoom";
import ChatMessageContainer from "../ChatMessageContainer";
import styles from "./styles";

type Props = {
  chatRoom: BaseChatRoom,
  renderMessage?: React$Component<{ message: ChatMessage }>
};

@observer
class ChatConversation extends React.Component<Props> {
  static defaultProps = {
    renderMessage: ChatMessageContainer
  };

  keyExtractor = (item: ChatMessage) => item.id;

  onEndReached = () => {
    this.props.chatRoom.getPreviousMessages();
  };

  render() {
    const { chatRoom, renderMessage: ChatMessageComponent } = this.props;
    return (
      <FlatListContainer
        loadingStatus={chatRoom.messagesStatus}
        renderComponent={ChatMessageComponent}
        data={chatRoom.roomMessagesByDate}
        itemPropLabel="message"
        style={styles.container}
        getItemKey={this.keyExtractor}
        onEndReached={this.onEndReached}
        inverted
      />
    );
  }
}
export default ChatConversation;
