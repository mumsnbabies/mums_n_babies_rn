/*
 * @flow
 * @Author: Chinmaya
 */

import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1
  },
  paginationLoader: {
    marginVertical: 6
  }
});
