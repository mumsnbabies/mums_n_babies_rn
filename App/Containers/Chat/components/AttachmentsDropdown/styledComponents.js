/*
 * @flow
 * @Author: Chinmaya
 */

import styled from "styled-components/native";

export const DropdownOptionsContainer = styled.View`
  flex-direction: column;
  height: 300;
`;

export const OptionsTitleContainer = styled.View`
  padding-left: 20;
  padding-right: 20;
  padding-top: 4;
  padding-bottom: 4;
  background-color: "blue";
`;

export const AttachText = styled(Link)``;

export const OptionItemContainer = styled.View`
  padding-left: 20;
  padding-right: 20;
  padding-top: 10;
  padding-bottom: 10;
  align-items: center;
  flex-direction: row;
`;

export const OptionItemTitle = styled(Body)`
  margin-left: 20;
`;
