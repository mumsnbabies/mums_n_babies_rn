/*
 * @flow
 * @Author: Chinmaya
 */

import * as React from "react";
import { observer } from "mobx-react/native";
import I18n from "react-native-i18n";
import { TouchableOpacity, View } from "react-native";
import ImagePicker from "react-native-image-crop-picker";
import AWSS3Utility from "@rn/aws-sdk/AWSS3Utility";
import ChatMessage from "@rn/react-native-mqtt-iot/app/stores/models/ChatMessage";
import Multimedia from "@rn/react-native-mqtt-iot/app/stores/models/Multimedia";
import { MULTIMEDIA_IMAGE } from "@rn/react-native-mqtt-iot/app/constants/ChatConstants";

import { S3Config } from "Common/utils/S3Config";
import getFileType from "Common/utils/MimeUtils";
import ImageIcon from "Chat/themes/icons/ImageAttachment/component";
import CameraIcon from "Chat/themes/icons/CameraAttachment/component";
// import Mic from 'Chat/themes/icons/Mic/component'
// import FileIcon from 'Chat/themes/icons/FileAttachment/component'
// import Headphones from 'Chat/themes/icons/Headphones/component'
// import VideoCamera from 'Chat/themes/icons/VideoCamera/component'

import { OptionItemContainer, OptionItemTitle } from "./styledComponents";

import type ChatRoom from "@rn/react-native-mqtt-iot/app/stores/models/ChatRoom"; // eslint-disable-line
import type { StyleObj } from "react-native/Libraries/StyleSheet/StyleSheetTypes"; // eslint-disable-line

type Props = {
  onOptionClicked: () => mixed,
  chatRoom: ChatRoom
};

type OptionItemProps = {
  title: string,
  renderIcon: React.Node,
  onPress: () => mixed,
  style?: StyleObj
};

const OptionItem = observer((props: OptionItemProps) => {
  const { title, renderIcon: Icon, onPress, style } = props;
  return (
    <TouchableOpacity style={style} onPress={onPress}>
      <OptionItemContainer>
        <Icon />
        <OptionItemTitle>{title}</OptionItemTitle>
      </OptionItemContainer>
    </TouchableOpacity>
  );
});

@observer
export class OptionsList extends React.Component<Props> {
  awsUploader: AWSS3Utility;

  constructor(props: Props) {
    super(props);
    this.awsUploader = new AWSS3Utility(S3Config, () => {}, () => {});
  }

  componentWillUnmount() {
    // ImagePicker.clean()
    //   .then(() => {
    //     console.log('removed all tmp images from tmp directory')
    //   })
    //   .catch((e) => {
    //     Alert.alert(e)
    //   })
  }

  onOptionClicked = (clickFn: Function) => input => {
    clickFn(input);
    this.props.onOptionClicked();
  };

  addImagesToChatRoom = (func: Function) => images => {
    const message = ChatMessage.createDummyMessage(this.props.chatRoom);
    const multimediaList = images.map(image => {
      const fileType = getFileType(image.mime);
      const multimedia = {};
      switch (fileType.mediaType) {
        case "image":
          multimedia.multimedia_type = MULTIMEDIA_IMAGE;
          break;
        // TODO: Add multimedia_type for other extensions
        default:
      }
      multimedia.multimedia_type = MULTIMEDIA_IMAGE;
      multimedia.url = image.path;
      multimedia.progress = 0;
      const multimediaObject = new Multimedia(multimedia);
      return multimediaObject;
    });
    message.multimediaList.replace(multimediaList);
    this.props.chatRoom.addNewMessage(message);
    Promise.all(
      images.map(image => {
        const multimediaObject = message.multimediaList.filter(
          media => media.url === image.path
        )[0];
        return func(image, message, multimediaObject);
      })
    ).then(() => {
      this.props.chatRoom.publishMessage({
        content: "",
        id: message.id,
        multimedia_urls: message.multimediaList.map(media =>
          media.toRequestObject()
        )
      });
    });
  };

  onAttachFromGallery = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      multiple: true,
      includeExif: true
    })
      .then(
        this.onOptionClicked(
          this.addImagesToChatRoom(async (image, message, multimedia) => {
            const currentUnixTime = new Date().getTime();
            console.log(
              "Pic selection completed: ",
              image,
              image.path,
              currentUnixTime,
              message
            );

            const fileInfo = getFileType(image.mime);
            if (__DEV__) console.log("File Info ===>", fileInfo);
            const imageUploadResponse = this.awsUploader.uploadImageFromURI(
              {
                // Body: new Buffer(body, 'base64'),
                path: image.path,
                Key: `${S3Config.path}chat/IMG_${currentUnixTime}.${
                  fileInfo.extension
                }`,
                Bucket: S3Config.bucketName,
                ACL: "public-read",
                ContentType: image.mime
              },
              (error, data) => {
                if (__DEV__) console.log("Uploaded to s3", error, data);
              }
            );
            const imageUploadResponsePromise = await imageUploadResponse;
            imageUploadResponsePromise
              .on("httpUploadProgress", evt => {
                console.log("Progress:", evt.loaded, "/", evt.total);
                multimedia.updateProgress(evt.loaded * 100 / evt.total); // eslint-disable-line
              })
              .send((err, data) => {
                console.log(err, data);
              });
            const imageUploadResponseData = await imageUploadResponsePromise.promise();
            if (__DEV__) {
              console.log(
                "Image Upload response data:  ",
                imageUploadResponseData
              );
            }
            multimedia.updateURL(imageUploadResponseData.Location);
            // this.setState({ url: imageUploadResponseData.Location })
            // this.setState({ media_type: fileInfo.mediaType })
          })
        )
      )
      .catch(
        this.onOptionClicked(error => {
          if (__DEV__) console.log("In Pic Selction Error ==> ", error);
        })
      );
  };

  openCamera = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true
    }).then(image => {
      console.log(image);
    });
  };
  openDocumentPicker = () => {};
  openAudioFilePicker = () => {};
  openCameraWithVideoMode = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      mediaType: "video"
    }).then(image => {
      console.log(image);
    });
  };
  startAudioRecording = () => {};
  render() {
    return (
      <View>
        <OptionItem
          key="photos"
          renderIcon={ImageIcon}
          title={I18n.t("chatModule.photosAndVideos")}
          onPress={this.onAttachFromGallery}
        />
        <OptionItem
          key="takePhoto"
          renderIcon={CameraIcon}
          title={I18n.t("chatModule.takePhoto")}
          onPress={this.openCamera}
        />
        {/* <OptionItem
          key="recordVideo"
          renderIcon={VideoCamera}
          title={I18n.t('chatModule.recordVideo')}
          onPress={this.openCameraWithVideoMode}
          />
          <OptionItem
          key="audio"
          renderIcon={Headphones}
          title={I18n.t('chatModule.audio')}
          onPress={this.openAudioFilePicker}
          />
          <OptionItem
          key="recordAudio"
          renderIcon={Mic}
          title={I18n.t('chatModule.recordAudio')}
          onPress={this.startAudioRecording}
          />
          <OptionItem
          key="other"
          renderIcon={FileIcon}
          title={I18n.t('chatModule.otherFiles')}
          onPress={this.openDocumentPicker}
        /> */}
      </View>
    );
  }
}

export default OptionsList;
