/*
 * @flow
 * @Author: Chinmaya
 */

import StyleSheet from "@rn/commons/app/components/GenericComponents/GenericStyleSheet";

export default StyleSheet.create({
  iconContainer: {
    paddingHorizontal: 8,
    paddingVertical: 8,
    borderRadius: 18,
    height: 36,
    width: 36
  },
  dropdownStyle: {
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 6,
    shadowOpacity: 1
  }
});
