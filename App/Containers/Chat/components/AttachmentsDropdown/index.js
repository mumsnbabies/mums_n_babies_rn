/*
*
* @flow
* @author : Chinmaya
*/
/* eslint react/sort-comp: 0 react-native/no-color-literals: 0 */

import React, { Component } from "react";
import { View } from "react-native";
import { observable, action } from "mobx";
import { observer } from "mobx-react/native";
import ModalDropdown from "react-native-modal-dropdown";
import I18n from "react-native-i18n";
import AttachmentIcon from "Chat/themes/icons/Attachment/component";
import Colors from "themes/Colors";

import OptionsList from "./OptionsList";
import styles from "./styles";
import {
  DropdownOptionsContainer,
  OptionsTitleContainer,
  AttachText
} from "./styledComponents";

import type ChatRoom from "@rn/react-native-mqtt-iot/app/stores/models/ChatRoom"; // eslint-disable-line
import type { StyleObj } from "react-native/Libraries/StyleSheet/StyleSheetTypes"; // eslint-disable-line

type Props = {
  /**
   *  style of this component root container
   */
  dropdownContainerStyle?: StyleObj,

  /**
   * Specifies which key to take from object to show text default Value is text
   */
  valueKey?: string,

  /**
   * Callback returns index and value of the selected item
   */
  onOptionSelect?: Function,

  /**
   * Menu option component height
   */
  itemHeight?: number,

  /**
   * Callback to expose is dropdown expanded or not
   */

  onDropDownStateChanged?: (index: string, value: any) => mixed,

  /**
   * index to be selected defaultly
   */

  defaultIndex?: number,

  /**
   * index of Reset option
   */

  resetOptionIndex?: number,

  /**
   * Callback when reset option is clicked
   */

  onResetOptionSelect?: (index: string, value: any) => mixed,

  /**
   * buttonStyles for the static dropdown button
   */
  buttonStyles?: number,
  chatRoom: ChatRoom
};

const AttachmentIconContainer = observer(props => {
  const { isExpanded } = props;
  const backgroundColor = isExpanded ? Colors.lightSkyBlue : "transparent";
  const iconColor = isExpanded ? Colors.clearBlue : Colors.strickler;
  return (
    <View style={[styles.iconContainer, { backgroundColor }]}>
      <AttachmentIcon color={iconColor} />
    </View>
  );
});

const DROP_DOWN_DEFAULT_HEIGHT = 200;
@observer
class Dropdown extends Component<Props> {
  static defaultProps = {
    dropdownButtonText: I18n.t("chatModule.filter"),
    renderDropDownButton: undefined,
    valueKey: "text",
    onOptionSelect: () => {},
    dropdownContainerStyle: 0,
    itemHeight: 32,
    onDropDownStateChanged: () => {},
    defaultIndex: -1,
    resetOptionIndex: -1,
    onResetOptionSelect: () => {}
  };

  @observable dropdownExpanded: boolean;
  @observable selectedIndex: number;
  dropdown: ModalDropdown;
  constructor(props) {
    super(props);
    this.selectedIndex = Number(props.defaultIndex);
    this.dropdownExpanded = false;
  }

  setDropdownExpanded = action(bool => {
    this.dropdownExpanded = bool;
  });

  onOptionClicked = () => {
    if (this.dropdown) {
      this.setDropdownExpanded(false);
      this.dropdown.hide();
    }
  };

  renderRow = (rowData, rowID, highlighted) => {
    console.log("render row ", rowData, rowID, highlighted);
    return (
      <DropdownOptionsContainer>
        <OptionsTitleContainer>
          <AttachText>{I18n.t("chatModule.attach")}</AttachText>
        </OptionsTitleContainer>
        <OptionsList
          onOptionClicked={this.onOptionClicked}
          chatRoom={this.props.chatRoom}
        />
      </DropdownOptionsContainer>
    );
  };

  onOptionSelected = (index, value) => {
    this.selectedIndex = Number(index);
    this.props.onOptionSelect(index, value);
    return true;
  };

  /**
   * To calculate height of dropdown view
   */
  getDropDownHeight = () => DROP_DOWN_DEFAULT_HEIGHT;

  adjustFrame = style => {
    console.log("adjust frame style", style);
    return {
      ...style,
      top: 560,
      left: 272,
      height: 120,
      borderWidth: 0
    };
  };

  renderSeparator = () => null;

  render() {
    const { dropdownContainerStyle, buttonStyles, ...other } = this.props; // eslint-disable-line
    return (
      <ModalDropdown
        ref={c => {
          this.dropdown = c;
        }}
        options={["test"]}
        renderRow={this.renderRow}
        renderSeparator={this.renderSeparator}
        adjustFrame={this.adjustFrame}
        onDropdownWillShow={() => {
          this.setDropdownExpanded(true);
        }}
        onDropdownWillHide={() => {
          this.setDropdownExpanded(false);
        }}
        dropdownStyle={styles.dropdownStyle}
      >
        <AttachmentIconContainer isExpanded={this.dropdownExpanded} />
      </ModalDropdown>
    );
  }
}

export default Dropdown;
