/*
 * @flow
 * @Author: Chinmaya
 */

import React, { Component } from "react";
import { observer } from "mobx-react/native";

import AttachmentsDropdown from "../AttachmentsDropdown"; // eslint-disable-line

import type { StyleObj } from "react-native/Libraries/StyleSheet/StyleSheetTypes"; // eslint-disable-line
import type ChatRoom from "@rn/react-native-mqtt-iot/app/stores/models/ChatRoom"; // eslint-disable-line
import type ChatMessage from "@rn/react-native-mqtt-iot/app/stores/models/ChatMessage"; // eslint-disable-line

type Props = {
  style?: StyleObj,
  chatRoom: ChatRoom,
  message: ChatMessage
};

@observer
export class ChatFooterAttachments extends Component<Props> {
  render() {
    const { style, chatRoom, message } = this.props; // eslint-disable-line
    return <AttachmentsDropdown {...this.props} />;
  }
}

export default ChatFooterAttachments;
