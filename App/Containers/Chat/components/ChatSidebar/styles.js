/*
 * @flow
 * @Author: Chinmaya
 */

import StyleSheet from '@rn/commons/app/components/GenericComponents/GenericStyleSheet'
import Colors from 'themes/Colors' // eslint-disable-line

export default StyleSheet.create({
  contentContainerStyle: {
    alignItems: 'stretch'
  }
})
