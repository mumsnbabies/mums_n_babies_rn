/*
 * @flow
 * @Author: Chinmaya
 */

import styled from 'styled-components/native'
import { Dimensions } from 'react-native'
import Colors from 'themes/Colors' // eslint-disable-line
// import Metrics from 'themes/Metrics' // eslint-disable-line

const { width } = Dimensions.get('window') // eslint-disable-line

export const SidebarContainer = styled.ScrollView`
  background-color: ${Colors.toby};
  flex: 1;
`
