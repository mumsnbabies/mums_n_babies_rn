/*
 * @flow
 * @Author: Chinmaya
 */

import React, { Component } from "react";
import { observer } from "mobx-react/native";
import I18n from "react-native-i18n";
import { SidebarChatRoomContainer } from "../SidebarChatRoomContainer";
import { ChatStore } from "../../stores/ChatStore";
import {
  ChatMemberItem,
  ChatChannelItem,
  ChatIssueItem
} from "../ChatRoomItem";
import { SidebarContainer } from "./styledComponents";

import styles from "./styles";

import type ChatRoom from "@rn/react-native-mqtt-iot/app/stores/models/ChatRoom"; // eslint-disable-line

type Props = {
  chatStore: ChatStore
};

@observer
export class ChatSidebar extends Component<Props> {
  props: Props;

  onChatRoomPress = (chatRoom: ChatRoom) => {
    requestAnimationFrame(() => {
      this.props.chatStore.setSelectedChatRoom(chatRoom);
    });
  };

  render() {
    const { chatStore } = this.props;
    const chatRoomItemProps = { handleOnPress: this.onChatRoomPress };
    return (
      <SidebarContainer contentContainerStyle={styles.contentContainerStyle}>
        <SidebarChatRoomContainer
          title={I18n.t("chatModule.channels")}
          renderItem={ChatChannelItem}
          chatRooms={chatStore.latestGroupRooms}
          chatRoomItemProps={chatRoomItemProps}
        />
        <SidebarChatRoomContainer
          title={I18n.t("chatModule.members")}
          renderItem={ChatMemberItem}
          chatRooms={chatStore.latestMemberRooms}
          chatRoomItemProps={chatRoomItemProps}
        />
        <SidebarChatRoomContainer
          title={I18n.t("chatModule.issues")}
          renderItem={ChatIssueItem}
          chatRooms={chatStore.latestIssueRooms}
          chatRoomItemProps={chatRoomItemProps}
        />
      </SidebarContainer>
    );
  }
}

export default ChatSidebar;
