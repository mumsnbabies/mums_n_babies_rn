/**
 * @flow
 * @author: Anesh Parvatha
 */

import styled from 'styled-components/native'

export const MessageStatusImage = styled.View`
  width: 18;
  height: 18;
  margin-left: 10;
`
