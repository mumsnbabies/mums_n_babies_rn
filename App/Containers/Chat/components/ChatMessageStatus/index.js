/**
 * @flow
 * @author: Anesh Parvatha
 */

import * as React from "react";
import { computed } from "mobx";
import { observer } from "mobx-react/native";
import type { MessageStatus } from "@rn/react-native-mqtt-iot/app/constants/ChatConstants";
import {
  NOT_DELIVERED,
  DELIVERED,
  READ,
  SENT
} from "@rn/react-native-mqtt-iot/app/constants/ChatConstants";

import Sent from "../../themes/icons/Sent/component";
import Received from "../../themes/icons/Received/component";
import Read from "../../themes/icons/Read/component";
import Sending from "../../themes/icons/Sending/component";

import { MessageStatusImage } from "./styledComponents";

type Props = {
  status: MessageStatus
};

@observer
class ChatMessageStatus extends React.Component<Props> {
  props: Props;
  @computed
  get statusIcon(): React.Element {
    const { status } = this.props;
    switch (status) {
      case NOT_DELIVERED:
        return Sending;
      case SENT:
        return Sent;
      case DELIVERED:
        return Received;
      case READ:
        return Read;
      default:
        return Sending;
    }
  }
  render() {
    const StatusIcon = this.statusIcon;
    return (
      <MessageStatusImage>
        <StatusIcon />
      </MessageStatusImage>
    );
  }
}

export default ChatMessageStatus;
