/**
 * @flow
 * @author: Anesh Parvatha
 */

import styled from 'styled-components/native'

export const MessageHeaderContainer = styled.View`
  flex-direction: row;
  justify-content: flex-end;
`

export const MessageHeader = styled.View`
  align-items: center;
`

export const SelfMessageHeader = MessageHeader.extend`
  flex-direction: row-reverse;
`

export const OtherMessageHeader = MessageHeader.extend`
  flex-direction: row;
`
