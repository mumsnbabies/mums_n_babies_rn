/**
 * @flow
 * @author: Anesh Parvatha
 */
import React, { Component } from "react";
import { observer } from "mobx-react/native";

import type ChatMessage from "@rn/react-native-mqtt-iot/app/stores/models/ChatMessage";
import { SELF } from "@rn/react-native-mqtt-iot/app/constants/ChatConstants";

import ChatMessageSenderName from "../ChatMessageSenderName";
import ChatMessageTime from "../ChatMessageTime";
import ChatMessageStatus from "../ChatMessageStatus";

import {
  MessageHeaderContainer,
  SelfMessageHeader,
  OtherMessageHeader
} from "./styledComponents";

type Props = {
  message: ChatMessage
};

@observer
class ChatMessageHeader extends Component<Props> {
  props: Props;
  renderMessageHeader = (props: Props) => {
    const { message } = props;
    if (message.type === SELF) {
      return (
        <SelfMessageHeader>
          <ChatMessageStatus status={message.status} />
          <ChatMessageSenderName sender={{ name: "You" }} />
          <ChatMessageTime timestamp={message.serverLogTimeStamp} />
        </SelfMessageHeader>
      );
    }
    return (
      <OtherMessageHeader>
        <ChatMessageSenderName sender={message.sender} />
        <ChatMessageTime timestamp={message.serverLogTimeStamp} />
      </OtherMessageHeader>
    );
  };
  render() {
    return (
      <MessageHeaderContainer>
        {this.renderMessageHeader(this.props)}
      </MessageHeaderContainer>
    );
  }
}

export default ChatMessageHeader;
