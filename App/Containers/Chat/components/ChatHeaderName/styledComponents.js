/*
 * @flow
 * @Author: Chinmaya
 */

import styled from "styled-components/native";

export const HeaderTitleText = styled.Text`
  margin-left: 20;
  flex: 1;
  margin-right: 20;
`;
