/*
 * @flow
 * @Author: Chinmaya
 */

/* eslint-disable */

import React, { Component } from "react";
import { observer } from "mobx-react/native";

import { HeaderTitleText } from "./styledComponents";

import type { StyleObj } from "react-native/Libraries/StyleSheet/StyleSheetTypes";
import type ChatRoom from "@rn/react-native-mqtt-iot/app/stores/models/ChatRoom";

type Props = {
  style?: StyleObj,
  chatRoom: ChatRoom
};

@observer
export class ChatHeaderName extends Component<Props> {
  static defaultProps = { style: 0 };

  render() {
    const { style, chatRoom } = this.props;
    // TODO: handle titles for different types of chat rooms

    return (
      <HeaderTitleText style={style} numberOfLines={1}>
        {chatRoom.name}
      </HeaderTitleText>
    );
  }
}

export default ChatHeaderName;
