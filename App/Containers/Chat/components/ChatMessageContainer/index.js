/*
 * @flow
 * @author: Anesh Parvatha
 */

import React from "react";
import { observer } from "mobx-react/native";
import type ChatMessageType from "@rn/react-native-mqtt-iot/app/stores/models/ChatMessage";
import { DATE_MESSAGE } from "../../constants/MessageTypeConstants";

import ChatDateMessage from "../ChatDateMessage";
import ChatMessage from "../ChatMessage";

import { ChatLogMessage } from "../ChatMessage/styledComponents";

type Props = {
  message: ChatMessageType
};

const ChatMessageContainer = (props: Props) => {
  const { message } = props;
  switch (message.eventType) {
    case DATE_MESSAGE:
      return (
        <ChatLogMessage>
          <ChatDateMessage dateString={message.content} />
        </ChatLogMessage>
      );
    default:
      return <ChatMessage message={message} />;
  }
};

export default observer(ChatMessageContainer);
