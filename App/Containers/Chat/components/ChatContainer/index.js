/*
 * @flow
 * @Author: Chinmaya
 */

import React, { Component } from "react";
import { observer } from "mobx-react/native";

import ChatHeader from "../ChatHeader";
import ChatConversation from "../ChatConversation";
import ChatFooter from "../ChatFooter";

import { ChatContainerView } from "./styledComponents";

import type ChatRoom from "@rn/react-native-mqtt-iot/app/stores/models/ChatRoom"; // eslint-disable-line

type DefaultProps = {
  renderHeader: React$Component<{ chatRoom: ChatRoom }>,
  renderFooter: React$Component<{ chatRoom: ChatRoom }>,
  renderMessages: React$Component<{ chatRoom: ChatRoom }>
};

type Props = DefaultProps & {
  chatRoom: ChatRoom
};

@observer
export class ChatContainer extends Component<Props> {
  static defaultProps: DefaultProps = {
    renderFooter: ChatFooter,
    renderHeader: ChatHeader,
    renderMessages: ChatConversation
  };

  render() {
    const {
      renderHeader: Header,
      renderFooter: Footer,
      renderMessages: Messages,
      ...other // eslint-disable-line
    } = this.props;
    const { chatRoom } = this.props;
    if (chatRoom === undefined || chatRoom === null) {
      return <ChatContainerView />;
    }
    return (
      // eslint-disable-next-line
      <ChatContainerView>
        <Header {...other} />
        <Messages {...other} />
        <Footer {...other} />
      </ChatContainerView>
    );
  }
}

export default ChatContainer;
