/*
 * @flow
 * @Author: Chinmaya
 */

import * as React from 'react'
import { storiesOf } from '@storybook/react-native'

import ChatRoom from 'Chat/stores/models/GroupChatRoom'

import ChatContainer from './index'

const apiChatRoom = {
  chat_room_id: '79382452452',
  name: 'Lokesh Dokara',
  room_type: 'USER',
  members: [],
  removed_members: [],
  room_online_status: 'ONLINE',
  last_msg: {},
  unread_msgs_count: 2,
  multimedia_urls: [],
  created_time_stamp: '29/05/17 12:00AM',
  last_updated_time_stamp: '29/05/17 12:00AM'
}

storiesOf('ChatContainer', module).add('default', () => (
  <ChatContainer
    chatRoom={new ChatRoom(apiChatRoom, { connectionParams: {} })}
  />
))
