/*
 * @flow
 * @Author: Chinmaya
 */

import styled from 'styled-components/native'
import Colors from 'themes/Colors' // eslint-disable-line

export const ChatContainerView = styled.View`
  flex-direction: column;
  align-items: stretch;
  justify-content: space-between;
  flex: 3;
`
