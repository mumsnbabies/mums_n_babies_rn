/*
 * @flow
 * @Author: Chinmaya
 */

import { StyleSheet } from "react-native";

export default StyleSheet.create({
  buttonContainer: {
    marginLeft: 20
  }
});
