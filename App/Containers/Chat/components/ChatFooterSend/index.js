/*
 * @flow
 * @Author: Chinmaya
 */

import React, { Component } from "react";
import { observer } from "mobx-react/native";
import { View, Button } from "react-native";
import styles from "./styles";

import type { StyleObj } from "react-native/Libraries/StyleSheet/StyleSheetTypes"; // eslint-disable-line

type Props = {
  onSend: () => mixed,
  style?: StyleObj
};

@observer
export class ChatFooterSend extends Component<Props> {
  render() {
    const { onSend, style } = this.props;
    return (
      <View>
        <Button
          title="Send"
          onPress={onSend}
          touchableStyle={[styles.buttonContainer, style]}
        />
      </View>
    );
  }
}

export default ChatFooterSend;
