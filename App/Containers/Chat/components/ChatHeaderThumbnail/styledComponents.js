/*
 * @flow
 * @Author: Chinmaya
 */

import styled from "styled-components/native";

import { MediumThumbnailImage } from "../ThumbnailImage";

export const HeaderThumbnail = styled(MediumThumbnailImage)`
  margin-left: 100;
`;
