/*
 * @flow
 * @Author: Chinmaya
 */

import React, { Component } from "react";
import { observer } from "mobx-react/native";

import { HeaderThumbnail } from "./styledComponents"; // eslint-disable-line

import type { StyleObj } from "react-native/Libraries/StyleSheet/StyleSheetTypes"; // eslint-disable-line
import type ChatRoom from "@rn/react-native-mqtt-iot/app/stores/models/ChatRoom"; // eslint-disable-line

type Props = {
  style?: StyleObj,
  chatRoom: ChatRoom
};

// TODO: Import circular image from commons after moving from issues

@observer
export class ChatHeaderThumbnail extends Component<Props> {
  render() {
    const { style, chatRoom } = this.props;
    return <HeaderThumbnail name={chatRoom.name} style={style} />;
  }
}

export default ChatHeaderThumbnail;
