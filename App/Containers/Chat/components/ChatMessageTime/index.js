/**
 * @flow
 * @author: Anesh Parvatha
 */

import React from "react";
import format from "date-fns/format";
import { observer } from "mobx-react/native";

import { MessageTime } from "./styledComponents";

type Props = {
  timestamp: number
};

const ChatMessageTime = (props: Props) => {
  const { timestamp } = props;
  return <MessageTime>{format(timestamp, "HH:mm")}</MessageTime>;
};

export default observer(ChatMessageTime);
