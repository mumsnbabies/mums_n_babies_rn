/**
 * @flow
 * @author: Anesh Parvatha
 */

import Styled from "styled-components/native";
export const MessageTime = Styled.Text`
  color: black;
  margin-left: 10;
  text-align: right;
  font-size: 10;
`;
