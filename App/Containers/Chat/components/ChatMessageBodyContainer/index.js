/**
 * @flow
 * @author: Anesh Parvatha
 */
import React, { Component } from "react";
import { observer } from "mobx-react/native";
import type ChatMessage from "@rn/react-native-mqtt-iot/app/stores/models/ChatMessage";
import { SELF } from "@rn/react-native-mqtt-iot/app/constants/ChatConstants";

import ChatMessageHeader from "../ChatMessageHeader";
import ChatMessageBody from "../ChatMessageBody";

import {
  SelfMessageBodyContainer,
  OtherMessageBodyContainer
} from "./styledComponents";

type Props = {
  message: ChatMessage
};

@observer
class ChatMessageBodyContainer extends Component<Props> {
  props: Props;
  render() {
    const { message } = this.props;
    if (message.type === SELF) {
      return (
        <SelfMessageBodyContainer>
          {/* <ChatMessageHeader message={message} /> */}
          <ChatMessageBody message={message} />
        </SelfMessageBodyContainer>
      );
    }
    return (
      <OtherMessageBodyContainer>
        {/* <ChatMessageHeader message={message} /> */}
        <ChatMessageBody message={message} />
      </OtherMessageBodyContainer>
    );
  }
}

export default ChatMessageBodyContainer;
