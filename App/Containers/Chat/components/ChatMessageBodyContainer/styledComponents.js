/**
 * @flow
 * @author: Anesh Parvatha
 */

import styled from "styled-components/native";

export const MessageBodyContainer = styled.View`
  margin-left: 20;
  margin-right: 20;
`;

export const SelfMessageBodyContainer = MessageBodyContainer.extend`
  align-items: flex-end;
`;

export const OtherMessageBodyContainer = MessageBodyContainer.extend`
  align-items: flex-start;
`;
