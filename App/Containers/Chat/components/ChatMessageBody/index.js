/**
 * @flow
 * @author: Anesh Parvatha
 */
import React, { Component } from "react";
import { observer } from "mobx-react/native";
import type ChatMessage from "@rn/react-native-mqtt-iot/app/stores/models/ChatMessage";
import { SELF } from "@rn/react-native-mqtt-iot/app/constants/ChatConstants";

import ChatTextMessage from "../ChatTextMessage";
import ChatMessageStatus from "../ChatMessageStatus";
import { MessageBody, MultimediaContainerView } from "./styledComponents";
import ImageAttachment from "./ImageAttachment";

type Props = {
  message: ChatMessage
};

const MultimediaContainer = observer(({ type, children }) => (
  <MultimediaContainerView type={type}>{children}</MultimediaContainerView>
));
@observer
class ChatMessageBody extends Component<Props> {
  props: Props;
  renderMessage = (props: Props) => {
    const { message } = props;
    if (message.hasMultimedia) {
      const messages = [];
      messages.push(
        <ChatTextMessage key={message.content} message={message} />
      );
      messages.push(
        message.multimediaList.map((multimedia, index) => {
          // TODO: Change media type to constants
          switch (multimedia.type) {
            case "IMAGE":
              return (
                <MultimediaContainer
                  key={`${index}-${multimedia.url}`} // eslint-disable-line
                  type={message.type}
                >
                  <ImageAttachment multimedia={multimedia} />
                </MultimediaContainer>
              );
            case "VIDEO":
              break;
            case "AUDIO":
              break;
            default:
              return null;
          }
          return null;
        })
      );
      // TODO: implement ChatMultimediaMessage component and render here
      return messages;
    }
    return <ChatTextMessage message={message} />;
  };
  render() {
    let { message } = this.props;
    return (
      <MessageBody>
        {this.renderMessage(this.props)}
        {message.type === SELF ? (
          <ChatMessageStatus status={message.status} />
        ) : null}
      </MessageBody>
    );
  }
}

export default ChatMessageBody;
