/*
 * @flow
 * @Author: Chinmaya
 */

import { StyleSheet } from "react-native";

export default StyleSheet.create({
  imageAttachment: {
    borderRadius: 4,
    width: 240,
    height: 240,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "grey"
  }
});
