/*
 * @flow
 * @Author: Chinmaya
 */

import React, { Component } from "react";
import { observer } from "mobx-react/native";
import { ImageBackground } from "react-native";

import { LargeChatCircularProgress } from "../ChatCircularProgress";
import { ImageAttachmentContainer } from "./styledComponents";
import styles from "./styles";

import type { StyleObj } from "react-native/Libraries/StyleSheet/StyleSheetTypes"; // eslint-disable-line
import type Multimedia from "@rn/react-native-mqtt-iot/app/stores/models/Multimedia"; // eslint-disable-line

type Props = {
  multimedia: Multimedia,
  style?: StyleObj
};

@observer
export class ImageAttachment extends Component<Props> {
  render() {
    const { multimedia, style } = this.props;
    console.log("Image progress", multimedia.url, multimedia.progress);
    return (
      <ImageAttachmentContainer style={style}>
        <ImageBackground
          style={styles.imageAttachment}
          resizeMode="cover"
          source={{
            uri: multimedia.url,
            width: 240,
            height: 240
          }}
        >
          {multimedia.progress === 100 ? null : (
            <LargeChatCircularProgress
              progress={multimedia.progress / 100}
              onPress={() => {}}
            />
          )}
        </ImageBackground>
      </ImageAttachmentContainer>
    );
  }
}

export default ImageAttachment;
