/**
 * @flow
 * @author: Anesh Parvatha
 */

import styled from "styled-components/native";
import { SELF } from "@rn/react-native-mqtt-iot/app/constants/ChatConstants";

export const MessageBody = styled.View`
  margin-top: 5;
  flex-direction: row;
  align-items: flex-end;
`;

export const ImageAttachmentContainer = styled.View`
  border-width: 1;
  border-color: black;
  border-radius: 4;
  padding-left: 5;
  padding-right: 5;
  padding-top: 5;
  padding-bottom: 5;
`;

export const ImageAttachmentView = styled.Image`
  border-radius: 4;
  height: 240;
  width: 240;
`;

export const MultimediaContainerView = styled.View`
  align-self: ${props => (props.type === SELF ? "flex-end" : "flex-start")};
  margin-top: 5;
  margin-bottom: 5;
`;
