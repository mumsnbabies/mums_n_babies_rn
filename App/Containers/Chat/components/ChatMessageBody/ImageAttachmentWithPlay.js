/*
 * @flow
 * @Author: Chinmaya
 */

import * as React from 'react'
import { observer } from 'mobx-react/native'
import { View } from 'react-native'

import type { StyleObj } from 'react-native/Libraries/StyleSheet/StyleSheetTypes' // eslint-disable-line

type Props = {
  url: string, // eslint-disable-line
  style?: StyleObj // eslint-disable-line
}

@observer
export class ImageAttachmentWithPlay extends React.Component<Props> {
  render() {
    return <View />
  }
}

export default ImageAttachmentWithPlay
