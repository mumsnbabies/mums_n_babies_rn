import React, { Component } from "react"
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  TouchableOpacity,
  Alert
} from "react-native"
import { observable } from "mobx"
import { observer } from "mobx-react"
import AuthStore from "../stores/AuthStore"
import DoctorRequestsStore from "../stores/DoctorRequestsStore"
import PatientProfileStore from "../stores/PatientProfileStore"
import DoctorConnectedCard from "../Components/DoctorConnectedCard"
import { Actions as NavigationActions } from "react-native-router-flux"
import Loader from "../Components/Loader"

// Styles
import styles from "./Styles/RequestsStyles"
@observer
export default class ChatsList extends Component {
  @observable animating: Boolean
  @observable searchDoctorName: String
  @observable text: String
  @observable value: String
  constructor(props) {
    super(props)
    searchDoctorName = ""
    this.animating = false
  }

  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    })
  }
  componentDidMount() {
    this.getAcceptedRequests()
  }
  renderTitle = () => {
    return (
      <View style={styles.FindDoctorsHeader}>
        <Text style={styles.FindDoctorsHeaderText}>
          Patient Connect
        </Text>
      </View>
    )
  }
  getAcceptedRequests = () => {
    let search_qu
    if (DoctorRequestsStore.searchOn) {
      search_qu = this.searchDoctorName
    } else {
      search_qu = ""
    }
    let requestObject = {
      search_q: search_qu,
      limit: 10,
      relation_status: "ACCEPTED",
      offset: 0
    }
    DoctorRequestsStore.getDoctorAcceptedRequests(requestObject)
  }
  acceptPatientRequest = requestObject => {
    DoctorRequestsStore.acceptPatientRequest(requestObject)
  }
  displayPatientProfile = requestObject => {
    PatientProfileStore.getPatientProfile(requestObject)
    NavigationActions.PatientProfile()
  }
  showComponents = () => {
    if (
      DoctorRequestsStore.getPendingRequestsStatus ===
      100 /*||
      DoctorRequestsStore.acceptRequestStatus === 100*/
    ) {
      return <Loader animating={true} />
    }

    if (DoctorRequestsStore.getAcceptedRequestsStatus === 100) {
      return <Loader animating={true} />
    }
    if (DoctorRequestsStore.getAcceptedRequestsStatus === 200) {
      if (DoctorRequestsStore.acceptedRequests.total) {
        let { patients } = DoctorRequestsStore.acceptedRequests
        return patients.map((eachPatient, index) => {
          return (
            <DoctorConnectedCard
              key={index}
              details={eachPatient}
              displayPatientProfile={this.displayPatientProfile}
              hideButtons={true}
            />
          )
        })
      }
      return null
    }
  }
  handleSearch = name => {
    this.searchDoctorName = name
  }
  showComponentOn = () => {
    DoctorRequestsStore.searchOn = 1
    this.getAcceptedRequests()
  }
  render() {
    return (
      <View style={styles.MainScreen}>
        <View style={styles.SearchingDoctor}>
          <View style={styles.DocInput}>
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              style={styles.DocInputField}
              placeholder="Search Doctor"
              underlineColorAndroid="transparent"
              onChangeText={this.handleSearch}
            />
          </View>
          <View style={styles.SearchDoctorButton}>
            <TouchableOpacity onPress={() => this.showComponentOn()}>
              <Text style={styles.AddNewPatientText}>
                Search Patient
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          {this.showComponents()}
        </ScrollView>
      </View>
    )
  }
}
