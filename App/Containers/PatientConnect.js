import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

import { Actions as NavigationActions } from "react-native-router-flux";
import _ from "lodash";
import { Actions } from "jumpstate";
import Loader from "../Components/Loader";

// Styles
import styles from "./Styles/PatientConnectStyles";

export default class PatientConnect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: false
    };
  }

  componentWillMount() {
    NavigationActions.refresh({
      renderRightButton: this.ItemDetailsRightButton,
      renderTitle: this.WishListTitle
    });
  }
  WishListTitle = () => {
    return (
      <View style={styles.PatientConnectHeader}>
        <Text style={styles.PatientConnectHeaderText}>
          Patient Connect
        </Text>
      </View>
    );
  };
  start = () => {
    return null;
  };
  render() {
    return (
      <View style={styles.MainScreen}>
        {/*<ScrollView
          style={styles.WishListScrollView}
          showsVerticalScrollIndicator={false}
          >

        </ScrollView>*/}
        <TouchableOpacity
          underlayColor="#eee"
          onPress={() => {
            NavigationActions.ChatsList();
          }}
        >
          <View style={styles.addView}>
            <Text>PatientConnect</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
