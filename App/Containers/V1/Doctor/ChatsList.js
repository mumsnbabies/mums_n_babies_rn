import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  StatusBar,
  Button,
  View,
  TextInput,
  TouchableOpacity,
  Alert
} from "react-native";
import { observable } from "mobx";
import { observer } from "mobx-react";
import AuthStore from "../../../stores/AuthStore";
import PatientChat from "../../../Components/PatientChat";
import DoctorRequestsStore from "../../../stores/DoctorRequestsStore";
import PatientProfileStore from "../../../stores/PatientProfileStore";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../Components/Loader";
import NoDataView from "../../../Components/NoDataView";
import moment from "moment";
import _ from "lodash";
import { chatFlow } from "../../Chat/stores/ChatStore";
import chatStore from "../../Chat/stores/ChatStore";

// Styles
import styles from "./Styles/RequestsStyles";
import { CHATLISTCONSTANTS } from "../../../Constants/Doctor";
@observer
export default class PatientConnect extends Component {
  @observable searchPatientName: String;
  @observable value: String;
  constructor(props) {
    super(props);
    this.searchPatientName = "";
    this.showComponentOn = _.debounce(this.showComponentOn, 300);
  }

  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  async componentDidMount() {
    this.getAcceptedRequests();
    await chatFlow(chatStore);
  }
  renderTitle = () => {
    return (
      <View style={styles.FindDoctorsHeader}>
        <Text style={styles.FindDoctorsHeaderText}>
          {CHATLISTCONSTANTS.CHAT_LIST_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };
  getAcceptedRequests = (searchPatientName = "") => {
    let requestObject = {
      search_q: searchPatientName,
      limit: 10,
      relation_status: "ACCEPTED",
      offset: 0
    };
    DoctorRequestsStore.getDoctorAcceptedRequests(requestObject);
  };
  getPatientChatProfile = user_id => {
    let chatRooms = chatStore.rooms.values();
    let patientProfile = {};
    let chatRoom = {};
    chatRooms.map(eachRoom => {
      eachRoom.members.map(eachMember => {
        if (Number(eachMember.id) === user_id) {
          patientProfile = eachMember;
          chatRoom = eachRoom;
        }
      });
    });
    return { patientProfile, chatRoom };
  };
  gotoChatList = chatRoom => {
    chatStore.setSelectedChatRoom(chatRoom);
    NavigationActions.ChatRoom({ title: chatRoom.name });
  };
  showComponents = () => {
    if (DoctorRequestsStore.getAcceptedRequestsStatus === 200) {
      if (DoctorRequestsStore.acceptedRequests.total === 0) {
        return (
          <View style={styles.noDataViewContainer}>
            <NoDataView />
          </View>
        );
      }
      if (DoctorRequestsStore.acceptedRequests.total) {
        let { patients } = DoctorRequestsStore.acceptedRequests;
        let chatRooms = chatStore.rooms.values();
        let chatList = chatRooms.map(eachChatRoom => {
          return eachChatRoom.members.map((eachMember, index) => {
            let displayChat = false;
            patients.find(eachPatient => {
              if (Number(eachMember.id) === eachPatient.user_id) {
                displayChat = true;
              }
            });
            if (eachChatRoom.name === eachMember.name && displayChat) {
              let patientProfile = eachMember;
              if (__DEV__) console.log(patientProfile);
              if (Object.keys(patientProfile).length > 0) {
                return (
                  <PatientChat
                    gotoChatList={this.gotoChatList}
                    key={index}
                    index={index}
                    item={patientProfile}
                    chatRoom={eachChatRoom}
                  />
                );
              }
            }
          });
        });
        // let chatList = patients.map((eachPatient, index) => {
        //   let { patientProfile, chatRoom } = this.getPatientChatProfile(
        //     eachPatient.user_id
        //   );
        //   if (__DEV__) console.log(patientProfile, chatRoom);
        //   if (Object.keys(patientProfile).length > 0) {
        //     return (
        //       <PatientChat
        //         gotoChatList={this.gotoChatList}
        //         key={index}
        //         index={index}
        //         item={patientProfile}
        //         chatRoom={chatRoom}
        //       />
        //     );
        //   }
        // });
        return chatList;
      }
      return null;
    }
  };
  handleSearch = name => {
    this.searchPatientName = name;
    this.showComponentOn(name);
  };
  showComponentOn = name => {
    this.getAcceptedRequests(name);
  };
  render() {
    return (
      <View style={styles.MainScreen}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <View style={styles.SearchingDoctor}>
          <View style={styles.DocInput}>
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              returnKeyType={"search"}
              style={styles.DocInputField}
              placeholder={CHATLISTCONSTANTS.SEARCH_PATINET_FIELD_PLACEHOLDER}
              underlineColorAndroid="transparent"
              onChangeText={this.handleSearch}
            />
          </View>
        </View>
        {DoctorRequestsStore.getAcceptedRequestsStatus === 100 ||
        chatStore.roomsListStatus === 100 ? (
          <Loader animating={true} />
        ) : (
          <ScrollView showsVerticalScrollIndicator={false}>
            {this.showComponents()}
          </ScrollView>
        )}
      </View>
    );
  }
}

/**
 * <View style={styles.SearchDoctorButton}>
            <TouchableOpacity
              onPress={() => this.showComponentOn(this.searchPatientName)}
            >
              <Text style={styles.AddNewPatientText}>Search Patient</Text>
            </TouchableOpacity>
          </View>
 */
