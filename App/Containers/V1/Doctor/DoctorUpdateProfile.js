import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  StatusBar,
  View,
  TouchableOpacity,
  TextInput,
  Alert,
  Platform
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Actions as NavigationActions } from "react-native-router-flux";
import _ from "lodash";
import { Actions } from "jumpstate";
import Loader from "../../../Components/Loader";
import AuthStore from "../../../stores/AuthStore";
var ImagePicker = require("react-native-image-picker");
import Images from "../../../Themes/Images";

// Styles
import styles from "./Styles/DoctorUpdateProfileStyles";
import { DOCTORUPDATEPROFILECONSTANTS } from "../../../Constants/Doctor";
import { uploadImage } from "./Utils";
@observer
export default class DoctorUpdateProfile extends Component {
  @observable doctor_update_name: String;
  @observable doctor_update_speci: String;
  @observable doctor_update_location: String;
  @observable doctor_update_city: String;
  @observable doctor_update_addr: String;
  @observable doctor_update_geoloc: String;
  @observable required_doctor_name: String;
  @observable required_doctor_speci: String;
  @observable required_doctor_location: String;
  @observable required_doctor_city: String;
  @observable required_doctor_addr: String;
  @observable avatarSource: Object;
  @observable doctor_code: String;
  constructor(props) {
    super(props);
    this.state = {
      imagePickerResponse: null,
      imgSource: "NOIMAGE"
    };
    this.required_doctor_name = null;
    this.required_doctor_speci = null;
    this.required_doctor_location = null;
    this.required_doctor_city = null;
    this.required_doctor_addr = null;
    this.doctor_code = AuthStore.loginResponse.doctor_code;
    this.doctor_update_name = AuthStore.loginResponse.name
      ? AuthStore.loginResponse.name
      : "";
    this.doctor_update_speci = AuthStore.loginResponse.specialization
      ? AuthStore.loginResponse.specialization
      : "";
    this.doctor_update_location = AuthStore.loginResponse.location
      ? AuthStore.loginResponse.location
      : "";
    this.doctor_update_city = AuthStore.loginResponse.city
      ? AuthStore.loginResponse.city
      : "";
    this.doctor_update_addr = AuthStore.loginResponse.address
      ? AuthStore.loginResponse.address
      : "";
    this.doctor_update_geoloc = AuthStore.loginResponse.geo_location
      ? AuthStore.loginResponse.geo_location
      : "";
    this.avatarSource = {
      uri: ""
    };
  }
  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
    if (AuthStore.loginResponse.pic === "") {
      this.avatarSource = {
        uri: ""
      };
    } else {
      this.avatarSource = { uri: AuthStore.loginResponse.pic };
    }
  }
  renderTitle = () => {
    return (
      <View style={styles.UpdateProfileHeader}>
        <Text style={styles.UpdateProfileHeaderText}>
          {DOCTORUPDATEPROFILECONSTANTS.UPDATE_PROFILE_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };

  uploadToS3 = request => {
    uploadImage(this.state.imagePickerResponse, this.state.imgSource, request);
  };

  handleUpdateName = update_name => {
    this.required_doctor_name = null;
    this.doctor_update_name = update_name;
    if (!this.doctor_update_name) {
      this.required_doctor_name = "*";
    }
  };
  handleUpdateSpeci = update_speci => {
    this.required_doctor_speci = null;
    this.doctor_update_speci = update_speci;
    if (!this.doctor_update_speci) {
      this.required_doctor_speci = "*";
    }
  };
  handleUpdateLocation = update_location => {
    this.required_doctor_location = null;
    this.doctor_update_location = update_location;
    if (!this.doctor_update_location) {
      this.required_doctor_location = "*";
    }
  };
  handleUpdateCity = update_city => {
    this.required_doctor_city = null;
    this.doctor_update_city = update_city;
    if (!this.doctor_update_city) {
      this.required_doctor_city = "*";
    }
  };
  handleUpdateAddr = update_addr => {
    this.required_doctor_addr = null;
    this.doctor_update_addr = update_addr;
    if (!this.doctor_update_addr) {
      this.required_doctor_addr = "*";
    }
  };
  handleUpdateGeoloc = update_geoloc => {
    this.doctor_update_geoloc = update_geoloc;
  };
  updateDoctorProfile = () => {
    if (this.doctor_update_name === "") {
      this.required_doctor_name = "*";
    } else if (this.doctor_update_speci === "") {
      this.required_doctor_speci = "*";
    } else if (this.doctor_update_location === "") {
      this.required_doctor_location = "*";
    } else if (this.doctor_update_city === "") {
      this.required_doctor_city = "*";
    } else if (this.doctor_update_addr === "") {
      this.required_doctor_addr = "*";
    } else {
      let requestObject = {
        pic_thumbnail: "",
        city: this.doctor_update_city,
        name: this.doctor_update_name,
        pic: this.avatarSource.uri,
        geo_location: this.doctor_update_geoloc,
        location: this.doctor_update_location,
        address: this.doctor_update_addr,
        specialization: this.doctor_update_speci
      };
      // AuthStore.DoctorUpdateProfileReq(requestObject);
      this.uploadToS3(requestObject);
    }
  };
  uploadPicture = () => {
    ImagePicker.showImagePicker(response => {
      if (response.didCancel) {
        // console.log("User cancelled image picker");
      } else if (response.error) {
        // console.log("ImagePicker Error: ", response.error);
      } else {
        let source = { uri: response.uri };
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.avatarSource = source;
        this.setState({
          imgSource: source.uri,
          imagePickerResponse: response
        });
      }
    });
  };
  render() {
    if (AuthStore.doctorProfileStatus === 200) {
      Alert.alert("Success", "Successfully updated your profile", [
        {
          text: "OK",
          onPress: () => {
            AuthStore.resetUpdateProfileState(),
              NavigationActions.DoctorHomeScreen();
          }
        }
      ]);
    }
    if (AuthStore.doctorProfileStatus === 100) {
      return <Loader animating={true} />;
    }
    if (AuthStore.doctorProfileStatus > 200) {
      Alert.alert(
        DOCTORUPDATEPROFILECONSTANTS.ALERT_ERROR_TITLE_TEXT,
        AuthStore.error.statusText,
        [{ text: "OK", onPress: () => AuthStore.resetUpdateProfileState() }]
      );
    }
    return (
      <View style={styles.MainScreen}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <KeyboardAwareScrollView extraHeight={200}>
          <ScrollView>
            <View style={styles.UpdateProfileImageView}>
              <View style={styles.UpdateProfileImageRelative}>
                {this.avatarSource.uri ? (
                  <Image
                    style={styles.UpdateProfileImage}
                    resizeMode="cover"
                    source={this.avatarSource}
                  />
                ) : (
                  <Image
                    style={styles.UpdateProfileImage}
                    resizeMode="cover"
                    source={Images.femaleDoctorPic}
                  />
                )}
                <TouchableOpacity
                  onPress={() => {
                    this.uploadPicture();
                  }}
                  style={styles.EditProfileImageView}
                >
                  <Image
                    source={Images.profileEdit}
                    style={styles.EditProfileIcon}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.DoctorCodeContainer}>
              <Text style={styles.DoctorCodeField}>
                {DOCTORUPDATEPROFILECONSTANTS.DOCTOR_CODE} {this.doctor_code}
              </Text>
            </View>
            <View style={styles.UpdateProfileInput}>
              {this.doctor_update_name ? (
                <Text style={styles.UpdateProfileTextField}>
                  {DOCTORUPDATEPROFILECONSTANTS.NAME_FIELD_HEADING}
                </Text>
              ) : null}
              <View style={{ flexDirection: "row" }}>
                <TextInput
                  style={styles.UpdateProfileInputField}
                  placeholder={
                    DOCTORUPDATEPROFILECONSTANTS.NAME_FIELD_PLACEHOLDER
                  }
                  underlineColorAndroid="transparent"
                  onChangeText={this.handleUpdateName}
                  autoCorrect={false}
                  value={this.doctor_update_name}
                />
                {this.required_doctor_name && (
                  <View style={styles.ImageContainer}>
                    <Image
                      style={styles.RequiredField}
                      source={Images.alertShield}
                    />
                  </View>
                )}
              </View>
            </View>
            <View style={styles.UpdateProfileInput}>
              {this.doctor_update_speci ? (
                <Text style={styles.UpdateProfileTextField}>
                  {DOCTORUPDATEPROFILECONSTANTS.SPECIALIZATION_FIELD_HEADING}
                </Text>
              ) : null}
              <View style={{ flexDirection: "row" }}>
                <TextInput
                  style={styles.UpdateProfileInputField}
                  placeholder={
                    DOCTORUPDATEPROFILECONSTANTS.SPECIALIZATION_FIELD_PLACEHOLDER
                  }
                  underlineColorAndroid="transparent"
                  onChangeText={this.handleUpdateSpeci}
                  autoCorrect={false}
                  value={this.doctor_update_speci}
                />
                {this.required_doctor_speci && (
                  <View style={styles.ImageContainer}>
                    <Image
                      style={styles.RequiredField}
                      source={Images.alertShield}
                    />
                  </View>
                )}
              </View>
            </View>
            <View style={styles.UpdateProfileInput}>
              {this.doctor_update_location ? (
                <Text style={styles.UpdateProfileTextField}>
                  {DOCTORUPDATEPROFILECONSTANTS.LOCATION_FIELD_HEADING}
                </Text>
              ) : null}
              <View style={{ flexDirection: "row" }}>
                <TextInput
                  style={styles.UpdateProfileInputField}
                  placeholder={
                    DOCTORUPDATEPROFILECONSTANTS.LOCATION_FIELD_PLACEHOLDER
                  }
                  underlineColorAndroid="transparent"
                  onChangeText={this.handleUpdateLocation}
                  autoCorrect={false}
                  value={this.doctor_update_location}
                />
                {this.required_doctor_location && (
                  <View style={styles.ImageContainer}>
                    <Image
                      style={styles.RequiredField}
                      source={Images.alertShield}
                    />
                  </View>
                )}
              </View>
            </View>
            <View style={styles.UpdateProfileInput}>
              {this.doctor_update_city ? (
                <Text style={styles.UpdateProfileTextField}>
                  {DOCTORUPDATEPROFILECONSTANTS.CITY_FIELD_HEADING}
                </Text>
              ) : null}
              <View style={{ flexDirection: "row" }}>
                <TextInput
                  style={styles.UpdateProfileInputField}
                  placeholder={
                    DOCTORUPDATEPROFILECONSTANTS.CITY_FIELD_PLACEHOLDER
                  }
                  underlineColorAndroid="transparent"
                  onChangeText={this.handleUpdateCity}
                  autoCorrect={false}
                  value={this.doctor_update_city}
                />
                {this.required_doctor_city && (
                  <View style={styles.ImageContainer}>
                    <Image
                      style={styles.RequiredField}
                      source={Images.alertShield}
                    />
                  </View>
                )}
              </View>
            </View>
            <View style={styles.UpdateProfileInput}>
              {this.doctor_update_addr ? (
                <Text style={styles.UpdateProfileTextField}>
                  {DOCTORUPDATEPROFILECONSTANTS.ADDRESS_FIELD_HEADING}
                </Text>
              ) : null}
              <View style={{ flexDirection: "row" }}>
                <TextInput
                  style={styles.UpdateProfileInputField}
                  placeholder={
                    DOCTORUPDATEPROFILECONSTANTS.ADDRESS_FIELD_PLACEHOLDER
                  }
                  underlineColorAndroid="transparent"
                  multiline={true}
                  numberOfLines={4}
                  autoCorrect={false}
                  onChangeText={this.handleUpdateAddr}
                  value={this.doctor_update_addr}
                />
                {this.required_doctor_addr && (
                  <View style={styles.ImageContainer}>
                    <Image
                      style={styles.RequiredField}
                      source={Images.alertShield}
                    />
                  </View>
                )}
              </View>
            </View>
            <View style={styles.UpdateProfileInput}>
              {this.doctor_update_geoloc ? (
                <Text style={styles.UpdateProfileTextField}>
                  {DOCTORUPDATEPROFILECONSTANTS.GEO_LOCATION_FIELD_HEADING}
                </Text>
              ) : null}
              <View style={{ flexDirection: "row" }}>
                <TextInput
                  style={styles.UpdateProfileInputField}
                  placeholder={
                    DOCTORUPDATEPROFILECONSTANTS.GEO_LOCATION_FIELD_PLACEHOLDER
                  }
                  underlineColorAndroid="transparent"
                  onChangeText={this.handleUpdateGeoloc}
                  autoCorrect={false}
                  value={this.doctor_update_geoloc}
                />
              </View>
            </View>
            <TouchableOpacity
              underlayColor="#eee"
              activeOpacity={1}
              onPress={() => this.updateDoctorProfile()}
            >
              <View style={styles.UpdateProfileButton}>
                <Text style={styles.UpdateProfileButtonText}>
                  {DOCTORUPDATEPROFILECONSTANTS.UPDATE_PROFILE_BUTTON_TEXT}
                </Text>
              </View>
            </TouchableOpacity>
          </ScrollView>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
