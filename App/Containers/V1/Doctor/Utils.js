import { Platform } from "react-native";
import AuthStore from "../../../stores/AuthStore";
import { Generate64LengthString } from "../../../Utils/UtilsFun";

//import Utils
import AWSS3Utility from "../../../Utils/S3Utils/AWSS3Utility";
import S3Config from "../../../Utils/S3Utils/S3Configs";

export const uploadImage = (imagePickerResponse, imageSource, request) => {
  const awsUploader = new AWSS3Utility(
    S3Config.PatientBucketConfig,
    () => {},
    () => {}
  );
  if (__DEV__) {
    console.log(imageSource, imagePickerResponse);
  }
  if (imageSource === "NOIMAGE") {
    AuthStore.DoctorUpdateProfileReq(request);
    return null;
  }
  if (imageSource != "NOIMAGE" && imagePickerResponse === null) {
    AuthStore.DoctorUpdateProfileReq(request);
    return null;
  }
  if (imagePickerResponse != null) {
    uploadToS3(imagePickerResponse, awsUploader, request);
    return;
  }
};

export const uploadToS3 = (imagePickerResponse, awsUploader, request) => {
  let generatedFileName = "";
  if (imagePickerResponse.fileName) {
    if (Platform.OS === "ios") {
      let splittedValues = imagePickerResponse.fileName.split(".");
      generatedFileName = Generate64LengthString().concat(
        splittedValues[0] + "." + splittedValues[1].toLowerCase()
      );
    } else {
      generatedFileName = Generate64LengthString().concat(
        imagePickerResponse.fileName
      );
    }
  } else {
    generatedFileName = Generate64LengthString().concat("_profile_pic.jpg");
  }
  let changedResponse = {
    fileName: generatedFileName
  };
  const updatedResponse = Object.assign(
    {},
    imagePickerResponse,
    changedResponse
  );
  let options = {
    Bucket: S3Config.PatientBucketConfig.bucketName,
    Key: `alpha/profilePictures/${updatedResponse.fileName}`,
    ContentType: updatedResponse.type,
    ACL: "public-read",
    subscribe: true,
    completionhandler: true,
    path: updatedResponse.path
  };
  if (Platform.OS === "ios") {
    options["ContentType"] = updatedResponse.fileName
      ? updatedResponse.fileName.split(".")[1]
      : "image/jpeg";
    options["path"] = updatedResponse.uri;
  }
  if (__DEV__) {
    console.log(options);
  }
  //TODO: Need to delete whatever is there in bucket,

  //update with new file updateImageURi
  awsUploader.uploadImageFromURI(options, () =>
    uploadImageSuccess(updatedResponse, request)
  );
};

export const uploadImageSuccess = (imagePickerResponse, request) => {
  const { fileName } = imagePickerResponse;
  //Ncall
  let reqobj = AuthStore.loginResponse;

  let documentUrl = `https://s3.ap-south-1.amazonaws.com/mnb-backend-media-static-mumbai/alpha/profilePictures/${fileName}`;
  request["pic"] = documentUrl;
  if (__DEV__) {
    console.log(request);
  }
  AuthStore.DoctorUpdateProfileReq(request);
};
