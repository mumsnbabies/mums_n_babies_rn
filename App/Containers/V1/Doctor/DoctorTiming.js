import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  StatusBar,
  Button,
  View,
  TouchableOpacity,
  TextInput,
  Alert,
  Picker
} from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../Components/Loader";
import AuthStore from "../../../stores/AuthStore";
import moment from "moment";
import ModalPicker from "react-native-modal-picker";
// Styles
import styles from "./Styles/DoctorUpdateTimingsStyles";
import { DOCTORTIMINGCONSTANT } from "../../../Constants/Doctor";
@observer
export default class DoctorTiming extends Component {
  @observable isDateTimePickerVisible: Boolean;
  @observable addfieldStatus: Number;
  @observable showAddField: String;
  @observable ddd: String;
  @observable eee: String;
  @observable date: String;
  @observable se: Number;
  @observable idx: Number;

  constructor(props) {
    super(props);
    this.se = 0;
    this.idx = 0;
    this.showAddField = "";
    this.addfieldStatus = 0;
    this.isDateTimePickerVisible = false;
    this.eee = "";
    this.ddd = "";
    this.date = moment(new Date()).format("YYYY-MM-DD");
  }
  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  componentDidMount() {
    if (AuthStore.event_id > 0) {
      let requestObject = {
        event_id: AuthStore.event_id
      };
      AuthStore.getEventDetail(requestObject);
    }
  }
  renderTitle = () => {
    return (
      <View style={styles.UpdateTimingsHeader}>
        <Text style={styles.UpdateTimingsHeaderText}>
          {DOCTORTIMINGCONSTANT.DOCTOR_TIMING_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };
  handleDay = (option, id) => {
    AuthStore.timings[id].title = option.label;
  };
  getOccurrences = () => {
    let occurrences = [];
    AuthStore.timings.map((eachDay, index) => {
      if (eachDay.start && eachDay.end && eachDay.title) {
        occurrences.push({
          start: this.date + " " + eachDay.start,
          end: this.date + " " + eachDay.end,
          title: eachDay.title,
          description: ""
        });
      }
    });
    return occurrences;
  };
  checkDoctorTimings = () => {
    let status = 1;
    AuthStore.timings.map((eachDay, index) => {
      if (!eachDay.start || !eachDay.end || !eachDay.title) {
        status = 0;
      }
    });
    return status;
  };

  updatetimingRequest = () => {
    let flag = this.checkDoctorTimings();
    if (flag) {
      if (AuthStore.event_id === 0) {
        let requestObject1 = {
          entity_id: AuthStore.loginResponse.doctor_id,
          entity_type: "DOCTOR_TIMINGS",
          title: AuthStore.loginResponse.name,
          occurrences: this.getOccurrences()
        };
        AuthStore.updatetiming(requestObject1);
      }
      if (AuthStore.event_id !== 0) {
        let requestObject = {
          title: AuthStore.loginResponse.name,
          occurrences: this.getOccurrences()
        };
        AuthStore.updatetiming1(requestObject);
      }
    } else {
      alert("Please Fill all the Field");
      return;
    }
    this.addfieldStatus = 0;
  };

  _showDateTimePicker = (index, se) => {
    this.se = se;
    this.idx = index;
    this.isDateTimePickerVisible = true;
  };

  _hideDateTimePicker = () => {
    this.isDateTimePickerVisible = false;
  };

  _handleDatePicked = date => {
    if (this.se === 1) {
      AuthStore.timings[this.idx].start = moment(date).format("HH:mm");
    } else if (this.se === 2) {
      AuthStore.timings[this.idx].end = moment(date).format("HH:mm");
    }
    this._hideDateTimePicker();
  };
  addField = () => {
    //this.addfieldStatus = 1;
    let newfield = {
      id: -1,
      start: "",
      end: "",
      title: ""
    };
    AuthStore.timings.push(newfield);
  };
  delField = id => {
    AuthStore.timings.map((eachDay, index) => {
      if (index === id) {
        AuthStore.timings.splice(index, 1);
      }
    });
  };
  render() {
    if (AuthStore.updatetimingStatus === 200) {
      Alert.alert(
        DOCTORTIMINGCONSTANT.ALERT_SUCCESS_TITLE_TEXT,
        DOCTORTIMINGCONSTANT.ALERT_SUCCESS_UPDATE_TEXT,
        [
          {
            text: "OK",
            onPress: () => NavigationActions.popTo("DoctorHomeScreen")
          }
        ]
      );
    }
    if (AuthStore.updatetimingStatus === 100) {
      return <Loader animating={true} />;
    }
    if (AuthStore.updatetimingStatus > 200) {
      Alert.alert(
        DOCTORTIMINGCONSTANT.ALERT_ERROR_TITLE_TEXT,
        AuthStore.error.statusText,
        [{ text: "OK", onPress: () => AuthStore.resetUpdatetimingState() }]
      );
    }
    let data = [
      { label: "Sunday" },
      { label: "Monday" },
      { label: "Tuesday" },
      { label: "Wednesday" },
      { label: "Thursday" },
      { label: "Friday" },
      { label: "Saturday" }
    ];

    return (
      <View style={styles.MainScreen}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <DateTimePicker
          mode="time"
          isVisible={this.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          {AuthStore.timings.map((eachDay, index) => {
            return (
              <View key={index} style={styles.EachInputField}>
                <View style={styles.DayMinus}>
                  <View style={styles.UpdateTimingsInputDay}>
                    <ModalPicker
                      data={data}
                      initValue={
                        eachDay.title ||
                        DOCTORTIMINGCONSTANT.SELECT_DAY_PICKER_TEXT
                      }
                      onChange={option => this.handleDay(option, index)}
                      value={eachDay.title}
                    />
                  </View>
                  <TouchableOpacity onPress={() => this.delField(index)}>
                    <View style={styles.CircleMinusView}>
                      <Text style={styles.CircleMinusText}>-</Text>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={styles.FromToView}>
                  <View style={styles.ShowingTimingsInput}>
                    <TouchableOpacity
                      onPress={() => this._showDateTimePicker(index, 1)}
                    >
                      {AuthStore.timings[index].start ? (
                        <Text style={styles.UpdateTimingsInputField}>
                          {AuthStore.timings[index].start || "From"}
                        </Text>
                      ) : (
                        <Text style={styles.UpdateTimingsInitalField}>
                          From
                        </Text>
                      )}
                    </TouchableOpacity>
                  </View>

                  <View style={styles.ShowingTimingsInput}>
                    <TouchableOpacity
                      onPress={() => this._showDateTimePicker(index, 2)}
                    >
                      {AuthStore.timings[index].end ? (
                        <Text style={styles.UpdateTimingsInputField}>
                          {AuthStore.timings[index].end}
                        </Text>
                      ) : (
                        <Text style={styles.UpdateTimingsInitalField}>To</Text>
                      )}
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            );
          })}
        </ScrollView>
        <TouchableOpacity
          underlayColor="#eee"
          activeOpacity={1}
          onPress={this.updatetimingRequest}
        >
          <View style={styles.UpdateTimingsButton}>
            <Text style={styles.UpdateTimingsButtonText}>
              {DOCTORTIMINGCONSTANT.UPDATE_TIMINGS_BUTTON_TEXT}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.addField} style={styles.CircleButton}>
          <Text style={styles.CircleButtonText}>+</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
