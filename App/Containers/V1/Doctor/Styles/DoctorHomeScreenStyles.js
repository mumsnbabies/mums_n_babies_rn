import { StyleSheet, Platform, Dimensions } from "react-native";
import { Metrics } from "../../../../Themes";
import Fonts from "../../../../Themes/Fonts";
import Colors from "../../../../Themes/Colors";
const x = Dimensions.get("window").width;
const y = Dimensions.get("window").height;

export default StyleSheet.create({
  MainAppName: {
    alignItems: "center",
    justifyContent: "center"
  },
  MainAppNameText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold,
    marginLeft: 15
  },
  DropDownButton: {
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10
  },
  modal: {
    justifyContent: "center",
    alignItems: "center"
  },
  btn: {
    margin: 10,
    backgroundColor: Colors.background,
    color: Colors.blackColor,
    padding: 10
  },
  MainScreen: {
    flex: 1,
    backgroundColor: Colors.whiteColor
  },
  Container: {
    flex: 1
  },
  MainImageView: {
    height: 300,
    marginBottom: 20
  },
  MainImage: {
    flex: 1,
    width: "100%"
  },
  SectionView: {
    backgroundColor: Colors.primaryColor,
    height: x / 2 - 30,
    width: x / 2 - 30,
    elevation: 3,
    borderRadius: 5,
    margin: 10,
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  SectionIconView: {
    height: 35,
    width: 35,
    marginBottom: 5
  },
  SectionRowHolder: {
    flexDirection: "row",
    flex: 1,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center"
  },
  SectionImage: {
    height: 35,
    width: 35
  },
  SectionText: {
    color: Colors.whiteColor,
    // marginTop: 5,
    fontSize: Fonts.sizes.normal,
    fontFamily: Fonts.family.fontFamilyBold
  }
});
