import { StyleSheet, Platform } from "react-native";
import Fonts from "../../../../Themes/Fonts";
import Colors from "../../../../Themes/Colors";
export default StyleSheet.create({
  UpdateTimingsHeader: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.whiteColor
  },
  UpdateTimingsHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  MainScreen: {
    flex: 1,
    backgroundColor: Colors.whiteColor
  },
  UpdateTimingsHeading: {
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  FromToInputFields: {
    flexDirection: "row"
  },
  FromToInputField: {
    marginLeft: 10,
    marginRight: 10,
    height: 50
  },
  WeekDay: {
    fontSize: Fonts.sizes.large,
    marginLeft: 10,
    marginTop: 20,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  EachInputField: {
    // flexDirection: "row",
    marginBottom: 10
  },
  UpdateTimingsInputDay: {
    marginLeft: 5,
    marginRight: 5,
    borderColor: Colors.blackColor,
    borderWidth: 0.5,
    borderRadius: 5,
    marginTop: 10,
    width: "48%"
  },
  DayMinus: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  CircleMinus: {
    justifyContent: "flex-end",
    marginRight: 10
  },
  FromToView: {
    flexDirection: "row"
  },
  CircleMinusView: {
    justifyContent: "center",
    alignItems: "center",
    height: 40,
    width: 40,
    borderRadius: 100,
    backgroundColor: Colors.primaryColor,
    marginRight: 5
  },
  CircleMinusText: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.h4
    //marginBottom: 8
  },
  UpdateTimingsInput: {
    marginLeft: 10,
    marginRight: 10,
    borderColor: Colors.lightblack,
    borderWidth: 0.5,
    borderRadius: 5,
    marginTop: 10
  },
  ShowingTimingsInput: {
    marginLeft: 5,
    marginRight: 5,
    borderColor: Colors.blackColor,
    borderWidth: 0.5,
    borderRadius: 5,
    marginTop: 10,
    flex: 2
  },
  UpdateTimingsInputField: {
    color: Colors.blackColor,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 20,
    height: 20,
    fontFamily: Fonts.family.fontfamily
  },
  UpdateTimingsInitalField: {
    color: Colors.lightblack,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 20,
    height: 20,
    fontFamily: Fonts.family.fontfamily
  },
  UpdateTimingsInputUnderline: {
    flex: 1,
    borderBottomWidth: 0.7,
    borderColor: Colors.blackColor
  },
  UpdateTimingsButton: {
    marginTop: 20,
    alignSelf: "center",
    alignItems: "center",
    marginBottom: 40,
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 60,
    paddingRight: 60,
    backgroundColor: Colors.primaryColor,
    borderRadius: 60
  },
  UpdateTimingsButtonText: {
    fontFamily: Fonts.family.fontfamily,
    alignItems: "center",
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.normal
  },
  AddingInputField: {
    flexDirection: "column"
  },
  CircleButton: {
    position: "absolute",
    width: 50,
    height: 50,
    borderRadius: 25,
    bottom: 10,
    right: 5,
    backgroundColor: Colors.primaryColor,
    justifyContent: "center",
    alignItems: "center"
  },
  CircleButtonText: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.h2,
    marginBottom: 4
  },
  CircleButton1: {
    position: "absolute",
    width: 20,
    height: 20,
    borderRadius: 14,
    top: 25,
    right: 7,
    backgroundColor: Colors.primaryColor,
    justifyContent: "center",
    alignItems: "center"
  },
  AddingInputField1: {
    flexDirection: "row"
  },
  CircleButtonText1: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.h3,
    paddingTop: 0
  }
});
