import { StyleSheet, Platform } from "react-native";
import Fonts from "../../../../Themes/Fonts";
import Colors from "../../../../Themes/Colors";
export default StyleSheet.create({
  MainScreen: {
    backgroundColor: Colors.primaryColor,
    paddingTop: 50,
    flex: 1
  },
  MyProfileHeader: {
    marginTop: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  MyProfileHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  QuickRecordsContainer: {
    flexDirection: "row",
    height: 120,
    marginLeft: 30,
    marginRight: 30,
    marginTop: 40,
    marginBottom: 10,
    elevation: 2,
    justifyContent: "center"
  },
  Cards: {
    justifyContent: "center",
    alignItems: "center",
    height: 100,
    width: 120,
    backgroundColor: Colors.whiteColor,
    borderRadius: 5,
    elevation: 2,
    marginRight: 5
  },
  CardImage: {
    width: 40,
    height: 40
  },
  CardName: {
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold,
    marginTop: 8
  }
});
