import { StyleSheet, Platform } from "react-native";
import Fonts from "../../../../Themes/Fonts";
import Colors from "../../../../Themes/Colors";
export default StyleSheet.create({
  FindDoctorsHeader: {
    alignItems: "center",
    justifyContent: "center"
  },
  SearchDoctorButton: {
    alignItems: "flex-end",
    alignSelf: "flex-end",
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10,
    borderRadius: 20,
    backgroundColor: Colors.background1,
    overflow: "hidden"
  },
  SearchingDoctor: {
    marginBottom: 10,
    marginTop: Platform.OS === "ios" ? 20 : 0
  },
  DocInput: {
    borderColor: Colors.blackColor,
    marginLeft: 10,
    marginRight: 10,
    borderWidth: 0.5,
    // padding: 10,
    marginTop: 10,
    borderRadius: 5
  },
  DocInputField: {
    color: Colors.blackColor,
    marginLeft: 5,
    height: 40,
    fontFamily: Fonts.family.fontfamily
  },
  FindDoctorsHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  ChatBtns: {
    flexDirection: "row",
    height: 30,
    marginLeft: 10,
    marginRight: 5,
    marginTop: Platform.OS === "ios" ? 20 : 10,
    marginBottom: 10,
    elevation: 2,
    justifyContent: "center"
  },
  Underline: {
    borderBottomWidth: 0.7,
    borderBottomColor: Colors.bottom,
    marginTop: 5

    // marginBottom: 5
  },
  MainScreen: {
    flex: 1,
    backgroundColor: Colors.whiteColor
  },
  AcceptBtn: {
    backgroundColor: Colors.requestButtom,
    borderColor: Colors.requestButtom,
    borderWidth: 0.5,
    borderRadius: 40,
    flex: 1,
    alignItems: "center",
    marginRight: 5,
    justifyContent: "center"
  },
  AcceptOpenBtn: {
    backgroundColor: Colors.whiteColor,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginRight: 5,
    borderRadius: 40,
    borderColor: Colors.blackColor,
    borderWidth: 1
  },
  AcceptOpenBtnText: {
    color: Colors.blackColor,
    fontSize: Fonts.sizes.mediumsmall,
    fontFamily: Fonts.family.fontFamilyBold
  },
  AcceptBtnText: {
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.mediumsmall,
    fontFamily: Fonts.family.fontFamilyBold
  },
  AddNewPatient: {
    marginTop: 5,
    marginBottom: 5,
    flexDirection: "row",
    justifyContent: "flex-end",
    marginRight: 10
  },
  AddNewPatientText: {
    paddingTop: 6,
    paddingBottom: 6,
    paddingLeft: 30,
    paddingRight: 30,
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.normal,
    fontFamily: Fonts.family.fontfamily
  },
  ChatContainer: {
    borderWidth: 0.3,
    borderColor: Colors.blackColor,
    marginBottom: 10
  },
  tab2: {
    flex: 1,
    backgroundColor: Colors.secondaryColor,
    height: 20,
    alignItems: "center",
    justifyContent: "center",
    marginRight: 5,
    borderRadius: 5
  },
  daycount2: {
    justifyContent: "center",
    alignItems: "center",
    height: 22,
    width: 130,
    backgroundColor: Colors.secondaryColor,
    borderRadius: 30,
    elevation: 2,
    left: 235,
    marginBottom: 15
  },
  inputField: {
    height: 45,
    width: 355,
    color: Colors.blackColor,
    marginLeft: 20
  },
  input: {
    marginLeft: 10,
    width: 355,
    borderColor: Colors.blackColor,
    borderWidth: 0.5,
    borderRadius: 5,
    marginBottom: 10
  },
  container: {
    flex: 1
  },
  //chat component styles///
  ChatListSection: {
    height: 80,
    flexDirection: "row",
    padding: 5,
    marginBottom: 15
  },
  ChatLeftTopSection: {
    width: 50,
    height: 50,
    marginRight: 15
  },
  personImage: {
    width: 40,
    height: 40,
    margin: 5
  },
  personName: {
    color: Colors.blackColor,
    fontSize: Fonts.sizes.normal,
    fontFamily: Fonts.family.fontfamily
  },
  personDetails: {
    color: Colors.blackColor,
    fontSize: Fonts.size.mini,
    fontFamily: Fonts.family.fontfamily
  },
  ChatRightSection: {
    flex: 1,
    flexDirection: "column"
  },
  ChatRightTopSection: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  ChatRightBottomSection: {
    marginTop: 10
  },

  //chat component styles end//

  noDataViewContainer: {
    justifyContent: "center",
    alignItems: "center",
    width: 300,
    height: 150,
    marginHorizontal: 30
  }
});
