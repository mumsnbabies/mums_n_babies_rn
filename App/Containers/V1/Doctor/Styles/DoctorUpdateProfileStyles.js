import { StyleSheet, Platform } from "react-native";
import Fonts from "../../../../Themes/Fonts";
import Colors from "../../../../Themes/Colors";
export default StyleSheet.create({
  UpdateProfileHeader: {
    alignItems: "center",
    justifyContent: "center"
  },
  UpdateProfileHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  MainScreen: {
    flex: 1,
    backgroundColor: Colors.whiteColor
  },
  UpdateProfileImageView: {
    marginTop: 20,
    alignItems: "center",
    justifyContent: "center",
    position: "relative"
  },
  UpdateProfileImageRelative: {
    marginBottom: 30,
    height: 140,
    width: 140
  },
  UpdateProfileTextField: {
    marginTop: "2%",
    marginLeft: 15,
    color: Colors.lightblack,
    fontFamily: Fonts.family.fontFamilyBold
  },
  DoctorCodeField: {
    marginLeft: 15,
    textAlign: "center",
    color: Colors.primaryColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  UpdateProfileImage: {
    height: 140,
    width: 140,
    borderRadius: 70,
    borderColor: Colors.graycolor,
    borderWidth: 0.2
  },
  EditProfileImageView: {
    borderRadius: 40,
    position: "absolute",
    backgroundColor: Colors.primaryColor,
    alignItems: "center",
    justifyContent: "center",
    bottom: 8,
    right: 8,
    marginBottom: -20,
    marginRight: -15,
    padding: 15
  },
  EditProfileIcon: {
    justifyContent: "center",
    alignItems: "center"
  },
  RequiredField: {
    width: 20,
    height: 20
  },
  ImageContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-end",
    marginRight: 15
  },
  DoctorCodeContainer: {
    justifyContent: "center",
    marginRight: 15
  },
  UpdateProfileInput: {
    marginLeft: 10,
    marginRight: 10,
    borderColor: Colors.blackColor,
    borderWidth: 0.5,
    borderRadius: 5,
    marginTop: 10
  },
  UpdateProfileInputField: {
    width: "80%",
    color: Colors.blackColor,
    marginLeft: 20,
    height: 50,
    fontFamily: Fonts.family.fontfamily
  },
  UpdateProfileInputUnderline: {
    flex: 1,
    borderBottomWidth: 0.7,
    borderColor: Colors.graycolor
  },
  UpdateProfileButton: {
    marginTop: 20,
    alignSelf: "center",
    alignItems: "center",
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 60,
    paddingRight: 60,
    backgroundColor: Colors.primaryColor,
    borderRadius: 60,
    marginBottom: 20
  },
  UpdateProfileButtonText: {
    fontFamily: Fonts.family.fontfamily,
    alignItems: "center",
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.normal
  }
});
