import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  AsyncStorage,
  TouchableOpacity,
  Platform,
  TextInput,
  StatusBar,
  Alert,
  Keyboard
} from "react-native";
//import PatientProfileStore from "../../../../stores/PatientProfileStore"
import DateTimePicker from "react-native-modal-datetime-picker";
import PopupMenu from "../../../../Components/PopupMenu";
import DoctorTiming from "../DoctorTiming";
import PopupMenuIOS from "../../../../Components/PopupMenuIOS";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../../Components/Loader";
import { observable } from "mobx";
import { observer } from "mobx-react";
import AuthStore from "../../../../stores/AuthStore";
import Images from "../../../../Themes/Images";
import { Fonts, Colors } from "../../../../Themes/";
import ModalPicker from "react-native-modal-picker";
import moment from "moment";
// Styles
import styles from "./styles.js";
import { DOCTORWELLCOMESCREENCONSTANTS } from "../../../../Constants/Doctor";
@observer
export default class DoctorWelcomeScreen extends Component {
  @observable timings: Array;
  @observable se: Number;
  @observable idx: Number;
  @observable specialization: String;
  @observable isDateTimePickerVisible: Boolean;
  constructor(props) {
    super(props);
    this.timings = [];
    this.se = 0;
    this.idx = 0;
    this.specialization = "";
    this.isDateTimePickerVisible = false;
  }

  handleSpecialization = specialization => {
    this.specialization = specialization;
  };
  getOccurrences = () => {
    let occurrences = [];
    this.timings.map((eachDay, index) => {
      if (eachDay.start && eachDay.end && eachDay.title) {
        occurrences.push({
          start: moment(new Date()).format("YYYY-MM-DD") + " " + eachDay.start,
          end: moment(new Date()).format("YYYY-MM-DD") + " " + eachDay.end,
          title: eachDay.title,
          description: ""
        });
      }
    });
    return occurrences;
  };
  checkDoctorTimings = () => {
    let flag = 1;
    this.timings.map((eachDay, index) => {
      if (!eachDay.start || !eachDay.end || !eachDay.title) {
        flag = 0;
      }
    });
    return flag;
  };
  handleCreateProfile = () => {
    let flag = this.checkDoctorTimings();
    if (!flag) {
      alert("Please Fill/delete the timings detials");
      return;
    }
    if (!this.specialization) {
      return;
    } else {
      let requestObject1 = {
        entity_id: AuthStore.loginResponse.doctor_id,
        entity_type: "DOCTOR_TIMINGS",
        title: AuthStore.loginResponse.name,
        occurrences: this.getOccurrences()
      };
      AuthStore.updatetiming(requestObject1);
      let requestObject = {
        pic_thumbnail: "",
        city: "",
        name: AuthStore.loginResponse.name,
        pic: "",
        geo_location: "",
        location: "",
        address: "",
        specialization: this.specialization
      };

      AuthStore.DoctorUpdateProfileReq(requestObject);
    }
  };
  navigateToCreateProfile = () => {
    this.specialization = "";
    this.timings = [];
    AuthStore.resetUpdateProfileState();
    AuthStore.resetUpdatetimingState();
    NavigationActions.DoctorHomeScreen({ type: "reset" });
  };
  handleDay = (option, id) => {
    this.timings[id].title = option.label;
  };
  addField = () => {
    let newfield = {
      id: -1,
      start: "",
      end: "",
      title: ""
    };
    this.timings.push(newfield);
  };
  delField = id => {
    this.timings.map((eachDay, index) => {
      if (index === id) {
        this.timings.splice(index, 1);
      }
    });
  };
  _showDateTimePicker = (index, se) => {
    Keyboard.dismiss();
    this.se = se;
    this.idx = index;
    this.isDateTimePickerVisible = true;
  };

  _hideDateTimePicker = () => {
    this.isDateTimePickerVisible = false;
  };

  _handleDatePicked = date => {
    if (this.se === 1) {
      this.timings[this.idx].start = moment(date).format("HH:mm");
    } else if (this.se === 2) {
      this.timings[this.idx].end = moment(date).format("HH:mm");
    }
    this._hideDateTimePicker();
  };
  doctorTiming = () => {
    let data = [
      { label: "Sunday" },
      { label: "Monday" },
      { label: "Tuesday" },
      { label: "Wednesday" },
      { label: "Thursday" },
      { label: "Friday" },
      { label: "Saturday" }
    ];
    return this.timings.map((eachDay, index) => {
      return (
        <View key={index} style={styles.EachInputField}>
          <DateTimePicker
            mode="time"
            isVisible={this.isDateTimePickerVisible}
            onConfirm={this._handleDatePicked}
            onCancel={this._hideDateTimePicker}
          />
          <View style={styles.DayMinus}>
            <View style={styles.UpdateTimingsInputDay}>
              <ModalPicker
                data={data}
                initValue={
                  eachDay.title ||
                  DOCTORWELLCOMESCREENCONSTANTS.SELECT_DAY_INITVALUE_TEXT
                }
                onChange={option => this.handleDay(option, index)}
                value={eachDay.title}
              />
            </View>
            <View style={styles.CircleMinusView}>
              <TouchableOpacity onPress={() => this.delField(index)}>
                <Text style={styles.CircleMinusText}>-</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.FromToView}>
            <View style={styles.ShowingTimingsInput}>
              <TouchableOpacity
                onPress={() => this._showDateTimePicker(index, 1)}
              >
                {this.timings[index].start ? (
                  <Text style={styles.UpdateTimingsInputField}>
                    {this.timings[index].start || "From"}
                  </Text>
                ) : (
                  <Text style={styles.UpdateTimingsInitalField}>From</Text>
                )}
              </TouchableOpacity>
            </View>
            <View style={styles.ShowingTimingsInput}>
              <TouchableOpacity
                onPress={() => this._showDateTimePicker(index, 2)}
              >
                {this.timings[index].end ? (
                  <Text style={styles.UpdateTimingsInputField}>
                    {this.timings[index].end}
                  </Text>
                ) : (
                  <Text style={styles.UpdateTimingsInitalField}>To</Text>
                )}
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    });
  };
  render() {
    const submitBtnStyles = () => {
      return this.specialization !== ""
        ? [styles.btnContainer]
        : [styles.btnContainer, styles.disabledBtn];
    };
    if (
      AuthStore.updatetimingStatus === 100 ||
      AuthStore.doctorProfileStatus === 100
    ) {
      return <Loader animating={true} />;
    } else if (
      AuthStore.updatetimingStatus === 200 &&
      AuthStore.doctorProfileStatus === 200
    ) {
      Alert.alert(
        DOCTORWELLCOMESCREENCONSTANTS.ALERT_SUCCESS_TITLE_TEXT,
        DOCTORWELLCOMESCREENCONSTANTS.ALERT_SUCCESS_UPDATE_TEXT,
        [{ text: "OK", onPress: () => this.navigateToCreateProfile() }]
      );
    } else if (
      AuthStore.updatetimingStatus > 200 ||
      AuthStore.doctorProfileStatus > 200
    ) {
      Alert.alert(
        DOCTORWELLCOMESCREENCONSTANTS.ALERT_ERROR_TITLE_TEXT,
        AuthStore.error.statusText,
        [
          {
            text: "OK",
            onPress: () => {
              AuthStore.resetUpdateProfileState();
              AuthStore.resetUpdatetimingState();
            }
          }
        ]
      );
    }
    if (!AuthStore.loginResponse.name) {
      return <Loader animating={true} />;
    }
    return (
      <View style={styles.mainContainer}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.headingContainer}>
            <Text style={styles.headingStyles}>
              {DOCTORWELLCOMESCREENCONSTANTS.WELCOME_TEXT}
            </Text>
            <Text style={styles.headingStyles}>
              {AuthStore.loginResponse.name}
            </Text>
          </View>
          <View style={styles.DoctorCodeContainer}>
            <Text style={styles.DoctorCodeText}>Your Code:</Text>
            <Text style={styles.DoctorCode}>
              {AuthStore.loginResponse.doctor_code}
            </Text>
          </View>
          <View style={styles.bodyContainer}>
            <View style={styles.formHeadingContainer}>
              <Text style={styles.formHeadingStyles}>
                {DOCTORWELLCOMESCREENCONSTANTS.PLEASE_FILL_DETAILS_NOTE}
              </Text>
            </View>
            <View style={styles.formContainer}>
              <View style={styles.eachFormField}>
                <View style={styles.textinputImageField}>
                  <TextInput
                    autoCorrect={false}
                    returnKeyType={"next"}
                    placeholder={
                      DOCTORWELLCOMESCREENCONSTANTS.SPACIALIZATION_PLACEHOLDER_TEXT
                    }
                    style={styles.inputField}
                    underlineColorAndroid="transparent"
                    onChangeText={this.handleSpecialization}
                    value={this.specialization}
                  />
                  {this.specialization === "" ? (
                    <View style={styles.ImageContainer}>
                      <Image
                        style={styles.RequiredField}
                        source={Images.alertShield}
                      />
                    </View>
                  ) : null}
                </View>
              </View>
            </View>
            <View style={styles.yourTimingsCircleButtonContainer}>
              <View style={styles.yourTimingsContainer}>
                <Text style={styles.yourTimingsText}>
                  {DOCTORWELLCOMESCREENCONSTANTS.YOUR_TIMINGS_HEADING}
                </Text>
              </View>
              <TouchableOpacity
                onPress={this.addField}
                style={styles.CircleButton}
              >
                <Text style={styles.CircleButtonText}>+</Text>
              </TouchableOpacity>
            </View>
          </View>
          {this.doctorTiming()}
        </ScrollView>
        <View style={styles.footerContainer}>
          <TouchableOpacity onPress={this.handleCreateProfile}>
            <View style={submitBtnStyles()}>
              <Text style={styles.btnTextStyles}>
                {DOCTORWELLCOMESCREENCONSTANTS.CREATE_PROFILE_BUTTON_TEXT}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
