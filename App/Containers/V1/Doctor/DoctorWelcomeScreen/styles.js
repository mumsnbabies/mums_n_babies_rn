import { StyleSheet, Platform } from "react-native";
import { Fonts, Colors } from "../../../../Themes/";

export default StyleSheet.create({
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  mainContainer: {
    flex: 1,
    justifyContent: "center",
    marginTop: 20
  },
  headingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 15
  },
  headingStyles: {
    fontSize: Fonts.sizes.heading,
    color: Colors.primaryColor,
    fontFamily: Fonts.family.fontfamily
  },
  DoctorCodeContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  DoctorCodeText: {
    fontFamily: Fonts.family.fontfamily,
    color: Colors.lightblack,
    fontSize: Fonts.sizes.normal
  },
  DoctorCode: {
    fontFamily: Fonts.family.fontFamilyBold,
    color: Colors.primaryColor,
    fontSize: Fonts.size.input,
    paddingLeft: 10
  },
  bodyContainer: {
    flex: 1,
    marginBottom: 20,
    marginTop: 20,
    marginHorizontal: 10
  },
  formHeadingContainer: {
    marginBottom: 10
  },
  formHeadingStyles: {
    fontSize: Fonts.sizes.large,
    color: Colors.primaryColor,
    fontFamily: Fonts.family.fontfamily
  },
  formContainer: {
    borderWidth: 0.8,
    borderRadius: 4,
    borderColor: Colors.lightblack,
    flex: 1
  },
  eachFormField: {
    flexDirection: "column",
    flex: 1,
    borderBottomWidth: 0.9,
    borderColor: Colors.lightblack
  },
  textinputImageField: {
    flexDirection: "row"
  },
  inputField: {
    flex: 4,
    marginLeft: 10,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  ImageContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  RequiredField: {
    width: 20,
    height: 20
  },
  footerContainer: {
    height: 55
  },
  btnContainer: {
    height: 55,
    backgroundColor: "#e91e63",
    justifyContent: "center",
    alignItems: "center"
  },
  disabledBtn: {
    backgroundColor: Colors.lightblack
  },
  btnTextStyles: {
    fontSize: Fonts.sizes.large,
    color: Colors.whiteColor,
    fontFamily: Fonts.family.fontfamily
  },
  UpdateTimingsHeader: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: Platform.OS === "ios" ? 30 : 15,
    backgroundColor: Colors.whiteColor
  },
  UpdateTimingsHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  MainScreen: {
    flex: 1,
    paddingTop: 64
  },
  UpdateTimingsHeading: {
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  FromToInputFields: {
    flexDirection: "row"
  },
  FromToInputField: {
    marginLeft: 10,
    marginRight: 10,
    height: 50
  },
  WeekDay: {
    fontSize: Fonts.sizes.large,
    marginLeft: 10,
    marginTop: 20,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  EachInputField: {
    // flexDirection: "row",
    marginBottom: 10
  },
  UpdateTimingsInputDay: {
    marginLeft: 5,
    marginRight: 5,
    borderColor: Colors.blackColor,
    borderWidth: 0.5,
    borderRadius: 5,
    marginTop: 10,
    width: "48%"
  },
  DayMinus: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  CircleMinus: {
    justifyContent: "flex-end",
    marginRight: 10
  },
  FromToView: {
    flexDirection: "row"
  },
  CircleMinusView: {
    justifyContent: "center",
    alignItems: "center",
    height: 40,
    width: 40,
    borderRadius: 100,
    backgroundColor: Colors.primaryColor,
    marginRight: 5
  },
  CircleMinusText: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.h4,
    marginBottom: 8
  },
  UpdateTimingsInput: {
    marginLeft: 10,
    marginRight: 10,
    borderColor: Colors.lightblack,
    borderWidth: 0.5,
    borderRadius: 5,
    marginTop: 10
  },
  ShowingTimingsInput: {
    marginLeft: 5,
    marginRight: 5,
    borderColor: Colors.blackColor,
    borderWidth: 0.5,
    borderRadius: 5,
    marginTop: 10,
    flex: 2
  },
  UpdateTimingsInputField: {
    color: Colors.blackColor,
    marginTop:10,
    marginBottom:10,
    marginLeft: 20,
    height: 20,
    fontFamily: Fonts.family.fontfamily
  },
  UpdateTimingsInitalField: {
    color: Colors.lightblack,
    marginTop:10,
    marginBottom:10,
    marginLeft: 20,
    height: 20,
    fontFamily: Fonts.family.fontfamily
  },
  UpdateTimingsInputUnderline: {
    flex: 1,
    borderBottomWidth: 0.7,
    borderColor: Colors.blackColor
  },
  UpdateTimingsButton: {
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 40
  },
  UpdateTimingsButtonText: {
    justifyContent: "center",
    backgroundColor: Colors.primaryColor,
    fontFamily: Fonts.family.fontfamily,
    borderRadius: 60,
    alignItems: "center",
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.normal,
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 60,
    paddingRight: 60
  },
  AddingInputField: {
    flexDirection: "column"
  },
  yourTimingsText: {
    fontSize: Fonts.sizes.large,
    color: Colors.primaryColor,
    fontFamily: Fonts.family.fontfamily
  },
  yourTimingsCircleButtonContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10
  },
  CircleButton: {
    backgroundColor: Colors.primaryColor,
    width: 50,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 25
  },
  CircleButtonText: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.h2,
    marginBottom: 4
  },
  CircleButton1: {
    position: "absolute",
    width: 20,
    height: 20,
    borderRadius: 14,
    top: 25,
    right: 7,
    backgroundColor: Colors.primaryColor,
    justifyContent: "center",
    alignItems: "center"
  },
  AddingInputField1: {
    flexDirection: "row"
  },
  CircleButtonText1: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.h3,
    paddingTop: 0
  }
});
