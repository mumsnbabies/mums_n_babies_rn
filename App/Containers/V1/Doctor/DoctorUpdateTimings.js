import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  StatusBar,
  TouchableOpacity,
  TextInput,
  Alert
} from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../Components/Loader";
import AuthStore from "../../../stores/AuthStore";
import moment from "moment";
// Styles
import styles from "./Styles/DoctorUpdateTimingsStyles";
@observer
export default class DoctorUpdateTimings extends Component {
  @observable isDateTimePickerVisible: Boolean;

  @observable ddd;
  @observable date;
  constructor(props) {
    super(props);
    this.isDateTimePickerVisible = false;
    this.ddd = "";
    //this.date = moment(this.ddd).format("YYYY-MM-DD");
  }
  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  componentDidMount() {
    let requestObject = {
      event_id: AuthStore.event_id
    };
    AuthStore.getEventDetail(requestObject);
  }
  renderTitle = () => {
    return (
      <View style={styles.UpdateTimingsHeader}>
        <Text style={styles.UpdateTimingsHeaderText}>UPDATE TIMINGS</Text>
      </View>
    );
  };
  handleMomF = mon => {
    AuthStore.mon_f = mon;
  };
  handleMonT = mon => {
    AuthStore.mon_t = mon;
  };
  handleTueF = tue => {
    AuthStore.tue_f = tue;
  };
  handleTueT = tue => {
    AuthStore.tue_t = tue;
  };
  handleWedF = wed => {
    AuthStore.wed_f = wed;
  };
  handleWedT = wed => {
    AuthStore.wed_t = wed;
  };
  handleThrF = thr => {
    AuthStore.thr_f = thr;
  };
  handleThrT = thr => {
    AuthStore.thr_t = thr;
  };
  handleFriF = fri => {
    AuthStore.fri_f = fri;
  };
  handleFriT = fri => {
    AuthStore.fri_t = fri;
  };
  handleSatF = sat => {
    AuthStore.sat_f = sat;
  };
  handleSatT = sat => {
    AuthStore.sat_t = sat;
  };
  handleSunF = sun => {
    AuthStore.sun_f = sun;
  };
  handleSunT = sun => {
    AuthStore.sun_t = sun;
  };
  updatetimingRequest = () => {
    let requestObject = {
      entity_id: AuthStore.loginResponse.doctor_id,
      entity_type: "DOCTOR_TIMINGS",
      title: AuthStore.loginResponse.name,
      occurrences: [
        {
          start: this.mon_f,
          end: this.mon_t,
          description: "",
          title: "Monday"
        },
        {
          start: this.tue_f,
          end: this.tue_t,
          description: "",
          title: "Tuesday"
        },
        {
          start: this.wed_f,
          end: this.wed_t,
          description: "",
          title: "Wednesday"
        },
        {
          start: this.thr_f,
          end: this.thr_t,
          description: "",
          title: "Thrusday"
        },
        {
          start: this.fri_f,
          end: this.fri_t,
          description: "",
          title: "Friday"
        },
        {
          start: this.sat_f,
          end: this.sat_t,
          description: "",
          title: "Saturday"
        },
        {
          start: this.sun_f,
          end: this.sun_t,
          description: "",
          title: "Sunday"
        }
      ]
    };
    AuthStore.updatetiming(requestObject);
  };

  _showDateTimePicker = () => {
    this.isDateTimePickerVisible = true;
  };

  _hideDateTimePicker = () => {
    this.isDateTimePickerVisible = false;
  };

  _handleDatePicked = date => {
    this.ddd = moment(date).format("HH:mm");
    alert(date);
    this._hideDateTimePicker();
  };

  render() {
    if (AuthStore.updatetimingStatus === 200) {
      NavigationActions.DoctorHomeScreen();
    }
    if (AuthStore.updatetimingStatus === 100) {
      return <Loader animating={true} />;
    }
    if (AuthStore.updatetimingStatus > 200) {
      Alert.alert("Error", AuthStore.error.statusText, [
        { text: "OK", onPress: () => AuthStore.resetUpdatetimingState() }
      ]);
    }
    return (
      <View style={styles.MainScreen}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <View style={{ flex: 1 }}>
          <TouchableOpacity onPress={this._showDateTimePicker}>
            <Text>Show TimePicker</Text>
          </TouchableOpacity>
          <DateTimePicker
            isVisible={this.isDateTimePickerVisible}
            mode="time"
            onConfirm={this._handleDatePicked}
            onCancel={this._hideDateTimePicker}
          />
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          {/* <View style={styles.UpdateTimingsHeading}>
            <Text style={styles.UpdateTimingsHeadingText}>Update Timings</Text>
          </View> */}
          <Text style={styles.WeekDay}>Sunday</Text>
          <View style={styles.EachInputField}>
            <View style={styles.UpdateTimingsInput}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.UpdateTimingsInputField}
                placeholder="From"
                underlineColorAndroid="transparent"
                onChangeText={this.handleSunF}
                onFocus={this._showDateTimePicker}
                value={AuthStore.sun_f}
              />
            </View>
            <View style={styles.UpdateTimingsInput}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.UpdateTimingsInputField}
                placeholder="To"
                underlineColorAndroid="transparent"
                onChangeText={this.handleSunT}
                value={AuthStore.sun_t}
              />
            </View>
          </View>
          <Text style={styles.WeekDay}>Monday</Text>
          <View style={styles.EachInputField}>
            <View style={styles.UpdateTimingsInput}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.UpdateTimingsInputField}
                placeholder="From"
                underlineColorAndroid="transparent"
                onChangeText={this.handleMomF}
                value={AuthStore.mon_f}
              />
            </View>
            <View style={styles.UpdateTimingsInput}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.UpdateTimingsInputField}
                placeholder="To"
                underlineColorAndroid="transparent"
                onChangeText={this.handleMonT}
                value={AuthStore.mon_t}
              />
            </View>
          </View>
          <Text style={styles.WeekDay}>Tuesday</Text>
          <View style={styles.EachInputField}>
            <View style={styles.UpdateTimingsInput}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.UpdateTimingsInputField}
                placeholder="From"
                underlineColorAndroid="transparent"
                onChangeText={this.handleTueT}
                value={AuthStore.tue_f}
              />
            </View>
            <View style={styles.UpdateTimingsInput}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.UpdateTimingsInputField}
                placeholder="To"
                underlineColorAndroid="transparent"
                onChangeText={this.handleTueT}
                value={AuthStore.tue_t}
              />
            </View>
          </View>
          <Text style={styles.WeekDay}>Wednesday</Text>
          <View style={styles.EachInputField}>
            <View style={styles.UpdateTimingsInput}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.UpdateTimingsInputField}
                placeholder="From"
                underlineColorAndroid="transparent"
                onChangeText={this.handleWedF}
                value={AuthStore.wed_f}
              />
            </View>
            <View style={styles.UpdateTimingsInput}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.UpdateTimingsInputField}
                placeholder="To"
                underlineColorAndroid="transparent"
                onChangeText={this.handleWedT}
                value={AuthStore.wed_t}
              />
            </View>
          </View>
          <Text style={styles.WeekDay}>Thursday</Text>
          <View style={styles.EachInputField}>
            <View style={styles.UpdateTimingsInput}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.UpdateTimingsInputField}
                placeholder="From"
                underlineColorAndroid="transparent"
                onChangeText={this.handleThrF}
                value={AuthStore.thr_f}
              />
            </View>
            <View style={styles.UpdateTimingsInput}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.UpdateTimingsInputField}
                placeholder="To"
                underlineColorAndroid="transparent"
                onChangeText={this.handleThrT}
                value={AuthStore.thr_t}
              />
            </View>
          </View>
          <Text style={styles.WeekDay}>Friday</Text>
          <View style={styles.EachInputField}>
            <View style={styles.UpdateTimingsInput}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.UpdateTimingsInputField}
                placeholder="From"
                underlineColorAndroid="transparent"
                onChangeText={this.handleFriF}
                value={AuthStore.fri_f}
              />
            </View>
            <View style={styles.UpdateTimingsInput}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.UpdateTimingsInputField}
                placeholder="To"
                underlineColorAndroid="transparent"
                onChangeText={this.handleFriT}
                value={AuthStore.fri_t}
              />
            </View>
          </View>
          <Text style={styles.WeekDay}>Saturday</Text>
          <View style={styles.EachInputField}>
            <View style={styles.UpdateTimingsInput}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.UpdateTimingsInputField}
                placeholder="From"
                underlineColorAndroid="transparent"
                onChangeText={this.handleSatF}
                value={this.sat_f}
              />
            </View>
            <View style={styles.UpdateTimingsInput}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.UpdateTimingsInputField}
                placeholder="To"
                underlineColorAndroid="transparent"
                onChangeText={this.handleSatT}
                value={this.sat_t}
              />
            </View>
          </View>
          <TouchableOpacity
            underlayColor="#eee"
            activeOpacity={1}
            onPress={this.updatetimingRequest}
          >
            <View style={styles.UpdateTimingsButton}>
              <Text style={styles.UpdateTimingsButtonText}>Update Timings</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}
