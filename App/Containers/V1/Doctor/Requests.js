import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  Platform,
  Alert
} from "react-native";
import { observable } from "mobx";
import { observer } from "mobx-react";
import AuthStore from "../../../stores/AuthStore";
import DoctorRequestsStore from "../../../stores/DoctorRequestsStore";
import PatientProfileStore from "../../../stores/PatientProfileStore";
import SearchDoctors from "../../../Components/SearchDoctors";
import DoctorPendingCard from "../../../Components/DoctorPendingCard";
import DoctorConnectedCard from "../../../Components/DoctorConnectedCard";
import Icon from "react-native-vector-icons/Ionicons";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../Components/Loader";
import NoDataView from "../../../Components/NoDataView";
import moment from "moment";
import _ from "lodash";

// Styles
import styles from "./Styles/RequestsStyles";
import { REQUESTSCONSTANTS } from "../../../Constants/Doctor";
@observer
export default class Requests extends Component {
  @observable animating: Boolean;
  @observable searchPatientName: String;
  @observable text: String;
  @observable value: String;
  constructor(props) {
    super(props);
    this.searchPatientName = "";
    this.animating = false;
    this.text = "Search Patient";
    this.value = "Pending";
    this.showComponentOn = _.debounce(this.showComponentOn, 300);
  }

  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  componentDidMount() {
    this.getPendingRequests();
  }
  renderTitle = () => {
    return (
      <View style={styles.FindDoctorsHeader}>
        <Text style={styles.FindDoctorsHeaderText}>
          {this.value.toUpperCase()} {REQUESTSCONSTANTS.REQUESTS_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };
  getPendingRequests = (searchPatientName = "") => {
    let requestObject = {
      search_q: searchPatientName,
      limit: 10,
      relation_status: "PENDING",
      offset: 0
    };
    DoctorRequestsStore.getDoctorPendingRequests(requestObject);
  };
  getAcceptedRequests = (searchPatientName = "") => {
    let requestObject = {
      search_q: searchPatientName,
      limit: 10,
      relation_status: "ACCEPTED",
      offset: 0
    };
    DoctorRequestsStore.getDoctorAcceptedRequests(requestObject);
  };
  acceptPatientRequest = requestObject => {
    //AuthStore.changeRelationCount()
    DoctorRequestsStore.acceptPatientRequest(requestObject);
  };
  displayPatientProfile = (requestObject, patient_name) => {
    PatientProfileStore.getPatientProfile(requestObject);
    let date = new Date();
    let old_date = new Date().setMonth(new Date().getMonth() - 6);
    let request = {
      search_q: "",
      entity_id: requestObject.patient_id,
      entity_type: "PATIENT",
      limit: 10,
      filters: {
        from_date: moment(old_date).format("YYYY-MM-DD HH:mm"),
        to_date: moment(date).format("YYYY-MM-DD HH:mm")
      },
      offset: 0
    };
    PatientProfileStore.getPatientDocuments(request);
    NavigationActions.PatientProfile({
      patientName: patient_name,
      patient_id: requestObject.patient_id
    });
  };
  changeComponents = value => {
    this.value = value;
    if (value === "Pending") {
      this.getPendingRequests();
      this.searchPatientName = "";
    }
    if (value === "Connected") {
      this.getAcceptedRequests();
      this.searchPatientName = "";
    }
    NavigationActions.refresh({
      renderTitle: this.renderTitle,
      titleName: this.value
    });
  };
  showComponents = () => {
    if (
      this.value === "Pending" &&
      DoctorRequestsStore.getPendingRequestsStatus === 200
    ) {
      if (DoctorRequestsStore.pendingRequests.total === 0) {
        return (
          <View style={styles.noDataViewContainer}>
            <NoDataView />
          </View>
        );
      }
      if (DoctorRequestsStore.pendingRequests.total) {
        let { patients } = DoctorRequestsStore.pendingRequests;
        return patients.map((eachPatient, index) => {
          return (
            <DoctorPendingCard
              key={index}
              details={eachPatient}
              acceptPatientRequest={this.acceptPatientRequest}
              displayPatientProfile={this.displayPatientProfile}
            />
          );
        });
      }
      return null;
    } else if (DoctorRequestsStore.getPendingRequestsStatus > 200) {
      Alert.alert(
        REQUESTSCONSTANTS.ALERT_ERROR_TITLE_TEXT,
        DoctorRequestsStore.error.statusText,
        [
          {
            text: "OK",
            onPress: () => DoctorRequestsStore.resetPendingRequestsState()
          }
        ]
      );
    }

    if (
      this.value === "Connected" &&
      DoctorRequestsStore.getAcceptedRequestsStatus === 200
    ) {
      if (DoctorRequestsStore.acceptedRequests.total === 0) {
        return (
          <View style={styles.noDataViewContainer}>
            <NoDataView />
          </View>
        );
      }
      if (DoctorRequestsStore.acceptedRequests.total) {
        let { patients } = DoctorRequestsStore.acceptedRequests;
        return patients.map((eachPatient, index) => {
          return (
            <DoctorConnectedCard
              key={index}
              details={eachPatient}
              displayPatientProfile={this.displayPatientProfile}
            />
          );
        });
      }
      return null;
    }
  };
  handleSearch = name => {
    this.searchPatientName = name;
    this.showComponentOn(name);
  };
  showComponentOn = searchPatientName => {
    if (this.value === "Connected") {
      this.getAcceptedRequests(searchPatientName);
    } else if (this.value === "Pending") {
      this.getPendingRequests(searchPatientName);
    }
  };
  render() {
    let selectedTab = {};
    let selectedTabText = {};
    let unselectedTab = {};
    let unselectedTabText = {};
    selectedTab = styles.AcceptBtn;
    selectedTabText = styles.AcceptBtnText;
    unselectedTab = styles.AcceptOpenBtn;
    unselectedTabText = styles.AcceptOpenBtnText;
    return (
      <View style={styles.MainScreen}>
        <View style={styles.ChatBtns}>
          <View style={this.value === "Pending" ? selectedTab : unselectedTab}>
            <TouchableOpacity
              onPress={() => {
                this.changeComponents("Pending");
              }}
            >
              <Text
                style={
                  this.value === "Pending" ? selectedTabText : unselectedTabText
                }
              >
                {REQUESTSCONSTANTS.PENDING_BUTTON_TEXT}
                (
                {DoctorRequestsStore.pendingRequests.total
                  ? DoctorRequestsStore.pendingRequests.total
                  : DoctorRequestsStore.pendingRequests.total}
                )
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={this.value === "Connected" ? selectedTab : unselectedTab}
          >
            <TouchableOpacity
              onPress={() => {
                this.changeComponents("Connected");
              }}
            >
              <Text
                style={
                  this.value === "Connected"
                    ? selectedTabText
                    : unselectedTabText
                }
              >
                {REQUESTSCONSTANTS.CONNECTED_BUTTON_TEXT}
                (
                {DoctorRequestsStore.acceptedRequests.total
                  ? DoctorRequestsStore.acceptedRequests.total
                  : AuthStore.loginResponse.relation_stats.connected_relations}
                )
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.SearchingDoctor}>
          <View style={styles.DocInput}>
            <TextInput
              autoCapitalize="none"
              returnKeyType={"search"}
              autoCorrect={false}
              style={styles.DocInputField}
              placeholder={REQUESTSCONSTANTS.FIND_PATIENT_FIELD_PLACEHOLDER}
              underlineColorAndroid="transparent"
              onChangeText={this.handleSearch}
              value={this.searchPatientName}
            />
          </View>
        </View>
        {DoctorRequestsStore.getPendingRequestsStatus === 100 ||
        DoctorRequestsStore.getAcceptedRequestsStatus === 100 ||
        DoctorRequestsStore.searchDoctorStatus === 100 ? (
          <Loader animating={true} />
        ) : (
          <ScrollView showsVerticalScrollIndicator={false}>
            {this.showComponents()}
          </ScrollView>
        )}
      </View>
    );
  }
}
