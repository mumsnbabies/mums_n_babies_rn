import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  StatusBar,
  Button,
  View,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from "react-native";
import Mqtt from "@rn/react-native-mqtt-iot";
import AuthStore from "../../../stores/AuthStore";
import PopupMenu from "../../../Components/PopupMenu";
import PopupMenuIOS from "../../../Components/PopupMenuIOS";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../Components/Loader";
import Images from "../../../Themes/Images";

// Styles
import styles from "./Styles/DoctorHomeScreenStyles";

import { DOCTORHOMESCREENCONSTANTS } from "../../../Constants/Doctor";

export default class Home extends Component {
  constructor(props) {
    super(props);
  }
  static onEnter = () => {
    AuthStore.updatetimingStatus = 0;
    NavigationActions.refresh({
      right: renderRightButton,
      renderTitle: renderTitle,
      left: null
    });
  };

  render() {
    return (
      <View style={styles.MainScreen}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <ScrollView
          style={styles.Container}
          showsVerticalScrollIndicator={false}
        >
          <View style={styles.MainImageView}>
            <Image
              style={styles.MainImage}
              source={require("../../../Images/Doctor_Cover_Pic.jpg")}
            />
          </View>
          <View style={styles.SectionRowHolder}>
            {/*<TouchableOpacity
              underlayColor="#eee"
              activeOpacity={1}
              onPress={() => {
                NavigationActions.Analytics();
              }}
            >
              <View style={styles.SectionView}>
                <View style={styles.SectionIconView}>
                  <Image
                    style={styles.SectionImage}
                    resizeMode="contain"
                    source={require("../../../Images/analytics.png")}
                  />
                </View>
                <Text style={styles.SectionText}>Analytics</Text>
              </View>
            </TouchableOpacity>*/}

            <TouchableOpacity
              underlayColor="#eee"
              activeOpacity={1}
              onPress={() => {
                NavigationActions.ChatsList();
              }}
            >
              <View style={styles.SectionView}>
                <View style={styles.SectionIconView}>
                  <Image
                    style={styles.SectionImage}
                    resizeMode="contain"
                    source={require("../../../Images/patient_connect.png")}
                  />
                </View>
                <Text style={styles.SectionText}>
                  {DOCTORHOMESCREENCONSTANTS.DOCTOR_PATIENT_CONNECT_TAB_TEXT}
                </Text>
              </View>
            </TouchableOpacity>
            {/*          </View>

          <View style={styles.SectionRowHolder}>
            <TouchableOpacity
              activeOpacity={1}
              underlayColor="#eee"
              onPress={() => {
                NavigationActions.Appointments();
              }}
            >
              <View style={styles.SectionView}>
                <View style={styles.SectionIconView}>
                  <Image
                    style={styles.SectionImage}
                    resizeMode="contain"
                    source={require("../../../Images/appointments.png")}
                  />
                </View>
                <Text style={styles.SectionText}>Appoinments</Text>
              </View>
            </TouchableOpacity>
*/}
            <TouchableOpacity
              activeOpacity={1}
              underlayColor="#eee"
              onPress={() => {
                NavigationActions.Requests();
              }}
            >
              <View style={styles.SectionView}>
                <View style={styles.SectionIconView}>
                  <Image
                    style={styles.SectionImage}
                    resizeMode="contain"
                    source={Images.patientProfilePic}
                  />
                </View>
                <Text style={styles.SectionText}>
                  {DOCTORHOMESCREENCONSTANTS.DOCTOR_REQUESTS_TAB_TEXT}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const onPopupEvent = (eventName, index) => {
  if (index === 0) {
    NavigationActions.ChangePassword();
  } else if (index === 1) {
    AuthStore.DoctorProfileStatusUpdate();
    NavigationActions.DoctorUpdateProfile();
  } else if (index === 3) {
    AsyncStorage.multiRemove(
      ["AccessToken", "doctor_id"],
      async function() {
        AuthStore.resetState();
        await Mqtt.logoutChatService();
        NavigationActions.Initial({ type: "reset" });
      }.bind(this)
    );
    // AsyncStorage.removeItem(
    //   "AccessToken",
    //   function() {
    //     AuthStore.resetState()
    //     NavigationActions.Initial({ type: "reset" })
    //   }.bind(this)
    // )
  } else if (index === 2) {
    NavigationActions.DoctorTiming();
  }
};
const renderTitle = () => {
  return (
    <View style={styles.MainAppName}>
      <Text style={styles.MainAppNameText}>
        {DOCTORHOMESCREENCONSTANTS.DOCTOR_HOME_SCREEN_NAVBAR_HEADING}
      </Text>
    </View>
  );
};
const renderRightButton = () => {
  if (Platform.OS === "ios") {
    return (
      <View style={styles.DropDownButton}>
        <PopupMenuIOS
          actions={[
            "Change Password",
            "Update Profile",
            "Update Timings",
            "Log Out",
            "Cancel"
          ]}
        />
      </View>
    );
  }
  return (
    <PopupMenu
      actions={[
        "Change Password",
        "Update Profile",
        "Update Timings",
        "Log Out"
      ]}
      onPress={onPopupEvent}
    />
  );
};
