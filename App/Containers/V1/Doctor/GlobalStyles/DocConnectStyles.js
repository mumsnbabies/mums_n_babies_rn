import { StyleSheet, Platform } from "react-native"
import Fonts from "../../../../Themes/Fonts"

export default StyleSheet.create({
  DocConnectHeader: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: Platform.OS === "ios" ? 30 : 15
  },
  DocConnectHeaderText: {
    Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  mainContainer: {
    backgroundColor: Colors.whiteColor,
    margin: 10,
    padding: 10,
    borderRadius: 10,
    shadowOffset: { width: 3, height: 3 },
    shadowColor: Colors.blackColor,
    shadowRadius: 1,
    shadowOpacity: 0.3,
    elevation: 1,
    flexDirection: "row",
    height: 100
  },
  profilePicContainer: {
    flex: 1,
    alignItems: "center"
  },
  profilePic: {
    height: 60,
    width: 60,
    borderWidth: 0.3,
    borderRadius: 30
  },
  bodyContentContainer: {
    marginLeft: 10,
    flex: 4,
    flexDirection: "column"
  },
  profileDetailsContainer: {
    flex: 2
  },
  nameContainer: {
    flex: 1
  },
  nameTextStyles: {
    fontFamily: Fonts.family.fontFamilyBold,
    color: Colors.blackColor,
    Fonts.sizes.mediumsmall
  },
  designationContainer: {
    flex: 1
  },
  designationTextStyles: {
    fontFamily: Fonts.family.fontFamilyBold,
    color: Colors.lightblack
  },
  buttonContainer: {
    flexDirection: "row",
    flex: 2,
    marginRight: 30,
    alignItems: "flex-end"
  },
  firstButtonContainer: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.requestButtom,
    borderRadius: 5,
    height: 30,
    flex: 1,
    elevation: 1
  },
  firstButtonTextStyles: {
    color: Colors.whiteColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  secondButtonContainer: {
    height: 30,
    flex: 1,
    marginLeft: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: Colors.requestButtom,
    elevation: 1
  },
  secondButtonTextStyles: {
    fontFamily: Fonts.family.fontFamilyBold,
    color: Colors.requestButtom
  }
})
