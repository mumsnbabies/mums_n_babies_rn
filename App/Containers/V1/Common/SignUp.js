import React, { Component } from "react";
import {
  ScrollView,
  Text,
  View,
  AsyncStorage,
  TouchableOpacity,
  StatusBar,
  TextInput,
  Alert,
  Image
} from "react-native";
import { observable } from "mobx";
import { observer } from "mobx-react";
import AuthStore from "../../../stores/AuthStore";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../Components/Loader";
import Images from "../../../Themes/Images";

// Styles
import styles from "./Styles/SignUpStyles";

import { SIGNUPCONSTANTS } from "../../../Constants/Common";

@observer
export default class SignUp extends Component {
  @observable phone: String;
  @observable pwd: String;
  @observable name: String;
  @observable email: String;
  @observable required_phone: String;
  @observable required_pwd: String;
  @observable required_name: String;
  @observable required_email: String;
  @observable emailRegex: String;
  constructor(props) {
    super(props);
    this.phone = "";
    this.pwd = "";
    this.name = "";
    this.email = "";
    this.required_phone = null;
    this.required_pwd = null;
    this.required_name = null;
    this.required_email = null;
    this.emailRegex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$";
  }
  addName = name => {
    this.required_name = null;
    this.name = name;
    if (!this.name) {
      this.required_name = "*";
    }
  };
  addPhone = phone => {
    this.required_phone = null;
    this.phone = phone;
    if (!this.phone) {
      this.required_phone = "*";
    }
  };
  addPwd = pwd => {
    this.required_pwd = null;
    this.pwd = pwd;
    if (!this.pwd) {
      this.required_pwd = "*";
    }
  };
  addEmail = email => {
    this.required_email = null;
    this.email = email;
    if (!this.email) {
      this.required_email = "*";
    }
  };

  componentWillMount() {
    AuthStore.resetRegisterState();
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  validate = email => {
    let returnValue = false;
    let emailString = this.emailRegex;
    let re = new RegExp(emailString, "i");
    if (re.test(email)) {
      returnValue = true;
    } else {
      returnValue = false;
    }
    return returnValue;
  };
  renderTitle = () => {
    return (
      <View style={styles.SignupHeader}>
        <Text style={styles.SignupText}>
          {SIGNUPCONSTANTS.SIGN_UP_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };
  handleRegister = () => {
    if (this.name === "") {
      this.required_name = "*";
    } else if (this.phone === "") {
      this.required_phone = "*";
    } else if (this.email === "") {
      this.required_email = "*";
    } else if (this.pwd === "") {
      this.required_pwd = "*";
    } else {
      let requestObject = {
        phone_number: this.phone,
        password: this.pwd,
        name: this.name,
        country_code: "+91",
        email: this.email
      };
      if (__DEV__) {
        console.log("requestSignUp: ", requestObject);
      }
      if (this.validate(this.email)) {
        if (AuthStore.mode === 0) {
          AuthStore.registerDoctor(requestObject);
        } else {
          AuthStore.registerPatient(requestObject);
        }
      } else {
        alert(SIGNUPCONSTANTS.MAIL_ID_ERROR);
      }
    }
  };
  render() {
    if (AuthStore.registerStatus === 200) {
      NavigationActions.SignUpVerify({
        email: this.email,
        phoneno: this.phone
      });
    }
    if (AuthStore.registerStatus === 100) {
      return <Loader animating={true} />;
    }
    if (AuthStore.registerStatus > 200) {
      Alert.alert(
        SIGNUPCONSTANTS.ALERT_ERROR_TEXT,
        AuthStore.error.statusText,
        [{ text: "OK", onPress: () => AuthStore.resetRegisterState() }]
      );
    }
    return (
      <View style={styles.MainScreen}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.SignupScrollView}
        >
          <View style={styles.InputDetail}>
            {this.name ? (
              <Text style={styles.UpdateProfileTextField}>
                {SIGNUPCONSTANTS.NAME_FIELD_HEADING}
              </Text>
            ) : null}
            <View style={{ flexDirection: "row" }}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                autoFocus={true}
                returnKeyType={"next"}
                style={styles.InputField}
                onChangeText={this.addName}
                placeholder={SIGNUPCONSTANTS.NAME_FIELD_PLACEHOLDER}
                underlineColorAndroid="transparent"
                value={this.name}
              />
              {this.required_name && (
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>
              )}
            </View>
            <View style={styles.InputUnderline} />
            {this.phone ? (
              <Text style={styles.UpdateProfileTextField}>
                {SIGNUPCONSTANTS.MOBILE_FIELD_HEADING}
              </Text>
            ) : null}
            <View style={{ flexDirection: "row" }}>
              <TextInput
                keyboardType="phone-pad"
                autoCapitalize="none"
                autoCorrect={false}
                returnKeyType={"next"}
                style={styles.InputField}
                onChangeText={this.addPhone}
                placeholder={SIGNUPCONSTANTS.MOBILE_FIELD_PLACEHOLDER}
                underlineColorAndroid="transparent"
                value={this.phone}
              />
              {this.required_phone && (
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>
              )}
            </View>
            <View style={styles.InputUnderline} />
            {this.email ? (
              <Text style={styles.UpdateProfileTextField}>
                {SIGNUPCONSTANTS.MAIL_ID_FIELD_HEADING}
              </Text>
            ) : null}
            <View style={{ flexDirection: "row" }}>
              <TextInput
                keyboardType="email-address"
                autoCapitalize="none"
                autoCorrect={false}
                returnKeyType={"next"}
                style={styles.InputField}
                onChangeText={this.addEmail}
                placeholder={SIGNUPCONSTANTS.MAIL_ID_FIELD_PLACEHOLDER}
                underlineColorAndroid="transparent"
                value={this.email}
              />
              {this.required_email && (
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>
              )}
            </View>
            <View style={styles.InputUnderline} />
            {this.pwd ? (
              <Text style={styles.UpdateProfileTextField}>
                {SIGNUPCONSTANTS.PASSWORD_FIELD_HEADING}
              </Text>
            ) : null}
            <View style={{ flexDirection: "row" }}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.InputField}
                onChangeText={this.addPwd}
                secureTextEntry
                placeholder={SIGNUPCONSTANTS.PASSWORD_FIELD_PLACEHOLDER}
                underlineColorAndroid="transparent"
                value={this.pwd}
              />
              {this.required_pwd && (
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>
              )}
            </View>
          </View>
          <View style={styles.EmptyHolder}>
            <TouchableOpacity
              underlayColor="#eee"
              activeOpacity={1}
              onPress={() => {
                this.handleRegister();
              }}
            >
              <View style={styles.RegistorButton}>
                <Text style={styles.RegButtonText}>
                  {SIGNUPCONSTANTS.SIGN_UP_BUTTON_TEXT}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}
