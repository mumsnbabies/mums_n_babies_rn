import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  StatusBar,
  Alert,
  Image
} from "react-native";
import { observable } from "mobx";
import AuthStore from "../../../stores/AuthStore";
import { observer } from "mobx-react";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../Components/Loader";
import Images from "../../../Themes/Images";
// Styles
import styles from "./Styles/RecoverPasswordStyles";
import { RECOVERPASSWORDCONSTANTS } from "../../../Constants/Common";
@observer
export default class RecoverPassword extends Component {
  @observable recovery_email: String;
  @observable recovery_phone_number: String;
  @observable emailRegex: String;
  @observable required_email: String;
  @observable required_phone: String;
  constructor(props) {
    super(props);
    this.required_email = null;
    this.required_phone = null;
    this.recovery_phone_number = "";
    this.recovery_email = "";
    this.emailRegex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$";
  }
  componentWillMount() {
    AuthStore.resetRecoverPwdState();
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  renderTitle = () => {
    return (
      <View style={styles.RecoverPasswordHeader}>
        <Text style={styles.RecoverPasswordHeaderText}>
          {RECOVERPASSWORDCONSTANTS.RECOVER_PASSWORD_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };
  validate = email => {
    let returnValue = false;
    let emailString = this.emailRegex;
    let re = new RegExp(emailString, "i");
    if (re.test(email)) {
      returnValue = true;
    } else {
      returnValue = false;
    }
    return returnValue;
  };
  recoverPasswordRequest = () => {
    // if (this.recovery_email === "") {
    //   this.required_email = "*";
    // }
    if (this.recovery_phone_number === "") {
      this.required_phone = "*";
    } else {
      let requestObject = {
        username: "",
        country_code: "+91",
        phone_number: this.recovery_phone_number,
        // email: this.recovery_email,
        client_secret: "",
        client_id: "",
        auth_type: "phone_number"
      };
      AuthStore.recoverPassword(requestObject);
      // if (this.validate(this.recovery_email)) {
      //   AuthStore.recoverPassword(requestObject);
      // } else {
      //   alert("Invalid Email ID");
      // }
    }
  };
  handleEmail = recovery_email => {
    this.required_email = null;
    this.recovery_email = recovery_email;
    if (!this.recovery_email) {
      this.required_email = "*";
    }
  };
  handlePhoneNo = recovery_phone_number => {
    this.required_phone = null;
    this.recovery_phone_number = recovery_phone_number;
    if (!this.recovery_phone_number) {
      this.required_phone = "*";
    }
  };
  render() {
    if (AuthStore.recoverPasswordStatus === 200) {
      NavigationActions.ResetPassword({
        email: this.recovery_email,
        phoneno: this.recovery_phone_number
      });
    }
    if (AuthStore.recoverPasswordStatus === 100) {
      return <Loader animating={true} />;
    }
    if (AuthStore.recoverPasswordStatus > 200) {
      Alert.alert(
        RECOVERPASSWORDCONSTANTS.ALERT_ERROR_TEXT,
        AuthStore.error.statusText,
        [
          {
            text: "OK",
            onPress: () => AuthStore.resetRecoverPwdState()
          }
        ]
      );
    }
    return (
      <View style={styles.RecoverPasswordMainScreen}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <View style={styles.RecoverPasswordInput}>
          {this.recovery_phone_number ? (
            <Text style={styles.UpdateProfileTextField}>
              {RECOVERPASSWORDCONSTANTS.MOBILE_NO_FIELD_HEADING}
            </Text>
          ) : null}
          <View style={{ flexDirection: "row" }}>
            <TextInput
              keyboardType="phone-pad"
              autoCapitalize="none"
              autoCorrect={false}
              autoFocus={true}
              style={styles.RecoverPasswordInputField}
              onChangeText={this.handlePhoneNo}
              placeholder={RECOVERPASSWORDCONSTANTS.MOBILE_NO_FIELD_PLACEHOLDER}
              underlineColorAndroid="transparent"
              value={this.recovery_phone_number}
            />
            {this.required_phone && (
              <View style={styles.ImageContainer}>
                <Image
                  style={styles.RequiredField}
                  source={Images.alertShield}
                />
              </View>
            )}
          </View>
          {/*this.recovery_email
            ? <Text style={styles.UpdateProfileTextField}>Email ID</Text>
            : null*/}
          {/*<View style={{ flexDirection: "row" }}>
            <TextInput
              keyboardType="email-address"
              autoCapitalize="none"
              autoCorrect={false}
              style={styles.RecoverPasswordInputField}
              onChangeText={this.handleEmail}
              placeholder={RECOVERPASSWORDCONSTANTS.EMAIL_ID_FIELD_PLACEHOLDER}
              underlineColorAndroid="transparent"
              value={this.recovery_email}
            />
            {this.required_email &&
              <View style={styles.ImageContainer}>
                <Image
                  style={styles.RequiredField}
                  source={Images.alertShield}
                />
              </View>}
          </View>*/}
          <View style={styles.RecoverPasswordInputUnderline} />
        </View>
        <TouchableOpacity
          underlayColor="#eee"
          activeOpacity={1}
          onPress={() => {
            this.recoverPasswordRequest();
          }}
        >
          <View style={styles.RecoverPasswordButton}>
            <Text style={styles.RecoverPasswordButtonText}>
              {RECOVERPASSWORDCONSTANTS.RECOVER_PASSWORD_BUTTON_TEXT}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
