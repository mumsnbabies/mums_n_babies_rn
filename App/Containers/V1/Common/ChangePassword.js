import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  TextInput,
  Alert,
  Image,
  ScrollView
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";
import AuthStore from "../../../stores/AuthStore";
import Loader from "../../../Components/Loader";
import { observable } from "mobx";
import { observer } from "mobx-react";
import Images from "../../../Themes/Images";
// Styles
import styles from "./Styles/ChangePasswordStyles";
import { CHANGEPASSWORDCONSTANTS } from "../../../Constants/Common";
@observer
export default class ChangePassword extends Component {
  @observable old_pwd: String;
  @observable new_pwd: String;
  @observable cnfm_pwd: String;
  @observable required_old_pwd = null;
  @observable required_new_pwd = null;
  @observable required_cnfm_pwd = null;
  constructor(props) {
    super(props);
    this.old_pwd = "";
    this.new_pwd = "";
    this.cnfm_pwd = "";
    this.required_old_pwd = null;
    this.required_new_pwd = null;
    this.required_cnfm_pwd = null;
  }
  handleDoctorPwd = old_pwd => {
    this.required_old_pwd = null;
    this.old_pwd = old_pwd;
    if (!this.old_pwd) {
      this.required_old_pwd = "*";
    }
  };
  handleDoctorNewPwd = new_pwd => {
    this.required_new_pwd = null;
    this.new_pwd = new_pwd;
    if (!this.new_pwd) {
      this.required_new_pwd = "*";
    }
  };
  handleDoctorCnfmPwd = cnfm_pwd => {
    this.required_cnfm_pwd = null;
    this.cnfm_pwd = cnfm_pwd;
    if (!this.cnfm_pwd) {
      this.required_cnfm_pwd = "*";
    }
  };
  componentWillMount() {
    AuthStore.resetUpdatePasswordStatus();
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  renderTitle = () => {
    return (
      <View style={styles.ChangePasswordHeader}>
        <Text style={styles.ChangePasswordHeaderText}>
          {CHANGEPASSWORDCONSTANTS.CHANGE_PASSWORD_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };
  ChangePwdRequest = () => {
    if (this.old_pwd === "") {
      this.required_old_pwd = "*";
    } else if (this.new_pwd === "") {
      this.required_new_pwd = "*";
    } else if (this.cnfm_pwd === "") {
      this.required_cnfm_pwd = "*";
    } else if (this.new_pwd != this.cnfm_pwd) {
      alert(CHANGEPASSWORDCONSTANTS.CHANGE_PASSWORD_CONFIRM_PASSWORD_MISMATCH);
      this.new_pwd = "";
      this.cnfm_pwd = "";
    } else {
      let requestObject = {
        new_password: this.new_pwd,
        old_password: this.old_pwd
      };
      AuthStore.updatePassword(requestObject);
    }
  };
  render() {
    if (AuthStore.updatePasswordStatus === 200) {
      if (AuthStore.mode === 0) {
        NavigationActions.DoctorHomeScreen({ type: "reset" });
      } else {
        NavigationActions.PatientHomeScreen({ type: "reset" });
      }
    }
    if (AuthStore.updatePasswordStatus === 100) {
      return <Loader animating={true} />;
    }
    if (AuthStore.updatePasswordStatus > 200) {
      Alert.alert(
        CHANGEPASSWORDCONSTANTS.ALERT_ERROR_TEXT,
        AuthStore.error.statusText,
        [
          {
            text: "OK",
            onPress: () => AuthStore.resetUpdatePasswordState()
          }
        ]
      );
    }
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.ChangePasswordMainScreen}
      >
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <View style={styles.ChangePasswordMainScreen}>
          <View style={styles.ChangePasswordInput}>
            {this.old_pwd ? (
              <Text style={styles.UpdateProfileTextField}>
                {CHANGEPASSWORDCONSTANTS.CURRENT_PASSWORD_FIELD_HEADING}
              </Text>
            ) : null}
            <View style={{ flexDirection: "row" }}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                returnKeyType={"next"}
                autoFocus={true}
                style={styles.ChangePasswordInputField}
                onChangeText={this.handleDoctorPwd}
                placeholder={
                  CHANGEPASSWORDCONSTANTS.CURRENT_PASSWORD_FIELD_PLACEHOLDER
                }
                secureTextEntry
                underlineColorAndroid="transparent"
                value={this.old_pwd}
              />
              {this.required_old_pwd && (
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>
              )}
            </View>
            <View style={styles.ChangePasswordInputUnderline} />
            {this.new_pwd ? (
              <Text style={styles.UpdateProfileTextField}>
                {CHANGEPASSWORDCONSTANTS.NEW_PASSWORD_FIELD_HEADING}
              </Text>
            ) : null}
            <View style={{ flexDirection: "row" }}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                returnKeyType={"next"}
                style={styles.ChangePasswordInputField}
                onChangeText={this.handleDoctorNewPwd}
                placeholder={
                  CHANGEPASSWORDCONSTANTS.NEW_PASSWORD_FIELD_PLACEHOLDER
                }
                secureTextEntry
                underlineColorAndroid="transparent"
                value={this.new_pwd}
              />
              {this.required_new_pwd && (
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>
              )}
            </View>
            <View style={styles.ChangePasswordInputUnderline} />
            {this.cnfm_pwd ? (
              <Text style={styles.UpdateProfileTextField}>
                {CHANGEPASSWORDCONSTANTS.CONFIRM_PASSWORD_FIELD_HEADING}
              </Text>
            ) : null}
            <View style={{ flexDirection: "row" }}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.ChangePasswordInputField}
                onChangeText={this.handleDoctorCnfmPwd}
                placeholder={
                  CHANGEPASSWORDCONSTANTS.CONFIRM_PASSWORD_FIELD_PLACEHOLDER
                }
                secureTextEntry
                underlineColorAndroid="transparent"
                value={this.cnfm_pwd}
              />
              {this.required_cnfm_pwd && (
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>
              )}
            </View>
          </View>
          <TouchableOpacity
            underlayColor="#eee"
            activeOpacity={1}
            onPress={() => {
              this.ChangePwdRequest();
            }}
          >
            <View style={styles.ChangePasswordButton}>
              <Text style={styles.ChangePasswordButtonText}>
                {CHANGEPASSWORDCONSTANTS.CHANGE_PASSWORD_BUTTON_TEXT}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}
