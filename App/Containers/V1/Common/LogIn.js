import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Alert,
  StatusBar,
  Image,
  AsyncStorage
} from "react-native";
import { observable } from "mobx";
import { observer } from "mobx-react";
import AuthStore from "../../../stores/AuthStore";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../Components/Loader";
import Images from "../../../Themes/Images";
// Styles
import styles from "./Styles/LogInStyles";

import { LOGINCONSTANTS } from "../../../Constants/Common";

@observer
export default class LogIn extends Component {
  @observable email: String;
  @observable phone_number: String;
  @observable password: String;
  @observable email_regex: String;
  @observable required_email: String;
  @observable required_phone: String;
  @observable required_pswd: String;
  @observable navigate: String;
  constructor(props) {
    super(props);
    this.required_email = null;
    this.required_phone = null;
    this.required_pswd = null;
    this.email =
      AuthStore.mode === 0 ? "new_doctor@mnb.com" : "parent_mom@mnb.com";
    this.phone_number = AuthStore.mode === 0 ? "" : "";
    this.password = AuthStore.mode === 0 ? "" : "";
    this.email_regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$";
    this.navigate = false;
  }
  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  renderTitle = () => {
    return (
      <View style={styles.LoginHeader}>
        <Text style={styles.LoginText}>
          {LOGINCONSTANTS.SIGN_IN_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };
  // handleEmail = email => {
  //   this.required_email = null
  //   this.email = email
  //   if (!this.email) {
  //     this.required_email = "*"
  //   }
  // }
  handlePhoneNo = phone_no => {
    this.required_phone = null;
    this.phone_number = phone_no;
    if (!this.phone_number) {
      this.required_phone = "*";
    }
  };
  handlePassword = password => {
    this.required_pswd = null;
    this.password = password;
    if (!this.password) {
      this.required_pswd = "*";
    }
  };
  validate = email => {
    let returnValue = false;
    let emailString = this.email_regex;
    let re = new RegExp(emailString, "i");
    if (re.test(email)) {
      returnValue = true;
    } else {
      returnValue = false;
    }
    return returnValue;
  };
  handleLogin = () => {
    // if (this.email === "") {
    //   this.required_email = "*"
    // }
    if (this.phone_number === "") {
      this.required_phone = "*";
    } else if (this.password === "") {
      this.required_pswd = "*";
    } else {
      let requestObject = {
        phone_number: this.phone_number,
        country_code: "+91",
        client_secret: "client_secret",
        password: this.password,
        // email: this.email,
        client_id: "client_id"
      };
      if (AuthStore.mode === 0) {
        AuthStore.loginDoctor(requestObject);
      } else {
        AuthStore.loginPatient(requestObject);
      }

      // if (this.validate(this.email)) {

      //   if (AuthStore.mode === 0) {

      //     AuthStore.loginDoctor(requestObject)

      //   } else {

      //     AuthStore.loginPatient(requestObject)

      //   }

      // } else {

      //   alert(constants.MailIdError)

      // }
    }
  };
  getAccessToken = async () => {
    let access_token = await AsyncStorage.getItem("AccessToken");
    if (access_token !== null) {
      this.navigate = true;
    }
    return access_token;
  };
  render() {
    this.getAccessToken();
    if (AuthStore.loginStatus === 200 && this.navigate) {
      if (AuthStore.mode === 0) {
        let requestObject1 = {
          limit: 1,
          filters: {
            entity_id: AuthStore.loginResponse.doctor_id,
            entity_type: "DOCTOR_TIMINGS"
          },
          offset: 0
        };
        AuthStore.getEventId(requestObject1);
        NavigationActions.DoctorHomeScreen({ type: "reset" });
      } else {
        NavigationActions.PatientHomeScreen({ type: "reset" });
      }
    }
    if (AuthStore.loginStatus === 100) {
      return <Loader animating={true} />;
    }
    if (AuthStore.loginStatus > 200) {
      Alert.alert(LOGINCONSTANTS.ALERT_ERROR_TEXT, AuthStore.error.statusText, [
        { text: "OK", onPress: () => AuthStore.resetLoginState() }
      ]);
    }
    return (
      <View style={styles.LoginMainScreen}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <View style={styles.Input}>
          {/*this.email
            ? <Text style={styles.UpdateProfileTextField}>
                {LOGINCONSTANTS.EMAIL_ID_FIELD_HEADING}
              </Text>
            : null*/}
          {/*<View style={{ flexDirection: "row" }}>
            <TextInput
              keyboardType="email-address"
              autoCorrect={false}
              style={styles.InputField}
              onChangeText={this.handleEmail}
              placeholder={LOGINCONSTANTS.EMAIL_ID_FIELD_PLACEHOLDER}
              underlineColorAndroid="transparent"
              value={this.email}
            />
            {this.required_email &&
              <View style={styles.ImageContainer}>
                <Image
                  style={styles.RequiredField}
                  source={Images.alertShield}
                />
              </View>}
          </View>*/}
          {this.phone_number ? (
            <Text style={styles.UpdateProfileTextField}>
              {LOGINCONSTANTS.MOBILE_NO_FIELD_HEADING}
            </Text>
          ) : null}
          <View style={{ flexDirection: "row" }}>
            <TextInput
              keyboardType="phone-pad"
              autoCorrect={false}
              returnKeyType={"next"}
              style={styles.InputField}
              onChangeText={this.handlePhoneNo}
              placeholder={LOGINCONSTANTS.MOBILE_NO_FIELD_PLACEHOLDER}
              underlineColorAndroid="transparent"
              value={this.phone_number}
            />
            {this.required_phone && (
              <View style={styles.ImageContainer}>
                <Image
                  style={styles.RequiredField}
                  source={Images.alertShield}
                />
              </View>
            )}
          </View>
          <View style={styles.InputUnderline} />
          {this.password ? (
            <Text style={styles.UpdateProfileTextField}>
              {LOGINCONSTANTS.PASSWORD_FIELD_HEADING}
            </Text>
          ) : null}
          <View style={{ flexDirection: "row" }}>
            <TextInput
              autoCorrect={false}
              style={styles.InputField}
              onChangeText={this.handlePassword}
              placeholder={LOGINCONSTANTS.PASSWORD_FIELD_PLACEHOLDER}
              secureTextEntry
              underlineColorAndroid="transparent"
              value={this.password}
            />
            {this.required_pswd && (
              <View style={styles.ImageContainer}>
                <Image
                  style={styles.RequiredField}
                  source={Images.alertShield}
                />
              </View>
            )}
          </View>
        </View>
        <View style={styles.EmptyHolder}>
          <TouchableOpacity
            underlayColor="#eee"
            activeOpacity={1}
            onPress={this.handleLogin}
          >
            <View style={styles.loginButton}>
              <Text style={styles.loginButtonText}>
                {LOGINCONSTANTS.SIGN_IN_BUTTON_TEXT}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            underlayColor="#eee"
            activeOpacity={1}
            onPress={() => {
              NavigationActions.RecoverPassword();
            }}
          >
            <View style={styles.LoginForgotTextView}>
              <Text style={styles.LoginForgotText}>
                {LOGINCONSTANTS.FORGOT_PASSWORD_TEXT}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
