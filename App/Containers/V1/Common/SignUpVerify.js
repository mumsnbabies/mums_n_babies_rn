import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  TextInput,
  Alert,
  Image
} from "react-native";
import { observable } from "mobx";
import { observer } from "mobx-react/native";
import AuthStore from "../../../stores/AuthStore";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../Components/Loader";
import Images from "../../../Themes/Images";
// Styles
import styles from "./Styles/SignUpStyles";

import { SIGNUPVERIFYCONSTANTS } from "../../../Constants/Common";

@observer
export default class SignUpVerify extends Component {
  @observable verify_token: String;
  @observable required_verify_token: String;
  constructor(props) {
    super(props);
    this.required_verify_token = null;
    this.verify_token = "";
  }

  componentWillMount() {
    AuthStore.resetRegisterState();
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  renderTitle = () => {
    return (
      <View style={styles.SignupHeader}>
        <Text style={styles.SignupText}>
          {SIGNUPVERIFYCONSTANTS.SIGN_UP_VERIFY_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };
  handleVerifyToken = verifytoken => {
    this.required_verify_token = null;
    this.verify_token = verifytoken;
    if (!this.verify_token) {
      this.required_verify_token = "*";
    }
  };
  handleRegister = () => {
    if (this.verify_token === "") {
      this.required_verify_token = "*";
    } else {
      let requestObject = {
        phone_number: this.props.phoneno,
        country_code: "+91",
        client_secret: "client_secret",
        // email: this.props.email,
        client_id: "client_id",
        verify_token: this.verify_token
      };
      if (AuthStore.mode === 0) {
        AuthStore.verifyRegisteredDoctor(requestObject);
      } else {
        AuthStore.verifyRegisteredPatient(requestObject);
      }
    }
  };
  handleResend = () => {
    let requestObject = {
      client_secret: "client_secret",
      client_id: "client_id",
      auth_type: "phone_number",
      phone_number: this.props.phoneno,
      country_code: "+91"
    };
    AuthStore.resendOTP(requestObject);
  };
  render() {
    if (AuthStore.verifyRegisterStatus === 200) {
      if (AuthStore.mode === 0) {
        NavigationActions.DoctorWelcomeScreen({ type: "reset" });
      } else {
        NavigationActions.PatientPreWelcomeScreen({ type: "reset" });
      }
    }
    if (AuthStore.verifyRegisterStatus === 100) {
      return <Loader animating={true} />;
    }
    if (AuthStore.verifyRegisterStatus > 200) {
      Alert.alert(
        SIGNUPVERIFYCONSTANTS.ALERT_ERROR_TEXT,
        AuthStore.error.statusText,
        [
          {
            text: "OK",
            onPress: () => AuthStore.resetVerifyRegisterState()
          }
        ]
      );
    }

    const submitBtnStyles = () => {
      return [styles.btnContainer];
    };
    return (
      <View style={styles.MainScreen}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <View style={styles.InputDetail}>
          {this.verify_token ? (
            <Text style={styles.UpdateProfileTextField}>
              {SIGNUPVERIFYCONSTANTS.OTP_FIELD_HEADING}
            </Text>
          ) : null}
          <View style={{ flexDirection: "row" }}>
            <TextInput
              keyboardType="numeric"
              autoCapitalize="none"
              autoCorrect={false}
              autoFocus={true}
              onChangeText={this.handleVerifyToken}
              style={styles.InputField}
              placeholder={SIGNUPVERIFYCONSTANTS.OTP_FIELD_PLACEHOLDER}
              underlineColorAndroid="transparent"
              value={this.verify_token}
            />
            {this.required_verify_token && (
              <View style={styles.ImageContainer}>
                <Image
                  style={styles.RequiredField}
                  source={Images.alertShield}
                />
              </View>
            )}
          </View>
        </View>
        <View style={styles.buttonsContainer}>
          <TouchableOpacity
            underlayColor="#eee"
            activeOpacity={1}
            onPress={() => {
              this.handleRegister();
            }}
            style={styles.btnContainer}
          >
            <Text style={styles.btnTextStyles}>
              {SIGNUPVERIFYCONSTANTS.SIGN_UP_VERIFY_BUTTON_TEXT}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            underlayColor="#eee"
            activeOpacity={1}
            onPress={() => {
              this.handleResend();
            }}
            style={styles.ResendTextView}
          >
            <Text style={styles.ResendText}>
              {SIGNUPVERIFYCONSTANTS.RESEND_BUTTON_TEXT}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
