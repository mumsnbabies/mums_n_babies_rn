import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  StatusBar,
  AsyncStorage,
  TouchableOpacity
} from "react-native";
import { observable } from "mobx";
import { observer } from "mobx-react";
import PatientProfileStore from "../../../stores/PatientProfileStore";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../Components/Loader";
import Graph from "../../../Components/Graph";
import moment from "moment";
import ImageViewer from "ImageViewer";
import Images from "../../../Themes/Images";

// Styles
import styles from "./Styles/PatientProfileStyles";

var s = new Set();

@observer
export default class PatientProfile extends Component {
  @observable selectedPerson: String;
  @observable selectedChild: Number;
  @observable record: String;
  @observable recordValueKeys: Array;
  @observable curIndex: Number;
  @observable shown: Boolean;
  @observable dashboardName: String;
  constructor(props) {
    super(props);
    this.selectedPerson = "Parent";
    this.selectedChild = null;
    this.record = "";
    this.recordValueKeys = [];
    this.curIndex = 0;
    this.shown = false;
    this.dashboardName = this.props.patientName;
  }

  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  componentDidMount() {
    this.getPeriodicalAttributes();
  }

  renderTitle = () => {
    return (
      <View style={styles.PatientProfileHeader}>
        <Text style={styles.PatientProfileHeaderText}>
          {this.dashboardName}
        </Text>
      </View>
    );
  };

  changeSelection = (child_id, type, name) => {
    this.selectedChild = child_id;
    this.selectedPerson = type;
    this.getDocuments();
    this.getPeriodicalAttributes();
    this.dashboardName = name;
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  };
  getPeriodicalAttributes = () => {
    let requestObject = {};
    if (this.selectedPerson === "Parent") {
      requestObject["entity_id"] = this.props.patient_id;
      requestObject["entity_type"] = "PATIENT";
    } else {
      requestObject["entity_id"] = this.selectedChild;
      requestObject["entity_type"] = "CHILD";
    }
    PatientProfileStore.getPeriodicalAttributes(requestObject);
  };
  getDocuments = () => {
    let date = new Date();
    let old_date = new Date().setMonth(new Date().getMonth() - 6);
    let requestObject = {
      search_q: "",
      entity_id:
        this.selectedPerson === "Parent"
          ? PatientProfileStore.patientDetails.patient_id
          : this.selectedChild,
      entity_type: this.selectedPerson === "Parent" ? "PATIENT" : "CHILD",
      limit: 10,
      filters: {
        from_date: moment(old_date).format("YYYY-MM-DD HH:mm"),
        to_date: moment(date).format("YYYY-MM-DD HH:mm")
      },
      offset: 0
    };
    if (__DEV__) {
      console.log(
        "Check",
        this.selectedPerson,
        PatientProfileStore.patientDetails.patient_id,
        requestObject
      );
    }
    PatientProfileStore.getPatientDocuments(requestObject);
  };
  displayDashboardOptions = () => {
    let { name, children } = PatientProfileStore.patientDetails;
    if (children) {
      return children.map((eachChild, index) => {
        return (
          <TouchableOpacity
            key={index}
            onPress={() =>
              this.changeSelection(eachChild.child_id, "Child", eachChild.name)
            }
            style={
              eachChild.child_id === this.selectedChild
                ? styles.ActiveDashboardProfile
                : styles.DeactiveDashboardProfileView
            }
          >
            <Text
              style={
                eachChild.child_id === this.selectedChild
                  ? styles.ActiveDashboardProfileText
                  : styles.DeactiveDashboardProfileViewText
              }
            >
              {eachChild.name}
            </Text>
          </TouchableOpacity>
        );
      });
    }
    return null;
  };
  displayQuickRecordKeys = records => {
    let quickRecordKeys = this.getQuickRecordKeys();
    return quickRecordKeys.map((eachQuickRecord, index) => {
      return (
        <TouchableOpacity
          key={index}
          onPress={() => {
            this.record = eachQuickRecord;
            this.recordValueKeys = this.getRecordValueKeys(eachQuickRecord);
          }}
        >
          <View
            style={
              this.record === eachQuickRecord
                ? styles.selectedCard
                : styles.Cards
            }
          >
            <Text style={styles.QuickRecordsCardsWeight}>
              {records[eachQuickRecord]}
            </Text>
            <Text style={styles.QuickRecordsCardsText}>{eachQuickRecord}</Text>
          </View>
        </TouchableOpacity>
      );
    });
  };
  getQuickRecordKeys = () => {
    let records = [];
    if (PatientProfileStore.periodicalAttributes.length > 0) {
      PatientProfileStore.periodicalAttributes.map((eachRecord, index) => {
        records.push(eachRecord.attr_key);
      });
    }
    return records;
  };
  getRecordValueKeys = key => {
    let valueKeys = [];
    PatientProfileStore.periodicalAttributes.map((eachRecord, index) => {
      if (eachRecord.attr_key === key) {
        eachRecord.attr_values.map((eachValue, index) => {
          valueKeys.push(eachValue.value_key);
        });
      }
    });
    return valueKeys;
  };
  checkAttributes = (attributes, children) => {
    let quickRecords = this.getQuickRecordKeys();
    if (quickRecords.length > 0 && this.record === "") {
      this.record = quickRecords[0];
      this.recordValueKeys = this.getRecordValueKeys(quickRecords[0]);
    }
    let returningRecords = {};
    // let attribs = ["BMI", "WEIGHT", "HEIGHT", "SUGAR"]
    // let returningAttribs = { BMI: "", WEIGHT: "", HEIGHT: "", SUGAR: "" }
    if (this.selectedPerson === "Parent") {
      if (attributes && attributes.length > 0) {
        attributes.map((eachAttrib, index) => {
          if (quickRecords.indexOf(eachAttrib.attr_key) > -1) {
            let attr_value = "";
            eachAttrib.attr_values.map((eachValue, index) => {
              attr_value = attr_value + eachValue.value + " / ";
            });
            attr_value = attr_value.slice(0, attr_value.length - 3);
            returningRecords[eachAttrib.attr_key] = attr_value;
          }
        });
        if (__DEV__) {
          console.log(returningRecords);
        }
      }
    } else {
      if (children && children.length > 0) {
        children.map((eachChild, index) => {
          if (eachChild.child_id === this.selectedChild) {
            if (eachChild.child_attributes) {
              eachChild.child_attributes.map((eachAttrib, index) => {
                if (quickRecords.indexOf(eachAttrib.attr_key) > -1) {
                  let attr_value = "";
                  eachAttrib.attr_values.map((eachValue, index) => {
                    attr_value = attr_value + eachValue.value + " / ";
                  });
                  attr_value = attr_value.slice(0, attr_value.length - 3);
                  returningRecords[eachAttrib.attr_key] = attr_value;
                }
              });
            }
            if (__DEV__) {
              console.log(returningRecords);
            }
          }
        });
      }
    }
    return returningRecords;
  };
  closeViewer() {
    this.shown = false;
    this.curIndex = 0;
  }
  openViewer(index) {
    this.shown = true;
    this.curIndex = index;
  }
  render() {
    let {
      name,
      address,
      pic,
      expected_date,
      patient_attributes,
      children,
      patient_id
    } = PatientProfileStore.patientDetails;

    if (this.props.patientName != name) {
      return <Loader animating={true} />;
    }
    let quickRecords = this.checkAttributes(patient_attributes, children);
    let { total, documents } = PatientProfileStore.patientDocuments;
    if (
      PatientProfileStore.getDocumentsStatus === 200 &&
      PatientProfileStore.patientDocuments.total > 0
    ) {
      documents.map((doc, index) => {
        s.add(doc.document_url);
      });
    }
    let imgsArr = Array.from(s);
    if (__DEV__) {
      console.log(imgsArr, this.shown, this.curIndex);
    }
    return (
      <View style={styles.MainScreen}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.MainView}>
            {pic ? (
              <Image
                style={styles.ProfileImage}
                // resizeMode="contain"
                source={{
                  uri: pic
                }}
              />
            ) : (
              <Image
                style={styles.ProfileImage}
                // resizeMode="contain"
                source={Images.patientProfilePic}
              />
            )}
            <Text style={styles.PatientProfileName}>{name}</Text>
            <Text style={styles.PatientProfileDetails}>
              {address && address.address}
            </Text>
          </View>
          <View style={styles.DashboardMainView}>
            {/* <Text style={styles.PatientProfileMainName}>
              {(name && WEIGHT) || HEIGHT || BMI || SUGAR
                ? name + "'s Profile"
                : null}
            </Text> */}
            {children && children.length > 0 ? (
              <Text style={styles.PatientProfileDashboardText}>
                Change Dashboard to:
              </Text>
            ) : null}
          </View>
          {children && children.length > 0 ? (
            <View style={styles.PatientProfileDashboardView}>
              <TouchableOpacity
                onPress={() =>
                  this.changeSelection(0, "Parent", this.props.patientName)
                }
                style={
                  this.selectedPerson === "Parent"
                    ? styles.ActiveDashboardProfile
                    : styles.DeactiveDashboardProfileView
                }
              >
                <Text
                  style={
                    this.selectedPerson === "Parent"
                      ? styles.ActiveDashboardProfileText
                      : styles.DeactiveDashboardProfileViewText
                  }
                >
                  {name + "'s"}
                </Text>
              </TouchableOpacity>
              {this.displayDashboardOptions()}
            </View>
          ) : null}
          {Object.keys(quickRecords).length > 0 ? (
            <View style={styles.PatientProfilePregnancyTracker}>
              <Text style={styles.PatientProfilePregnancyTrackerText}>
                {this.selectedPerson === "Parent" ? "Pregnancy" : null}{" "}
                {this.record} Tracker
              </Text>
            </View>
          ) : null}
          {Object.keys(quickRecords).length > 0 ? (
            <View>
              {/* {expected_date
                  ? <View style={styles.PatientProfileDayCount}>
                      <Text style={styles.DayCountText}>
                        {expected_date[0]} days for 👶
                        {/*replace with expected_date
                      </Text>
                    </View>
                  : null} */}
              <View style={styles.PatientProfileGraph}>
                <Graph
                  selectedPerson={this.selectedPerson}
                  selectedChild={this.selectedChild}
                  selectedParent={patient_id}
                  record={this.record}
                  recordValueKeys={this.recordValueKeys}
                />
              </View>
              <Text style={styles.QuickRecordsText}>Quick Records</Text>
            </View>
          ) : null}
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={styles.QuickRecordsContainer}>
              {this.displayQuickRecordKeys(quickRecords)}
            </View>
          </ScrollView>
          {PatientProfileStore.getDocumentsStatus === 200 &&
          PatientProfileStore.patientDocuments.total > 0 ? (
            <View>
              <Text style={styles.QuickRecordsText}>Medical Records</Text>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                <View style={styles.QuickRecordsContainer}>
                  {imgsArr.map((url, index) => {
                    return (
                      <TouchableOpacity
                        key={index}
                        activeOpacity={1}
                        onPress={this.openViewer.bind(this, index)}
                      >
                        <Image
                          source={{ uri: url }}
                          resizeMode="cover"
                          style={styles.ProfileImage2}
                        />
                      </TouchableOpacity>
                    );
                  })}
                </View>
              </ScrollView>
            </View>
          ) : null}
          <ImageViewer
            shown={this.shown}
            imageUrls={imgsArr}
            onClose={this.closeViewer.bind(this)}
            index={this.curIndex}
          />
        </ScrollView>
      </View>
    );
  }
}
