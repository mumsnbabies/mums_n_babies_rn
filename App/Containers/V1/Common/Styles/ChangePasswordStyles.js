import { StyleSheet, Platform } from "react-native";
import Colors from "../../../../Themes/Colors";
import Fonts from "../../../../Themes/Fonts";

export default StyleSheet.create({
  ChangePasswordHeader: {
    alignItems: "center",
    justifyContent: "center"
  },
  ChangePasswordHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  ChangePasswordMainScreen: {
    flex: 1,
    backgroundColor: Colors.whiteColor
  },
  RequiredField: {
    width: 20,
    height: 20
  },
  ImageContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-end",
    marginRight: 15
  },
  ChangePasswordInputField: {
    width: "80%",
    color: Colors.blackColor,
    marginLeft: 20,
    height: 50,
    fontFamily: Fonts.family.fontfamily
  },
  UpdateProfileTextField: {
    marginTop: "2%",
    marginLeft: 15,
    color: Colors.lightblack,
    fontFamily: Fonts.family.fontFamilyBold
  },
  ChangePasswordInputUnderline: {
    // flex: 1,
    borderBottomWidth: 0.7,
    borderColor: Colors.lightblack
  },
  ChangePasswordInput: {
    marginLeft: 10,
    marginRight: 10,
    borderColor: Colors.blackColor,
    borderWidth: 0.5,
    borderRadius: 5,
    marginTop: 30
  },
  ChangePasswordButton: {
    marginTop: 20,
    //justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    borderRadius: 60,
    backgroundColor: Colors.primaryColor,
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 60,
    paddingRight: 60
  },
  ChangePasswordButtonText: {
    justifyContent: "center",
    //backgroundColor: Colors.primaryColor,
    //borderRadius: 60,
    alignItems: "center",
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.normal,
    //paddingTop: 15,
    //paddingBottom: 15,
    //paddingLeft: 60,
    //paddingRight: 60,
    fontFamily: Fonts.family.fontfamily
  }
});
