import { StyleSheet, Platform } from "react-native";
import Colors from "../../../../Themes/Colors";
import Fonts from "../../../../Themes/Fonts";

export default StyleSheet.create({
  SignupHeader: {
    alignItems: "center",
    justifyContent: "center"
  },
  SignupText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  MainScreen: {
    flex: 1,
    backgroundColor: Colors.whiteColor
  },
  RequiredField: {
    width: 20,
    height: 20
  },
  ImageContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-end",
    marginRight: 15
  },
  InputField: {
    width: "80%",
    color: Colors.blackColor,
    marginLeft: 20,
    height: 50,
    fontFamily: Fonts.family.fontfamily
  },
  UpdateProfileTextField: {
    marginTop: "2%",
    marginLeft: 15,
    color: Colors.lightblack,
    fontFamily: Fonts.family.fontFamilyBold
  },
  InputUnderline: {
    flex: 1,
    borderBottomWidth: 0.7,
    borderBottomColor: Colors.lightblack
  },
  EmptyHolder: {
    alignItems: "center",
    marginBottom: 30
  },
  InputDetail: {
    marginLeft: 10,
    marginRight: 10,
    borderColor: Colors.blackColor,
    borderWidth: 0.5,
    borderRadius: 5,
    marginTop: 30
  },
  RegistorButton: {
    backgroundColor: Colors.primaryColor,
    marginTop: 30,
    height: 50,
    borderRadius: 40,
    justifyContent: "center",
    alignItems: "center",
    width: 220
  },
  RegButtonText: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.family.fontfamily
  },
  buttonsContainer: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    marginTop: 20
  },
  btnContainer: {
    backgroundColor: Colors.primaryColor,
    width: 220,
    height: 50,
    borderRadius: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  ResendTextView: {
    marginTop: 30,
    justifyContent: "center",
    alignItems: "center"
  },
  ResendText: {
    fontSize: Fonts.size.medium,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  btnTextStyles: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.family.fontfamily
  },
  disabledBtnColor: {
    backgroundColor: Colors.lightblack
  }
});
