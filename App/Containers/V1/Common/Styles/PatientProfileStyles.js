import { StyleSheet, Platform } from "react-native";
import Colors from "../../../../Themes/Colors";
import Fonts from "../../../../Themes/Fonts";

export default StyleSheet.create({
  PatientProfileHeader: {
    alignItems: "center",
    justifyContent: "center"
  },
  PatientProfileHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  MainScreen: {
    flex: 1,
    backgroundColor: Colors.primaryColor,
    position: "relative"
  },
  ProfileImage: {
    height: 150,
    width: 150,
    marginBottom: 15,
    borderRadius: 75
  },
  ProfileImage2: {
    marginTop: 5,
    height: 90,
    width: 90,
    marginBottom: 15,
    borderRadius: 5,
    marginRight: 10
  },
  PatientProfileName: {
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.mediumsmall,
    fontFamily: Fonts.family.fontfamily
  },
  PatientProfileDetails: {
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.normal,
    fontFamily: Fonts.family.fontfamily
  },
  MainView: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 15
  },
  DashboardMainView: {
    marginTop: 20,
    marginLeft: 30
  },
  PatientProfileMainName: {
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.smallheading,
    fontFamily: Fonts.family.fontfamily
  },
  PatientProfileDashboardText: {
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.large,
    fontFamily: Fonts.family.fontfamily
  },
  PatientProfileDashboardView: {
    flexDirection: "row",
    // flex: 1,
    marginLeft: 30,
    marginTop: 10,
    marginRight: 30,
    marginBottom: 10,
    borderRadius: 5,
    borderColor: Colors.unselectedTab,
    height: 30
  },
  ActiveDashboardProfile: {
    flex: 1,
    backgroundColor: Colors.selectedTab,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 40,
    borderWidth: 0.7,
    borderColor: Colors.selectedTab,
    marginRight: 5
  },
  ActiveDashboardProfileText: {
    fontSize: Fonts.sizes.mediumsmall,
    fontFamily: Fonts.family.fontfamily,
    color: Colors.secondaryColor
  },
  DeactiveDashboardProfileView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginRight: 5,
    backgroundColor: Colors.unselectedTab,
    borderRadius: 40,
    borderWidth: 0.7,
    borderColor: Colors.unselectedTab
  },
  DeactiveDashboardProfileViewText: {
    fontSize: Fonts.sizes.mediumsmall,
    color: Colors.secondaryColor,
    fontFamily: Fonts.family.fontfamily
  },
  PatientProfileDayCount: {
    justifyContent: "flex-end",
    alignItems: "flex-end",
    marginTop: 10,
    marginRight: 20

    // marginBottom: 20
  },
  DayCountText: {
    backgroundColor: Colors.whiteColor,
    borderRadius: 30,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 15,
    paddingRight: 15,
    fontFamily: Fonts.family.fontfamily
  },
  PatientProfileGraph: {
    paddingTop: 10
  },
  PatientProfilePregnancyTracker: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 20
  },
  PatientProfilePregnancyTrackerText: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.family.fontFamilyBold
  },
  QuickRecordsText: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.input,
    marginTop: 35,
    marginLeft: 30,
    fontFamily: Fonts.family.fontFamilyBold
  },
  QuickRecordsContainer: {
    flexDirection: "row",
    height: 100,
    marginLeft: 20,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10,
    elevation: 2,
    justifyContent: "center"
  },
  Cards: {
    justifyContent: "center",
    alignItems: "center",
    height: 90,
    width: 120,
    backgroundColor: Colors.unselectedTab,
    borderRadius: 5,
    // elevation: 2,
    marginRight: 5
  },
  selectedCard: {
    justifyContent: "center",
    alignItems: "center",
    height: 90,
    width: 120,
    backgroundColor: Colors.selectedTab,
    borderRadius: 5,
    // elevation: 2,
    marginRight: 5
  },
  QuickRecordsCardsWeight: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor
  },
  QuickRecordsCardsText: {
    marginTop: 5,
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  QuickRecordsCardsEdit: {
    fontSize: Fonts.size.mini,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  }
});
