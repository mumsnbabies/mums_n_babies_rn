import { StyleSheet, Platform, Dimensions } from "react-native";
import Colors from "../../../../Themes/Colors";
import Fonts from "../../../../Themes/Fonts";

export default StyleSheet.create({
  MainScreen: {
    flex: 1,
    position: "relative",
    alignItems: "center",
    justifyContent: "center"
  },
  BgImg: {
    zIndex: -1,
    height: Dimensions.get("window").height
  },
  AppNameView: {
    position: "absolute",
    top: 50,
    left: 0,
    right: 0,
    flex: 1
  },
  AppName: {
    textAlign: "center",
    backgroundColor: "transparent",
    fontSize: Fonts.sizes.heading,
    color: Colors.primaryColor,
    fontFamily: Platform.OS === "ios" ? "Segoe Script" : "OpenSegoeScript"
  },
  AppNameTagLine: {
    textAlign: "center",
    backgroundColor: "transparent",
    fontSize: Fonts.size.h5,
    color: Colors.primaryColor,
    fontFamily: Platform.OS === "ios" ? "Segoe Script" : "OpenSegoeScript"
  },
  ButtonHolder: {
    position: "absolute",
    bottom: 40
  },
  RadioButtonHolder: {
    backgroundColor: "transparent",
    justifyContent: "center",
    marginBottom: 10
  },
  RadioButton: {
    justifyContent: "space-between"
  },
  RadioButtonText: {
    fontFamily: Fonts.family.fontfamily,
    fontSize: Fonts.size.input
  },
  SmallButtonHolder: {
    marginBottom: 25
  },
  DoctorMode: {
    color: Colors.whiteColor,
    textAlign: "center",
    fontSize: Fonts.sizes.large,
    backgroundColor: "transparent",
    fontFamily: Fonts.family.fontfamily
  },
  SigupBtn: {
    backgroundColor: Colors.primaryColor,
    marginTop: 6,
    height: 50,
    borderRadius: 40,
    justifyContent: "center",
    alignItems: "center",
    width: 220
  },
  SiginBtn: {
    backgroundColor: Colors.whiteColor,
    height: 50,
    borderRadius: 40,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    width: 220
  },
  SignupBtnText: {
    textAlign: "center",
    color: Colors.whiteColor,
    fontSize: Fonts.size.input,
    fontFamily: Fonts.family.fontfamily
  },
  SigninBtnText: {
    textAlign: "center",
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  }
});
