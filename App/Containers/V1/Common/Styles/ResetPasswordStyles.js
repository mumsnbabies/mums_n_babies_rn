import { StyleSheet, Platform } from "react-native";
import Colors from "../../../../Themes/Colors";
import Fonts from "../../../../Themes/Fonts";

export default StyleSheet.create({
  ResetPasswordHeader: {
    alignItems: "center",
    justifyContent: "center"
  },
  ResetPasswordHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  RequiredField: {
    width: 20,
    height: 20
  },
  ImageContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-end",
    marginRight: 15
  },
  UpdateProfileTextField: {
    marginTop: "2%",
    marginLeft: 15,
    color: Colors.lightblack,
    fontFamily: Fonts.family.fontFamilyBold
  },
  ResetPasswordMainScreen: {
    flex: 1,
    backgroundColor: Colors.whiteColor
  },
  ResetPasswordInputField: {
    width: "80%",
    color: Colors.blackColor,
    marginLeft: 20,
    height: 50,
    fontFamily: Fonts.family.fontfamily
  },
  ResetPasswordInputUnderline: {
    flex: 1,
    borderBottomWidth: 0.7,
    borderColor: Colors.lightblack
  },
  ResetPasswordInput: {
    marginLeft: 10,
    marginRight: 10,
    borderColor: Colors.blackColor,
    borderWidth: 0.5,
    borderRadius: 5,
    marginTop: 30
  },
  ResetPasswordButton: {
    marginTop: 20,
    alignSelf: "center",
    alignItems: "center",
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 60,
    paddingRight: 60,
    backgroundColor: Colors.primaryColor,
    borderRadius: 60
  },
  ResetPasswordButtonText: {
    justifyContent: "center",
    alignItems: "center",
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.normal,
    fontFamily: Fonts.family.fontfamily
  }
});
