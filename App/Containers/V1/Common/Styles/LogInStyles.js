import { StyleSheet, Platform } from "react-native";
import Colors from "../../../../Themes/Colors";
import Fonts from "../../../../Themes/Fonts";

export default StyleSheet.create({
  LoginHeader: {
    alignItems: "center",
    justifyContent: "center"
  },
  RequiredField: {
    width: 20,
    height: 20
  },
  ImageContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-end",
    marginRight: 15
  },
  LoginText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  LoginMainScreen: {
    flex: 1,
    paddingTop: 30,
    backgroundColor: Colors.whiteColor
  },
  UpdateProfileTextField: {
    marginTop: "2%",
    marginLeft: 15,
    color: Colors.lightblack,
    fontFamily: Fonts.family.fontFamilyBold
  },
  InputField: {
    width: "80%",
    color: Colors.blackColor,
    marginLeft: 20,
    height: 50,
    fontFamily: Fonts.family.fontfamily
  },
  InputUnderline: {
    flex: 1,
    borderBottomWidth: 1,
    borderColor: Colors.lightblack
  },
  Input: {
    marginLeft: 10,
    marginRight: 10,
    borderColor: Colors.blackColor,
    borderWidth: 0.5,
    borderRadius: 5
  },
  EmptyHolder: {
    alignItems: "center"
  },
  loginButton: {
    backgroundColor: Colors.primaryColor,
    marginTop: 30,
    height: 50,
    borderRadius: 40,
    justifyContent: "center",
    alignItems: "center",
    width: 220
  },
  LoginForgotTextView: {
    marginTop: 30,
    justifyContent: "center",
    alignItems: "center"
  },
  LoginForgotText: {
    fontSize: Fonts.size.medium,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  loginButtonText: {
    justifyContent: "center",
    alignItems: "center",
    color: Colors.whiteColor,
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.family.fontfamily
  }
});
