import { StyleSheet, Platform } from "react-native";
import Colors from "../../../../Themes/Colors";
import Fonts from "../../../../Themes/Fonts";

export default StyleSheet.create({
  RecoverPasswordHeader: {
    alignItems: "center",
    justifyContent: "center"
  },
  RecoverPasswordHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  RequiredField: {
    width: 20,
    height: 20
  },
  ImageContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-end",
    marginRight: 15
  },
  RecoverPasswordMainScreen: {
    flex: 1,
    paddingTop: 30,
    backgroundColor: Colors.whiteColor
  },
  UpdateProfileTextField: {
    marginTop: "2%",
    marginLeft: 15,
    color: Colors.lightblack,
    fontFamily: Fonts.family.fontFamilyBold
  },
  RecoverPasswordInputField: {
    width: "80%",
    color: Colors.blackColor,
    marginLeft: 20,
    height: 50,
    fontFamily: Fonts.family.fontfamily
  },
  RecoverPasswordInputUnderline: {
    flex: 1,
    borderBottomWidth: 0.7,
    borderColor: Colors.lightblack
  },
  RecoverPasswordInput: {
    marginLeft: 10,
    marginRight: 10,
    borderColor: Colors.blackColor,
    borderWidth: 0.5,
    borderRadius: 5
  },
  RecoverPasswordButton: {
    marginTop: 20,
    alignSelf: "center",
    alignItems: "center",
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 60,
    paddingRight: 60,
    backgroundColor: Colors.primaryColor,
    borderRadius: 60
  },
  RecoverPasswordButtonText: {
    justifyContent: "center",
    alignItems: "center",
    fontFamily: Fonts.family.fontfamily,
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.normal
  }
});
