import React, { Component } from "react";
import {
  Image,
  Text,
  View,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import { observable } from "mobx";
import { observer } from "mobx-react";
import Loader from "../../../Components/Loader";
import RadioForm from "react-native-simple-radio-button";
import AuthStore from "../../../stores/AuthStore";

import { Actions as NavigationActions } from "react-native-router-flux";

import styles from "./Styles/InitialStyles";
import Colors from "../../../Themes/Colors";

import { INITIALSCREENCONSTANTS } from "../../../Constants/Common";

@observer
export default class Initial extends Component {
  @observable mode_value: String;
  @observable navigate: Boolean;
  @observable displayScreen: Boolean;
  constructor(props) {
    super(props);
    this.mode_value = INITIALSCREENCONSTANTS.DOCTOR_MODE;
    this.navigate = false;
    this.displayScreen = false;
  }

  componentDidMount() {
    this.getAccessToken();
  }

  getAccessToken = async () => {
    let async_keys = await AsyncStorage.getAllKeys();
    let async_values = await AsyncStorage.multiGet(async_keys);
    if (async_keys.length > 0) {
      this.navigate = true;
      AuthStore.setAccessToken(
        async_values[async_keys.indexOf("AccessToken")][1]
      );
      if (async_keys.indexOf("patient_id") > -1) {
        AuthStore.getPatientProfileAPI({
          patient_id: async_values[async_keys.indexOf("patient_id")][1]
        });
      } else if (async_keys.indexOf("doctor_id") > -1) {
        AuthStore.getDoctorProfileAPI({
          doctor_id: async_values[async_keys.indexOf("doctor_id")][1]
        });
      }
    } else {
      this.displayScreen = true;
    }
  };

  handleMode = value => {
    AuthStore.mode = value;
    if (value === 0) {
      this.mode_value = INITIALSCREENCONSTANTS.DOCTOR_MODE;
    } else {
      this.mode_value = INITIALSCREENCONSTANTS.PATIENT_MODE;
    }
  };

  render() {
    let radio_props = [
      { label: INITIALSCREENCONSTANTS.DOCTOR, value: 0 },
      { label: INITIALSCREENCONSTANTS.PATIENT, value: 1 }
    ];
    if (
      this.navigate &&
      AuthStore.getProfileStatus === 200 &&
      NavigationActions.currentScene === "Initial"
    ) {
      if (AuthStore.mode === 0) {
        if (
          AuthStore.loginResponse.specialization === "" ||
          !AuthStore.loginResponse.specialization
        ) {
          NavigationActions.DoctorWelcomeScreen();
        } else {
          NavigationActions.DoctorHomeScreen();
        }
      } else {
        if (__DEV__) {
          console.log(
            !AuthStore.loginResponse.expected_date,
            !AuthStore.loginResponse.menstruation_date
          );
        }
        if (
          !AuthStore.loginResponse.expected_date &&
          !AuthStore.loginResponse.menstruation_date &&
          AuthStore.loginResponse.children.length < 1
        ) {
          NavigationActions.PatientPreWelcomeScreen();
        } else {
          // else if (AuthStore.loginResponse.patient_attributes.length < 1) {
          //   NavigationActions.PatientWelcomeScreen({ type: "reset" });
          // }
          NavigationActions.PatientHomeScreen();
        }
      }
    }
    if (this.displayScreen) {
      return (
        <View style={styles.MainScreen}>
          <Image
            style={styles.BgImg}
            source={require("../../../Images/LaunchImage.jpg")}
            resizeMode="contain"
          />
          <View style={styles.AppNameView}>
            <Text style={styles.AppName}>
              {INITIALSCREENCONSTANTS.APP_NAME}
            </Text>
            <Text style={styles.AppNameTagLine}>
              {INITIALSCREENCONSTANTS.APP_NAME_TAGLINE}
            </Text>
          </View>
          <View style={styles.ButtonHolder}>
            {/*<View style={styles.SmallButtonHolder}>
              <Text style={styles.DoctorMode}>
                {this.mode_value}
              </Text>
            </View>*/}
            <View style={styles.RadioButtonHolder}>
              <RadioForm
                radio_props={radio_props}
                initial={0}
                buttonSize={10}
                style={styles.RadioButton}
                formHorizontal={true}
                buttonColor={Colors.primaryColor}
                selectedButtonColor={Colors.primaryColor}
                labelColor={Colors.blackColor}
                labelStyle={styles.RadioButtonText}
                onPress={value => this.handleMode(value)}
              />
            </View>
            <TouchableOpacity
              underlayColor="transparent"
              activeOpacity={1}
              onPress={() => {
                NavigationActions.SignUp();
              }}
            >
              <View style={styles.SigupBtn}>
                <Text style={styles.SignupBtnText}>
                  {INITIALSCREENCONSTANTS.SIGNUP}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              underlayColor="transparent"
              activeOpacity={1}
              onPress={() => {
                NavigationActions.LogIn();
              }}
            >
              <View style={styles.SiginBtn}>
                <Text style={styles.SigninBtnText}>
                  {INITIALSCREENCONSTANTS.SIGNIN}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    return <Loader animating={true} />;
  }
}
