import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  StatusBar,
  Alert,
  Image,
  ScrollView
} from "react-native";
import AuthStore from "../../../stores/AuthStore";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../Components/Loader";
import Images from "../../../Themes/Images";

import styles from "./Styles/ResetPasswordStyles";
import { RESETPASSWORDCONSTANTS } from "../../../Constants/Common";

@observer
export default class ResetPassword extends Component {
  @observable otp: String;
  @observable new_pwd: String;
  @observable cnfm_pwd: String;
  @observable required_otp: String;
  @observable required_new_pwd: String;
  @observable required_cnfm_pwd: String;
  constructor(props) {
    super(props);
    this.required_otp = null;
    this.required_new_pwd = null;
    this.required_cnfm_pwd = null;
    this.otp = "";
    this.new_pwd = "";
    this.cnfm_pwd = "";
  }
  componentWillMount() {
    AuthStore.resetRecoverPwdState();
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  renderTitle = () => {
    return (
      <View style={styles.ResetPasswordHeader}>
        <Text style={styles.ResetPasswordHeaderText}>
          {RESETPASSWORDCONSTANTS.RESET_PASSWORD_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };
  handleOTP = otp => {
    this.required_otp = null;
    this.otp = otp;
    if (!this.otp) {
      this.required_otp = "*";
    }
  };
  handleNewPwd = new_pwd => {
    this.required_new_pwd = null;
    this.new_pwd = new_pwd;
    if (!this.new_pwd) {
      this.required_new_pwd = "*";
    }
  };
  handleCnfmPwd = cnfm_pwd => {
    this.required_cnfm_pwd = null;
    this.cnfm_pwd = cnfm_pwd;
    if (!this.cnfm_pwd) {
      this.required_cnfm_pwd = "*";
    }
  };
  resetPasswordRequest = () => {
    if (this.otp === "") {
      this.required_otp = "*";
    } else if (this.new_pwd === "") {
      this.required_new_pwd = "*";
    } else if (this.cnfm_pwd === "") {
      this.required_cnfm_pwd = "*";
    } else if (this.new_pwd !== this.cnfm_pwd) {
      alert(RESETPASSWORDCONSTANTS.RESET_PASSWORD_ERROR);
      this.new_pwd = "";
      this.cnfm_pwd = "";
    } else {
      let requestObject = {
        username: "",
        password: this.new_pwd,
        country_code: "+91",
        phone_number: this.props.phoneno,
        // email: this.props.email,
        client_secret: "client_secret",
        client_id: "client_id",
        auth_type: "phone_number",
        token: this.otp
      };
      AuthStore.resetPassword(requestObject);
    }
  };
  render() {
    if (AuthStore.resetPasswordStatus === 200) {
      NavigationActions.LogIn({ type: "popTo" });
    }
    if (AuthStore.resetPasswordStatus === 100) {
      return <Loader animating={true} />;
    }
    if (AuthStore.resetPasswordStatus > 200) {
      Alert.alert("Error", AuthStore.error.statusText, [
        {
          text: "OK",
          onPress: () => AuthStore.resetResetPasswordState()
        }
      ]);
    }
    return (
      <ScrollView
        showsVerticalScrollIndicatot={false}
        style={styles.ResetPasswordMainScreen}
      >
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <View style={styles.ResetPasswordMainScreen}>
          <View style={styles.ResetPasswordInput}>
            {this.otp ? (
              <Text style={styles.UpdateProfileTextField}>
                {RESETPASSWORDCONSTANTS.OTP_FIELD_HEADING}
              </Text>
            ) : null}
            <View style={{ flexDirection: "row" }}>
              <TextInput
                keyboardType="numeric"
                autoCapitalize="none"
                autoCorrect={false}
                autoFocus={true}
                returnKeyType={"next"}
                style={styles.ResetPasswordInputField}
                onChangeText={this.handleOTP}
                placeholder={RESETPASSWORDCONSTANTS.OTP_FIELD_PLACEHOLDER}
                underlineColorAndroid="transparent"
                value={this.otp}
              />
              {this.required_otp && (
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>
              )}
            </View>
            <View style={styles.ResetPasswordInputUnderline} />
            {this.new_pwd ? (
              <Text style={styles.UpdateProfileTextField}>
                {RESETPASSWORDCONSTANTS.NEW_PASSWORD_FIELD_HEADING}
              </Text>
            ) : null}
            <View style={{ flexDirection: "row" }}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                returnKeyType={"next"}
                style={styles.ResetPasswordInputField}
                onChangeText={this.handleNewPwd}
                secureTextEntry
                placeholder={
                  RESETPASSWORDCONSTANTS.NEW_PASSWORD_FIELD_PLACEHOLDER
                }
                underlineColorAndroid="transparent"
                value={this.new_pwd}
              />
              {this.required_new_pwd && (
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>
              )}
            </View>
            <View style={styles.ResetPasswordInputUnderline} />
            {this.cnfm_pwd ? (
              <Text style={styles.UpdateProfileTextField}>
                {RESETPASSWORDCONSTANTS.CONFIRM_PASSWORD_FIELD_HEADING}
              </Text>
            ) : null}
            <View style={{ flexDirection: "row" }}>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                style={styles.ResetPasswordInputField}
                onChangeText={this.handleCnfmPwd}
                secureTextEntry
                placeholder={
                  RESETPASSWORDCONSTANTS.CONFIRM_PASSWORD_FIELD_PLACEHOLDER
                }
                underlineColorAndroid="transparent"
                value={this.cnfm_pwd}
              />
              {this.required_cnfm_pwd && (
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>
              )}
            </View>
          </View>
          <TouchableOpacity
            underlayColor="#eee"
            activeOpacity={1}
            onPress={this.resetPasswordRequest}
          >
            <View style={styles.ResetPasswordButton}>
              <Text style={styles.ResetPasswordButtonText}>
                {RESETPASSWORDCONSTANTS.RESET_PASSWORD_BUTTON_TEXT}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}
