import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";
import _ from "lodash";
import { Actions } from "jumpstate";
import Loader from "../../../Components/Loader";

// Styles
import styles from "./Styles/DoctorProfileStyles";
export default class DoctorProfile1 extends Component {
  constructor(props) {
    super(props);
    this.value = "search";
  }

  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  renderTitle = () => {
    return (
      <View style={styles.DoctorProfileHeader}>
        <Text style={styles.DoctorProfileHeaderText}>
          Dr.Prasanth
        </Text>
      </View>
    );
  };
  start = () => {
    return null;
  };
  render() {
    return (
      <View style={styles.MainScreen}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.DoctorProfilePic}>
            <Image
              style={styles.ProfilePic}
              resizemode="contain"
              source={require("../../../Images/account_purple.png")}
            />
          </View>
          <View style={styles.DoctorDetails}>
            <Text style={styles.DoctorName}>Dr.Prasanth</Text>
            <Text style={styles.SpecialistIn}>All in One</Text>
            <View style={styles.OpenTimings}>
              <Text style={styles.OpenOrNot}>Open Now</Text>
              <Text style={styles.Timings}>9AM to 9PM</Text>
            </View>
          </View>
          <View style={styles.BookingnNetworkBtns}>
            <View style={styles.BookAppointmentView}>
              <TouchableOpacity>
                <Text style={styles.BookAppointmentText}>
                  Book Appointment
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.AddNetworkView}>
              <TouchableOpacity>
                <Text style={styles.AddNetworkText}>
                  Add to Network
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.AddressnNavigation}>
            <View style={styles.Address}>
              <Text style={styles.AddressHeading}>Address</Text>
              <Text style={styles.StreetName}>F2/11, Sunder Milan CHS</Text>
              <Text style={styles.AreaName}>
                Sunder Nagar, Malad West
              </Text>
              <Text style={styles.CityName}>Mumbai</Text>
              <View stle={styles.Pincode}>
                <View>
                  <Text style={styles.PincodeHeading}>Pincode</Text>
                </View>
                <View>
                  <Text style={styles.PincodeNumber}>522265</Text>
                </View>
              </View>
            </View>
            <View style={styles.NavigationImage}>
              <Image
                resizemode="contain"
                style={styles.NavigationMap}
                source={require("../../../Images/checkbox_on.png")}
              />
              <TouchableOpacity>
                <Text style={styles.NavigationText}>MAPS & NAVIGATION ></Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.DoctorTimings}>
            <Text style={styles.TimingsHead}>TIMINGS</Text>
            <View style={styles.TimingsView}>
              <View style={styles.EachDay}>
                <Text style={styles.DayName}>Monday</Text>
                <Text style={styles.DayTimings}>7AM to 7PM</Text>
              </View>
              <View style={styles.EachDay}>
                <Text style={styles.DayName}>Tuesday</Text>
                <Text style={styles.DayTimings}>7AM to 7PM</Text>
              </View>
              <View style={styles.EachDay}>
                <Text style={styles.DayName}>Wednesday</Text>
                <Text style={styles.DayTimings}>7AM to 7PM</Text>
              </View>
              <View style={styles.EachDay}>
                <Text style={styles.DayName}>Thursday</Text>
                <Text style={styles.DayTimings}>7AM to 7PM</Text>
              </View>
              <View style={styles.EachDay}>
                <Text style={styles.DayName}>Friday</Text>
                <Text style={styles.DayTimings}>7AM to 7PM</Text>
              </View>
              <View style={styles.EachDay}>
                <Text style={styles.DayName}>Saturday</Text>
                <Text style={styles.DayTimings}>7AM to 7PM</Text>
              </View>
              <View style={styles.EachDay}>
                <Text style={styles.DayName}>Sunday</Text>
                <Text style={styles.DayTimings}>7AM to 7PM</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
