import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  AsyncStorage,
  TouchableOpacity,
  WebView,
  Platform,
  TextInput,
  StatusBar
} from "react-native";
import PatientProfileStore from "../../../../stores/PatientProfileStore";
import PatientProfileUpdateStore from "../../../../stores/PatientProfileUpdateStore";
import PopupMenu from "../../../../Components/PopupMenu";
import PopupMenuIOS from "../../../../Components/PopupMenuIOS";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../../Components/Loader";
import { observable } from "mobx";
import { observer } from "mobx-react";
import AuthStore from "../../../../stores/AuthStore";
import Images from "../../../../Themes/Images";
import { Fonts, Colors } from "../../../../Themes/";
import FormFactory from "../../../../GenericComponents";
// Styles
import styles from "./styles.js";
import { PATIENTWELLCOMESCREENCONSTANTS } from "../../../../Constants/Patient";
@observer
export default class PatientWelcomeScreen extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
    PatientProfileStore.getDefaultAttributesFormAPI();
  }
  renderTitle = () => {
    return (
      <View style={styles.Header}>
        <Text style={styles.HeaderText}>Create Profile</Text>
      </View>
    );
  };
  navigateToCreateProfile = () => {
    // NavigationActions.PatientHomeScreen({ type: "reset" });
    PatientProfileUpdateStore.resetQuickRecordsState();
    PatientProfileStore.getPatientProfile({
      patient_id: this.props.selectedParent
    });

    let periodicalRequestObject = {
      entity_id: AuthStore.loginResponse.patient_id,
      entity_type: "PATIENT"
    };
    PatientProfileUpdateStore.getPeriodicalAttributes(periodicalRequestObject);
    NavigationActions.pop();
  };

  formattedForm = () => {
    let formattedFormObject = {};
    let array = PatientProfileStore.formAPIResponse.slice();
    array.map(formObject => {
      if (formObject.name === "ALL_FIELDS") {
        formattedFormObject = JSON.parse(formObject.form_json);
      }
    });
    return formattedFormObject;
  };

  createParameterRequestObject = parameterField => {
    let formattedAttributedValues = [];
    parameterField.parameterFieldList.map(field => {
      if (!field.value) {
        return;
      }
      formattedAttributedValues.push({
        value_key: field.name.toUpperCase(),
        value: field.value
      });
    });
    let finalObject = {
      attr_key: parameterField.name.toUpperCase(),
      description: parameterField.unit,
      attr_values: formattedAttributedValues,
      to_add: true
    };
    if (
      formattedAttributedValues.length ===
      parameterField.parameterFieldList.length
    ) {
      return finalObject;
    }
    return;
  };
  createFieldRequestObject = field => {
    if (!field.value) {
      return;
    }
    let formattedAttributedrequestObject = {
      attr_key: field.name.toUpperCase(),
      description: field.unit,
      attr_values: [
        {
          value_key: field.name.toUpperCase(),
          value: field.value
        }
      ],
      to_add: true
    };
    return formattedAttributedrequestObject;
  };

  onSubmit = formFields => {
    //Validation
    let validateAllFields = true;
    formFields.map(formField => {
      if (formField.type === "PARAMETER") {
        formField.parameterFieldList.map(parameterField => {
          if (parameterField.isError || parameterField.value === null) {
            validateAllFields = false;
          }
        });
      }
      if (formField.type === "FIELD") {
        if (formField.isError || formField.value === null) {
          validateAllFields = false;
        }
      }
    });
    // if (!validateAllFields) {
    //   alert("Please Fill All Details");
    //   return;
    // }

    let parameterRequestObjects = [];
    let fieldRequestObjects = [];
    formFields.map(formField => {
      if (formField.type === "PARAMETER") {
        let formattedObject = this.createParameterRequestObject(formField);
        if (formattedObject && formattedObject.attr_values.length > 0) {
          parameterRequestObjects.push(formattedObject);
        }
      }
      if (formField.type === "FIELD") {
        let formattedObject = this.createFieldRequestObject(formField);
        if (formattedObject.attr_values.length > 0) {
          fieldRequestObjects.push(formattedObject);
        }
      }
    });

    let requestObject = {
      attributes: [...fieldRequestObjects, ...parameterRequestObjects],
      entity_id: AuthStore.loginResponse.patient_id,
      activity_required: true,
      entity_type: "PATIENT"
    };
    if (!requestObject.attributes.length) {
      alert("Please Fill Atleast One Parameter Details");
      return;
    }
    if (__DEV__) {
      console.log(requestObject);
    }

    PatientProfileStore.createPatientAttributeAPI(
      requestObject,
      this.navigateToCreateProfile
    );

    //Make the network call
  };
  render() {
    // if (
    //   AuthStore.loginResponse === undefined || AuthStore.loginResponse === null
    // ) {
    //   return (
    //     <View style={styles.loaderContainer}>
    //       <Loader animating={true} color={Colors.primaryColor} />
    //     </View>
    //   )
    // }
    return (
      <View style={styles.mainContainer}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.bodyContainer}>
            <View style={styles.formHeadingContainer}>
              <Text style={styles.formHeadingStyles}>
                {PATIENTWELLCOMESCREENCONSTANTS.PLEASE_FILL_DETAILS_NOTE}
              </Text>
            </View>
            <FormFactory
              specFile={this.formattedForm()}
              onSubmit={this.onSubmit}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}
