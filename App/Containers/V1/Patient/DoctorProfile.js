import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";
import DoctorProfileStore from "../../../stores/DoctorProfileStore";
import _ from "lodash";
import { Actions } from "jumpstate";
import Loader from "../../../Components/Loader";
import { observer } from "mobx-react";
import moment from "moment";
import Images from "../../../Themes/Images";
import { DOCTORPROFILECONSTANTS } from "../../../Constants/Patient";

import chatStore from "../../Chat/stores/ChatStore";
// Styles
import styles from "./Styles/DoctorProfileStyles";
@observer
export default class DoctorProfile extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  renderTitle = () => {
    return (
      <View style={styles.DoctorProfileHeader}>
        <Text style={styles.DoctorProfileHeaderText}>
          {NavigationActions.currentParams.name}
        </Text>
      </View>
    );
  };

  getChatRoom = user_id => {
    let chatRooms = chatStore.rooms.values();
    let chatRoom = {};
    chatRooms.map(eachRoom => {
      eachRoom.members.map(eachMember => {
        if (Number(eachMember.id) === user_id) {
          chatRoom = eachRoom;
        }
      });
    });
    this.gotoChatList(chatRoom);
  };

  gotoChatList = chatRoom => {
    chatStore.setSelectedChatRoom(chatRoom);
    NavigationActions.ChatRoom({ title: chatRoom.name });
  };

  render() {
    let {
      doctor_id,
      pic_thumbnail,
      city,
      name,
      pic,
      geo_location,
      location,
      address,
      specialization
    } = DoctorProfileStore.doctorProfileResponse;
    if (__DEV__)
      console.log("Render", DoctorProfileStore.doctorProfileResponse);
    return (
      <View style={styles.MainScreen}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.DoctorProfilePic}>
            {pic ? (
              <Image
                style={styles.ProfilePic}
                resizeMode="cover"
                source={{ uri: pic }}
              />
            ) : (
              <Image
                style={styles.ProfilePic}
                resizeMode="cover"
                source={Images.femaleDoctorPic}
              />
            )}
          </View>
          <View style={styles.DoctorDetails}>
            <Text style={styles.DoctorName}>{name}</Text>
            <Text style={styles.SpecialistIn}>{specialization}</Text>
            {/*<View style={styles.OpenTimings}>
              <Text style={styles.OpenOrNot}>Open Now</Text>
              <Text style={styles.Timings}>9AM to 9PM</Text>
            </View>*/}
          </View>
          {/*<View style={styles.BookingnNetworkBtns}>
            <View style={styles.BookAppointmentView}>
              <TouchableOpacity>
                <Text style={styles.BookAppointmentText}>
                  Book Appointment
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.AddNetworkView}>
              <TouchableOpacity>
                <Text style={styles.AddNetworkText}>
                  Add to Network
                </Text>
              </TouchableOpacity>
            </View>
          </View>*/}
          {this.props.status === "connected" &&
          chatStore.rooms.values().length > 0 ? (
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                onPress={() => {
                  this.getChatRoom(this.props.user_id);
                }}
                style={styles.chatButtonContainer}
              >
                <Text style={styles.chatButtonTextStyles}>Chat</Text>
              </TouchableOpacity>
            </View>
          ) : null}
          <View style={styles.AddressnNavigation}>
            <View style={styles.Address}>
              <Text style={styles.AddressHeading}>
                {DOCTORPROFILECONSTANTS.ADDRESS_FIELD_TEXT}
              </Text>
              <Text style={styles.StreetName}>{location}</Text>
              <Text style={styles.AreaName}>{city}</Text>
              <Text style={styles.CityName}>{address}</Text>
              {/*<View stle={styles.Pincode}>
                <View>
                  <Text style={styles.PincodeHeading}>Pincode</Text>
                </View>
                <View>
                  <Text style={styles.PincodeNumber}>522265</Text>
                </View>
              </View>*/}
            </View>
            {/*<View style={styles.NavigationImage}>
              <Image
                resizemode="contain"
                style={styles.NavigationMap}
                source={require("../../../Images/checkbox_on.png")}
              />
              <TouchableOpacity>
                <Text style={styles.NavigationText}>MAPS & NAVIGATION ></Text>
              </TouchableOpacity>
            </View>*/}
          </View>
          <View style={styles.DoctorTimings}>
            <Text style={styles.TimingsHead}>
              {DOCTORPROFILECONSTANTS.TIMING_FIELD_TEXT}
            </Text>
            <View style={styles.TimingsView}>
              {DoctorProfileStore.doctorTimingsResponse.total > 0 &&
                DoctorProfileStore.doctorTimingsResponse.events[0].occurrences.map(
                  (data, index) => {
                    return (
                      <View style={styles.EachDay} key={index}>
                        <Text style={styles.DayName}>{data.title}</Text>
                        <Text style={styles.DayTimings}>
                          {moment(data.start).format("HH:mm")} to{" "}
                          {moment(data.end).format("HH:mm")}
                        </Text>
                      </View>
                    );
                  }
                )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
