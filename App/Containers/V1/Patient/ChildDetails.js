import React from "react";
import {
  TouchableOpacity,
  View,
  Text,
  ScrollView,
  TextInput,
  Image
} from "react-native";
import RadioForm, {
  RadioButtonInput,
  RadioButtonLabel
} from "react-native-simple-radio-button"
import { observable, toJS } from "mobx";
import { observer } from "mobx-react";
import AuthStore from "../../../stores/AuthStore";
import Images from "../../../Themes/Images";
import Colors from "../../../Themes/Colors";
import styles from "./Styles/ChildDetailsStyles";
import Fonts from "../../../Themes/Fonts";
import {CHILDDETAILSCONSTANTS} from "../../../Constants/Patient";
@observer
export default class ChildDetails extends React.Component {
  @observable value: Array;
  @observable children: Array;
  constructor(props) {
    super(props);
    this.children = toJS(AuthStore.loginResponse.children);
    this.value = [];
  }
  componentWillMount() {
    if (AuthStore.loginResponse.children.length === 0) {
      let new_child = {
        gender: "M",
        age: "",
        child_attributes: [],
        name: "",
        child_id: -1,
        to_add: true
      };
      this.children = [new_child];
    }
  }
  handleName = (name, id) => {
    let value;
    this.children[id].name = name;
    if (!this.children[id].name) {
      value = "*";
      this.children[id].requiredName = value;
    } else {
      value = null;
      this.children[id].requiredName = value;
    }
  };
  handleAge = (age, id) => {
    let value;
    this.children[id].age = age;
    if (!this.children[id].age) {
      value = "*";
      this.children[id].requiredAge = value;
    } else {
      value = null;
      this.children[id].requiredAge = value;
    }
  };
  handleOnPress = (gender, id) => {
    let value;
    if (gender === 0) {
      value = "M";
      this.children[id].gender = value;
    } else if (gender === 1) {
      value = "F";
      this.children[id].gender = value;
    }
  };
  addChild = () => {
    let new_child = {
      gender: "M",
      age: "",
      child_attributes: [],
      name: "",
      child_id: -1,
      to_add: true,
      requiredName: null,
      requiredAge: null
    };
    this.children.push(new_child);
  };
  displayChildFields = () => {
    let gender = [
     { label: "Male", value: 0 },
     { label: "Female", value: 1 }
   ]
    return this.children.map((data, index) => {
      return (
        <View style={styles.SignUpComponent2View} key={index}>
          <View style={styles.NameInput}>
            {data.name
              ? <Text style={styles.UpdateProfileTextField}>{CHILDDETAILSCONSTANTS.NAME_FIELD_HEADING}</Text>
              : null}
            <View style={{ flexDirection: "row" }}>
              <TextInput
                style={styles.InputField}
                returnKeyType={"next"}
                placeholder={CHILDDETAILSCONSTANTS.NAME_FIELD_PLACEHOLDER}
                autoCorrect={false}
                underlineColorAndroid="transparent"
                value={data.name}
                onChangeText={value => this.handleName(value, index)}
              />
              {data.requiredName &&
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>}
            </View>
          </View>
          <View style={styles.NameInput}>
            {data.age
              ? <Text style={styles.UpdateProfileTextField}>{CHILDDETAILSCONSTANTS.AGE_FIELD_HEADING} </Text>
              : null}
            <View style={{ flexDirection: "row" }}>
              <TextInput
                keyboardType="numeric"
                style={styles.InputField}
                placeholder={CHILDDETAILSCONSTANTS.AGE_FIELD_PLACEHOLDER}
                autoCorrect={false}
                underlineColorAndroid="transparent"
                value={"" + data.age}
                onChangeText={value => this.handleAge(value, index)}
              />
              {data.requiredAge &&
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>}
            </View>
          </View>

          {/* <View style={styles.ChildrenDetailsHead} /> */}

          {data.age
            ? <Text style={styles.UpdateProfileTextField}>{CHILDDETAILSCONSTANTS.GENDER_FIELD_HEADING_TEXT} </Text>
            : null}
          <View style={styles.RadiobuttonView}>
            <RadioForm
                radio_props={gender}
                initial={data.gender === "M" ? 0 : 1}
                buttonSize={10}
                formHorizontal={true}
                labelHorizontal={true}
                buttonColor={Colors.primaryColor}
                labelStyle={{
                  color: Colors.blackColor,
                  fontFamily: Fonts.family.fontfamily
                }}
                animation={true}
                style={styles.RadioFormStylesGender}
                onPress={value => this.handleOnPress(value,index)}
              />
          </View>
        </View>
      );
    });
  };
  render() {
    if (__DEV__) {
      console.log(
        "child Details",
        this.children,
        AuthStore.loginResponse.children
      );
    }
    return (
      <View style={styles.Card}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.AddAnotherChildView}>
            <Text style={styles.ComponentHeader}>{CHILDDETAILSCONSTANTS.CHILD_DETAILS_CARD_TEXT}</Text>
            <TouchableOpacity onPress={() => this.addChild()}>
              <View style={styles.Daycount2}>
                <Text style={styles.AddAnotherChild}>{CHILDDETAILSCONSTANTS.ADD_ANOTHER_CHILD_BUTTON_TEXT}</Text>
              </View>
            </TouchableOpacity>
          </View>
          {this.displayChildFields()}
          {/*date picker*/}
        </ScrollView>
      </View>
    );
  }
}
