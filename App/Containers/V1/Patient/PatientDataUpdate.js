import React, { Component } from "react";
import {
  ScrollView,
  Text,
  Image,
  View,
  TouchableOpacity,
  StatusBar,
  TextInput,
  Alert,
  Picker,
  Keyboard
} from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import { observable, toJS } from "mobx";
import { observer } from "mobx-react";
import { Actions as NavigationActions } from "react-native-router-flux";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Loader from "../../../Components/Loader";
import PatientProfileUpdateStore from "../../../stores/PatientProfileUpdateStore";
import PatientProfileStore from "../../../stores/PatientProfileStore";
import moment from "moment";
import Images from "../../../Themes/Images";
// Styles
import styles from "./Styles/PatientProfileUpdateStyles";
import { PATIENTDATAUPDATECONSTANTS } from "../../../Constants/Patient";

//import genericcomponents
import FormFactory from "../../../GenericComponents";

@observer
export default class PatientDataUpdate extends Component {
  @observable isDateTimePickerVisible: Boolean;
  @observable date: String;
  @observable records: Array;
  @observable required_field: String;
  @observable required_date: String;

  @observable attribute_keys: Array;
  @observable selectedValuePicker: String;
  constructor(props) {
    super(props);
    this.isDateTimePickerVisible = false;
    this.date = null;
    this.records = [];
    this.required_field = null;
    this.required_date = null;

    this.attribute_keys = [];

    this.selectedValuePicker = props.selectedRecord;

    //this.props.selectedValuePicker
  }
  renderTitle = () => {
    return (
      <View style={styles.PatientYourProfileHeader}>
        <Text style={styles.PatientYourProfileHeaderText}>
          {PATIENTDATAUPDATECONSTANTS.YOUR_DATA_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };
  goBack = () => {
    PatientProfileUpdateStore.resetQuickRecordsState();
    PatientProfileStore.getPatientProfile({
      patient_id: this.props.selectedParent
    });
    NavigationActions.pop();
  };
  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle,
      onBack: this.goBack
    });
    PatientProfileStore.getDefaultAttributesFormAPI();
  }

  getData = () => {
    if (__DEV__) {
      console.log("Get Data", this.date);
    }
    let date = this.date;
    let requestObject = {
      attr_keys: PatientProfileStore.defaultAttributesArray,
      from_date: moment(date).format("YYYY-MM-DD") + " 00:00",
      to_date: moment(date).format("YYYY-MM-DD") + " 23:59",
      child_id:
        this.props.selectedPerson === "Parent" ? -1 : this.props.selectedChild,
      is_child_data: this.props.selectedPerson === "Parent" ? false : true
    };
    if (__DEV__) {
      console.log(requestObject);
    }
    PatientProfileUpdateStore.getQuickRecords(requestObject);
  };

  _showDateTimePicker = () => {
    Keyboard.dismiss();
    this.isDateTimePickerVisible = true;
  };

  _hideDateTimePicker = () => {
    this.isDateTimePickerVisible = false;
  };

  _handleDatePicked = date => {
    //Get values for that date
    this.date = moment(date).format("YYYY-MM-DD");
    this.required_date = null;
    this.records = [];
    this.getData();
    this._hideDateTimePicker();
  };

  // getAttributes = () => {
  //   let records = []
  //   if (this.records.length > 0) {
  //     this.records.map((eachRecord, index) => {
  //       records.push({
  //         to_update: true,
  //         attr_key: eachRecord.attr_key,
  //         activity_id: eachRecord.activity_id,
  //         creation_datetime: eachRecord.update_time,
  //         value_string: eachRecord.attr_value
  //       })
  //     })
  //     return records
  //   }
  //   PatientProfileUpdateStore.quickRecords.map((eachRecord, index) => {
  //     records.push({
  //       to_update: true,
  //       attr_key: eachRecord.attr_key,
  //       activity_id: eachRecord.activity_id,
  //       creation_datetime: eachRecord.update_time,
  //       value_string: eachRecord.attr_value
  //     })
  //   })
  //   return records
  // }
  // getTodayUpdatedRecord = () => {
  //   let records = []
  //   PatientProfileUpdateStore.quickRecords.map((eachRecord, index) => {
  //     records.push({
  //       attr_key: eachRecord.attr_key,
  //       description: "",
  //       attr_id: -1,
  //       attr_value: eachRecord.attr_value,
  //       to_add: true
  //     })
  //   })
  //   return records
  // }
  // saveData = () => {
  //   if (!this.date) {
  //     this.required_date = "*"
  //   } else if (
  //     this.date &&
  //     (this.records.length > 0 ||
  //       PatientProfileUpdateStore.quickRecords.length > 0)
  //   ) {
  //     if (
  //       this.records.length > 0 &&
  //       this.date === moment(new Date()).format("YYYY-MM-DD")
  //     ) {
  //       let requestObject = {
  //         attributes: this.records,
  //         entity_id: this.props.selectedPerson === "Parent"
  //           ? this.props.selectedParent
  //           : this.props.selectedChild,
  //         activity_required: true,
  //         entity_type: this.props.selectedPerson === "Parent"
  //           ? "PATIENT"
  //           : "CHILD"
  //       }
  //       if (__DEV__) {
  //         console.log(toJS(requestObject), "Create Record")
  //       }
  //       PatientProfileUpdateStore.createRecord(requestObject)
  //     } else if (
  //       PatientProfileUpdateStore.quickRecords.length > 0 &&
  //       this.date === moment(new Date()).format("YYYY-MM-DD")
  //     ) {
  //       let requestObject = {
  //         attributes: this.getTodayUpdatedRecord(),
  //         entity_id: this.props.selectedPerson === "Parent"
  //           ? this.props.selectedParent
  //           : this.props.selectedChild,
  //         activity_required: true,
  //         entity_type: this.props.selectedPerson === "Parent"
  //           ? "PATIENT"
  //           : "CHILD"
  //       }
  //       if (__DEV__) {
  //         console.log(toJS(requestObject), "Create Record")
  //       }
  //       PatientProfileUpdateStore.createRecord(requestObject)
  //     } else if (this.records.length > 0) {
  //       let requestObject = {
  //         analytics_data: this.getAttributes(),
  //         child_id: this.props.selectedPerson === "Parent"
  //           ? -1
  //           : this.props.selectedChild,
  //         is_child_data: this.props.selectedPerson === "Parent" ? false : true
  //       }
  //       if (__DEV__) {
  //         console.log(toJS(requestObject), "Update Record")
  //       }
  //       PatientProfileUpdateStore.updateRecordAnalytics(requestObject)
  //     } else {
  // let requestObject = {
  //   analytics_data: this.getAttributes(),
  //   child_id: this.props.selectedPerson === "Parent"
  //     ? -1
  //     : this.props.selectedChild,
  //   is_child_data: this.props.selectedPerson === "Parent" ? false : true
  // }
  //       if (__DEV__) {
  //         console.log(toJS(requestObject), "Update Record")
  //       }
  //       PatientProfileUpdateStore.updateRecordAnalytics(requestObject)
  //     }
  //   } else {
  //     this.required_field = "*"
  //   }
  // }

  handleRecordType = option => {
    this.selectedValuePicker = option;
  };

  createParameterRequestObject = parameterField => {
    let formattedAttributedValues = [];
    parameterField.parameterFieldList.map(field => {
      formattedAttributedValues.push({
        value_key: field.name.toUpperCase(),
        value: field.value
      });
    });
    let finalObject = {
      attr_key: parameterField.name.toUpperCase(),
      description: parameterField.unit,
      attr_values: formattedAttributedValues,
      to_add: true
    };
    return finalObject;
  };
  createFieldRequestObject = field => {
    let formattedAttributedrequestObject = {
      attr_key: field.name.toUpperCase(),
      description: field.unit,
      attr_values: [
        {
          value_key: field.name.toUpperCase(),
          value: field.value
        }
      ],
      to_add: true
    };
    return formattedAttributedrequestObject;
  };

  getAttributes = formFields => {
    let analytics_data = [];
    formFields.map(formField => {
      if (formField.type === "PARAMETER") {
        if (__DEV__) {
          console.log(toJS(formField));
        }
        formField.parameterFieldList.map((eachField, index) => {
          let formattedObject = {
            to_update: true,
            attr_key: eachField.name,
            activity_id: -1,
            creation_datetime: moment(this.date).format("YYYY-MM-DD HH:mm"),
            value_string: eachField.value
          };
          PatientProfileUpdateStore.quickRecords.map((eachRecord, index) => {
            if (eachRecord.attr_key === eachField.name) {
              formattedObject.activity_id = eachRecord.activity_id;
              formattedObject.creation_datetime = moment(
                eachRecord.update_time
              ).format("YYYY-MM-DD HH:mm");
            }
          });
          if (__DEV__) {
            console.log(formattedObject);
          }
          analytics_data.push(formattedObject);
        });
      }
      if (formField.type === "FIELD") {
        if (__DEV__) {
          console.log(formField);
        }
        let formattedObject = {
          to_update: true,
          attr_key: formField.name,
          activity_id: -1,
          creation_datetime: moment(this.date).format("YYYY-MM-DD HH:mm"),
          value_string: formField.value
        };
        PatientProfileUpdateStore.quickRecords.map((eachRecord, index) => {
          if (eachRecord.attr_key === formField.name) {
            formattedObject.activity_id = eachRecord.activity_id;
            formattedObject.creation_datetime = moment(
              eachRecord.update_time
            ).format("YYYY-MM-DD HH:mm");
          }
        });
        if (__DEV__) {
          console.log(formattedObject);
        }
        analytics_data.push(formattedObject);
      }
    });
    if (__DEV__) {
      console.log(analytics_data);
    }
    return analytics_data;
  };

  onSubmit = formFields => {
    if (this.selectedValuePicker === "SELECT_RECORD_TYPE") {
      alert("Please select Record Type");
      return;
    }
    //Validation
    let validateAllFields = true;
    formFields.map(formField => {
      if (formField.type === "PARAMETER") {
        formField.parameterFieldList.map(parameterField => {
          if (parameterField.isError || parameterField.value === null) {
            validateAllFields = false;
          }
        });
      }
      if (formField.type === "FIELD") {
        if (formField.isError || formField.value === null) {
          validateAllFields = false;
        }
      }
    });
    if (!validateAllFields) {
      alert("Please Fill All Details");
      return;
    }

    // let parameterRequestObjects = []
    // let fieldRequestObjects = []
    // formFields.map(formField => {
    //   if (formField.type === "PARAMETER") {
    //     let formattedObject = this.createParameterRequestObject(formField)
    //     parameterRequestObjects.push(formattedObject)
    //   }
    //   if (formField.type === "FIELD") {
    //     let formattedObject = this.createFieldRequestObject(formField)
    //     fieldRequestObjects.push(formattedObject)
    //   }
    // })

    // let requestObject = {
    //   attributes: [...fieldRequestObjects, ...parameterRequestObjects],
    //   entity_id:
    //     this.props.selectedPerson === "Parent"
    //       ? this.props.selectedParent
    //       : this.props.selectedChild,
    //   activity_required: true,
    //   entity_type: this.props.selectedPerson === "Parent" ? "PATIENT" : "CHILD"
    // }

    let requestObject = {
      analytics_data: this.getAttributes(formFields),
      child_id:
        this.props.selectedPerson === "Parent" ? -1 : this.props.selectedChild,
      is_child_data: this.props.selectedPerson === "Parent" ? false : true
    };

    if (__DEV__) {
      console.log(requestObject);
    }

    PatientProfileUpdateStore.updateRecordAnalytics(requestObject);

    //Make the network call
  };

  formattedFilledForm = () => {
    let formattedFormObjectArray = {};
    let formattedFormObject = {};
    let array = PatientProfileStore.formAPIResponse.slice();
    formattedFormObjectArray = array.filter(formObject => {
      return formObject.name === this.selectedValuePicker;
    });
    formattedFormObjectArray.map(formObject => {
      formattedFormObject = JSON.parse(formObject.form_json);
    });

    if (PatientProfileUpdateStore.quickRecords.length > 0) {
      if (__DEV__) {
        console.log(formattedFormObject);
      }
      formattedFormObject.fields.map((eachForm, index) => {
        if (eachForm.type === "FIELD") {
          PatientProfileUpdateStore.quickRecords.map((eachRecord, index) => {
            if (eachRecord.attr_key === eachForm.name) {
              eachForm.value = eachRecord.attr_value;
            }
          });
        } else if (eachForm.type === "PARAMETER") {
          eachForm.fields.map((eachField, index) => {
            PatientProfileUpdateStore.quickRecords.map((eachRecord, index) => {
              if (eachRecord.attr_key === eachField.name) {
                eachField.value = eachRecord.attr_value;
              }
            });
          });
        }
      });
    }

    //Filled values //Weight REsponse,BP,sugar

    return formattedFormObject;
  };
  getValues = () => {
    let records = PatientProfileUpdateStore.quickRecords;
    let values = {};
    let valueKeys = this.attribute_keys;
    if (records.length > 0) {
      records.map((eachRecord, index) => {
        values[eachRecord.attr_key] = "";
        if (valueKeys.indexOf(eachRecord.attr_key) > -1) {
          values[eachRecord.attr_key] = eachRecord.attr_value;
        }
      });
    } else if (this.records.length > 0) {
      this.records.map((eachRecord, index) => {
        values[eachRecord.attr_key] = "";
        if (valueKeys.indexOf(eachRecord.attr_key) > -1) {
          values[eachRecord.attr_key] = eachRecord.attr_value;
        }
      });
    } else {
      valueKeys.map((eachValue, index) => {
        values[eachValue] = "";
      });
    }
    if (__DEV__) {
      console.log(values);
    }
    return values;
  };

  renderPickerOptions = () => {
    let formattedFormObjectArray = [];
    let formattedFormObjectFilteredArray = [];
    let array = PatientProfileStore.formAPIResponse.slice();
    formattedFormObjectArray = array.filter(formObject => {
      return formObject.name != "ALL_FIELDS";
    });
    formattedFormObjectFilteredArray = formattedFormObjectArray.filter(
      formObject => {
        return formObject.name != "BMI";
      }
    );

    return formattedFormObjectFilteredArray.map(formObject => {
      return (
        <Picker.Item
          label={formObject.label}
          value={formObject.name}
          key={formObject.name}
        />
      );
    });
  };

  render() {
    if (
      PatientProfileUpdateStore.getQuickRecordsStatus === 100 ||
      PatientProfileUpdateStore.updateRecordsStatus === 100
    ) {
      return <Loader animating={true} />;
    } else if (PatientProfileUpdateStore.updateRecordsStatus === 200) {
      Alert.alert("Success", "Successfully updated your data", [
        {
          text: "OK",
          onPress: () => {
            this.goBack();
          }
        }
      ]);
      return null;
    } else {
      if (__DEV__) {
        console.log(
          PatientProfileUpdateStore.quickRecords,
          "PatientProfileUpdateStore.quickRecords"
        );
      }
      return (
        <ScrollView style={styles.MainScreen}>
          <KeyboardAwareScrollView extraHeight={200}>
            <DateTimePicker
              mode="date"
              isVisible={this.isDateTimePickerVisible}
              maximumDate={new Date()}
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDateTimePicker}
            />
            <View style={styles.DescriptionView}>
              <Text style={styles.Description}>
                {PATIENTDATAUPDATECONSTANTS.NOTE_YOUR_DATA_FOR_FIELD}
              </Text>
            </View>
            <View style={styles.DocInput}>
              {this.date ? (
                <Text style={styles.fieldHeadingText}>
                  {PATIENTDATAUPDATECONSTANTS.DATE_FIELD_TEXT}
                </Text>
              ) : null}

              <View style={{ flexDirection: "row" }}>
                <TextInput
                  onFocus={this._showDateTimePicker}
                  style={styles.DocInputField}
                  placeholder={
                    PATIENTDATAUPDATECONSTANTS.DATE_FIELD_PLACEHOLDER
                  }
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  value={
                    this.date ? moment(this.date).format("YYYY-MM-DD") : ""
                  }
                  returnKeyType="done"
                />
                {this.required_date && (
                  <View style={styles.ImageContainer}>
                    <Image
                      style={styles.RequiredField}
                      source={Images.alertShield}
                    />
                  </View>
                )}
              </View>
              <View style={styles.InputUnderline} />
            </View>
            <View style={styles.pickerContainer}>
              <Picker
                selectedValue={this.selectedValuePicker}
                style={styles.pickerContentStyles}
                onValueChange={option => this.handleRecordType(option)}
              >
                {this.renderPickerOptions()}
              </Picker>
            </View>

            <FormFactory
              specFile={this.formattedFilledForm()}
              onSubmit={this.onSubmit}
            />
          </KeyboardAwareScrollView>
        </ScrollView>
      );
    }
  }
}

/**
 *           <TouchableOpacity
            onPress={() =>
              NavigationActions.thresholdUpdate({
                selectedParent: this.props.selectedParent,
                selectedChild: this.props.selectedChild,
                selectedPerson: this.props.selectedPerson
              })}
            style={styles.buttonContainer}
          >
            <Text style={styles.btnTextStyles}>
              {PATIENTDATAUPDATECONSTANTS.UPDATE_MIN_MAX_BUTTON_TEXT}
            </Text>
          </TouchableOpacity>
          <View style={styles.SaveAndProceedSection}>
            <TouchableOpacity activeOpacity={1} onPress={this.saveData}>
              <View style={styles.SaveAndProceedView}>
                <Text style={styles.SaveAndProceedText}>
                  {this.records.length > 0 ? "Add" : "Save"}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
 */

/**
 *   saveData = () => {
    if (!this.date) {
      this.required_date = "*"
    } else if (
      this.date &&
      (this.records.length > 0 ||
        PatientProfileUpdateStore.quickRecords.length > 0)
    ) {
      if (
        this.records.length > 0 &&
        this.date === moment(new Date()).format("YYYY-MM-DD")
      ) {
        let requestObject = {
          attributes: this.records,
          entity_id: this.props.selectedPerson === "Parent"
            ? this.props.selectedParent
            : this.props.selectedChild,
          activity_required: true,
          entity_type: this.props.selectedPerson === "Parent"
            ? "PATIENT"
            : "CHILD"
        }
        if (__DEV__) {
          console.log(toJS(requestObject), "Create Record")
        }
        PatientProfileUpdateStore.createRecord(requestObject)
      } else if (
        PatientProfileUpdateStore.quickRecords.length > 0 &&
        this.date === moment(new Date()).format("YYYY-MM-DD")
      ) {
        let requestObject = {
          attributes: this.getTodayUpdatedRecord(),
          entity_id: this.props.selectedPerson === "Parent"
            ? this.props.selectedParent
            : this.props.selectedChild,
          activity_required: true,
          entity_type: this.props.selectedPerson === "Parent"
            ? "PATIENT"
            : "CHILD"
        }
        if (__DEV__) {
          console.log(toJS(requestObject), "Create Record")
        }
        PatientProfileUpdateStore.createRecord(requestObject)
      } else {
        let requestObject = {
          analytics_data: this.getAttributes(),
          child_id: this.props.selectedPerson === "Parent"
            ? -1
            : this.props.selectedChild,
          is_child_data: this.props.selectedPerson === "Parent" ? false : true
        }
        if (__DEV__) {
          console.log(toJS(requestObject), "Update Record")
        }
        PatientProfileUpdateStore.updateRecordAnalytics(requestObject)
      }
    } else {
      this.required_field = "*"
    }
  }
 */

/**
 * displayAttributeFields = () => {
    let values = this.getValues()
    if (__DEV__) {
      console.log(values)
    }
    if (Object.keys(values).length > 0) {
      return this.attribute_keys.map((eachAttribute, index) => {
        return (
          <View style={styles.InputUnderline} key={index}>
            {values[eachAttribute]
              ? <Text style={styles.fieldHeadingText}>
                  {eachAttribute}
                </Text>
              : null}
            <View style={{ flexDirection: "row" }}>
              <TextInput
                keyboardType="numeric"
                onChangeText={text =>
                  this.handleDataUpdation(text, eachAttribute)}
                style={styles.DocInputField}
                placeholder={eachAttribute}
                autoCorrect={false}
                underlineColorAndroid="transparent"
                value={values[eachAttribute]}
              />
              {this.required_field &&
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>}
            </View>
          </View>
        )
      })
    }
    return null
  }
 */

/**
 *  handleDataUpdation = (value, key) => {
    if (this.date) {
      if (PatientProfileUpdateStore.quickRecords.length > 0) {
        PatientProfileUpdateStore.updateValues(value, key)
      } else {
        if (this.records.length > 0) {
          let isRecord = false
          this.records.map((eachRecord, index) => {
            if (eachRecord.attr_key === key) {
              eachRecord.attr_value = value
              isRecord = true
            }
          })
          if (!isRecord) {
            this.records.push({
              attr_key: key,
              description: "",
              attr_id: -1,
              attr_value: value,
              to_add: true
            })
          }
        } else {
          this.records.push({
            attr_key: key,
            description: "",
            attr_id: -1,
            attr_value: value,
            to_add: true
          })
        }
      }
    } else {
      this.required_date = "*"
    }
    if (
      this.records.length > 0 ||
      PatientProfileUpdateStore.quickRecords.length > 0
    ) {
      this.required_field = null
    }
  }
 */

/**
 * componentDidMount() {
    if (__DEV__) {
      console.log("Did Mount", PatientProfileUpdateStore.periodicalAttributes)
    }
    if (PatientProfileUpdateStore.periodicalAttributes.length > 0) {
      PatientProfileUpdateStore.periodicalAttributes.map(
        (eachAttribute, index) => {
          this.attribute_keys.push(eachAttribute.attr_key)
        }
      )
    }
    if (__DEV__) {
      console.log("DidMount", toJS(this.attribute_keys))
    }
  }
 */
