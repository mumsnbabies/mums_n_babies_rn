import React from "react";
import { TouchableOpacity, View, Text, TextInput } from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import styles from "./Styles/ExpectingParentStyles";
import moment from "moment";
import { observable } from "mobx";
import { observer } from "mobx-react";
import AuthStore from "../../../stores/AuthStore";
import { EXPECTINGPARENTCONSTANTS } from "../../../Constants/Patient";
@observer
export default class ExpectingParent extends React.Component {
  @observable date: string;
  @observable menstruation_date: string;
  @observable isDateTimePickerVisible: Boolean;
  @observable isMenstruationDateTimePickerVisible: Boolean;
  constructor(props) {
    super(props);
    this.isDateTimePickerVisible = false;
    if (AuthStore.loginResponse.expected_date) {
      this.date = moment(AuthStore.loginResponse.expected_date).format(
        "YYYY-MM-DD"
      );
    } else {
      this.date =
        EXPECTINGPARENTCONSTANTS.EXPECTED_MOTHER_DETAILS_FIELD_DEFALUT_TEXT;
    }
    if (AuthStore.loginResponse.menstruation_date) {
      this.menstruation_date = moment(
        AuthStore.loginResponse.menstruation_date
      ).format("YYYY-MM-DD");
    } else {
      this.menstruation_date =
        EXPECTINGPARENTCONSTANTS.MENSTRUATION_DAY_FIELD_DEFAULT_TEXT;
    }
  }
  _showDateTimePicker = () => {
    this.isDateTimePickerVisible = true;
  };
  _showMenstruationDateTimePicker = () => {
    this.isMenstruationDateTimePickerVisible = true;
  };
  _hideDateTimePicker = () => {
    this.isDateTimePickerVisible = false;
  };
  _hideMenstruationDateTimePicker = () => {
    this.isMenstruationDateTimePickerVisible = false;
  };
  _handleDatePicked = date => {
    this.date = moment(date).format("YYYY-MM-DD");
    this._hideDateTimePicker();
  };
  _handleMenstruationDatePicked = date => {
    this.menstruation_date = moment(date).format("YYYY-MM-DD");
    this._hideMenstruationDateTimePicker();
  };

  render() {
    return (
      <View>
        <Text style={styles.Component}>
          {EXPECTINGPARENTCONSTANTS.EXPECTEED_MOTHER_CARD_TEXT}
        </Text>
        <View style={styles.SignUpComponent2View}>
          <View style={styles.NameInput}>
            {this.date !=
            EXPECTINGPARENTCONSTANTS.EXPECTED_MOTHER_DETAILS_FIELD_DEFALUT_TEXT ? (
              <Text style={styles.ComponentHeader}>
                {EXPECTINGPARENTCONSTANTS.EXPECTING_MOTHER_DETAILS_CARD_TEXT}
              </Text>
            ) : null}
            <TouchableOpacity onPress={this._showDateTimePicker}>
              <Text style={styles.ComponentHeaderText}>{this.date}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.NameInput}>
            {this.menstruation_date !=
            EXPECTINGPARENTCONSTANTS.MENSTRUATION_DAY_FIELD_DEFAULT_TEXT ? (
              <Text style={styles.ComponentHeader}>
                {EXPECTINGPARENTCONSTANTS.MENSTRUATION_DAY_FIELD_TEXT}
              </Text>
            ) : null}
            <TouchableOpacity onPress={this._showMenstruationDateTimePicker}>
              <Text style={styles.ComponentHeaderText}>
                {this.menstruation_date}
              </Text>
            </TouchableOpacity>
          </View>
          <DateTimePicker
            isVisible={this.isDateTimePickerVisible}
            onConfirm={this._handleDatePicked}
            minimumDate={new Date()}
            onCancel={this._hideDateTimePicker}
          />
          <DateTimePicker
            isVisible={this.isMenstruationDateTimePickerVisible}
            onConfirm={this._handleMenstruationDatePicked}
            maximumDate={new Date()}
            onCancel={this._hideMenstruationDateTimePicker}
          />
        </View>
      </View>
    );
  }
}
