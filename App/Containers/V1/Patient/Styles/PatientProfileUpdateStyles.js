import { StyleSheet, Platform, Dimensions } from "react-native";
import Fonts from "../../../../Themes/Fonts";
import Colors from "../../../../Themes/Colors";

export default StyleSheet.create({
  PatientYourProfileHeader: {
    alignItems: "center",
    justifyContent: "center"
  },

  patientfoodpreference: {
    marginTop: "2%",
    marginBottom: 10,
    marginLeft: 15,
    color: Colors.lightblack,
    fontFamily: Fonts.family.fontFamilyBold
  },
  pickerstyles: {
    marginLeft: 10,
    width: "82%"
  },
  RadioFormStylesOccupation: {
    justifyContent: "space-between",
    marginLeft: 20,
    marginRight: 60,
    marginBottom: 10,
    flex: 2
  },
  RadioFormStylesTravel: {
    justifyContent: "space-between",
    marginLeft: 20,
    marginRight: 92,
    marginBottom: 10,
    flex: 2
  },
  DocInput: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    borderColor: Colors.blackColor,
    borderWidth: 0.5,
    borderRadius: 5
  },
  UpdateProfileImageView: {
    marginTop: 20,
    alignItems: "center",
    justifyContent: "center",
    position: "relative"
  },
  UpdateProfileImageRelative: {
    marginBottom: 30,
    height: 140,
    width: 140
  },
  UpdateProfileImage: {
    height: 140,
    width: 140,
    borderRadius: 70,
    borderColor: Colors.graycolor,
    borderWidth: 0.2
  },
  EditProfileImageView: {
    borderRadius: 40,
    position: "absolute",
    backgroundColor: Colors.primaryColor,
    alignItems: "center",
    justifyContent: "center",
    bottom: 8,
    right: 8,
    marginBottom: -20,
    marginRight: -15,
    padding: 15
  },

  patientname: {
    marginTop: "2%",
    marginLeft: 15,
    color: Colors.lightblack,
    fontFamily: Fonts.family.fontFamilyBold
  },
  InputUnderline: {
    flex: 1,
    borderBottomWidth: 0.7,
    borderColor: Colors.lightblack
  },
  DocInputField: {
    width: "80%",
    color: Colors.blackColor,
    marginLeft: 20,
    height: 50,
    fontFamily: Fonts.family.fontfamily
  },
  RequiredField: {
    width: 20,
    height: 20
  },
  ImageContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  PatientYourProfileHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  MainScreen: {
    flex: 1,
    backgroundColor: Colors.whiteColor
  },
  SelectOption: {
    flex: 1,
    paddingTop: 40
  },
  SelectOptionView: {
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 30
  },
  SelectOptionText: {
    fontSize: Fonts.sizes.normal,
    fontFamily: Fonts.family.fontFamilyBold,
    color: Colors.blackColor
  },
  RadioButtonStyles: {
    borderColor: Colors.graycolor,
    borderWidth: 0.4,
    marginBottom: 10,
    marginTop: 10

    // flex: 1
  },
  Underline: {
    borderColor: Colors.graycolor,
    borderBottomWidth: 0.4,
    marginTop: 10
  },
  RadioButtonView: {
    marginLeft: 30,
    flexDirection: "row"
  },
  RadiobuttonText: {
    fontFamily: Fonts.family.fontfamily,
    fontSize: Fonts.size.regular,
    color: Colors.blackColor
  },
  RadioButtonText: {
    marginLeft: 20,
    fontFamily: Fonts.family.fontfamily,
    fontSize: Fonts.size.regular,
    color: Colors.blackColor
  },
  SignUpComponentView: {
    marginTop: 40
  },
  InputField: {
    color: Colors.blackColor,
    marginLeft: 20,
    height: 50,
    fontFamily: Fonts.family.fontfamily
  },
  Input: {
    marginLeft: 10,
    marginRight: 10,
    borderColor: Colors.blackColor,
    borderWidth: 0.5,
    borderRadius: 5
  },
  PrimaryButton: {
    backgroundColor: Colors.primaryColor,
    height: 40,
    borderRadius: 40,
    justifyContent: "center",
    alignItems: "center",
    width: 180,
    marginTop: 40,
    marginLeft: 95
  },
  forgotTextView: {
    justifyContent: "center"
  },
  ButtonText: {
    color: Colors.whiteColor
  },
  SaveAndProceedSection: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10
  },
  SaveAndProceedView: {
    height: 40,
    width: 200,
    backgroundColor: Colors.primaryColor,
    borderRadius: 30,
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonContainer: {
    height: 40,
    width: 200,
    backgroundColor: Colors.primaryColor,
    borderRadius: 30,
    marginTop: 30,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  btnTextStyles: {
    color: Colors.whiteColor,
    fontFamily: Fonts.family.fontFamilyBold,
    fontSize: Fonts.sizes.normal
  },
  SaveAndProceedText: {
    color: Colors.whiteColor,
    fontFamily: Fonts.family.fontFamilyBold,
    fontSize: Fonts.sizes.normal
  },
  fieldHeadingText: {
    fontSize: Fonts.size.small,
    fontFamily: Fonts.family.fontFamilyBold,
    marginLeft: 15,
    marginTop: 5
  },
  DescriptionView: {
    marginTop: 20,
    marginBottom: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  Description: {
    fontFamily: Fonts.family.fontFamilyBold,
    fontSize: Fonts.sizes.normal
  },
  halfScreen: {
    width: (Dimensions.get("window").width - 70) / 2
  },
  pickerContentStyles: {
    color: Colors.lightblack,
    width: "95%",
    marginLeft: 18,
    marginRight: 20
  }
});
