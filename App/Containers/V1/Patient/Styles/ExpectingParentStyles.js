import { StyleSheet, Platform } from "react-native";
import Fonts from "../../../../Themes/Fonts";
import Colors from "../../../../Themes/Colors";

export default StyleSheet.create({

  ComponentHeader: {
    marginTop: 5,
    color: Colors.lightblack,
    marginLeft: 10,
    fontFamily: Fonts.family.fontFamilyBold
  },
  Component:{
    color: Colors.blackColor,
    marginTop: 10,
    marginLeft:10,
    height: 25,
    fontSize: Fonts.size.small,
    fontFamily: Fonts.family.fontFamilyBold
  },
  ComponentHeaderView: {
    justifyContent: "center",
    backgroundColor: Colors.whiteColor,
    marginTop: Platform.OS === "ios" ? 30 : 15,
    height: 40,
    marginRight: 20,
    width: "100%",
    borderRadius: 4,
    borderColor: Colors.lightblack,
    borderWidth: 0.7
  },
  ComponentHeaderText: {
    marginLeft: 20,
    marginTop:20,
    marginBottom:20,
    flex: 2,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  NameInput: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 20,
    backgroundColor: Colors.whiteColor,
    flex: 1,
    height: 40,
    marginRight: 10,
    borderRadius: 1,
    marginBottom: 15
  },
  Name: {
    width: "20%",
    fontSize: Fonts.sizes.normal,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  NameInput: {
    // backgroundColor: Colors.blackColor,
    flex: 1,
    flexDirection: "column",
    borderBottomWidth: 0.9,
    borderColor: Colors.graycolor
  },
  SignUpComponent2View: {
    flex: 1,
    borderColor: Colors.lightblack,
    borderWidth: 0.7,
    borderRadius: 4,
    marginTop: 10,
    marginLeft:10,
    marginRight:10,
    marginBottom: 20
  },
  InputField: {
    height: 40,
    flex: 1,
    color: Colors.blackColor,
    alignItems: "center",
    fontFamily: Fonts.family.fontfamily
  },
  menstruationDay: {
    fontSize: Fonts.sizes.small,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold,
    marginTop: 10
  }
});
