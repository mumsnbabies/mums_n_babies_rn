import { StyleSheet } from "react-native";
import Fonts from "../../../../Themes/Fonts";
import Colors from "../../../../Themes/Colors";

export default StyleSheet.create({
  PatientYourProfileHeader: {
    alignItems: "center",
    justifyContent: "center"
  },
  PatientYourProfileHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  MainScreen: {
    flex: 1,
    backgroundColor: Colors.whiteColor
  },
  SelectOptionView: {
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 10
  },
  SelectOptionText: {
    fontSize: Fonts.sizes.normal,
    fontFamily: Fonts.family.fontFamilyBold,
    color: Colors.blackColor
  },
  RadioButtonView: {
    marginLeft: 10,
    flexDirection: "row",
    alignItems: "center"
  },
  RadiobuttonText: {
    fontFamily: Fonts.family.fontfamily,
    fontSize: Fonts.size.regular,
    color: Colors.blackColor,
    paddingLeft: 10
  },
  RadioButtonStyles: {
    borderColor: Colors.graycolor,
    borderWidth: 0.4,
    marginBottom: 10,
    marginTop: 10
    // flex: 1
  },
  checkBoxStyles: {
    marginRight: 10
  },
  SignUpComponentView: {
    marginTop: 10
  },
  SaveAndProceedSection: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    marginTop: 10
  },
  SaveAndProceedView: {
    height: 40,
    width: 200,
    backgroundColor: Colors.primaryColor,
    borderRadius: 30,
    marginTop: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  SaveAndProceedText: {
    color: Colors.whiteColor,
    fontFamily: Fonts.family.fontFamilyBold,
    fontSize: Fonts.sizes.normal
  }
});
