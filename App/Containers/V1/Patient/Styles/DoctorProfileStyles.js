import { StyleSheet, Platform } from "react-native";
import Fonts from "../../../../Themes/Fonts";
import Colors from "../../../../Themes/Colors";

export default StyleSheet.create({
  MainScreen: {
    flex: 1,
    backgroundColor: Colors.whiteColor
  },
  DoctorProfileHeader: {
    justifyContent: "center",
    alignItems: "center"
  },
  DoctorProfileHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  DoctorProfilePic: {
    marginTop: 20,
    alignItems: "center",
    justifyContent: "center"
  },
  ProfilePic: {
    height: 140,
    width: 140,
    borderRadius: 70
  },
  DoctorDetails: {
    marginTop: 20,
    alignItems: "center",
    justifyContent: "center"
  },
  DoctorName: {
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily,
    fontSize: Fonts.sizes.mediumsmall,
    marginBottom: 6
  },
  SpecialistIn: {
    marginBottom: 6,
    fontSize: Fonts.sizes.normal,
    fontFamily: Fonts.family.fontfamily
  },
  OpenTimings: {
    flexDirection: "row"
  },
  OpenOrNot: {
    color: Colors.greencolor,
    fontFamily: Fonts.family.fontFamilyBold,
    fontSize: Fonts.size.medium
  },
  Timings: {
    fontSize: Fonts.size.medium,
    marginLeft: 5,
    fontFamily: Fonts.family.fontFamilyBold
  },
  BookingnNetworkBtns: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10
  },
  BookAppointmentView: {
    marginRight: 5,
    width: "48%",
    backgroundColor: Colors.graycolor,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  BookAppointmentText: {
    fontFamily: Fonts.family.fontfamily,
    color: Colors.blackColor,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: Fonts.size.small
  },
  AddNetworkView: {
    marginLeft: 5,
    width: "48%",
    backgroundColor: Colors.graycolor,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  AddNetworkText: {
    fontFamily: Fonts.family.fontfamily,
    color: Colors.blackColor,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: Fonts.size.small
  },
  AddressnNavigation: {
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    paddingBottom: 30,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  Address: {
    width: "50%"
  },
  AddressHeading: {
    fontSize: Fonts.size.regular,
    marginBottom: 10,
    fontFamily: Fonts.family.fontFamilyBold,
    color: Colors.blackColor
  },
  StreetName: {
    fontSize: Fonts.sizes.mediumsmall,
    marginBottom: 4,
    fontFamily: Fonts.family.fontfamily
  },
  AreaName: {
    fontSize: Fonts.sizes.mediumsmall,
    marginBottom: 4,
    fontFamily: Fonts.family.fontfamily
  },
  CityName: {
    fontSize: Fonts.sizes.mediumsmall,
    marginBottom: 4,
    fontFamily: Fonts.family.fontfamily
  },
  Pincode: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  PincodeHeading: {
    fontFamily: Fonts.family.fontfamily,
    fontSize: Fonts.sizes.mediumsmall
  },
  PincodeNumber: {
    fontFamily: Fonts.family.fontfamily,
    fontSize: Fonts.sizes.mediumsmall
  },
  NavigationImage: {
    width: "40%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 30
  },
  NavigationMap: {
    width: 80,
    height: 60
  },
  NavigationText: {
    color: Colors.whiteblack,
    fontFamily: Fonts.family.fontFamilyBold,
    paddingTop: 10,
    textAlign: "center"
  },
  DoctorTimings: {
    borderTopWidth: 2,
    borderColor: Colors.graycolor
  },
  TimingsView: {
    marginLeft: 20,
    marginTop: 10,
    marginBottom: 30
  },
  TimingsHead: {
    fontSize: Fonts.size.regular,
    marginTop: 20,
    marginLeft: 20,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  EachDay: {
    flexDirection: "row"
  },
  DayName: {
    fontFamily: Fonts.family.fontFamilyBold,
    color: Colors.blackColor
  },
  DayTimings: {
    marginLeft: 10,
    fontFamily: Fonts.family.fontFamilyBold
  },
  buttonContainer: {
    // flexDirection: "row",
    // flex: 2,
    marginRight: 30,
    alignItems: "flex-end"
  },
  chatButtonContainer: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.requestButtom,
    borderRadius: 40,
    height: 30,
    paddingHorizontal: 20,
    // flex: 1,
    elevation: 1
  },
  chatButtonTextStyles: {
    color: Colors.whiteColor,
    fontFamily: Fonts.family.fontFamilyBold
  }
});
