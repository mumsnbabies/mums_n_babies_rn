import { StyleSheet, Platform } from "react-native";
import Fonts from "../../../../Themes/Fonts";
import Colors from "../../../../Themes/Colors";

export default StyleSheet.create({
  Card: {
    flex: 1,
    borderRadius: 4,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5
    // borderColor: Colors.lightblack,
    // borderWidth: 0.7
  },
  RequiredField: {
    width: 20,
    height: 20
  },
  ImageContainer: {
    marginRight: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  ComponentHeader: {
    color: Colors.blackColor,
    marginTop: 10,
    height: 25,
    fontSize: Fonts.size.small,
    fontFamily: Fonts.family.fontFamilyBold
  },
  NameInput: {
    // backgroundColor: Colors.blackColor,
    flex: 1,
    flexDirection: "column",
    borderBottomWidth: 0.9,
    borderColor: Colors.graycolor
  },
  UpdateProfileTextField: {
    marginTop: 5,
    color: Colors.lightblack,
    marginLeft: 10,
    fontFamily: Fonts.family.fontFamilyBold
  },
  InputField: {
    marginLeft: 20,
    // height: 50,
    flex: 2,
    // color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  SignUpComponent2View: {
    flex: 1,
    borderColor: Colors.lightblack,
    borderWidth: 0.7,
    borderRadius: 4,
    marginTop: 10,
    marginBottom: 20,
    elevation: 1
  },
  Name: {
    flex: 2,
    marginLeft: 10,
    fontSize: Fonts.sizes.normal,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  ChildrenDetailsHead: {
    borderColor: Colors.graycolor,
    borderWidth: 0.1,
    flex: 1,
    height: 1,
    marginLeft: 19,
    marginRight: 10
  },
  RadiobuttonView: {
    backgroundColor: Colors.whiteColor,
    justifyContent: "space-around",
    flexDirection: "row",
    height: 45,
    borderRadius: 4,
    flex: 1,
    alignItems: "center"
  },
  RadioFormStylesGender: {
    justifyContent: "space-between",
    marginLeft:25,
    marginRight:60,
    flex: 2
  },
  RadiobuttonText: {
    marginLeft: 20,
    fontSize: Fonts.sizes.normal,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  AddAnotherChildView: {
    justifyContent: "space-between",
    flexDirection: "row",
    alignItems: "center"
  },
  Daycount2: {
    justifyContent: "center",
    alignItems: "center",
    height: 35,
    width: 130,
    backgroundColor: Colors.lightblack,
    borderRadius: 20,
    // elevation: 2,
    // left: 202,
    marginTop: 10,
    marginBottom: 10
  },
  AddAnotherChild: {
    fontSize: Fonts.size.minitiny,
    color: Colors.whiteColor,
    fontFamily: Fonts.family.fontFamilyBold
  }
});
