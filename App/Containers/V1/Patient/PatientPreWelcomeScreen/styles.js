import { StyleSheet, Platform } from "react-native"
import { Fonts, Colors } from "../../../../Themes/"

export default StyleSheet.create({
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  mainContainer: {
    flex: 1,
    justifyContent: "center",
    marginTop: 20
  },
  headingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  headingStyles: {
    fontSize: Fonts.sizes.heading,
    color: Colors.primaryColor,
    fontFamily: Fonts.family.fontfamily
  },
  bodyContainer: {
    flex: 1,
    marginBottom: 20,
    marginHorizontal: 10,
    marginTop: -40
  },
  formHeadingContainer: {},
  formHeadingStyles: {
    fontSize: Fonts.sizes.large,
    color: Colors.primaryColor,
    fontFamily: Fonts.family.fontfamily
  },
  formContainer: {
    borderWidth: 0.8,
    borderRadius: 4,
    borderColor: Colors.lightblack,
    flex: 1
  },
  eachFormField: {
    flexDirection: "column",
    flex: 1,
    borderBottomWidth: 0.9,
    borderColor: Colors.lightblack
  },
  textinputImageField: {
    flexDirection: "row"
  },
  inputField: {
    flex: 4,
    marginLeft: 10,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  ImageContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  RequiredField: {
    width: 20,
    height: 20
  },
  footerContainer: {
    height: 55
  },
  btnContainer: {
    height: 55,
    backgroundColor: "#e91e63",
    justifyContent: "center",
    alignItems: "center"
  },
  disabledBtn: {
    backgroundColor: Colors.lightblack
  },
  btnTextStyles: {
    fontSize: Fonts.sizes.large,
    color: Colors.whiteColor,
    fontFamily: Fonts.family.fontfamily
  }
})
