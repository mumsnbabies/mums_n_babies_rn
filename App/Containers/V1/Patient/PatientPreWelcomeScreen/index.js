import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  AsyncStorage,
  TouchableOpacity,
  WebView,
  Platform,
  TextInput,
  StatusBar
} from "react-native";
import PatientProfileStore from "../../../../stores/PatientProfileStore";
import PopupMenu from "../../../../Components/PopupMenu";
import PopupMenuIOS from "../../../../Components/PopupMenuIOS";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../../Components/Loader";
import { observable } from "mobx";
import { observer } from "mobx-react";
import AuthStore from "../../../../stores/AuthStore";
import Images from "../../../../Themes/Images";
import { Fonts, Colors } from "../../../../Themes/";
// Styles
import styles from "./styles.js";

import PatientProfileDetails from "../PatientProfileDetails";
import { PATIENTPREWELLCOMESCREENCONSTANTS } from "../../../../Constants/Patient";
@observer
export default class PatientPreWelcomeScreen extends Component {
  constructor(props) {
    super(props);
  }
  submitChildDetails = () => {
    NavigationActions.PatientHomeScreen({ type: "reset" });
    // NavigationActions.PatientWelcomeScreen({})
  };

  render() {
    if (
      AuthStore.loginResponse === undefined ||
      AuthStore.loginResponse === null
    ) {
      return (
        <View style={styles.loaderContainer}>
          <Loader animating={true} color={Colors.primaryColor} />
        </View>
      );
    }

    return (
      <View style={styles.mainContainer}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.headingContainer}>
            <Text style={styles.headingStyles}>
              {PATIENTPREWELLCOMESCREENCONSTANTS.WELCOME_TITLE_TEXT}
            </Text>
            <Text style={styles.headingStyles}>
              {AuthStore.loginResponse.name}
            </Text>
          </View>
          <View style={styles.bodyContainer}>
            <PatientProfileDetails onSubmitProfile={this.submitChildDetails} />
          </View>
        </ScrollView>
      </View>
    );
  }
}
