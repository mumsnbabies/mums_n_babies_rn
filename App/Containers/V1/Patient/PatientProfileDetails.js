import React, { Component } from "react";
import {
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  Alert
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import CheckBox from "react-native-check-box";
import { observable, toJS } from "mobx";
import Loader from "../../../Components/Loader";
import { Actions as NavigationActions } from "react-native-router-flux";
import { observer } from "mobx-react";
import styles from "./Styles/PatientProfileDetailsStyles";
import ExpectingParent from "./ExpectingParent";
import ChildDetails from "./ChildDetails";
import PatientProfileUpdateStore from "../../../stores/PatientProfileUpdateStore";
import AuthStore from "../../../stores/AuthStore";
import { PATIENTDETAILSCONSTANTS } from "../../../Constants/Patient";

@observer
export default class PatientProfileUpdateSecondScreen extends Component {
  @observable expect_date: Boolean;
  @observable already_parent: Boolean;
  constructor(props) {
    super(props);
    this.expect_date = false;
    this.already_parent = false;
  }
  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  componentDidMount() {
    if (AuthStore.loginResponse.expected_date) {
      this.expect_date = true;
    }
    if (AuthStore.loginResponse.children.length > 0) {
      this.already_parent = true;
    }
  }
  renderTitle = () => {
    return (
      <View style={styles.PatientYourProfileHeader}>
        <Text style={styles.PatientYourProfileHeaderText}>
          {PATIENTDETAILSCONSTANTS.PROFILE_DETAILS_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };
  getChildsDetails = () => {
    let occurrences = [];
    let children;
    if (this.childDetails) {
      children = this.childDetails.children;
    }
    children.map((eachChild, index) => {
      if (eachChild.name && eachChild.age && eachChild.gender) {
        occurrences.push({
          name: eachChild.name,
          age: eachChild.age,
          gender: eachChild.gender,
          child_id: eachChild.child_id,
          to_add: true
        });
      }
    });
    if (__DEV__) {
      console.log("Child Details2:", toJS(occurrences));
    }
    return occurrences;
  };
  getDeleteChildsDetails = () => {
    let occurrences = [];
    AuthStore.loginResponse.children.map((eachChild, index) => {
      if (eachChild.name && eachChild.age && eachChild.gender) {
        occurrences.push({
          child_id: eachChild.child_id,
          to_add: false
        });
      }
    });
    if (__DEV__) {
      console.log("Child Details2:", occurrences);
    }
    return occurrences;
  };

  validateExpectingParent = () => {
    if (!this.expectingParent) {
      return false;
    }
    if (
      this.expectingParent.date === "Expected Date" &&
      this.expectingParent.menstruation_date === "Last Menstruation Date"
    ) {
      alert(PATIENTDETAILSCONSTANTS.ALERT_PLEASE_FILL_ATLEAST_ONE);
      return false;
    }
    return true;
  };

  validateChildDetails = () => {
    if (!this.childDetails) {
      return false;
    }
    if (this.childDetails.children.length > 0) {
      let validateChildsCount = 0;
      let childrenToBevalidated = this.childDetails.children;

      childrenToBevalidated.map((eachChild, index) => {
        if (
          eachChild.name === "" ||
          eachChild.age === "" ||
          eachChild.gender === ""
        ) {
          validateChildsCount = validateChildsCount + 1;
        }
      });
      if (validateChildsCount != 0) {
        alert(PATIENTDETAILSCONSTANTS.ALREST_FILL_ALL_CHILDREN_DETAILS);
        return false;
      }
    }
    return true;
  };

  patientDetailsRequest = () => {
    if (!this.expectingParent && !this.childDetails) {
      alert(PATIENTDETAILSCONSTANTS.ALREST_SELECT_ANY_ONE_OPTIONS);
      return;
    }

    let expected_date = "";
    let menstruationDate = "";

    if (this.expectingParent) {
      //Expecting Parent Validation
      let isValidateExpectingParent = this.validateExpectingParent();
      if (!isValidateExpectingParent) {
        return;
      }

      //Expecting Parent

      if (this.expectingParent.date != "Expected Date") {
        expected_date = this.expectingParent.date;
      }
      if (this.expectingParent.menstruation_date != "Last Menstruation Date") {
        menstruationDate = this.expectingParent.menstruation_date;
      }
    }

    if (this.childDetails) {
      //Already Parent Validation
      let isValidChildDetails = this.validateChildDetails();
      if (!isValidChildDetails) {
        //Already Parent is not selected
        return;
      }
    }

    let { patient_id, name, age } = AuthStore.loginResponse;
    if (this.props.onSubmitProfile) {
      let requestObject = {
        patient_id: patient_id,
        category: "",
        name: name,
        children: this.childDetails
          ? this.getChildsDetails()
          : this.getDeleteChildsDetails(),
        menstruation_date: menstruationDate,
        expected_date: expected_date
      };
      PatientProfileUpdateStore.updatePatientDetails(
        requestObject,
        this.props.onSubmitProfile
      );
    } else {
      let requestObject = {
        patient_id: patient_id,
        category: "",
        name: name,
        age: age,
        pic: AuthStore.loginResponse.pic,
        address: {
          city: AuthStore.loginResponse.address.city,
          geo_location: "",
          location: AuthStore.loginResponse.address.location,
          address: AuthStore.loginResponse.address.address
        },
        children: this.childDetails
          ? this.getChildsDetails()
          : this.getDeleteChildsDetails(),
        menstruation_date: menstruationDate,
        expected_date: expected_date
      };
      PatientProfileUpdateStore.updatePatientDetails(
        requestObject,
        this.navigateToHomeScreen
      );
    }
  };

  navigateToHomeScreen = () => {
    NavigationActions.PatientHomeScreen({
      type: "replace"
    });
  };
  handleExpectedDate = () => {
    this.expect_date = !this.expect_date;
  };
  handleAlready = () => {
    this.already_parent = !this.already_parent;
  };
  render() {
    if (PatientProfileUpdateStore.detailProgressStatus === 100) {
      return <Loader animating={true} />;
    } else if (PatientProfileUpdateStore.detailProgressStatus > 200) {
      Alert.alert(
        PATIENTDETAILSCONSTANTS.ALERT_ERROR_TITLE_TEXT,
        PatientProfileUpdateStore.error.statusText,
        [
          {
            text: "OK",
            onPress: () =>
              PatientProfileUpdateStore.resetUpdatePatientProState()
          }
        ]
      );
    }
    return (
      <View style={styles.MainScreen}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <KeyboardAwareScrollView extraHeight={200}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.SelectOptionView}>
              <Text style={styles.SelectOptionText}>
                {PATIENTDETAILSCONSTANTS.SELECT_OPTION_FIELD_TEXT}
              </Text>
            </View>
            <View style={styles.RadioButtonView}>
              <CheckBox
                isChecked={this.expect_date}
                onClick={this.handleExpectedDate}
                style={styles.checkBoxStyles}
              />
              <Text style={styles.RadioButtonText}>
                {PATIENTDETAILSCONSTANTS.EXPECTING_MOTHER_RADIO_BUTTON_TEXT}
              </Text>
            </View>
            <View style={styles.RadioButtonStyles} />
            <View style={styles.RadioButtonView}>
              <CheckBox
                isChecked={this.already_parent}
                onClick={this.handleAlready}
                style={styles.checkBoxStyles}
              />
              <Text style={styles.RadioButtonText}>
                {PATIENTDETAILSCONSTANTS.ALREADY_PARENT_RADIO_BUTTON_TEXT}
              </Text>
            </View>
            <View style={styles.RadioButtonStyles} />

            <View style={styles.SignUpComponentView}>
              {this.expect_date ? (
                <ExpectingParent ref={e => (this.expectingParent = e)} />
              ) : null}
              {this.already_parent ? (
                <ChildDetails ref={e => (this.childDetails = e)} />
              ) : null}
            </View>
            <View style={styles.SaveAndProceedSection}>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                  this.patientDetailsRequest();
                }}
              >
                <View style={styles.SaveAndProceedView}>
                  <Text style={styles.SaveAndProceedText}>
                    {PATIENTDETAILSCONSTANTS.SAVE_PROCEED_BUTTON_TEXT}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
