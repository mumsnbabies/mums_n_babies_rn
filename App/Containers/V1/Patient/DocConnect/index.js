import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";
import { observable } from "mobx";
import { observer } from "mobx-react";
import _ from "lodash";

//import common components
import Loader from "../../../../Components/Loader";
import NoDataView from "../../../../Components/NoDataView";

// Import Feature Components

import PendingList from "./components/PendingList";
import ConnectedList from "./components/ConnectedList";
import SearchList from "./components/SearchList";
import DocConnectModal from "./components/DocConnectModal";
import DocShareDetailsModal from "./components/DocShareDetailsModal";

//import stores
import PatientProfileStore from "../../../../stores/PatientProfileStore";
import PatientRequestsStore from "../../../../stores/PatientRequestsStore";
import AuthStore from "../../../../stores/AuthStore";
import DoctorProfileStore from "../../../../stores/DoctorProfileStore";
import { DOCCONNECTMAINSCREEN } from "../../../../Constants/Patient";

import { chatFlow } from "../../../Chat/stores/ChatStore";
import chatStore from "../../../Chat/stores/ChatStore";
// Styles
import styles from "./styles.js";

//import constants
//import {DocConnectMainScreen} from '../../../../Constants/Doctor'
@observer
export default class DocConnect extends Component {
  @observable value: String;
  @observable searchDoctorName: String;
  @observable modalVisible: Boolean;
  @observable doctorId: Number;
  @observable isShareDetailsModalVisible: Boolean;

  constructor(props) {
    super(props);
    this.value = "search";
    this.searchDoctorName = "";
    this.modalVisible = false;
    this.showComponentOn = _.debounce(this.showComponentOn, 300);
    this.isShareDetailsModalVisible = false;
  }

  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }

  componentDidMount() {
    this.requestSearchDoctor();
  }

  toggleModalVisible = () => {
    this.modalVisible = !this.modalVisible;
  };

  toggleShareDetailsModalVisible = () => {
    this.isShareDetailsModalVisible = !this.isShareDetailsModalVisible;
  };

  setDoctorIdModal = id => {
    this.doctorId = id;
    this.toggleModalVisible();
  };

  setDoctorShareDetails = doctorObject => {
    if (this.value != "connected") {
      return;
    }
    this.doctorId = doctorObject.doctor_id;
    this.toggleShareDetailsModalVisible();
  };

  renderDoctorCodeModal = () => {
    return (
      <DocConnectModal
        modalVisible={this.modalVisible}
        toggleModalVisible={this.toggleModalVisible}
        presentationStyle={"overFullScreen"}
        doctorId={this.doctorId}
        status={PatientRequestsStore.connectToDoctorAPIStatus}
      />
    );
  };

  renderShareDetailsModal = () => {
    if (this.isShareDetailsModalVisible) {
      return (
        <DocShareDetailsModal
          modalVisible={this.isShareDetailsModalVisible}
          toggleModalVisible={this.toggleShareDetailsModalVisible}
          presentationStyle={"overFullScreen"}
          doctorId={this.doctorId}
        />
      );
    }
  };

  renderTitle = () => {
    return (
      <View style={styles.DocConnectHeader}>
        <Text style={styles.DocConnectHeaderText}>
          {DOCCONNECTMAINSCREEN.FIND_DOCTOR_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };

  requestSearchDoctor = (searchDoctorName = "") => {
    let requestObject = {
      search_q: searchDoctorName,
      limit: 20,
      offset: 0
    };
    PatientRequestsStore.getDoctorsList(requestObject);
  };

  getPendingDoctorRequest = (searchDoctorName = "") => {
    let requestObject = {
      search_q: searchDoctorName,
      limit: 20,
      relation_status: "REQUESTED",
      offset: 0
    };
    PatientRequestsStore.requestGetPendingDoctorRequest(requestObject);
  };

  getConnectDoctor = async (searchDoctorName = "") => {
    let requestObject = {
      search_q: searchDoctorName,
      limit: 20,
      relation_status: "ACCEPTED",
      offset: 0
    };
    PatientRequestsStore.getPatientAcceptedRequests(requestObject);

    await chatFlow(chatStore);
  };

  changeComponents = value => {
    this.value = value;
    if (value === "search") {
      if (__DEV__) {
        console.log("search");
      }
      //change textinput to ""
      this.requestSearchDoctor();
      this.searchDoctorName = "";
    }
    if (value === "pending") {
      this.getPendingDoctorRequest();
      this.searchDoctorName = "";
    }
    if (value === "connected") {
      if (__DEV__) {
        console.log("connected");
      }
      this.getConnectDoctor();
      this.searchDoctorName = "";
    }
  };

  requestDoctorProfileDetails = (doctor_id, doctor_name, user_id) => {
    let requestObject = {};
    DoctorProfileStore.getDoctorProfile(requestObject, doctor_id);
    let requestObject1 = {
      limit: 1,
      filters: {
        entity_id: doctor_id,
        entity_type: "DOCTOR_TIMINGS"
      },
      offset: 0
    };
    if (__DEV__) {
      console.log("requestObject1: ", requestObject1);
    }
    DoctorProfileStore.getDoctorTiming(requestObject1);
    NavigationActions.PdoctorProfile({
      name: doctor_name,
      status: this.value,
      user_id: user_id
    });
  };

  showComponents = () => {
    if (
      this.value === "pending" &&
      PatientRequestsStore.getPendingRequestsStatus === 200
    ) {
      if (PatientRequestsStore.doctor_requests.total === 0) {
        return (
          <View style={styles.noDataViewContainer}>
            <NoDataView />
          </View>
        );
      }
      if (PatientRequestsStore.doctor_requests) {
        let { doctors } = PatientRequestsStore.doctor_requests;
        if (__DEV__) {
          console.log("doctor", doctors);
        }
        return doctors.map((eachDoctor, index) => {
          let { doctor_name, specialization, doctor_id } = eachDoctor;
          return (
            <PendingList
              key={index}
              details={eachDoctor}
              requestDoctorProfileDetails={() =>
                this.requestDoctorProfileDetails(doctor_id, doctor_name)
              }
            />
          );
        });
        return null;
      }
    }

    if (
      this.value === "connected" &&
      PatientRequestsStore.getAcceptedRequestsStatus === 200
    ) {
      if (PatientRequestsStore.acceptedRequests.total === 0) {
        return (
          <View style={styles.noDataViewContainer}>
            <NoDataView />
          </View>
        );
      }
      if (
        PatientRequestsStore.acceptedRequests &&
        PatientRequestsStore.acceptedRequests.doctors
      ) {
        let { doctors } = PatientRequestsStore.acceptedRequests;

        if (__DEV__) {
          console.log("Doctor:", doctors);
        }

        return doctors.map((eachDoctor, index) => {
          let {
            doctor_name,
            doctor_speciality,
            doctor_id,
            user_id
          } = eachDoctor;
          return (
            <ConnectedList
              key={index}
              details={eachDoctor}
              setDoctorShareDetails={() =>
                this.setDoctorShareDetails(eachDoctor)
              }
              requestDoctorProfileDetails={() =>
                this.requestDoctorProfileDetails(
                  doctor_id,
                  doctor_name,
                  user_id
                )
              }
            />
          );
          if (__DEV__) {
            console.log("Name:", doctor_name);
          }
        });
        return null;
      }
    }

    if (
      this.value === "search" &&
      PatientRequestsStore.searchDoctorStatus === 200
    ) {
      if (PatientRequestsStore.search_doctors.total === 0) {
        return (
          <View style={styles.noDataViewContainer}>
            <NoDataView />
          </View>
        );
      }

      if (PatientRequestsStore.search_doctors) {
        let { doctors } = PatientRequestsStore.search_doctors;

        if (__DEV__) {
          console.log(doctors);
        }

        return doctors.map((eachDoctor, index) => {
          let { name, specialization, doctor_id } = eachDoctor;
          return (
            <SearchList
              key={index}
              details={eachDoctor}
              toggleModalVisible={this.toggleModalVisible}
              setDoctorIdModal={this.setDoctorIdModal}
              requestDoctorProfileDetails={() =>
                this.requestDoctorProfileDetails(doctor_id, name)
              }

              //displayPatientProfile={this.displayPatientProfile}
            />
          );
        });
      }

      return null;
    }
  };
  handleSearch = name => {
    this.searchDoctorName = name;
    this.showComponentOn(name);
  };
  showComponentOn = (name = "") => {
    if (this.value === "search") {
      this.requestSearchDoctor(name);
    } else if (this.value === "connected") {
      this.getConnectDoctor(name);
    } else if (this.value === "pending") {
      this.getPendingDoctorRequest(name);
    }
  };

  render() {
    let selectedTab = styles.activeTabView;
    let selectedTabText = styles.activeTabText;
    let unselectedTab = styles.deActiveTabView;
    let unselectedTabText = styles.deActiveTabText;
    return (
      <View style={styles.mainContainer}>
        {this.renderDoctorCodeModal()}
        {this.renderShareDetailsModal()}
        <View style={styles.tabContainer}>
          <View style={this.value === "search" ? selectedTab : unselectedTab}>
            <TouchableOpacity
              onPress={() => {
                this.changeComponents("search");
              }}
            >
              <Text
                style={
                  this.value === "search" ? selectedTabText : unselectedTabText
                }
              >
                {DOCCONNECTMAINSCREEN.SEARCH_BUTTON_TEXT} (
                {PatientRequestsStore.search_doctors
                  ? PatientRequestsStore.search_doctors.total
                  : 0}
                )
              </Text>
            </TouchableOpacity>
          </View>
          <View style={this.value === "pending" ? selectedTab : unselectedTab}>
            <TouchableOpacity
              onPress={() => {
                this.changeComponents("pending");
              }}
            >
              <Text
                style={
                  this.value === "pending" ? selectedTabText : unselectedTabText
                }
              >
                {DOCCONNECTMAINSCREEN.PENDING_BUTTON_TEXT}(
                {PatientRequestsStore.doctor_requests.total
                  ? PatientRequestsStore.doctor_requests.total
                  : AuthStore.loginResponse.relation_stats.pending_relations}
                )
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={this.value === "connected" ? selectedTab : unselectedTab}
          >
            <TouchableOpacity
              onPress={() => {
                this.changeComponents("connected");
              }}
            >
              <Text
                style={
                  this.value === "connected"
                    ? selectedTabText
                    : unselectedTabText
                }
              >
                {DOCCONNECTMAINSCREEN.CONNECTED_BUTTON_TEXT}
                (
                {PatientRequestsStore.acceptedRequests.total
                  ? PatientRequestsStore.acceptedRequests.total
                  : AuthStore.loginResponse.relation_stats.connected_relations}
                )
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.searchFieldContainer}>
          <TextInput
            autoCapitalize="none"
            returnKeyType={"search"}
            autoCorrect={false}
            style={styles.searchInputStyles}
            placeholder={DOCCONNECTMAINSCREEN.FIND_DOCTOR_FIELD_PLACEHOLDER}
            underlineColorAndroid="transparent"
            onChangeText={this.handleSearch}
            value={this.searchDoctorName}
          />
        </View>

        {PatientRequestsStore.getPendingRequestsStatus === 100 ||
        PatientRequestsStore.getAcceptedRequestsStatus === 100 ||
        PatientRequestsStore.searchDoctorStatus === 100 ? (
          <Loader animating={true} />
        ) : (
          <ScrollView showsVerticalScrollIndicator={false}>
            {this.showComponents()}
          </ScrollView>
        )}
      </View>
    );
  }
}

/**
 * <View style={styles.SearchDoctorButton}>

            <TouchableOpacity
              onPress={() => this.showComponentOn(this.searchDoctorName)}
            >

              <Text style={styles.SearchDoctorButtonText}>Search Doctor</Text>

            </TouchableOpacity>

          </View>
 */
