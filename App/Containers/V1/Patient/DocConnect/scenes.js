import { Scene, Router } from "react-native-router-flux"
import React, { Component } from "react"

// Import Components

import PendingList from "./components/PendingList"
import ConnectedList from "./components/ConnectedList"
import SearchList from "./components/SearchList"

import DocConnect from "./index"

const scenes = [
  <Scene
    hideNavBar={false}
    key="DocConnect"
    component={DocConnect}
    navigatonBarStyle={{ backgroundColor: "#ffffff" }}
  />,
  <Scene
    hideNavBar={false}
    key="DocConnectPending"
    component={PendingList}
    navigatonBarStyle={{ backgroundColor: "#ffffff" }}
  />,
  <Scene
    hideNavBar={false}
    key="DocConnectConnected"
    component={ConnectedList}
    navigatonBarStyle={{ backgroundColor: "#ffffff" }}
  />,
  <Scene
    hideNavBar={false}
    key="DocConnectSearch"
    component={SearchList}
    navigatonBarStyle={{ backgroundColor: "#ffffff" }}
  />
]

export default scenes
