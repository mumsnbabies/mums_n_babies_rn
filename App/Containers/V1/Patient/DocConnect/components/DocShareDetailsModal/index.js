import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Modal,
  TouchableHighlight,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Platform
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";
import { observable } from "mobx";
import { observer } from "mobx-react";
import CheckBox from "react-native-check-box";

// import stores
import AuthStore from "../../../../../../stores/AuthStore";
import PatientRequestsStore from "../../../../../../stores/PatientRequestsStore";

// Styles
import styles from "./styles.js";

import Loader from "../../../../../../Components/Loader";

@observer
class CustomCheckbox extends Component {
  @observable isChecked: boolean;

  constructor(props) {
    super(props);
    this.isChecked = this.props.isChecked;
  }
  toggleCheckboxChange = () => {
    const { handleCheckboxChange, label } = this.props;
    this.isChecked = !this.isChecked;
    handleCheckboxChange(label);
  };
  render() {
    const { label } = this.props;
    const { isChecked } = this;
    if (__DEV__) console.log("rendering...checkbox");
    return (
      <View style={styles.checkBoxChild}>
        <CheckBox
          isChecked={this.isChecked}
          onClick={this.toggleCheckboxChange}
        />
        <Text style={styles.checkBoxLabelText}>{label}</Text>
      </View>
    );
  }
}

@observer
export default class DocShareDetailsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      patientPrivacyLabels: this.preparePrivacyObject(),
      patientPrivacyErrorText: null,
      responseHolder: null
    };
    this.selectedCheckboxes = new Set();
    this.oldDoctorAccessInfo = new Set();
  }

  preparePrivacyObject = () => {
    let patientPrivacyLabels = [];
    patientPrivacyLabels.push(AuthStore.loginResponse.name);
    AuthStore.loginResponse.children.map(child => {
      patientPrivacyLabels.push(child.name);
    });
    return patientPrivacyLabels;
  };

  componentWillMount() {
    let requestObject = {
      doctor_id: this.props.doctorId
    };
    PatientRequestsStore.getDoctorAccessProfilesAPI(
      requestObject,
      this.handleDoctorAccessProfileResponse
    );
  }
  handleDoctorAccessProfileResponse = response => {
    if (__DEV__) console.log(response);

    response.map(entityObject => {
      if (entityObject.entity_id === AuthStore.loginResponse.patient_id) {
        this.oldDoctorAccessInfo.add(AuthStore.loginResponse.name);
        this.selectedCheckboxes.add(AuthStore.loginResponse.name);
      }
      AuthStore.loginResponse.children.map(child => {
        if (entityObject.entity_id === child.child_id) {
          this.oldDoctorAccessInfo.add(child.name);
          this.selectedCheckboxes.add(child.name);
        }
      });
    });
    this.setState({ responseHolder: response });
  };

  toggleCheckbox = label => {
    if (this.selectedCheckboxes.has(label)) {
      this.selectedCheckboxes.delete(label);
    } else {
      this.selectedCheckboxes.add(label);
    }
  };

  createCheckbox = label => {
    let checkedInitialState = false;
    if (this.oldDoctorAccessInfo.has(label)) {
      checkedInitialState = true;
    }
    if (__DEV__)
      console.log(checkedInitialState, "checkedInitialState", label, "label");
    return (
      <CustomCheckbox
        label={label}
        handleCheckboxChange={this.toggleCheckbox}
        key={label}
        isChecked={checkedInitialState}
      />
    );
  };

  createCheckboxes = () => {
    if (
      this.state.responseHolder !== undefined &&
      this.state.responseHolder !== null
    ) {
      return this.state.patientPrivacyLabels.map(this.createCheckbox);
    }
    return <Loader animating={true} />;
  };

  isValidPrivacyData = () => {
    //Validation of Checkbox group
    if (this.selectedCheckboxes.size === 0) {
      this.setState({
        patientPrivacyErrorText: "Please Select Atleast one detail"
      });
      return false;
    }
    this.setState({
      patientPrivacyErrorText: null
    });
    return true;
  };
  handleSubmit = () => {
    const { toggleModalVisible, doctorId } = this.props;
    if (!this.isValidPrivacyData()) {
      return;
    }
    //Make a network call
    this.setState({
      patientPrivacyErrorText: null
    });

    if (this.selectedCheckboxes.size != 0) {
      let patientaccessRequestObject = this.makePatientPrivacyRequestObject(
        doctorId
      );
      PatientRequestsStore.updateDoctorAccessPatientProfilesAPI(
        patientaccessRequestObject,
        this.resetSetVariables
      );
    }
    toggleModalVisible();
  };

  resetSetVariables = () => {
    this.selectedCheckboxes = new Set();
    this.oldDoctorAccessInfo = new Set();
  };

  makePatientPrivacyRequestObject = doctorId => {
    let patientaccessRequestObject = {
      entities: [],
      doctor_id: doctorId,
      is_allowed: true
    };
    AuthStore.loginResponse.children.map(child => {
      if (this.selectedCheckboxes.has(child.name)) {
        let entityObject = {
          entity_id: child.child_id,
          entity_type: "CHILD"
        };
        patientaccessRequestObject.entities.push(entityObject);
      }
    });
    if (this.selectedCheckboxes.has(AuthStore.loginResponse.name)) {
      let entityObject = {
        entity_id: AuthStore.loginResponse.patient_id,
        entity_type: "PATIENT"
      };
      patientaccessRequestObject.entities.push(entityObject);
    }
    return patientaccessRequestObject;
  };

  render() {
    if (
      this.state.responseHolder === undefined ||
      this.state.responseHolder === null
    ) {
      return (
        <Modal
          animationType={"fade"}
          transparent={true}
          visible={this.props.modalVisible}
          onRequestClose={() => {
            this.props.toggleModalVisible();
          }}
        >
          <View style={styles.modalMainContainer}>
            <Loader animating={true} />
          </View>
        </Modal>
      );
    }
    return (
      <Modal
        animationType={"fade"}
        transparent={true}
        visible={this.props.modalVisible}
        onRequestClose={() => {
          this.props.toggleModalVisible();
        }}
      >
        <View style={styles.modalMainContainer}>
          <View style={styles.modalContainer}>
            <View style={styles.headerMainContainer}>
              <View style={styles.headerContainer}>
                <Text style={styles.headerTextStyles}>
                  Select Doctor Access Details
                </Text>
              </View>
              <View style={styles.closeBtnPostionContainer}>
                <TouchableOpacity
                  onPress={() => this.props.toggleModalVisible()}
                >
                  <View style={styles.closeBtnContainer}>
                    <Text style={styles.closeBtnTextStyles}>X</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.checkBoxMainContainer}>
              <View style={styles.checkBoxBlockContainer}>
                {this.createCheckboxes()}
              </View>
              <View style={styles.checkboxErrorContainer}>
                {this.state.patientPrivacyErrorText && (
                  <Text style={styles.errorTextStyles}>
                    {this.state.patientPrivacyErrorText}
                  </Text>
                )}
              </View>
            </View>

            <View style={styles.footerButtonContainer}>
              <TouchableOpacity
                onPress={() => {
                  this.handleSubmit();
                }}
              >
                <View style={styles.proceedBtnStyles}>
                  <Text style={styles.btnTextStyles}>Proceed</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
