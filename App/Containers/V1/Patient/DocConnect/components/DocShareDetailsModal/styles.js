import { StyleSheet, Platform } from "react-native"
import Fonts from "../../../../../../Themes/Fonts"

export default StyleSheet.create({
  modalMainContainer: {
    backgroundColor: "rgba(0, 0, 0, 0.77)",
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  modalContainer: {
    justifyContent: "center",

    flexDirection: "column",
    backgroundColor: "white",
    width: 240,
    borderRadius: 5,
    borderColor: "#dcdcdc",
    borderWidth: 0.7,
    position: "relative"
  },
  headerContainer: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    flex: 4
  },
  headerMainContainer: {
    flexDirection: "row",
    marginVertical: 10,
    marginHorizontal: 10,
    justifyContent: "space-between"
  },
  closeBtnPostionContainer: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "center"
  },
  closeBtnContainer: {
    width: 20,
    height: 20,
    borderRadius: 10,
    borderColor: "black",
    borderWidth: 1,
    paddingLeft: 5
  },
  closeBtnTextStyles: {
    color: "black",
    fontSize: 12,
    fontWeight: "600"
  },
  headerTextStyles: {
    fontSize: 16,
    fontFamily: Fonts.family.fontfamily
  },
  headerSubTextStyles: {
    fontSize: 14
  },

  bodyContainer: {},
  doctorCodeInputContainer: {
    margin: 10
  },
  doctorCodeInput: {
    fontSize: 14,
    backgroundColor: "#F5F5F5",
    width: 240
  },
  errorInput: {
    borderColor: "red",
    borderRadius: 4,
    borderWidth: 0.4
  },
  errorTextStyles: {
    color: "red",
    fontSize: 14
  },
  btnTextStyles: {
    fontSize: 18,
    color: "white",
    fontFamily: Fonts.family.fontfamily
  },
  cancelBtnStyles: {},
  footerButtonContainer: {
    alignSelf: "center",
    justifyContent: "center",
    backgroundColor: "#387ef5",
    borderRadius: 5,
    height: 40,
    elevation: 1,
    marginBottom: 10,
    width: 120
  },
  proceedBtnStyles: {
    alignSelf: "center"
  },

  //CheckboxGroup and Checkbox

  checkBoxMainContainer: {
    flexDirection: "column",
    marginHorizontal: 10
  },
  checkBoxBlockContainer: {
    flexDirection: "column",
    alignItems: "flex-start"
  },
  checkboxErrorContainer: {
    marginBottom: 5
  },
  checkBoxChild: {
    flexDirection: "row",
    alignItems: "center"
  },
  checkBoxLabelText: {
    fontSize: 13
  }
})
