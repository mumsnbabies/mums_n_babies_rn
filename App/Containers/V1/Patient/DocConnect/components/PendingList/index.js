import React, { Component } from "react";

import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";

// Import Feature Components
import Loader from "../../../../../../Components/Loader";
import Images from "../../../../../../Themes/Images";

// Styles
import styles from "../../../GlobalStyles/DocConnectStyles";
import {PENDINGDOCTORSCONSTANTS} from "../../../../../../Constants/Patient";
export default class PendingList extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  renderTitle = () => {
    return (
      <View style={styles.DocConnectHeader}>
        <Text style={styles.DocConnectHeaderText}>{PENDINGDOCTORSCONSTANTS.PENDING_NAVBAR_HEADING}</Text>
      </View>
    );
  };
  render() {
    let {
      doctor_name: name,
      doctor_speciality: specialization
    } = this.props.details;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.profilePicContainer}>
          <Image
            style={styles.profilePic}
            resizemode="contain"
            source={Images.femaleDoctorPic}
          />
        </View>
        <View style={styles.bodyContentContainer}>
          <View style={styles.profileDetailsContainer}>
            <View style={styles.nameContainer}>
              <Text style={styles.nameTextStyles}>
                {name ? name.toUpperCase() : null}
              </Text>
            </View>
            <View style={styles.designationContainer}>
              <Text style={styles.designationTextStyles}>
                {specialization ? specialization.toUpperCase() : null}
              </Text>
            </View>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.firstButtonContainer}>
              <Text style={styles.firstButtonTextStyles}>{PENDINGDOCTORSCONSTANTS.PENDING_BUTTON_TEXT}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.requestDoctorProfileDetails();
              }}
              style={styles.secondButtonContainer}
            >
              <Text style={styles.secondButtonTextStyles}>{PENDINGDOCTORSCONSTANTS.OPEN_BUTTON_TEXT}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
