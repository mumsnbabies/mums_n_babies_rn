import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  View,
  TouchableOpacity,
  Platform,
  StyleSheet
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";

// Import Feature Components
import Loader from "../../../../../../Components/Loader";

// Styles
import styles from "../../../GlobalStyles/DocConnectStyles";
import Fonts from "../../../../../../Themes/Fonts";
import Images from "../../../../../../Themes/Images";
import { CONNECTEDLISTCONSTANTS } from "../../../../../../Constants/Patient";
const specificStyles = StyleSheet.create({
  thirdButtonContainer: {
    height: 30,
    flex: 1,
    marginLeft: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: "#00bfff",
    elevation: 1
  },
  thirdButtonTextStyles: {
    fontFamily: Fonts.family.fontFamilyBold,
    color: "#387ef5"
  }
});
export default class ConnectedList extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  renderTitle = () => {
    return (
      <View style={styles.DocConnectHeader}>
        <Text style={styles.DocConnectHeaderText}>
          {CONNECTEDLISTCONSTANTS.CONNECTED_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };
  render() {
    let {
      doctor_name: name,
      doctor_speciality: specialization
    } = this.props.details;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.profilePicContainer}>
          <Image
            style={styles.profilePic}
            resizemode="contain"
            source={Images.femaleDoctorPic}
          />
        </View>
        <View style={styles.bodyContentContainer}>
          <View style={styles.profileDetailsContainer}>
            <View style={styles.nameContainer}>
              <Text style={styles.nameTextStyles}>
                {name ? name.toUpperCase() : null}
              </Text>
            </View>
            <View style={styles.designationContainer}>
              <Text style={styles.designationTextStyles}>{specialization}</Text>
            </View>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              onPress={() => {
                this.props.requestDoctorProfileDetails();
              }}
              style={styles.firstButtonContainer}
            >
              <Text style={styles.firstButtonTextStyles}>
                {CONNECTEDLISTCONSTANTS.OPEN_BUTTON_TEXT}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.secondButtonContainer}
              onPress={() => this.props.setDoctorShareDetails()}
            >
              <Text style={styles.secondButtonTextStyles}>
                {CONNECTEDLISTCONSTANTS.SHARE_BUTTON_TEXT}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
