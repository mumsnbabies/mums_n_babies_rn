import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  AsyncStorage,
  TouchableOpacity,
  Platform,
  Alert,
  Model,
  TouchableHighlight
} from "react-native";
//import Alert from "rnkit-alert-view";
import { Actions as NavigationActions } from "react-native-router-flux";
import { observable } from "mobx";
import { observer } from "mobx-react";

import Loader from "../../../../../../Components/Loader";
import Images from "../../../../../../Themes/Images";
// Styles
import styles from "../../../GlobalStyles/DocConnectStyles";
import {SEARCHDOCTORSCONSTANTS} from "../../../../../../Constants/Patient";
@observer
export default class SearchList extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  renderTitle = () => {
    return (
      <View style={styles.DocConnectHeader}>
        <Text style={styles.DocConnectHeaderText}>{SEARCHDOCTORSCONSTANTS.SEARCH__NAVBAR_HEADING}</Text>
      </View>
    );
  };

  render() {
    let { name, specialization, doctor_id } = this.props.details;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.profilePicContainer}>
          <Image
            style={styles.profilePic}
            resizemode="contain"
            source={Images.femaleDoctorPic}
          />
        </View>
        <View style={styles.bodyContentContainer}>
          <View style={styles.profileDetailsContainer}>
            <View style={styles.nameContainer}>
              <Text style={styles.nameTextStyles}>
                {name ? name.toUpperCase() : null}
              </Text>
            </View>
            <View style={styles.designationContainer}>
              <Text style={styles.designationTextStyles}>
                {specialization}
              </Text>
            </View>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              onPress={() => this.props.setDoctorIdModal(doctor_id)}
              style={styles.firstButtonContainer}
            >
              <Text style={styles.firstButtonTextStyles}>{SEARCHDOCTORSCONSTANTS.CONNECT_BUTTON_TEXT}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.requestDoctorProfileDetails();
              }}
              style={styles.secondButtonContainer}
            >
              <Text style={styles.secondButtonTextStyles}>{SEARCHDOCTORSCONSTANTS.OPEN_BUTTON_TEXT}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
