import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Modal,
  TouchableHighlight,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Platform,
  Keyboard
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";
import { observable } from "mobx";
import { observer } from "mobx-react";
import CheckBox from "react-native-check-box";
import Loader from "../../../../../../Components/Loader";

// import stores
import AuthStore from "../../../../../../stores/AuthStore";
import PatientRequestsStore from "../../../../../../stores/PatientRequestsStore";

// Styles
import styles from "./styles.js";
import { DOCCONNECTMODALCONSTANTS } from "../../../../../../Constants/Patient";
@observer
class CustomCheckbox extends Component {
  @observable isChecked: boolean;

  constructor(props) {
    super(props);
    this.isChecked = false;
  }
  toggleCheckboxChange = () => {
    const { handleCheckboxChange, label } = this.props;
    this.isChecked = !this.isChecked;
    handleCheckboxChange(label);
  };
  render() {
    const { label } = this.props;
    const { isChecked } = this;

    return (
      <View style={styles.checkBoxChild}>
        <CheckBox
          isChecked={this.isChecked}
          onClick={this.toggleCheckboxChange}
        />
        <Text style={styles.checkBoxLabelText}>{label}</Text>
      </View>
    );
  }
}

@observer
export default class DocConnectModal extends Component {
  @observable doctorCode: String;
  @observable errorText: String;
  @observable patientPrivacyErrorText: String;
  @observable patientPrivacyInfo: Object;
  @observable selectedCheckboxes: Array;
  constructor(props) {
    super(props);
    this.state = {
      doctorCode: "",
      errorText: null,
      patientPrivacyLabels: this.preparePrivacyObject(),
      patientPrivacyErrorText: null
    };
  }

  handleCodeChange = doctorCode => {
    this.setState({ doctorCode });
  };

  preparePrivacyObject = () => {
    let patientPrivacyLabels = [];
    patientPrivacyLabels.push(AuthStore.loginResponse.name);
    AuthStore.loginResponse.children.map(child => {
      patientPrivacyLabels.push(child.name);
    });
    return patientPrivacyLabels;
  };

  componentWillMount = () => {
    this.selectedCheckboxes = new Set();
  };

  toggleCheckbox = label => {
    if (this.selectedCheckboxes.has(label)) {
      this.selectedCheckboxes.delete(label);
    } else {
      this.selectedCheckboxes.add(label);
    }
    this.hideKeyboard();
  };

  createCheckbox = label => (
    <CustomCheckbox
      label={label}
      handleCheckboxChange={this.toggleCheckbox}
      key={label}
    />
  );

  createCheckboxes = () =>
    this.state.patientPrivacyLabels.map(this.createCheckbox);

  isvalidDoctorCode = () => {
    //Validation of Doctor code
    if (this.state.doctorCode.length <= 4) {
      this.setState({ errorText: "Doctor Code is required" });
      return false;
    }
    if (this.state.doctorCode.length === 0) {
      this.setState({ errorText: "Please Give valid Doctor Code" });
      return false;
    }
    this.setState({ errorText: null });
    return true;
  };

  isValidPrivacyData = () => {
    //Validation of Checkbox group
    if (this.selectedCheckboxes.size === 0) {
      this.setState({
        patientPrivacyErrorText: "Please Select Atleast one detail"
      });
      return false;
    }
    this.setState({
      patientPrivacyErrorText: null
    });
    return true;
  };
  handleSubmit = () => {
    this.hideKeyboard();
    const { toggleModalVisible, doctorId } = this.props;
    const { doctorCode } = this.state;
    if (!this.isvalidDoctorCode()) {
      return;
    }
    if (!this.isValidPrivacyData()) {
      return;
    }
    //Make a network call
    this.setState({
      errorText: null,
      patientPrivacyErrorText: null
    });

    let requestObject = {
      doctor_code: doctorCode,
      doctor_id: doctorId
    };
    PatientRequestsStore.requestToConnectDoctor(
      requestObject,
      this.sharePatientAccessDetails
    );
  };

  sharePatientAccessDetails = () => {
    const { toggleModalVisible, doctorId } = this.props;
    // toggleModalVisible();

    if (this.selectedCheckboxes.size != 0) {
      let patientaccessRequestObject = this.makePatientPrivacyRequestObject(
        doctorId
      );
      PatientRequestsStore.setPatientAccessDetails(
        patientaccessRequestObject,
        doctorId
      );
    }
  };

  makePatientPrivacyRequestObject = doctorId => {
    let patientaccessRequestObject = {
      entities: [],
      doctor_id: doctorId,
      is_allowed: true
    };
    if (__DEV__)
      console.log(
        "step 1 patientaccessRequestObject",
        patientaccessRequestObject
      );
    AuthStore.loginResponse.children.map(child => {
      if (this.selectedCheckboxes.has(child.name)) {
        let entityObject = {
          entity_id: child.child_id,
          entity_type: "CHILD"
        };
        patientaccessRequestObject.entities.push(entityObject);
      }
    });
    if (__DEV__)
      console.log(
        "step 2 patientaccessRequestObject",
        patientaccessRequestObject
      );
    if (this.selectedCheckboxes.has(AuthStore.loginResponse.name)) {
      let entityObject = {
        entity_id: AuthStore.loginResponse.patient_id,
        entity_type: "PATIENT"
      };
      patientaccessRequestObject.entities.push(entityObject);
    }
    if (__DEV__)
      console.log(
        "step 3 patientaccessRequestObject",
        patientaccessRequestObject
      );
    return patientaccessRequestObject;
  };
  hideKeyboard = () => {
    Keyboard.dismiss();
  };
  displayModal = () => {
    let { status, modalVisible, toggleModalVisible } = this.props;
    if (!modalVisible) {
      PatientRequestsStore.setConnectToDoctorsAPIStatus(0);
      return null;
    }
    if (status === 100) {
      return <Loader animating={true} />;
    } else if (status === 200) {
      toggleModalVisible();
    }
    this.selectedCheckboxes = new Set();
    return (
      <View style={styles.modalContainer}>
        <View style={styles.bodyContainer}>
          <View style={styles.doctorCodeInputContainer}>
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              style={
                this.state.errorText != null
                  ? [styles.doctorCodeInput, styles.errorInput]
                  : styles.doctorCodeInput
              }
              onChangeText={text => this.handleCodeChange(text)}
              onEndEditing={() => this.hideKeyboard()}
              underlineColorAndroid="transparent"
              placeholder={DOCCONNECTMODALCONSTANTS.DOCTOR_CODE_PLACEHOLDER}
              placeholderTextColor="#999999"
              placeholderStyle={styles.placeholderTextboxfield}
            />
          </View>
          <View style={styles.checkBoxMainContainer}>
            <View style={styles.checkboxHeadingContainer}>
              <Text style={styles.headingStyles}>
                {DOCCONNECTMODALCONSTANTS.SHARE_DETAILS_OF_HEADER_TEXT}
              </Text>
            </View>
            <View style={styles.checkBoxBlockContainer}>
              {this.createCheckboxes()}
            </View>
            <View style={styles.checkboxErrorContainer}>
              {this.state.patientPrivacyErrorText && (
                <Text style={styles.errorTextStyles}>
                  {this.state.patientPrivacyErrorText}
                </Text>
              )}
            </View>
          </View>
        </View>
        <View style={styles.footerButtonContainer}>
          <TouchableOpacity onPress={() => this.props.toggleModalVisible()}>
            <View style={styles.closeBtnContainer}>
              <Text style={styles.cancelBtnTextStyles}>Cancel</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.handleSubmit();
            }}
          >
            <View style={styles.proceedBtnStyles}>
              <Text style={styles.btnTextStyles}>
                {DOCCONNECTMODALCONSTANTS.PROCEED_BUTTON_TEXT}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  render() {
    let { status, modalVisible, toggleModalVisible } = this.props;
    return (
      <Modal
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          toggleModalVisible();
        }}
      >
        <View style={styles.modalMainContainer}>{this.displayModal()}</View>
      </Modal>
    );
  }
}
