import { StyleSheet, Platform } from "react-native";
import { Fonts, Colors } from "../../../../../../Themes/";

export default StyleSheet.create({
  modalMainContainer: {
    backgroundColor: "rgba(0, 0, 0, 0.77)",
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  modalContainer: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: Colors.whiteColor,

    borderRadius: 5,
    borderColor: Colors.graycolor,
    borderWidth: 0.7,
    position: "relative"
  },
  headerContainer: {
    marginVertical: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  headerContentContainer: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  // closeBtnPostionContainer: {
  //   position: "absolute",
  //   top: 2,
  //   right: 2,
  //   width: 20,
  //   height: 20,
  //   borderRadius: 10,
  //   borderColor: "black",
  //   borderWidth: 1,
  //   justifyContent: "center",
  //   alignItems: "center"
  // },
  closeBtnContainer: {
    marginRight: 10,
    height: 30,
    width: 90,
    borderRadius: 40,
    borderColor: Colors.primaryColor,
    borderWidth: 0.5,
    justifyContent: "center",
    alignItems: "center"
  },
  cancelBtnTextStyles: {
    color: Colors.blackColor,
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.family.fontfamily
  },
  headerTextStyles: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.family.fontfamily
  },
  headerSubTextStyles: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.family.fontfamily
  },

  bodyContainer: {},
  doctorCodeInputContainer: {
    margin: 10
  },
  doctorCodeInput: {
    fontSize: Fonts.size.medium,
    backgroundColor: Colors.lightwhite,
    width: 240,
    fontFamily: Fonts.family.fontfamily
  },
  errorInput: {
    borderColor: Colors.whiteblack,
    borderRadius: 4,
    borderWidth: 0.4
  },
  errorTextStyles: {
    color: Colors.whiteblack,
    fontSize: Fonts.size.medium
  },
  placeholderTextboxfield: {
    fontFamily: Fonts.family.fontfamily
  },
  btnTextStyles: {
    fontSize: Fonts.size.regular,
    color: Colors.whiteColor,
    fontFamily: Fonts.family.fontfamily
  },
  cancelBtnStyles: {},
  footerButtonContainer: {
    alignItems: "center",
    // justifyContent: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    //backgroundColor: "#387ef5",
    // backgroundColor: "#ff69b4",
    // borderRadius: 5,
    height: 40,
    // elevation: 1,
    marginBottom: 10
    // width: 120
  },
  proceedBtnStyles: {
    width: 100,
    height: 30,
    borderRadius: 40,
    elevation: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.primaryColor
  },

  //CheckboxGroup and Checkbox

  checkBoxMainContainer: {
    flexDirection: "column",
    marginHorizontal: 10
  },
  checkboxHeadingContainer: {
    marginTop: 10,
    marginBottom: 10
  },
  headingStyles: {
    fontSize: Fonts.size.input,
    fontFamily: Fonts.family.fontfamily
  },
  checkBoxBlockContainer: {
    minHeight: 30,
    maxHeight: 100,
    marginBottom: 32,
    justifyContent: "space-around",
    flexDirection: "column"
  },
  checkboxErrorContainer: {
    marginBottom: 5
  },
  checkBoxChild: {
    flexDirection: "row",
    alignItems: "center"
  },
  checkBoxLabelText: {
    fontSize: Fonts.sizes.mediumsmall,
    fontFamily: Fonts.family.fontfamily
  }
});
