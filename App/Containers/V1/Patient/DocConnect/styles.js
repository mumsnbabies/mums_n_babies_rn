import { StyleSheet, Platform } from "react-native";
import Fonts from "../../../../Themes/Fonts";
import Colors from "../../../../Themes/Colors";

export default StyleSheet.create({
  DocConnectHeader: {
    alignItems: "center",
    justifyContent: "center"
  },
  DocConnectHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  noDataViewContainer: {
    justifyContent: "center",
    alignItems: "center",
    width: 300,
    height: 150,
    marginHorizontal: 30
  },
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.whiteColor
  },
  tabContainer: {
    marginTop: 20,
    marginBottom: 20,
    flexDirection: "row",
    backgroundColor: Colors.whiteColor,
    borderColor: Colors.blackColor,
    marginLeft: 10,
    marginRight: 5
  },
  activeTabView: {
    backgroundColor: Colors.requestButtom,
    borderColor: Colors.requestButtom,
    borderWidth: 0.5,
    borderRadius: 40,
    flex: 1,
    alignItems: "center",
    marginRight: 5,
    height: 25,
    alignItems: "center",
    justifyContent: "center"
  },
  activeTabText: {
    color: Colors.whiteColor,
    fontFamily: Fonts.family.fontFamilyBold,
    fontSize: Fonts.sizes.mediumsmall
  },
  deActiveTabView: {
    backgroundColor: Colors.whiteColor,
    borderColor: Colors.blackColor,
    borderWidth: 1,
    borderRadius: 40,
    flex: 1,
    height: 25,
    justifyContent: "center",
    alignItems: "center",
    marginRight: 5
  },
  deActiveTabText: {
    fontFamily: Fonts.family.fontFamilyBold,
    fontSize: Fonts.sizes.mediumsmall
  },
  searchFieldContainer: {
    height: 40,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10
  },
  searchInputStyles: {
    height: Platform.OS === "ios" ? 40 : 40,
    borderColor: Colors.blackColor,
    paddingLeft: 10,
    fontFamily: Fonts.family.fontFamilyBold,
    borderWidth: 0.5,
    borderRadius: 5,
    fontFamily: Fonts.family.fontfamily
  }
});
