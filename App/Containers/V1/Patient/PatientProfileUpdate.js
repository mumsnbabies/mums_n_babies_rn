import React, { Component } from "react";
import {
  ScrollView,
  Text,
  Image,
  View,
  TouchableOpacity,
  StatusBar,
  TextInput,
  Alert,
  Picker,
  Platform
} from "react-native";
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel
} from "react-native-simple-radio-button";
import { observable, toJS } from "mobx";
import { observer } from "mobx-react";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../Components/Loader";
import ExpectingParent from "./ExpectingParent";
import ChildDetails from "./ChildDetails";
import AuthStore from "../../../stores/AuthStore";
import PatientProfileUpdateStore from "../../../stores/PatientProfileUpdateStore";
import Colors from "../../../Themes/Colors";
import Fonts from "../../../Themes/Fonts";
import Images from "../../../Themes/Images";
var ImagePicker = require("react-native-image-picker");
import { uploadImage } from "./Utils";
// Styles
import styles from "./Styles/PatientProfileUpdateStyles";
import { PATIENTPROFILEUPDATECONSTANTS } from "../../../Constants/Patient";
@observer
export default class PatientProfileUpdateFirstScreen extends Component {
  @observable patient_name: String;
  @observable patient_age: String;
  @observable patient_city: String;
  @observable patient_contact: String;
  @observable patient_blood: String;
  @observable patient_food: String;
  @observable patient_occupation: String;
  @observable patient_travel: String;
  @observable occupationValue: String;
  @observable patient_attributes: Array;
  @observable requiredName;
  @observable requiredAge;
  @observable requiredCity;
  @observable requiredBlood;
  @observable requiredFood;
  @observable requiredContact;
  constructor(props) {
    super(props);
    this.state = {
      imagePickerResponse: null,
      imgSource: "NOIMAGE"
    };

    this.patient_name = AuthStore.loginResponse.name;
    this.patient_age = AuthStore.loginResponse.age
      ? "" + AuthStore.loginResponse.age
      : "";
    this.patient_city =
      AuthStore.loginResponse.address && AuthStore.loginResponse.address.city
        ? AuthStore.loginResponse.address.city
        : "";
    this.occupationValue = "";
    this.patient_contact = "";
    this.patient_blood = "Select_Blood_Group";
    this.patient_food = "Select_Food_Preference";
    this.patient_occupation = "";
    this.patient_travel = "";
    this.requiredName = null;
    this.requiredAge = null;
    this.requiredCity = null;
    this.requiredBlood = null;
    this.requiredFood = null;
    this.requiredContact = null;
    this.patient_attributes = toJS(AuthStore.loginResponse.patient_attributes);
    this.avatarSource = {
      uri: ""
    };
  }
  componentWillMount() {
    if (__DEV__) {
      console.log(
        "WIll Mount",
        AuthStore.loginResponse.patient_attributes,
        this.patient_attributes
      );
    }
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
    if (this.patient_attributes.length > 0) {
      this.patient_attributes.map((data, index) => {
        if (data.attr_key === "CONTACT" && data.attr_values.length > 0) {
          this.patient_contact = data.attr_values[0].value;
        } else if (
          data.attr_key === "BLOOD_GROUP" &&
          data.attr_values.length > 0
        ) {
          this.patient_blood = data.attr_values[0].value;
        } else if (
          data.attr_key === "FOOD_PREFERENCE" &&
          data.attr_values.length > 0
        ) {
          this.patient_food = data.attr_values[0].value;
        } else if (
          data.attr_key === "OCCUPATION" &&
          data.attr_values.length > 0
        ) {
          this.patient_occupation = data.attr_values[0].value;
        } else if (data.attr_key === "TRAVEL" && data.attr_values.length > 0) {
          this.patient_travel = data.attr_values[0].value;
        }
      });
    }
    if (__DEV__) {
      console.log(AuthStore.loginResponse.pic);
    }
    this.avatarSource = {
      uri: AuthStore.loginResponse.pic
    };
  }

  uploadToS3 = (request, navigateToPatientDetails) => {
    uploadImage(
      this.state.imagePickerResponse,
      this.state.imgSource,
      request,
      navigateToPatientDetails
    );
  };

  uploadPicture = () => {
    if (__DEV__) {
      console.log("Upliad pic");
    }
    ImagePicker.showImagePicker(response => {
      if (response.didCancel) {
        // console.log("User cancelled image picker");
      } else if (response.error) {
        // console.log("ImagePicker Error: ", response.error);
      } else {
        let source = { uri: response.uri };
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.avatarSource = source;
        this.setState({
          imgSource: source.uri,
          imagePickerResponse: response
        });
      }
    });
  };

  renderTitle = () => {
    return (
      <View style={styles.PatientYourProfileHeader}>
        <Text style={styles.PatientYourProfileHeaderText}>
          {PATIENTPROFILEUPDATECONSTANTS.PROFILE_UPDATE_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };

  handlePatientName = name => {
    this.requiredName = null;
    this.patient_name = name;
    if (!this.patient_name) {
      this.requiredName = "*";
    }
  };
  handlePatientAge = age => {
    this.requiredAge = null;
    this.patient_age = age;
    if (!this.patient_age) {
      this.requiredAge = "*";
    }
  };
  handlePatientCity = city => {
    this.requiredCity = null;
    this.patient_city = city;
    if (!this.patient_city) {
      this.requiredCity = "*";
    }
  };
  handlePatientContact = contact => {
    this.requiredContact = null;
    let flag = 0;
    this.patient_contact = contact;
    if (!this.patient_contact) {
      this.requiredContact = "*";
    }
    this.patient_attributes.map((data, index) => {
      if (data.attr_key === "CONTACT") {
        flag = 1;
        data.attr_values[0].value = contact;
      }
    });
    if (flag === 0)
      this.patient_attributes.push({
        attr_key: "CONTACT",
        attr_values: [
          {
            value_key: "CONTACT",
            value: `${contact}`
          }
        ],
        description: "",
        update_time: "",
        attr_id: -1
      });
  };
  handlePatientBloodGroup = bloodgroup => {
    this.requiredBlood = null;
    let flag = 0;
    this.patient_blood = bloodgroup;
    if (this.patient_blood === "Select_Blood_Group") {
      this.requiredBlood = "*";
    }
    if (bloodgroup !== "Select_Blood_Group") {
      this.patient_attributes.map((data, index) => {
        if (data.attr_key === "BLOOD_GROUP") {
          flag = 1;
          data.attr_values[0].value = bloodgroup;
        }
      });
      if (flag === 0)
        this.patient_attributes.push({
          attr_key: "BLOOD_GROUP",
          attr_values: [
            {
              value_key: "BLOOD_GROUP",
              value: `${bloodgroup}`
            }
          ],
          description: "",
          update_time: "",
          attr_id: -1
        });
    }
  };
  handleFoodPreferance = foodpreference => {
    let flag = 0;
    this.requiredFood = null;
    this.patient_food = foodpreference;
    if (this.patient_food === "Select_Food_Preference") {
      this.requiredFood = "*";
    }
    if (foodpreference !== "Select_Food_Preference") {
      this.patient_attributes.map((data, index) => {
        if (data.attr_key === "FOOD_PREFERENCE") {
          flag = 1;
          data.attr_values[0].value = foodpreference;
        }
      });
      if (flag === 0)
        this.patient_attributes.push({
          attr_key: "FOOD_PREFERENCE",
          attr_values: [
            {
              value_key: "FOOD_PREFERENCE",
              value: `${foodpreference}`
            }
          ],
          description: "",
          update_time: "",
          attr_id: -1
        });
    }
  };
  handleOccupation = occupation => {
    let flag = 0;
    let value = "";

    if (occupation === 0) {
      value = "Service";
      this.occupationValue = "Service";
    } else {
      value = "Business";
      this.occupationValue = "Business";
    }
    this.patient_attributes.map((data, index) => {
      if (data.attr_key === "OCCUPATION") {
        flag = 1;
        data.attr_values[0].value = value;
      }
    });
    if (flag === 0)
      this.patient_attributes.push({
        attr_key: "OCCUPATION",
        attr_values: [
          {
            value_key: "OCCUPATION",
            value: value
          }
        ],
        description: "",
        update_time: "",
        attr_id: -1
      });
  };
  handleTravel = travel => {
    let flag = 0;
    let value = "";
    if (travel === 0) {
      value = "Yes";
    } else {
      value = "No";
    }
    this.patient_attributes.map((data, index) => {
      if (data.attr_key === "TRAVEL") {
        flag = 1;
        data.attr_values[0].value = value;
      }
    });
    if (flag === 0)
      this.patient_attributes.push({
        attr_key: "TRAVEL",
        attr_values: [
          {
            value_key: "TRAVEL",
            value: value
          }
        ],
        description: "",
        update_time: "",
        attr_id: -1
      });
  };
  getPatientAttributes = () => {
    let occurrences = [];

    //TODO: Hardcoded basic values here

    this.patient_attributes.map((data, index) => {
      if (!data.is_periodical && data.attr_values.length > 0) {
        occurrences.push({
          attr_key: data.attr_key,
          description: "",
          attr_id: data.attr_id,
          attr_values: data.attr_values,
          to_add: true
        });
      }
    });
    if (__DEV__) {
      console.log(occurrences);
    }
    return occurrences;
  };
  moveToSecondUpdatePage = () => {
    let flag = 1;
    if (this.patient_name === "") {
      flag = 0;
      this.requiredName = "*";
    }
    if (this.patient_age === "") {
      flag = 0;
      this.requiredAge = "*";
    }
    if (this.patient_city === "") {
      flag = 0;
      this.requiredCity = "*";
    }
    if (this.patient_contact === "") {
      flag = 0;
      this.requiredContact = "*";
    }
    if (this.patient_blood === "Select_Blood_Group") {
      flag = 0;
      this.requiredBlood = "*";
    }
    if (this.patient_food === "Select_Food_Preference") {
      flag = 0;
      this.requiredFood = "*";
    }
    if (flag === 1) {
      let requestObject = {
        attributes: this.getPatientAttributes(),
        entity_id: AuthStore.loginResponse.patient_id,
        entity_type: "PATIENT",
        activity_required: false
      };
      if (__DEV__) {
        console.log(requestObject);
      }
      PatientProfileUpdateStore.savePatientAttributesRequest(requestObject);
      let request = {
        name: this.patient_name,
        age: parseInt(this.patient_age),
        pic: this.avatarSource.uri,
        address: {
          city: this.patient_city,
          geo_location: "",
          location: "",
          address: ""
        }
      };
      if (__DEV__) {
        console.log(request);
      }
      this.uploadToS3(request, this.navigateToPatientDetails);
      // PatientProfileUpdateStore.updatePatientDetails(
      //   request,
      //   this.navigateToPatientDetails
      // )
    }
  };
  navigateToPatientDetails() {
    NavigationActions.PatientProfileDetails();
  }
  render() {
    if (PatientProfileUpdateStore.detailProgressStatus === 100) {
      return <Loader animating={true} />;
    } else if (PatientProfileUpdateStore.detailProgressStatus > 200) {
      Alert.alert(
        PATIENTPROFILEUPDATECONSTANTS.ALERT_ERROR_TITLE_TEXT,
        PatientProfileUpdateStore.error.statusText,
        [
          {
            text: "OK",
            onPress: () =>
              PatientProfileUpdateStore.resetUpdatePatientProState()
          }
        ]
      );
    }
    let travel = [{ label: "Yes", value: 0 }, { label: "No", value: 1 }];
    let occupation = [
      { label: "Service", value: 0 },
      { label: "Business", value: 1 }
    ];
    console.log("image url : " + this.avatarSource.uri);
    return (
      <ScrollView style={styles.MainScreen}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <View style={styles.UpdateProfileImageView}>
          <View style={styles.UpdateProfileImageRelative}>
            {this.avatarSource.uri ? (
              <Image
                style={styles.UpdateProfileImage}
                resizeMode="cover"
                source={this.avatarSource}
              />
            ) : (
              <Image
                style={styles.UpdateProfileImage}
                resizeMode="cover"
                source={Images.patientProfilePic}
              />
            )}
            <TouchableOpacity
              onPress={() => {
                this.uploadPicture();
              }}
              style={styles.EditProfileImageView}
            >
              <Image
                source={Images.profileEdit}
                style={styles.EditProfileIcon}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.DocInput}>
          {this.patient_name ? (
            <Text style={styles.patientname}>
              {PATIENTPROFILEUPDATECONSTANTS.NAME_FIELD_HEADING}
            </Text>
          ) : null}
          <View style={{ flexDirection: "row" }}>
            <TextInput
              isRequired={true}
              returnKeyType={"next"}
              style={styles.DocInputField}
              onChangeText={this.handlePatientName}
              placeholder={PATIENTPROFILEUPDATECONSTANTS.NAME_FIELD_PLACEHOLDER}
              autoCorrect={false}
              underlineColorAndroid="transparent"
              value={this.patient_name}
            />
            {this.requiredName && (
              <View style={styles.ImageContainer}>
                <Image
                  style={styles.RequiredField}
                  source={Images.alertShield}
                />
              </View>
            )}
          </View>
          <View style={styles.InputUnderline} />
          {this.patient_age ? (
            <Text style={styles.patientname}>
              {PATIENTPROFILEUPDATECONSTANTS.AGE_FIELD_HEADING}
            </Text>
          ) : null}
          <View style={{ flexDirection: "row" }}>
            <TextInput
              keyboardType="numeric"
              isRequired={true}
              returnKeyType={"next"}
              style={styles.DocInputField}
              onChangeText={this.handlePatientAge}
              placeholder={PATIENTPROFILEUPDATECONSTANTS.AGE_FIELD_PLACEHOLDER}
              autoCorrect={false}
              underlineColorAndroid="transparent"
              value={"" + this.patient_age}
            />
            {this.requiredAge && (
              <View style={styles.ImageContainer}>
                <Image
                  style={styles.RequiredField}
                  source={Images.alertShield}
                />
              </View>
            )}
          </View>

          <View style={styles.InputUnderline} />
          {this.patient_city ? (
            <Text style={styles.patientname}>
              {PATIENTPROFILEUPDATECONSTANTS.CITY_FIELD_HEADING}
            </Text>
          ) : null}
          <View style={{ flexDirection: "row" }}>
            <TextInput
              returnKeyType={"next"}
              onChangeText={this.handlePatientCity}
              style={styles.DocInputField}
              placeholder={PATIENTPROFILEUPDATECONSTANTS.CITY_FIELD_PLACEHOLDER}
              autoCorrect={false}
              underlineColorAndroid="transparent"
              value={this.patient_city}
            />
            {this.requiredCity && (
              <View style={styles.ImageContainer}>
                <Image
                  style={styles.RequiredField}
                  source={Images.alertShield}
                />
              </View>
            )}
          </View>
          <View style={styles.InputUnderline} />
          {this.patient_contact ? (
            <Text style={styles.patientname}>
              {PATIENTPROFILEUPDATECONSTANTS.CONTACT_NUMBER_FIELD_HEADING}
            </Text>
          ) : null}
          <View style={{ flexDirection: "row" }}>
            <TextInput
              returnKeyType={"next"}
              keyboardType="phone-pad"
              onChangeText={this.handlePatientContact}
              style={styles.DocInputField}
              placeholder={
                PATIENTPROFILEUPDATECONSTANTS.CONTACT_NUMBER_FIELD_PLACEHOLDER
              }
              autoCorrect={false}
              underlineColorAndroid="transparent"
              value={this.patient_contact}
            />
            {this.requiredContact && (
              <View style={styles.ImageContainer}>
                <Image
                  style={styles.RequiredField}
                  source={Images.alertShield}
                />
              </View>
            )}
          </View>
          <View style={styles.InputUnderline} />
          {this.patient_blood ? (
            <Text style={styles.patientname}>
              {PATIENTPROFILEUPDATECONSTANTS.BLOOD_GROUP_FIELD_HEADING}
            </Text>
          ) : null}
          <View style={{ flexDirection: "row" }}>
            {/* <TextInput
              onChangeText={this.handlePatientBloodGroup}
              style={styles.DocInputField}
              placeholder="Blood Group"
              autoCorrect={false}
              underlineColorAndroid="transparent"
              value={this.patient_blood}
            /> */}
            <Picker
              selectedValue={this.patient_blood}
              style={styles.pickerstyles}
              onValueChange={option => this.handlePatientBloodGroup(option)}
            >
              <Picker.Item
                label="Select Blood Group"
                value="Select_Blood_Group"
              />
              <Picker.Item label="A+" value="A+" />
              <Picker.Item label="O+" value="O+" />
              <Picker.Item label="B+" value="B+" />
              <Picker.Item label="AB+" value="AB+" />
              <Picker.Item label="A-" value="A-" />
              <Picker.Item label="O-" value="O-" />
              <Picker.Item label="B-" value="B-" />
              <Picker.Item label="AB-" value="AB-" />
            </Picker>
            {this.requiredBlood && (
              <View style={styles.ImageContainer}>
                <Image
                  style={styles.RequiredField}
                  source={Images.alertShield}
                />
              </View>
            )}
          </View>
          <View style={styles.InputUnderline} />
          <Text style={styles.patientfoodpreference}>
            {PATIENTPROFILEUPDATECONSTANTS.FOOD_PREFERENCE_FIELD_HEADING}
          </Text>
          <View style={{ flexDirection: "row" }}>
            <Picker
              selectedValue={this.patient_food}
              style={styles.pickerstyles}
              onValueChange={option => this.handleFoodPreferance(option)}
            >
              <Picker.Item
                label="Select Food Preference"
                value="Select_Food_Preference"
              />
              <Picker.Item label="Vegetarian" value="Vegetarian" />
              <Picker.Item label="Non-Vegetarian" value="Non-Vegetarian" />
              <Picker.Item label="Jain" value="Jain" />
            </Picker>
            {this.requiredFood && (
              <View style={styles.ImageContainer}>
                <Image
                  style={styles.RequiredField}
                  source={Images.alertShield}
                />
              </View>
            )}
          </View>
          <View style={styles.InputUnderline} />
          <Text style={styles.patientfoodpreference}>
            {PATIENTPROFILEUPDATECONSTANTS.OCCUPATION_FIELD_HEADING}
          </Text>
          <RadioForm
            radio_props={occupation}
            initial={this.patient_occupation === "Service" ? 0 : 1}
            buttonSize={10}
            formHorizontal={true}
            labelHorizontal={true}
            buttonColor={Colors.primaryColor}
            labelStyle={{
              color: Colors.blackColor,
              fontFamily: Fonts.family.fontfamily
            }}
            animation={true}
            style={styles.RadioFormStylesOccupation}
            onPress={value => this.handleOccupation(value)}
          />
          <View style={styles.InputUnderline} />
          <Text style={styles.patientfoodpreference}>
            {PATIENTPROFILEUPDATECONSTANTS.TRAVEL_FIELD_HEADING}
          </Text>
          <RadioForm
            radio_props={travel}
            initial={this.patient_travel === "Yes" ? 0 : 1}
            buttonSize={10}
            formHorizontal={true}
            labelHorizontal={true}
            labelStyle={{
              color: Colors.blackColor,
              fontFamily: Fonts.family.fontfamily
            }}
            buttonColor={Colors.primaryColor}
            animation={true}
            style={styles.RadioFormStylesTravel}
            onPress={value => this.handleTravel(value)}
          />
        </View>
        <View style={styles.SaveAndProceedSection}>
          <TouchableOpacity
            activeOpacity={1}
            onPress={this.moveToSecondUpdatePage}
          >
            <View style={styles.SaveAndProceedView}>
              <Text style={styles.SaveAndProceedText}>
                {PATIENTPROFILEUPDATECONSTANTS.SAVE_PROCEED_BUTTON_TEXT}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}
