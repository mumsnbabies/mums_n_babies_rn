import { Scene, Router } from "react-native-router-flux"
import React, { Component } from "react"

// Import Components

import AddMedicalDocument from "./components/AddMedicalDocument"
import UpdateDocument from "./components/UpdateDocument"
import ShareDocument from "./components/ShareDocument"
import AddNewAttribute from "./components/AddNewAttribute"

import PatientProfileHome from "./index"

const scenes = [
  <Scene
    hideNavBar={false}
    key="PatientProfileHome"
    component={PatientProfileHome}
    navigatonBarStyle={{ backgroundColor: "#ffffff" }}
  />,
  <Scene
    hideNavBar={false}
    key="AddMedicalDocument"
    component={AddMedicalDocument}
    navigatonBarStyle={{ backgroundColor: "#ffffff" }}
  />,
  <Scene
    hideNavBar={false}
    key="UpdateDocument"
    component={UpdateDocument}
    navigatonBarStyle={{ backgroundColor: "#ffffff" }}
  />,
  <Scene
    hideNavBar={false}
    key="ShareDocument"
    component={ShareDocument}
    navigatonBarStyle={{ backgroundColor: "#ffffff" }}
  />,
  <Scene
    hideNavBar={false}
    key="PatientAddNewAttribute"
    component={AddNewAttribute}
    navigatonBarStyle={{ backgroundColor: "#ff69b4" }}
  />
]

export default scenes
