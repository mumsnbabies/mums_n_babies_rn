import { StyleSheet, Platform } from "react-native";
import { Fonts, Colors } from "../../../../Themes/";

// #ff69b4
export default StyleSheet.create({
  patientProfileHeader: {
    alignItems: "center",
    justifyContent: "center"
  },
  patientProfileHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  profileMainContainer: {
    flex: 1,
    backgroundColor: Colors.primaryColor,
    position: "relative"
  }
});
