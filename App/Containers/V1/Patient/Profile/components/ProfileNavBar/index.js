import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";
import _ from "lodash";
import { Actions } from "jumpstate";
import { observable } from "mobx";
import { observer } from "mobx-react";
import Loader from "../../../../../../Components/Loader";

//Import sub Components
import Dashboard from "../Dashboard";
import MedicalRecords from "../MedicalRecordsWrapper";

// Styles
import styles from "./styles";
import { MYPROFILECONSTANTS } from "../../../../../../Constants/Patient";
export default class ProfileNavBar extends Component {
  @observable selectedComponent: String;
  constructor(props) {
    super(props);
    this.selectedComponent = this.props.selectedComponent;
  }

  render() {
    const { changeRenderComponent } = this.props;
    return (
      <View style={styles.MainScreen}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={styles.QuickRecordsContainer}>
            <TouchableOpacity
              onPress={() => {
                this.selectedComponent = Dashboard;
                changeRenderComponent(Dashboard, "Dashboard");
              }}
            >
              <View
                style={
                  this.selectedComponent === Dashboard
                    ? styles.selectedCard
                    : styles.Cards
                }
              >
                <Image
                  resizemode="contain"
                  style={
                    this.selectedComponent === Dashboard
                      ? styles.CardImage
                      : styles.selectedCardImage
                  }
                  source={require("../../../../../../Images/analytics.png")}
                />
                <Text
                  style={
                    this.selectedComponent === Dashboard
                      ? styles.CardName
                      : styles.selectedCardName
                  }
                >
                  {MYPROFILECONSTANTS.DASHBOARD_TAB_TEXT}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.selectedComponent = MedicalRecords;
                changeRenderComponent(MedicalRecords, "Medical Records");
              }}
            >
              <View
                style={
                  this.selectedComponent === Dashboard
                    ? styles.Cards
                    : styles.selectedCard
                }
              >
                <Image
                  resizemode="contain"
                  style={
                    this.selectedComponent === Dashboard
                      ? styles.selectedCardImage
                      : styles.CardImage
                  }
                  source={require("../../../../../../Images/medical-report-icon.png")}
                />
                <Text
                  style={
                    this.selectedComponent === Dashboard
                      ? styles.selectedCardName
                      : styles.CardName
                  }
                >
                  {MYPROFILECONSTANTS.MEDICAL_RECORDS}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

/**<TouchableOpacity>
              <View style={styles.Cards}>
                <Image
                  resizemode="contain"
                  style={styles.CardImage}
                  source={require("../../../../../../Images/analytics2.png")}
                />
                <Text style={styles.CardName}>Consult</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => changeRenderComponent(Calendar, "Calendar")}
            >
              <View style={styles.Cards}>
                <Image
                  resizemode="contain"
                  style={styles.CardImage}
                  source={require("../../../../../../Images/analytics2.png")}
                />
                <Text style={styles.CardName}>Calendar</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity>
              <View style={styles.Cards}>
                <Image
                  resizemode="contain"
                  style={styles.CardImage}
                  source={require("../../../../../../Images/analytics2.png")}
                />
                <Text style={styles.CardName}>Notes</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.Cards}>
                <Image
                  resizemode="contain"
                  style={styles.CardImage}
                  source={require("../../../../../../Images/analytics2.png")}
                />
                <Text style={styles.CardName}>Events</Text>
              </View>
            </TouchableOpacity>*/
