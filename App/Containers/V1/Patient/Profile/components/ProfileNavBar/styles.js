import { StyleSheet, Platform } from "react-native";
import { Fonts, Colors } from "../../../../../../Themes/";

// #ff69b4
export default StyleSheet.create({
  MainScreen: {
    backgroundColor: Colors.primaryColor,
    height: 220
  },
  MyProfileHeader: {
    marginTop: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  MyProfileHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  QuickRecordsContainer: {
    flexDirection: "row",
    height: 120,
    marginLeft: 30,
    marginRight: 30,
    marginTop: 40,
    marginBottom: 10,
    elevation: 2,
    justifyContent: "center"
  },
  Cards: {
    justifyContent: "center",
    alignItems: "center",
    height: 100,
    width: 120,
    backgroundColor: Colors.unselectedTab,
    borderRadius: 5,
    marginRight: 5
  },
  selectedCard: {
    justifyContent: "center",
    alignItems: "center",
    height: 100,
    width: 120,
    backgroundColor: Colors.selectedTab,
    borderRadius: 5,
    elevation: 2,
    marginRight: 5
  },
  CardImage: {
    width: 40,
    height: 40
  },
  selectedCardImage: {
    width: 40,
    height: 40,
    opacity: 0.6
  },
  CardName: {
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold,
    marginTop: 8
  },
  selectedCardName: {
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold,
    marginTop: 8,
    opacity: 0.6
  }
});
