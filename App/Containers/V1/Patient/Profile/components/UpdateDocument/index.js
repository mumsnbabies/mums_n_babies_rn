import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  Platform,
  Picker
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";
import { observable } from "mobx";
import { observer } from "mobx-react";
import moment from "moment";
import Loader from "../../../../../../Components/Loader";
import ImagePicker from "react-native-image-picker";
import DateTimePicker from "react-native-modal-datetime-picker";

import MedicalRecordsWrapper from "../MedicalRecordsWrapper";

// import Stores
import PatientProfileStore from "../../../../../../stores/PatientProfileStore";
import AuthStore from "../../../../../../stores/AuthStore";

// Styles
import styles from "./styles";

//Import Util fns
import { updateDocumentDetails } from "./Utils";
import { UPDATEMEDICALRECORDCONSTANS } from "../../../../../../Constants/Patient";
@observer
export default class UpdateMedicalDocument extends Component {
  constructor(props) {
    super(props);
    this.state = {
      others: false,
      documentType2: "",
      documentObject: {},
      documentId: 0,
      documentName: "",
      documentType: "MRI",
      documentEntityType: "PATIENT",
      documentEntityName: "",
      isDateTimePickerVisible: false,
      selectedDate: "",
      documentEntityId: 2,
      imagePickerResponse: null,
      imgSource: "NOIMAGE"
    };
  }
  static onEnter = () => {
    NavigationActions.refresh({
      renderTitle: renderTitle
    });
  };
  static onExit = () => {
    NavigationActions.PatientProfileHome({
      activeComponent: MedicalRecordsWrapper,
      activeComponentText: "Medical Records"
    });
  };
  componentWillMount() {
    let documentObject = PatientProfileStore.getSelectedDocument(
      this.props.documentId
    );
    if (__DEV__) console.log(documentObject, "documentObject");
    const {
      documentId,
      entityId,
      description,
      entityType,
      doctorAccessIds,
      documentUrl,
      documentType,
      documentDate
    } = documentObject;
    let documentType2 = "";
    let others = false;
    let documentTypes = [
      "MRI",
      "CityScan",
      "Blood Test",
      "Urine Test",
      "X-Ray",
      "MRI",
      "CT Scan",
      "ECG",
      "Ultra-Sonography",
      "Eye Test",
      "Audiometry (ENT)"
    ];
    this.setState({
      documentObject,
      documentId,
      documentEntityId: entityId,
      documentName: description,
      documentType,
      documentEntityType: entityType,
      selectedDate: moment(documentDate).format("YYYY-MM-DD"),
      imgSource: documentUrl,
      documentEntityName: documentObject.getDocumentAuthorName
    });
    if (documentTypes.indexOf(documentType) === -1) {
      this.setState({
        documentType2: documentType,
        documentType: "Others",
        others: true
      });
    }
  }

  handleDocumentName = value => {
    this.setState({ documentName: value });
  };
  handleDocumentValidation = () => {
    if (this.state.documentName === "") {
      alert(UPDATEMEDICALRECORDCONSTANS.ALERT_DOCUMENT_NAME_REQUIRED_TEXT);
    }
    if (this.state.selectedDate === "") {
      alert(UPDATEMEDICALRECORDCONSTANS.ALERT_DOCUMENT_DATE_REQUIRED_TEXT);
    }
  };
  handleUpdateDocument = () => {
    this.handleDocumentValidation();
    if (this.state.documentName != "" && this.state.selectedDate != "") {
      let documentType = this.state.documentType;
      if (this.state.documentType === "Others") {
        documentType = this.state.documentType2;
      }
      updateDocumentDetails(
        this.state.documentName,
        documentType,
        this.state.documentEntityType,
        this.state.documentEntityId,
        this.state.selectedDate,
        this.state.imagePickerResponse,
        this.state.imgSource,
        this.state.documentId,
        this.resetDocumentDetails
      );
    }
  };
  resetDocumentDetails = () => {
    this.setState({
      documentName: "",
      documentType: "MRI",
      documentEntityType: "PATIENT",
      documentEntityId: 2,
      imagePickerResponse: null,
      imgSource: "NOIMAGE",
      selectedDate: ""
    });
    NavigationActions.PatientProfileHome({
      activeComponent: MedicalRecordsWrapper,
      activeComponentText: "Medical Records",
      type: "replace"
    });
  };
  handleDocumentType = option => {
    if (option === "Others") {
      this.setState({
        others: true,
        documentType: option
      });
    } else {
      this.setState({ others: false, documentType: option });
    }
  };
  handleDocumentType2 = value => {
    this.setState({
      documentType2: value
    });
  };
  handleDocumentEntityType = option => {
    let entityId = this.state.documentEntityId;
    let entityType = this.state.documentEntityType;

    if (option === AuthStore.loginResponse.name) {
      entityId = AuthStore.loginResponse.patient_id;
      entityType = "PATIENT";
    }
    if (AuthStore.loginResponse.children.length > 0) {
      AuthStore.loginResponse.children.map(child => {
        if (option === child.name) {
          entityId = child.child_id;
          entityType = "CHILD";
        }
      });
    }
    this.setState({
      documentEntityName: option,
      documentEntityType: entityType,
      documentEntityId: entityId
    });
  };
  handleAttachFile = () => {
    const options = {
      title: "Select Image",
      customButtons: [],
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      if (__DEV__) console.log("Response = ", response);

      if (response.didCancel) {
        if (__DEV__) console.log("User cancelled image picker");
      } else if (response.error) {
        if (__DEV__) console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        if (__DEV__)
          console.log("User tapped custom button: ", response.customButton);
      } else {
        this.setState({
          imagePickerResponse: response,
          imgSource: response.uri
        });
      }
    });
  };

  toggleDateTimePicker = () => {
    this.datePickerInput.blur();
    this.setState({
      isDateTimePickerVisible: !this.state.isDateTimePickerVisible
    });
  };
  handleDatePicked = date => {
    this.setState({ selectedDate: moment(date).format("YYYY-MM-DD") });
    this.toggleDateTimePicker();
  };
  render() {
    return (
      <View style={styles.mainContainer}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <DateTimePicker
            mode="date"
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this.handleDatePicked}
            maximumDate={new Date()}
            onCancel={this.toggleDateTimePicker}
          />
          <View style={styles.formContainer}>
            {this.state.documentName ? (
              <Text style={styles.fieldHeading}>
                {UPDATEMEDICALRECORDCONSTANS.MEDICAL_RECORD_NAME_FIELD_TEXT}
              </Text>
            ) : null}
            <View style={styles.medicalDocumentsInputContainer}>
              <TextInput
                returnKeyType={"next"}
                autoCorrect={false}
                style={styles.medicalDocumentsInputField}
                placeholder={
                  UPDATEMEDICALRECORDCONSTANS.MEDICAL_RECORD_NAME_PLACEHOLDER
                }
                underlineColorAndroid="transparent"
                onChangeText={this.handleDocumentName}
                value={this.state.documentName}
              />
            </View>
            <View style={styles.underline} />
            {this.state.documentType ? (
              <Text style={styles.fieldHeading}>
                {UPDATEMEDICALRECORDCONSTANS.MEDICAL_REPORT_FIELD_TEXT}{" "}
              </Text>
            ) : null}
            <View style={styles.pickerContainer}>
              <Picker
                selectedValue={this.state.documentType}
                style={styles.pickerContentStyles}
                onValueChange={option => this.handleDocumentType(option)}
              >
                <Picker.Item label="Blood Test" value="Blood Test" />
                <Picker.Item label="Urine Test" value="Urine Test" />
                <Picker.Item label="X-Ray" value="X-Ray" />
                <Picker.Item label="MRI" value="MRI" />
                <Picker.Item label="CT Scan" value="CT Scan" />
                <Picker.Item label="ECG" value="ECG" />
                <Picker.Item
                  label="Ultra-Sonography"
                  value="Ultra-Sonography"
                />
                <Picker.Item label="Eye Test" value="Eye Test" />
                <Picker.Item
                  label="Audiometry (ENT)"
                  value="Audiometry (ENT)"
                />
                <Picker.Item label="Others" value="Others" />
              </Picker>
            </View>
            <View style={styles.underline} />
            {this.state.others ? (
              <View style={styles.pickerContainer}>
                <TextInput
                  returnKeyType={"next"}
                  autoCorrect={false}
                  style={styles.medicalDocumentsInputField}
                  placeholder={"Enter your option"}
                  underlineColorAndroid="transparent"
                  onChangeText={this.handleDocumentType2}
                  value={this.state.documentType2}
                />
              </View>
            ) : null}
            <View style={styles.underline} />
            {this.state.documentEntityName ? (
              <Text style={styles.fieldHeading}>
                {UPDATEMEDICALRECORDCONSTANS.AUTHOR_FIELD_TEXT}
              </Text>
            ) : null}
            <View style={styles.pickerContainer}>
              <Picker
                selectedValue={this.state.documentEntityName}
                style={styles.pickerContentStyles}
                onValueChange={option => this.handleDocumentEntityType(option)}
              >
                <Picker.Item
                  label={AuthStore.loginResponse.name}
                  value={AuthStore.loginResponse.name}
                />
                {AuthStore.loginResponse.children.map(child => {
                  return (
                    <Picker.Item
                      label={child.name}
                      value={child.name}
                      key={child.name}
                    />
                  );
                })}
              </Picker>
            </View>
            <View style={styles.underline} />
            {this.state.selectedDate ? (
              <Text style={styles.fieldHeading}>
                {UPDATEMEDICALRECORDCONSTANS.DATE_FIELD_TEXT}
              </Text>
            ) : null}
            <View style={styles.dateInputContainer}>
              <TextInput
                autoCorrect={false}
                ref={e => (this.datePickerInput = e)}
                style={styles.dateInputField}
                placeholder={UPDATEMEDICALRECORDCONSTANS.DATE_FIELD_PLACEHOLDER}
                underlineColorAndroid="transparent"
                onFocus={() => this.toggleDateTimePicker()}
                value={this.state.selectedDate}
              />
            </View>
          </View>
          {this.state.imgSource != "NOIMAGE" ? (
            <View style={styles.imageContainer}>
              <TouchableOpacity
                key={`${this.state.imgSource}`}
                activeOpacity={1}
              >
                <Image
                  source={{ uri: this.state.imgSource }}
                  style={styles.attachedImage}
                />
              </TouchableOpacity>
            </View>
          ) : null}
          <View style={styles.attachButtonsMainContainer}>
            <TouchableOpacity onPress={() => this.handleAttachFile()}>
              <View style={styles.btnContentStyles}>
                <Text style={styles.btnTextStyles}>
                  {UPDATEMEDICALRECORDCONSTANS.ATTACH_FILES_BUTTON_TEXT}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.handleUpdateDocument()}>
              <View style={styles.btnContentStyles}>
                <Text style={styles.btnTextStyles}>
                  {UPDATEMEDICALRECORDCONSTANS.UPDATE_RECORD_BUTTON_TEXT}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const renderTitle = () => {
  return (
    <View style={styles.MedicalDocumentsHeader}>
      <Text style={styles.MedicalDocumentsHeaderText}>
        {UPDATEMEDICALRECORDCONSTANS.UPDATE_MEDICAL_RECORD_NAVBAR_HEADING}
      </Text>
    </View>
  );
};
