import { StyleSheet, Platform, PixelRatio } from "react-native";
import { Fonts, Colors } from "../../../../../../Themes/";

export default StyleSheet.create({
  MedicalDocumentsHeader: {
    justifyContent: "center",
    alignItems: "center"
  },
  MedicalDocumentsHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  mainContainer: {
    backgroundColor: Colors.whiteColor,
    paddingHorizontal: 20,
    flex: 1,
    borderColor: Colors.lightblack
  },
  formContainer: {
    borderWidth: 0.5,
    borderRadius: 5,
    marginTop: 20
  },
  medicalDocumentsInputContainer: {
    borderColor: Colors.lightblack,
    backgroundColor: Colors.whiteColor,
    borderRadius: 5
  },
  fieldHeading: {
    marginTop: "4%",
    marginLeft: 15,
    fontFamily: Fonts.family.fontFamilyBold
  },
  underline: {
    borderColor: Colors.lightblack,
    borderBottomWidth: 0.5
  },
  medicalDocumentsInputField: {
    color: Colors.lightblack,
    marginLeft: 20,
    height: 50,
    fontFamily: Fonts.family.fontfamily
  },
  pickerContainer: {
    borderColor: Colors.lightblack
  },
  pickerContentStyles: {
    width: "90%",
    marginRight: 20,
    marginLeft: 16,
    borderWidth: 1,
    borderColor: Colors.lightblack
  },

  dateInputContainer: {
    borderColor: Colors.lightblack,
    // borderWidth: 1,
    borderRadius: 5,
    backgroundColor: Colors.whiteColor
  },

  dateInputField: {
    color: Colors.lightblack,
    marginLeft: 20,
    height: 50,
    fontFamily: Fonts.family.fontfamily
  },

  attachButtonsMainContainer: {
    marginTop: 30,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  btnContentStyles: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    height: 40,
    borderRadius: 40,
    backgroundColor: Colors.primaryColor,
    width: 130
  },
  btnTextStyles: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.small,
    fontFamily: Fonts.family.fontfamily
  },

  imageContainer: {
    alignItems: "center",
    marginTop: 10,
    elevation: 2,
    width: 50,
    backgroundColor: Colors.blackColor
  },
  avatarContainer: {
    borderColor: Colors.lightblack,
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center"
  },
  attachedImage: {
    width: 50,
    height: 50
  }
});
