import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  Platform,
  Picker
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";
import { observable } from "mobx";
import { observer } from "mobx-react";
import moment from "moment";
import Loader from "../../../../../../Components/Loader";
import ImagePicker from "react-native-image-picker";
import DateTimePicker from "react-native-modal-datetime-picker";
// import Stores
import PatientProfileStore from "../../../../../../stores/PatientProfileStore";
import AuthStore from "../../../../../../stores/AuthStore";
import Images from "../../../../../../Themes/Images";
// Styles
import styles from "./styles";

//Import Util fns
import { uploadDocumentDetails } from "./Utils";
import MedicalRecordsWrapper from "../MedicalRecordsWrapper";
import { ADDMEDICALDOCUMENTCONSTANTS } from "../../../../../../Constants/Patient";
@observer
export default class AddMedicalDocument extends Component {
  constructor(props) {
    super(props);
    this.state = {
      others: false,
      documentName: "",
      documentType: "MRI",
      documentType2: "",
      documentEntityType: "PATIENT",
      documentEntityName: AuthStore.loginResponse.name,
      isDateTimePickerVisible: false,
      selectedDate: moment(new Date()).format("YYYY-MM-DD"),
      documentEntityId: AuthStore.loginResponse.patient_id,
      imagePickerResponse: null,
      imgSource: "NOIMAGE",
      requiredDocumentName: null
    };
  }
  static onEnter = () => {
    NavigationActions.refresh({
      renderTitle: renderTitle
    });
  };
  static onExit = () => {
    NavigationActions.PatientProfileHome({
      activeComponent: MedicalRecordsWrapper,
      activeComponentText: "Medical Records"
    });
  };
  componentWillMount() {
    this.setDocumentEntityName();
  }

  handleDocumentName = value => {
    this.setState({ requiredDocumentName: null });
    this.setState({ documentName: value });
    if (!value) {
      this.setState({ requiredDocumentName: "*" });
    }
  };
  handleDocumentValidation = () => {
    if (this.state.documentName === "") {
      alert(ADDMEDICALDOCUMENTCONSTANTS.ADD_DOCUMENT_NAME_REQUIRED_TEXT);
    }
    if (this.state.selectedDate === "") {
      alert(ADDMEDICALDOCUMENTCONSTANTS.ADD_DOCUMENT_DATE_REQUIRED_TEXT);
    }
    if (this.state.imgSource === "NOIMAGE") {
      alert(ADDMEDICALDOCUMENTCONSTANTS.IMAGE_REQUIRED_TEXT);
    }
  };
  handleAddDocument = () => {
    this.handleDocumentValidation();
    if (
      this.state.documentName != "" &&
      this.state.selectedDate != "" &&
      this.state.imgSource != "NOIMAGE"
    ) {
      let documentType = this.state.documentType;
      if (this.state.documentType === "Others") {
        documentType = this.state.documentType2;
      }
      uploadDocumentDetails(
        this.state.documentName,
        documentType,
        this.state.documentEntityType,
        this.state.documentEntityId,
        this.state.selectedDate,
        this.state.imagePickerResponse,
        this.state.imgSource,
        this.resetDocumentDetails
      );
    }
  };
  resetDocumentDetails = () => {
    this.setState({
      documentName: "",
      documentType: "MRI",
      documentEntityType: "PATIENT",
      documentEntityId: AuthStore.loginResponse.patient_id,
      imagePickerResponse: null,
      imgSource: "NOIMAGE",
      selectedDate: moment(new Date()).format("YYYY-MM-DD")
    });

    NavigationActions.PatientProfileHome({
      activeComponent: MedicalRecordsWrapper,
      activeComponentText: "Medical Records",
      type: "replace"
    });
  };
  handleDocumentType = option => {
    if (option === "Others") {
      this.setState({
        documentType: option,
        others: true
      });
    } else {
      this.setState({ others: false, documentType: option });
    }
  };
  setDocumentEntityName = () => {
    if (PatientProfileStore.selectedPerson === "Child") {
      AuthStore.loginResponse.children.map(child => {
        if (child.child_id === PatientProfileStore.selectedChild) {
          this.setState({
            documentEntityName: child.name,
            documentEntityType: "CHILD",
            documentEntityId: child.child_id
          });
        }
      });
    }
  };
  handleDocumentEntityType = option => {
    let entityId = this.state.documentEntityId;
    let entityType = this.state.documentEntityType;

    if (option === AuthStore.loginResponse.name) {
      entityId = AuthStore.loginResponse.patient_id;
      entityType = "PATIENT";
      PatientProfileStore.storeSelectedValues(0, "Parent");
    }

    if (AuthStore.loginResponse.children.length > 0) {
      AuthStore.loginResponse.children.map(child => {
        if (option === child.name) {
          entityId = child.child_id;
          entityType = "CHILD";
          PatientProfileStore.storeSelectedValues(child.child_id, "Child");
        }
      });
    }
    this.setState({
      documentEntityName: option,
      documentEntityType: entityType,
      documentEntityId: entityId
    });
  };

  handleAttachFile = () => {
    const options = {
      title: "Select Image",
      customButtons: [],
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      if (__DEV__) console.log("Response initial", response);

      if (response.didCancel) {
        // if (__DEV__) console.log("User cancelled image picker")
      } else if (response.error) {
        // if (__DEV__) console.log("ImagePicker Error: ", response.error)
      } else if (response.customButton) {
        // if (__DEV__)
        //   console.log("User tapped custom button: ", response.customButton)
      } else {
        this.setState({
          imagePickerResponse: response,
          imgSource: response.uri
        });
      }
    });
  };
  toggleDateTimePicker = () => {
    this.setState({
      isDateTimePickerVisible: !this.state.isDateTimePickerVisible
    });
    this.datePickerInput.blur();
  };
  handleDatePicked = date => {
    this.setState({ selectedDate: moment(date).format("YYYY-MM-DD") });
    this.toggleDateTimePicker();
  };
  handleDocumentType2 = value => {
    this.setState({
      documentType2: value
    });
  };
  render() {
    const submitBtnStyles = () => {
      return this.state.documentName != "" &&
        this.state.selectedDate != "" &&
        this.state.imgSource != "NOIMAGE"
        ? [styles.submitBtnContentStyles, styles.submitBtnActiveColor]
        : [styles.submitBtnContentStyles];
    };
    return (
      <View style={styles.mainContainer}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.Formfield}>
            <DateTimePicker
              mode="date"
              isVisible={this.state.isDateTimePickerVisible}
              maximumDate={new Date()}
              onConfirm={this.handleDatePicked}
              onCancel={this.toggleDateTimePicker}
            />
            {this.state.documentName ? (
              <Text style={styles.fieldHeading}>
                {ADDMEDICALDOCUMENTCONSTANTS.MEDICAL_RECORD_NAME_FIELD_TEXT}
              </Text>
            ) : null}
            <View style={styles.medicalDocumentsInputContainer}>
              <View style={styles.textInputContainer}>
                <TextInput
                  returnKeyType={"next"}
                  autoCorrect={false}
                  style={styles.medicalDocumentsInputField}
                  placeholder={
                    ADDMEDICALDOCUMENTCONSTANTS.MEDICAL_RECORD_NAME_FIELD_PLACEHOLDER
                  }
                  underlineColorAndroid="transparent"
                  onChangeText={this.handleDocumentName}
                  value={this.state.documentName}
                />
              </View>
              {this.state.requiredDocumentName && (
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>
              )}
            </View>
            <View style={styles.underline} />
            {/* <View style={styles.pickerMainContainer}> */}
            {this.state.documentType ? (
              <Text style={styles.fieldHeading}>
                {ADDMEDICALDOCUMENTCONSTANTS.MEDICAL_REPORT_FIELD_TEXT}{" "}
              </Text>
            ) : null}
            <Picker
              selectedValue={this.state.documentType}
              style={styles.pickerContentStyles}
              onValueChange={option => this.handleDocumentType(option)}
            >
              <Picker.Item label="Blood Test" value="Blood Test" />
              <Picker.Item label="Urine Test" value="Urine Test" />
              <Picker.Item label="X-Ray" value="X-Ray" />
              <Picker.Item label="MRI" value="MRI" />
              <Picker.Item label="CT Scan" value="CT Scan" />
              <Picker.Item label="ECG" value="ECG" />
              <Picker.Item label="Ultra-Sonography" value="Ultra-Sonography" />
              <Picker.Item label="Eye Test" value="Eye Test" />
              <Picker.Item label="Audiometry (ENT)" value="Audiometry (ENT)" />
              <Picker.Item label="Others" value="Others" />
            </Picker>
            {this.state.others ? (
              <View style={{ marginLeft: 20 }}>
                <TextInput
                  returnKeyType={"next"}
                  autoCorrect={false}
                  style={styles.medicalDocumentsInputField}
                  placeholder={"Enter your option"}
                  underlineColorAndroid="transparent"
                  onChangeText={this.handleDocumentType2}
                  value={this.state.documentType2}
                />
              </View>
            ) : null}
            <View style={styles.underline} />
            {this.state.documentEntityName ? (
              <Text style={styles.fieldHeading}>
                {ADDMEDICALDOCUMENTCONSTANTS.AUTHOR_FIELD_TEXT}
              </Text>
            ) : null}
            <Picker
              selectedValue={this.state.documentEntityName}
              style={styles.pickerContentStyles}
              onValueChange={option => this.handleDocumentEntityType(option)}
            >
              <Picker.Item
                label={AuthStore.loginResponse.name}
                value={AuthStore.loginResponse.name}
              />

              {AuthStore.loginResponse.children.map(child => {
                return (
                  <Picker.Item
                    label={child.name}
                    value={child.name}
                    key={child.name}
                  />
                );
              })}
            </Picker>
            {/* </View> */}
            <View style={styles.underline} />
            {this.state.selectedDate ? (
              <Text style={styles.fieldHeading}>
                {ADDMEDICALDOCUMENTCONSTANTS.DATE_FIELD_TEXT}
              </Text>
            ) : null}
            <View style={styles.dateInputContainer}>
              <TextInput
                autoCorrect={false}
                ref={e => (this.datePickerInput = e)}
                style={styles.dateInputField}
                placeholder={ADDMEDICALDOCUMENTCONSTANTS.DATE_FIELD_PLACEHOLDER}
                underlineColorAndroid="transparent"
                onFocus={() => this.toggleDateTimePicker()}
                value={this.state.selectedDate}
              />
            </View>
          </View>
          {this.state.imgSource != "NOIMAGE" ? (
            <TouchableOpacity
              key={`${this.state.imgSource}`}
              activeOpacity={1}
              style={styles.imageRecordContainer}
            >
              <Image
                source={{ uri: this.state.imgSource }}
                style={styles.attachedImage}
              />
            </TouchableOpacity>
          ) : null}
          <View style={styles.attachButtonsMainContainer}>
            <TouchableOpacity onPress={() => this.handleAttachFile()}>
              <View style={styles.btnContentStyles}>
                <Text style={styles.btnTextStyles}>
                  {ADDMEDICALDOCUMENTCONSTANTS.ATTACH_FILES_FIELD_TEXT}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.handleAddDocument()}>
              <View style={submitBtnStyles()}>
                <Text style={styles.btnTextStyles}>
                  {ADDMEDICALDOCUMENTCONSTANTS.ADD_RECORD_FIELD_TEXT}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const renderTitle = () => {
  return (
    <View style={styles.MedicalDocumentsHeader}>
      <Text style={styles.MedicalDocumentsHeaderText}>
        {ADDMEDICALDOCUMENTCONSTANTS.ADD_MEDICAL_RECORD_NAVBAR_HEADING}
      </Text>
    </View>
  );
};
