import { StyleSheet, Platform, PixelRatio } from "react-native";
import { Fonts, Colors } from "../../../../../../Themes";

export default StyleSheet.create({
  MedicalDocumentsHeader: {
    justifyContent: "center",
    alignItems: "center"
  },
  MedicalDocumentsHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.whiteColor
  },
  Formfield: {
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: Colors.lightblack,
    elevation: 1,
    marginLeft: 20,
    marginRight: 20,
    shadowOffset: { width: 10, height: 10 },
    marginTop: 20
  },
  medicalDocumentsInputContainer: {
    // borderColor: Colors.lightblack,
    // backgroundColor: Colors.whiteColor,
    // paddingLeft: 20,
    // borderRadius: 5,
    // justifyContent: "center"
    //
    // borderColor: Colors.lightblack,
    backgroundColor: Colors.whiteColor,
    paddingLeft: 20,
    borderRadius: 5,
    marginTop: 10,
    flexDirection: "row"
  },
  medicalDocumentsInputField: {
    color: Colors.blackColor,
    borderRadius: 5,
    fontFamily: Fonts.family.fontfamily
  },
  fieldHeading: {
    marginTop: "4%",
    marginLeft: 15,
    fontFamily: Fonts.family.fontFamilyBold
  },
  textInputContainer: {
    flex: 10
  },
  pickerMainContainer: {
    flexDirection: "column",
    borderRadius: 5
  },
  underline: {
    borderBottomWidth: 0.8,
    borderColor: Colors.lightblack
  },
  pickerContentStyles: {
    width: "95%",
    marginLeft: 18,
    marginRight: 20
  },
  pickeritem: {
    fontFamily: Fonts.family.fontfamily
  },
  dateInputContainer: {
    borderColor: Colors.lightblack,
    marginTop: 10,
    borderRadius: 5,
    backgroundColor: Colors.whiteColor
  },
  ImageContainer: {
    paddingRight: 10,
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  imageRecordContainer: {
    marginLeft: 22,
    marginTop: 10
  },
  RequiredField: {
    width: 20,
    height: 20
  },
  dateInputField: {
    color: Colors.lightblack,
    marginLeft: 22,
    height: 50,
    fontFamily: Fonts.family.fontfamily
  },

  attachButtonsMainContainer: {
    marginTop: 30,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  btnContentStyles: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    height: 40,
    borderRadius: 40,
    backgroundColor: Colors.primaryColor,
    width: 140
  },
  submitBtnContentStyles: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    height: 40,
    borderRadius: 40,
    backgroundColor: Colors.lightblack,
    width: 140
  },
  submitBtnActiveColor: {
    backgroundColor: Colors.primaryColor
  },
  btnTextStyles: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.small,
    fontFamily: Fonts.family.fontfamily
  },

  avatarContainer: {
    borderColor: Colors.lightblack,
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center"
  },
  attachedImage: {
    width: 50,
    height: 50
  }
});
