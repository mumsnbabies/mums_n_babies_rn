// import Stores
import PatientProfileStore from "../../../../../../stores/PatientProfileStore";
import { Platform } from "react-native";
//import Utils
import AWSS3Utility from "../../../../../../Utils/S3Utils/AWSS3Utility";
import S3Config from "../../../../../../Utils/S3Utils/S3Configs";
import { Generate64LengthString } from "../../../../../../Utils/UtilsFun";

export const uploadDocumentDetails = (
  documentName,
  documentType,
  entityType,
  entityId,
  selectedDate,
  imagePickerResponse,
  imageSource,
  documentResetCallback
) => {
  const awsUploader = new AWSS3Utility(
    S3Config.PatientBucketConfig,
    () => {},
    () => {}
  );
  if (imageSource === "NOIMAGE") {
    uploadMedicalDocumentWithOutImage(
      documentName,
      documentType,
      entityType,
      entityId,
      selectedDate,
      documentResetCallback
    );
    return;
  }
  if (imagePickerResponse != null) {
    let generatedFileName = "";
    if (imagePickerResponse.fileName) {
      if (Platform.OS === "ios") {
        let splittedValues = imagePickerResponse.fileName.split(".");
        generatedFileName = Generate64LengthString().concat(
          splittedValues[0] + "." + splittedValues[1].toLowerCase()
        );
      } else {
        generatedFileName = Generate64LengthString().concat(
          imagePickerResponse.fileName
        );
      }
    } else {
      generatedFileName = Generate64LengthString().concat(
        "_medical_record.jpg"
      );
    }
    let changedResponse = {
      fileName: generatedFileName
    };
    const updatedResponse = Object.assign(
      {},
      imagePickerResponse,
      changedResponse
    );
    let options = {
      Bucket: S3Config.PatientBucketConfig.bucketName,
      Key: `alpha/medicalDocumentsPictures/${updatedResponse.fileName}`,
      ContentType: updatedResponse.type,
      ACL: "public-read",
      subscribe: true,
      completionhandler: true,
      path: updatedResponse.path
    };
    if (Platform.OS === "ios") {
      options["ContentType"] = updatedResponse.fileName.split(".")[1];
      options["path"] = updatedResponse.uri;
    }
    awsUploader.uploadImageFromURI(options, () =>
      uploadImageSuccess(
        documentName,
        documentType,
        entityType,
        entityId,
        selectedDate,
        updatedResponse,
        documentResetCallback
      )
    );
  }
};

export const uploadMedicalDocumentWithOutImage = (
  documentName,
  documentType,
  entityType,
  entityId,
  selectedDate,
  documentResetCallback
) => {
  let requestObject = {
    entity_id: entityId,
    description: documentName,
    entity_type: entityType,
    doctor_access_ids: [],
    document_url: "NOIMAGE",
    document_type: documentType,
    document_date: selectedDate
  };
  PatientProfileStore.createPatientDocumentAPI(requestObject, () =>
    documentResetCallback()
  );
};

export const uploadImageSuccess = (
  documentName,
  documentType,
  entityType,
  entityId,
  selectedDate,
  imagePickerResponse,
  documentResetCallback
) => {
  const { fileName } = imagePickerResponse;
  //Ncall
  let documentUrl = `https://s3.ap-south-1.amazonaws.com/mnb-backend-media-static-mumbai/alpha/medicalDocumentsPictures/${fileName}`;
  let requestObject = {
    entity_id: entityId,
    description: documentName,
    entity_type: entityType,
    doctor_access_ids: [],
    document_url: documentUrl,
    document_type: documentType,
    document_date: selectedDate
  };
  PatientProfileStore.createPatientDocumentAPI(requestObject, () =>
    documentResetCallback()
  );
};
