import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  Platform,
  Picker
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { observable, toJS } from "mobx";
import { observer } from "mobx-react";
import moment from "moment";
import Loader from "../../../../../../Components/Loader";
import DateTimePicker from "react-native-modal-datetime-picker";
import Images from "../../../../../../Themes/Images";
import { Colors } from "../../../../../../Themes";
import {
  API_INITIAL,
  API_FETCHING,
  API_SUCCESS,
  INIT,
  API_PAGINATION_FINISHED
} from "../../../../../../Utils/AppAPIConstants";

// import Stores
import PatientProfileStore from "../../../../../../stores/PatientProfileStore";
import PatientProfileUpdateStore from "../../../../../../stores/PatientProfileUpdateStore";
import AuthStore from "../../../../../../stores/AuthStore";

// Styles
import styles from "./styles";

//Import components
import Dashboard from "../Dashboard";
import { ADDNEWATTRIBUTECONSTANTS } from "../../../../../../Constants/Patient";

//import genericcomponents
import FormFactory from "../../../../../../GenericComponents";
import FormFormatter from "../../../../../../GenericComponents/Utils/FormFormatter";

@observer
export default class AddNewAttribute extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recordEntityId: this.props.entityId,
      recordEntityType: this.props.entityType,
      selectedValuePicker: "SELECT_RECORD_TYPE"

      /**
       * recordName: "",
      recordNameErrorText: false,
      currentValue: "",
      recordCurrentValueErrorText: false,
      minValue: "",
      maxValue: ""
       */
    };
  }
  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle,
      onBack: this.onBack
    });

    PatientProfileStore.getDefaultAttributesFormAPI();
  }
  onBack = () => {
    // NavigationActions.PatientProfileHome({
    //   activeComponent: Dashboard,
    //   activeComponentText: "Dashboard",
    //   type: "replace"
    // })
    NavigationActions.pop();
  };
  renderTitle = () => {
    return (
      <View style={styles.MedicalDocumentsHeader}>
        <Text style={styles.MedicalDocumentsHeaderText}>
          {ADDNEWATTRIBUTECONSTANTS.ADD_NEW_ATTRIBUTE_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };

  makeRecordSubmission = () => {
    let requestObject = {
      attributes: [
        {
          attr_key: this.state.recordName.toUpperCase(),
          description: "",
          attr_id: -1,
          attr_value: this.state.currentValue,
          to_add: true
        }
      ],
      entity_id: this.state.recordEntityId,
      activity_required: true,
      entity_type: this.state.recordEntityType
    };

    PatientProfileStore.createPatientAttributeAPI(
      requestObject,
      this.resetDocumentDetails
    );
  };

  makeLimitsSubmission = () => {
    let requestObject = {
      entity_id: this.state.recordEntityId,
      thresholds: [
        {
          attr_key: this.state.recordName,
          min_value: this.state.minValue,
          max_value: this.state.maxValue
        }
      ],
      entity_type: this.state.recordEntityType
    };

    PatientProfileStore.setMaxMinPatientAttributeAPI(
      requestObject,
      this.resetDocumentDetails
    );
  };
  handleCreateRecord = () => {
    this.handleRecordValidation();
    if (this.state.recordName != "" && this.state.currentValue != "") {
      this.makeRecordSubmission();
    }
    if (this.state.maxValue != "" && this.state.minValue != "") {
      this.makeLimitsSubmission();
    }
  };
  resetDocumentDetails = () => {
    this.setState({
      selectedValuePicker: "SELECT_RECORD_TYPE",
      recordEntityId: "",
      recordEntityType: ""

      /**
       * currentValue: "",
      minValue: "",
      maxValue: "",
      recordName: "",
      recordNameErrorText: false,
      recordCurrentValueErrorText: false
       */
    });

    // NavigationActions.PatientProfileHome({
    //   activeComponent: Dashboard,
    //   activeComponentText: "Dashboard",
    //   type: "replace"
    // })
    let requestObject = {
      entity_id: this.props.entityId,
      entity_type: this.props.entityType
    };
    let request = {
      patient_id: AuthStore.loginResponse.patient_id
    };
    PatientProfileStore.getPatientProfile(request);
    PatientProfileUpdateStore.resetPeriodicalAttributes();
    PatientProfileUpdateStore.getPeriodicalAttributes(requestObject);
    NavigationActions.pop();
  };

  createParameterRequestObject = parameterField => {
    let formattedAttributedValues = [];
    parameterField.parameterFieldList.map(field => {
      formattedAttributedValues.push({
        value_key: field.name.toUpperCase(),
        value: field.value
      });
    });
    let finalObject = {
      attr_key: parameterField.name.toUpperCase(),
      description: parameterField.unit,
      attr_values: formattedAttributedValues,
      to_add: true
    };
    return finalObject;
  };
  createFieldRequestObject = field => {
    let formattedAttributedrequestObject = {
      attr_key: field.name.toUpperCase(),
      description: field.unit,
      attr_values: [
        {
          value_key: field.name.toUpperCase(),
          value: field.value
        }
      ],
      to_add: true
    };
    return formattedAttributedrequestObject;
  };
  onSubmit = formFields => {
    if (this.state.selectedValuePicker === "SELECT_RECORD_TYPE") {
      alert("Please select Record Type");
      return;
    }
    //Validation
    let validateAllFields = true;
    formFields.map(formField => {
      if (formField.type === "PARAMETER") {
        formField.parameterFieldList.map(parameterField => {
          if (parameterField.isError || parameterField.value === null) {
            validateAllFields = false;
          }
        });
      }
      if (formField.type === "FIELD") {
        if (formField.isError || formField.value === null) {
          validateAllFields = false;
        }
      }
    });
    if (!validateAllFields) {
      alert("Please Fill All Details");
      return;
    }

    let parameterRequestObjects = [];
    let fieldRequestObjects = [];
    formFields.map(formField => {
      if (formField.type === "PARAMETER") {
        let formattedObject = this.createParameterRequestObject(formField);
        parameterRequestObjects.push(formattedObject);
      }
      if (formField.type === "FIELD") {
        let formattedObject = this.createFieldRequestObject(formField);
        fieldRequestObjects.push(formattedObject);
      }
    });

    let requestObject = {
      attributes: [...fieldRequestObjects, ...parameterRequestObjects],
      entity_id: this.state.recordEntityId,
      activity_required: true,
      entity_type: this.state.recordEntityType
    };

    if (__DEV__) {
      console.log(requestObject);
    }

    PatientProfileStore.createPatientAttributeAPI(
      requestObject,
      this.resetDocumentDetails
    );

    //Make the network call
  };

  renderPickerOptions = () => {
    let formattedFormObjectArray = {};
    // if (__DEV__) {
    //   console.log(toJS(PatientProfileUpdateStore.periodicalAttributes))
    // }
    let addedAttributes = [];
    PatientProfileUpdateStore.periodicalAttributes.map(
      (eachAttribute, index) => {
        addedAttributes.push(eachAttribute.attr_key);
      }
    );
    // if (__DEV__) {
    //   console.log(addedAttributes)
    // }
    let array = PatientProfileStore.formAPIResponse.slice();
    formattedFormObjectArray = array.filter(formObject => {
      return (
        formObject.name != "ALL_FIELDS" &&
        addedAttributes.indexOf(formObject.name) < 0
      );
    });

    return formattedFormObjectArray.map(formObject => {
      return (
        <Picker.Item
          label={formObject.label}
          value={formObject.name}
          key={formObject.name}
        />
      );
    });
  };

  handleRecordType = option => {
    this.setState({
      selectedValuePicker: option
    });
    PatientProfileUpdateStore.storeRecord(option);
  };

  formattedForm = () => {
    if (this.state.selectedValuePicker === "SELECT_RECORD_TYPE") {
      return {};
    }
    let formattedFormObject = {};
    let array = PatientProfileStore.formAPIResponse.slice();
    array.map(formObject => {
      if (this.state.selectedValuePicker === formObject.name) {
        formattedFormObject = JSON.parse(formObject.form_json);
      }
    });
    return formattedFormObject;
  };
  render() {
    // const submitBtnStyles = () => {
    //   return this.state.recordName != "" && this.state.currentValue != ""
    //     ? [styles.submitBtnContentStyles, styles.submitBtnActiveColor]
    //     : [styles.submitBtnContentStyles]
    // }
    if (PatientProfileStore.formAPIResponse.slice().length > 0) {
      return (
        <ScrollView style={styles.mainContainer}>
          <KeyboardAwareScrollView extraHeight={200}>
            <View style={styles.pickerContainer}>
              <Picker
                selectedValue={this.state.selectedValuePicker}
                style={styles.pickerContentStyles}
                onValueChange={option => this.handleRecordType(option)}
              >
                <Picker.Item
                  label="SELECT RECORD TYPE"
                  value="SELECT_RECORD_TYPE"
                />
                {this.renderPickerOptions()}
              </Picker>
            </View>
            <FormFactory
              specFile={this.formattedForm()}
              onSubmit={this.onSubmit}
            />
          </KeyboardAwareScrollView>
        </ScrollView>
      );
    }
    return (
      <View style={styles.loaderContainer}>
        <Loader animating={true} color={Colors.primaryColor} />
      </View>
    );
  }
}

/**
 *        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.formcontainer}>
            {this.state.recordName
              ? <Text style={styles.UpdateProfileTextField}>{ADDNEWATTRIBUTECONSTANTS.RECORD_NAME_FIELD_TEXT}</Text>
              : null}
            <View style={styles.inputFieldContainer}>
              <View style={styles.textInputContainer}>
                <TextInput
                  autoCorrect={false}
                  style={styles.inputField}
                  placeholder={ADDNEWATTRIBUTECONSTANTS.RECORD_NAME_FIELD_PLACEHOLDER}
                  underlineColorAndroid="transparent"
                  onChangeText={this.handleRecordName}
                  value={this.state.recordName}
                />
              </View>
              {this.state.recordNameErrorText &&
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>}
            </View>
            {this.state.currentValue
              ? <Text style={styles.UpdateProfileTextField}>{ADDNEWATTRIBUTECONSTANTS.CURRENT_VALUE_FIELD_TEXT}</Text>
              : null}
            <View style={styles.inputFieldContainer}>
              <View style={styles.textInputContainer}>
                <TextInput
                  autoCorrect={false}
                  style={styles.inputField}
                  placeholder={ADDNEWATTRIBUTECONSTANTS.CURRENT_VALUE_FIELD_PLACEHOLDER}
                  underlineColorAndroid="transparent"
                  onChangeText={this.handleCurrentValue}
                  value={this.state.currentValue}
                />
              </View>
              {this.state.recordCurrentValueErrorText &&
                <View style={styles.ImageContainer}>
                  <Image
                    style={styles.RequiredField}
                    source={Images.alertShield}
                  />
                </View>}
            </View>
            {this.state.maxValue
              ? <Text style={styles.UpdateProfileTextField}>{ADDNEWATTRIBUTECONSTANTS.MAX_VALUE_FIELD_TEXT}</Text>
              : null}
            <View style={styles.normalInputFieldContainer}>
              <TextInput
                autoCorrect={false}
                style={styles.inputField}
                placeholder={ADDNEWATTRIBUTECONSTANTS.MAX_VALUE_FIELD_PLACEHOLDER}
                underlineColorAndroid="transparent"
                onChangeText={this.handleMaxValue}
                value={this.state.maxValue}
              />
            </View>
            {this.state.minValue
              ? <Text style={styles.UpdateProfileTextField}>{ADDNEWATTRIBUTECONSTANTS.MIN_VALUE_FIELD_TEXT}</Text>
              : null}
            <View style={styles.normalInputFieldContainer}>
              <TextInput
                autoCorrect={false}
                style={styles.inputField1}
                placeholder={ADDNEWATTRIBUTECONSTANTS.MIN_VALUE_FIELD_PLACEHOLDER}
                underlineColorAndroid="transparent"
                onChangeText={this.handleMinValue}
                value={this.state.minValue}
              />
            </View>
          </View>

          <View style={styles.buttonsContainer}>
            <TouchableOpacity onPress={() => this.handleCreateRecord()}>
              <View style={submitBtnStyles()}>
                <Text style={styles.btnTextStyles}>{ADDNEWATTRIBUTECONSTANTS.CREATE_RECORD_BUTTON_TEXT}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
 */

/**
 * handleRecordName = value => {
    if (value === "") {
      this.setState({ recordNameErrorText: true, recordName: value })
    } else {
      this.setState({ recordName: value, recordNameErrorText: false })
    }
  }
  handleCurrentValue = value => {
    if (value === "") {
      this.setState({ recordCurrentValueErrorText: true, currentValue: value })
    } else {
      this.setState({
        currentValue: value,
        recordCurrentValueErrorText: false
      })
    }
  }

  handleMaxValue = value => {
    this.setState({ maxValue: value })
  }

  handleMinValue = value => {
    this.setState({ minValue: value })
  }
  handleRecordValidation = () => {
    if (this.state.recordName === "") {
      alert(ADDNEWATTRIBUTECONSTANTS.RECORD_NAME_REQUIRED_TEXT)
      return
    }
    let recordNameRegex = /^[a-zA-Z]*$/
    if (!recordNameRegex.test(this.state.recordName)) {
      alert(ADDNEWATTRIBUTECONSTANTS.RECORD_NAME_REQUIRED_SHOULD_TEXT)
      return
    }

    if (this.state.currentValue === "") {
      alert(ADDNEWATTRIBUTECONSTANTS.CURRENT_VALUE_REQUIRED_TEXT)
      return
    }

    let currentValueRegex = /^\d*$/
    if (!currentValueRegex.test(this.state.recordCurrentValue)) {
      alert(ADDNEWATTRIBUTECONSTANTS.CURRENT_VALUE_SHOULD_BE_NUMBER_TEXT)
      return
    }
  }
 */
