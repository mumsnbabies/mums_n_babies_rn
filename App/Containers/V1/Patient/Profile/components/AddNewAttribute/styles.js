import { StyleSheet, Platform, PixelRatio } from "react-native";
import { Fonts, Colors } from "../../../../../../Themes";

export default StyleSheet.create({
  MedicalDocumentsHeader: {
    justifyContent: "center",
    alignItems: "center"
  },
  MedicalDocumentsHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  mainContainer: {
    backgroundColor: Colors.whiteColor,
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1
  },
  UpdateProfileTextField: {
    marginTop: "2%",
    marginLeft: 15,
    color: Colors.lightblack,
    fontFamily: Fonts.family.fontFamilyBold
  },
  formcontainer: {
    borderColor: Colors.lightblack,
    borderRadius: 5,
    borderWidth: 0.7,
    marginTop: 20,
    elevation: 1
  },
  eachfieldhead: {
    marginLeft: 22,
    fontFamily: Fonts.family.fontFamilyBold
  },
  inputFieldContainer: {
    borderColor: Colors.lightblack,
    backgroundColor: Colors.whiteColor,
    borderRadius: 5,
    marginTop: 10,
    flexDirection: "row"
  },
  normalInputFieldContainer: {
    borderColor: Colors.lightblack,
    backgroundColor: Colors.whiteColor,
    borderRadius: 5,
    marginTop: 10
  },
  textInputContainer: {
    flex: 10
  },
  inputField: {
    color: Colors.blackColor,
    paddingLeft: 20,
    height: 40,
    borderBottomWidth: 0.8,
    borderColor: Colors.lightblack,
    fontFamily: Fonts.family.fontfamily
  },
  inputField1: {
    color: Colors.blackColor,
    paddingLeft: 20,
    height: 40,
    fontFamily: Fonts.family.fontfamily
  },
  ImageContainer: {
    paddingRight: 10,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    borderBottomWidth: 0.8
  },
  RequiredField: {
    width: 20,
    height: 20
  },
  buttonsContainer: {
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  btnContentStyles: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    width: 120,
    height: 32,
    borderRadius: 10,
    backgroundColor: Colors.secondaryColor
  },
  submitBtnContentStyles: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
    marginBottom: 10,
    width: 220,
    height: 50,
    borderRadius: 40,
    backgroundColor: Colors.lightblack
  },
  submitBtnActiveColor: {
    backgroundColor: Colors.primaryColor
  },
  btnTextStyles: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.family.fontfamily
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  pickerContentStyles: {
    color: Colors.lightblack,
    width: "95%",
    marginLeft: 18,
    marginRight: 20
  }
});
