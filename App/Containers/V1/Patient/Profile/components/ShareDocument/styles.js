import { StyleSheet, Platform, PixelRatio } from "react-native";
import { Fonts, Colors } from "../../../../../../Themes";

export default StyleSheet.create({
  navBarTitle: {
    alignItems: "center",
    justifyContent: "center"
  },
  navBarTitleText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.whiteColor
  },

  searchFieldContainer: {
    color: Colors.lightblack,
    marginTop: 10,
    marginBottom: 20,
    height: 40,
    fontFamily: Fonts.family.fontfamily,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10
  },
  searchInputStyles: {
    borderColor: Colors.lightblack,
    paddingLeft: 10,
    borderWidth: 0.5,
    borderRadius: 5
  },
  footerButtonContainer: {
    alignItems: "center",
    justifyContent: "center",
    height: 60,
    elevation: 2,
    backgroundColor: Colors.requestButtom
  },
  btnTextStyles: {
    fontFamily: Fonts.family.fontFamilyBold,
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.smallheading
  }
});
