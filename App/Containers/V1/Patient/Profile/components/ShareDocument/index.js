import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableHighlight,
  TouchableOpacity,
  Platform
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";
import { observable } from "mobx";
import { observer } from "mobx-react";
import _ from "lodash";

//import common components
import Loader from "../../../../../../Components/Loader";

// Import Feature Components

import ConnectedList from "./GenericPersonCard";

// import Stores
import PatientProfileStore from "../../../../../../stores/PatientProfileStore";
import PatientRequestsStore from "../../../../../../stores/PatientRequestsStore";
import AuthStore from "../../../../../../stores/AuthStore";
import DoctorProfileStore from "../../../../../../stores/DoctorProfileStore";

// Styles
import styles from "./styles";

//Import Util fns
import { handleShareDocumentPress } from "./Utils";

@observer
export default class ShareMedicalDocument extends Component {
  @observable searchDoctorName: String;
  @observable doctorId: Number;
  @observable selectedDoctorsList;
  constructor(props) {
    super(props);
    this.searchDoctorName = "";
    this.showComponentOn = _.debounce(this.showComponentOn, 300);
    this.selectedDoctorsList = new Set();
  }
  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  renderTitle = () => {
    return (
      <View style={styles.navBarTitle}>
        <Text style={styles.navBarTitleText}>
          {SHAREDOCUMENTCONSTANS.SHARE_DOCUMENT_NAVBAR_HEADING}
        </Text>
      </View>
    );
  };
  componentDidMount() {
    this.getConnectedDoctorsList();
  }
  getConnectedDoctorsList = (searchDoctorName = "") => {
    let requestObject = {
      search_q: searchDoctorName,
      limit: 20,
      relation_status: "ACCEPTED",
      offset: 0
    };
    PatientRequestsStore.getPatientAcceptedRequests(requestObject);
  };

  requestDoctorProfileDetails = (doctor_id, doctor_name) => {
    DoctorProfileStore.getDoctorProfile({}, doctor_id);
    let requestObject1 = {
      limit: 1,
      filters: {
        entity_id: doctor_id,
        entity_type: "DOCTOR_TIMINGS"
      },
      offset: 0
    };
    DoctorProfileStore.getDoctorTiming(requestObject1);
    NavigationActions.PdoctorProfile({ name: doctor_name });
  };
  showComponents = () => {
    if (PatientRequestsStore.getAcceptedRequestsStatus === 200) {
      if (
        PatientRequestsStore.acceptedRequests &&
        PatientRequestsStore.acceptedRequests.doctors
      ) {
        let { doctors } = PatientRequestsStore.acceptedRequests;
        if (__DEV__) console.log(doctors);
        return doctors.map((eachDoctor, index) => {
          let { doctor_id, doctor_name } = eachDoctor;
          return (
            <ConnectedList
              key={index}
              personInstance={eachDoctor}
              onPressFirstButton={id => this.handleSelectedDoctorsList(id)}
              onPressSecondButton={() =>
                this.requestDoctorProfileDetails(doctor_id, doctor_name)
              }
            />
          );
        });
        return null;
      }
    }
  };

  handleSearch = name => {
    this.searchDoctorName = name;
    this.showComponentOn(name);
  };
  showComponentOn = (name = "") => {
    this.getConnectedDoctorsList(name);
  };

  handleSelectedDoctorsList = id => {
    if (this.selectedDoctorsList.has(id)) {
      this.selectedDoctorsList.delete(id);
    } else {
      this.selectedDoctorsList.add(id);
    }
  };

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.searchFieldContainer}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.searchInputStyles}
            placeholder={SHAREDOCUMENTCONSTANS.SEARCH_DOCTOR_PLACEHOLDER_TEXT}
            underlineColorAndroid="transparent"
            onChangeText={this.handleSearch}
            value={this.searchDoctorName}
          />
        </View>
        {PatientRequestsStore.getAcceptedRequestsStatus === 100 ? (
          <Loader animating={true} />
        ) : (
          <ScrollView showsVerticalScrollIndicator={false}>
            {this.showComponents()}
          </ScrollView>
        )}
        <TouchableHighlight
          style={styles.footerButtonContainer}
          onPress={() =>
            this.handleShareDocumentPress(this.selectedDoctorsList)
          }
        >
          <Text style={styles.btnTextStyles}>
            {SHAREDOCUMENTCONSTANS.SHARE_BUTTON_TEXT}
          </Text>
        </TouchableHighlight>
      </View>
    );
  }
}
