import React, { Component } from "react";

import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  AsyncStorage,
  TouchableHighlight,
  Platform,
  StyleSheet
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";
import { Colors, Fonts } from "../../../../../../Themes";

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: Colors.whiteColor,
    margin: 10,
    padding: 10,
    borderRadius: 10,
    shadowOffset: { width: 3, height: 3 },
    shadowColor: Colors.blackColor,
    shadowRadius: 1,
    shadowOpacity: 0.3,
    elevation: 1,
    flexDirection: "row",
    height: 100
  },
  profilePicContainer: {
    flex: 1,
    alignItems: "center"
  },
  profilePic: {
    height: 60,
    width: 60,
    borderWidth: 0.3,
    borderRadius: 30
  },
  bodyContentContainer: {
    marginLeft: 10,
    flex: 4,
    flexDirection: "column"
  },
  profileDetailsContainer: {
    flex: 2
  },
  nameContainer: {
    flex: 1
  },
  nameTextStyles: {
    fontFamily: Fonts.family.fontFamilyBold,
    color: Colors.blackColor,
    fontSize: Fonts.sizes.mediumsmall
  },
  designationContainer: {
    flex: 1
  },
  designationTextStyles: {
    fontFamily: Fonts.family.fontfamily,
    color: Colors.lightblack
  },
  buttonContainer: {
    flexDirection: "row",
    flex: 2,
    marginRight: 30,
    alignItems: "flex-end"
  },
  firstButtonContainer: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    height: 30,
    flex: 1,
    elevation: 1,
    borderWidth: 0.5,
    borderColor: Colors.genericcardborder
  },
  firstButtonTextStyles: {
    color: Colors.requestButtom,
    fontFamily: Fonts.family.fontFamilyBold
  },
  secondButtonContainer: {
    height: 30,
    flex: 1,
    marginLeft: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: Colors.genericcardborder,
    elevation: 1
  },
  secondButtonTextStyles: {
    fontFamily: Fonts.family.fontFamilyBold,
    color: Colors.requestButtom
  }
});

export default class GenericPersonCard extends Component {
  state = {
    firstButtonActiveState: false
  };

  togglefirstButtonActiveState = () => {
    this.setState({
      firstButtonActiveState: !this.state.firstButtonActiveState
    });
  };
  render() {
    let {
      onPressFirstButton,
      onPressSecondButton,
      personInstance
    } = this.props;
    let {
      doctor_name: name,
      doctor_speciality: specialization,
      doctor_Id: doctorId
    } = personInstance;

    const firstButtonColor = () => {
      if (this.state.firstButtonActiveState) {
        return [
          styles.firstButtonContainer,
          { backgroundColor: Colors.requestButtom }
        ];
      }
      return [styles.firstButtonContainer];
    };
    const firstButtonTextColor = () => {
      if (this.state.firstButtonActiveState) {
        return [styles.firstButtonTextStyles, { color: Colors.whiteColor }];
      }
      return [styles.firstButtonTextStyles];
    };
    const firstButtonText = () => {
      if (this.state.firstButtonActiveState) {
        return "UnSelect";
      }
      return "Select";
    };
    return (
      <View style={styles.mainContainer}>
        <View style={styles.profilePicContainer}>
          <Image
            style={styles.profilePic}
            resizemode="contain"
            source={{
              uri:
                "http://sanasassi.mondocteur.tn/uploads/site_image/doctor.jpg"
            }}
          />
        </View>
        <View style={styles.bodyContentContainer}>
          <View style={styles.profileDetailsContainer}>
            <View style={styles.nameContainer}>
              <Text style={styles.nameTextStyles}>
                {name ? name.toUpperCase() : null}
              </Text>
            </View>
            <View style={styles.designationContainer}>
              <Text style={styles.designationTextStyles}>{specialization}</Text>
            </View>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableHighlight
              onPress={() => {
                onPressFirstButton(doctorId);
                this.togglefirstButtonActiveState();
              }}
              style={firstButtonColor()}
            >
              <Text style={firstButtonTextColor()}>{firstButtonText()}</Text>
            </TouchableHighlight>
            <TouchableHighlight
              onPress={() => onPressSecondButton()}
              style={styles.secondButtonContainer}
            >
              <Text style={styles.secondButtonTextStyles}>Open</Text>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    );
  }
}
