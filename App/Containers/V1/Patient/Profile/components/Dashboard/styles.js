import { StyleSheet, Platform } from "react-native";
import { Fonts, Colors } from "../../../../../../Themes";

export default StyleSheet.create({
  PatientProfileHeader: {
    alignItems: "center",
    justifyContent: "center"
  },
  PatientProfileHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  MainScreen: {
    flex: 1,
    backgroundColor: Colors.primaryColor,
    position: "relative"
  },
  DashboardMainView: {
    marginLeft: 30
  },
  PatientProfileDashboardText: {
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.large,
    fontFamily: Fonts.family.fontfamily
  },
  PatientProfileDashboardView: {
    flexDirection: "row",
    // flex: 1,
    marginLeft: 30,
    marginTop: 10,
    marginRight: 30,
    marginBottom: 10,
    height: 30
  },
  ActiveDashboardProfile: {
    flex: 1,
    backgroundColor: Colors.selectedTab,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 40,
    borderWidth: 0.7,
    borderColor: Colors.selectedTab,
    marginRight: 5
  },
  ActiveDashboardProfileText: {
    fontSize: Fonts.sizes.mediumsmall,
    fontFamily: Fonts.family.fontfamily,
    color: Colors.secondaryColor
  },
  DeactiveDashboardProfileView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginRight: 5,
    backgroundColor: Colors.unselectedTab,
    borderRadius: 40,
    borderWidth: 0.7,
    borderColor: Colors.unselectedTab
  },
  DeactiveDashboardProfileViewText: {
    fontSize: Fonts.sizes.mediumsmall,
    color: Colors.secondaryColor,
    fontFamily: Fonts.family.fontfamily,
    opacity: 0.6
  },
  PatientProfileDayCount: {
    justifyContent: "flex-end",
    alignItems: "flex-end",
    marginTop: 10,
    marginRight: 20

    // marginBottom: 20
  },
  DayCountText: {
    backgroundColor: Colors.whiteColor,
    borderRadius: 30,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 15,
    paddingRight: 15,
    fontFamily: Fonts.family.fontfamily
  },
  PatientProfileGraph: {
    paddingTop: 10
  },
  PatientProfilePregnancyTracker: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 20
  },
  PatientProfilePregnancyTrackerText: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.family.fontFamilyBold
  },
  recordsHeaderContainer: {
    justifyContent: "space-between",
    flexDirection: "row",
    marginTop: 35,
    marginRight: 30,
    marginLeft: 30,
    height: 30,
    alignItems: "center"
  },
  headerTextStyles: {
    color: Colors.whiteColor,
    fontSize: Fonts.size.input,
    fontFamily: Fonts.family.fontFamilyBold
  },

  addUpdateBtnContainer: {
    borderRadius: 40,
    backgroundColor: Colors.whiteColor,
    height: 30,
    width: 100,
    borderWidth: 0.7,
    borderColor: Colors.darkColor,
    justifyContent: "center",
    alignItems: "center"
  },
  addUpdateTextStyles: {
    fontFamily: Fonts.family.fontfamily
  },
  createProfileContainer: {
    borderRadius: 40,
    backgroundColor: Colors.whiteColor,
    height: 30,
    width: 150,
    borderWidth: 0.7,
    borderColor: Colors.darkColor,
    justifyContent: "center",
    alignSelf: "center"
  },
  createProfileTextStyles: {
    fontFamily: Fonts.family.fontFamilyBold,
    textAlign: "center"
  },
  noDataViewContainer: {
    justifyContent: "center",
    alignItems: "center",
    width: 300,
    height: 150,
    marginRight: 30,
    marginLeft: 30
  },
  QuickRecordsContainer: {
    flexDirection: "row",
    height: 100,
    marginLeft: 20,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10,
    justifyContent: "center"
  },
  Cards: {
    justifyContent: "center",
    alignItems: "center",
    height: 90,
    width: 120,
    backgroundColor: Colors.unselectedTab,
    borderRadius: 5,
    marginRight: 5
  },
  selectedCard: {
    justifyContent: "center",
    alignItems: "center",
    height: 90,
    width: 120,
    backgroundColor: Colors.selectedTab,
    borderRadius: 5,
    elevation: 2,
    marginRight: 5
  },
  QuickRecordsCardsWeight: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  QuickRecordsCardsWeightOpacity: {
    opacity: 0.6,
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  QuickRecordsCardsText: {
    marginTop: 5,
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily,
    marginLeft: 10,
    marginRight: 10,
    textAlign: "center"
  },
  QuickRecordsCardsTextOpacity: {
    opacity: 0.6,
    marginTop: 5,
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily,
    marginLeft: 10,
    marginRight: 10,
    textAlign: "center"
  },
  QuickRecordsCardsEdit: {
    fontSize: Fonts.size.mini,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  },
  addRecordImage: {
    width: 50,
    height: 50
  },
  addRecordTextStyles: {
    fontSize: Fonts.size.medium,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontfamily
  }
});
