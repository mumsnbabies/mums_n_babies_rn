import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  AsyncStorage,
  TouchableHighlight,
  TouchableOpacity
} from "react-native";
import { observable, toJS } from "mobx";
import { observer } from "mobx-react";
import PatientProfileStore from "../../../../../../stores/PatientProfileStore";
import PatientProfileUpdateStore from "../../../../../../stores/PatientProfileUpdateStore";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../../../../Components/Loader";
import Graph from "../../../../../../Components/Graph";
import NoDataView from "../../../../../../Components/NoDataView";
import moment from "moment";
import ImageViewer from "ImageViewer";
import Images from "../../../../../../Themes/Images";

// Styles
import styles from "./styles";
import { DASHBOARDCONSTANTS } from "../../../../../../Constants/Patient";
var s = new Set();

@observer
export default class Dashboard extends Component {
  @observable selectedPerson: String;
  @observable selectedChild: Number;
  @observable record: String;
  @observable recordValueKeys: Array;
  @observable curIndex: Number;
  @observable shown: Boolean;
  @observable quickRecords: Array;
  constructor(props) {
    if (__DEV__) {
      console.log("constructor", PatientProfileUpdateStore.selectedRecord);
    }
    super(props);
    this.selectedPerson = PatientProfileUpdateStore.selectedPerson;
    this.selectedChild = PatientProfileUpdateStore.selectedChild;
    this.record = PatientProfileUpdateStore.selectedRecord;
    this.recordValueKeys = this.getRecordValueKeys(this.record);
    this.curIndex = 0;
    this.shown = false;
    this.quickRecords = [];
  }
  componentDidMount() {
    this.checkAttributes();
  }
  changeSelection = (child_id, type) => {
    this.selectedChild = child_id;
    this.selectedPerson = type;
    this.record = "";
    PatientProfileUpdateStore.storeSelectedTab(child_id, type);
    this.getPeriodicalAttributes();
  };
  displayDashboardOptions = () => {
    let { name, children } = PatientProfileStore.patientDetails;
    if (children) {
      return children.map((eachChild, index) => {
        return (
          <TouchableHighlight
            key={index}
            underlayColor="transparent"
            onPress={() => this.changeSelection(eachChild.child_id, "Child")}
            style={
              eachChild.child_id === this.selectedChild ? (
                styles.ActiveDashboardProfile
              ) : (
                styles.DeactiveDashboardProfileView
              )
            }
          >
            <Text
              style={
                eachChild.child_id === this.selectedChild ? (
                  styles.ActiveDashboardProfileText
                ) : (
                  styles.DeactiveDashboardProfileViewText
                )
              }
            >
              {eachChild.name}
            </Text>
          </TouchableHighlight>
        );
      });
    }
    return null;
  };
  getQuickRecordKeys = () => {
    let records = [];
    if (PatientProfileUpdateStore.periodicalAttributes.length > 0) {
      PatientProfileUpdateStore.periodicalAttributes.map(
        (eachRecord, index) => {
          records.push(eachRecord.attr_key);
        }
      );
    }
    return records;
  };
  getRecordValueKeys = key => {
    let valueKeys = [];
    PatientProfileUpdateStore.periodicalAttributes.map((eachRecord, index) => {
      if (eachRecord.attr_key === key) {
        eachRecord.attr_values.map((eachValue, index) => {
          valueKeys.push(eachValue.value_key);
        });
      }
    });
    return valueKeys;
  };
  checkAttributes = () => {
    let attributes = PatientProfileStore.patientDetails.patient_attributes;
    let { children } = PatientProfileStore.patientDetails;
    let quickRecords = this.getQuickRecordKeys();
    if (__DEV__) {
      console.log(toJS(quickRecords));
    }
    if (quickRecords.length > 0 && !this.record) {
      this.record = quickRecords[0];
      this.recordValueKeys = this.getRecordValueKeys(quickRecords[0]);
      PatientProfileUpdateStore.storeRecord(this.record);
    }
    let returningRecords = {};
    // let attribs = ["BMI", "WEIGHT", "HEIGHT", "SUGAR"]
    // let returningAttribs = { BMI: "", WEIGHT: 0, HEIGHT: "", SUGAR: "" }
    if (this.selectedPerson === "Parent") {
      if (attributes) {
        attributes.map((eachAttrib, index) => {
          if (quickRecords.indexOf(eachAttrib.attr_key) > -1) {
            if (__DEV__) {
              console.log(toJS(eachAttrib));
            }
            let attr_value = "";
            eachAttrib.attr_values.map((eachValue, index) => {
              attr_value = attr_value + eachValue.value + " / ";
            });
            attr_value = attr_value.slice(0, attr_value.length - 3);
            if (__DEV__) {
              console.log(attr_value);
            }
            returningRecords[eachAttrib.attr_key] = attr_value;
          }
        });
        if (__DEV__) {
          console.log(returningRecords);
        }
      }
    } else {
      if (children.length > 0) {
        children.map((eachChild, index) => {
          if (eachChild.child_id === this.selectedChild) {
            if (eachChild.child_attributes) {
              eachChild.child_attributes.map((eachAttrib, index) => {
                if (quickRecords.indexOf(eachAttrib.attr_key) > -1) {
                  let attr_value = "";
                  eachAttrib.attr_values.map((eachValue, index) => {
                    attr_value = attr_value + eachValue.value + " / ";
                  });
                  attr_value = attr_value.slice(0, attr_value.length - 3);
                  returningRecords[eachAttrib.attr_key] = attr_value;
                }
              });
            }
            if (__DEV__) {
              console.log(returningRecords);
            }
          }
        });
      }
    }
    this.quickRecords = returningRecords;
  };
  closeViewer() {
    this.shown = false;
    this.curIndex = 0;
  }
  openViewer(index) {
    this.shown = true;
    this.curIndex = index;
  }
  getPeriodicalAttributes = () => {
    let { patient_id } = PatientProfileStore.patientDetails;
    let requestObject = {};
    if (this.selectedPerson === "Parent") {
      requestObject["entity_id"] = patient_id;
      requestObject["entity_type"] = "PATIENT";
    } else {
      requestObject["entity_id"] = this.selectedChild;
      requestObject["entity_type"] = "CHILD";
    }
    PatientProfileUpdateStore.getPeriodicalAttributes(
      requestObject,
      this.checkAttributes
    );
  };
  navigateToDataUpdate = patient_id => {
    // this.getPeriodicalAttributes()
    NavigationActions.PatientDataUpdate({
      selectedPerson: this.selectedPerson,
      selectedChild: this.selectedChild,
      selectedParent: patient_id,
      selectedRecord: this.record
    });
  };

  navigateToAddNewAttribute = () => {
    let entity_id =
      this.selectedPerson === "Parent"
        ? PatientProfileStore.patientDetails.patient_id
        : this.selectedChild;
    let entity_type = this.selectedPerson === "Parent" ? "PATIENT" : "CHILD";
    NavigationActions.PatientAddNewAttribute({
      entityId: entity_id,
      entityType: entity_type
      // type: "replace"
    });
  };
  displayQuickRecordKeys = records => {
    let quickRecordKeys = this.getQuickRecordKeys();
    return quickRecordKeys.map((eachQuickRecord, index) => {
      if (__DEV__) {
        console.log(toJS(records));
      }
      return (
        <TouchableOpacity
          key={index}
          onPress={() => {
            this.record = eachQuickRecord;
            this.recordValueKeys = this.getRecordValueKeys(eachQuickRecord);
            PatientProfileUpdateStore.storeRecord(this.record);
          }}
        >
          <View
            style={
              this.record === eachQuickRecord ? (
                styles.selectedCard
              ) : (
                styles.Cards
              )
            }
          >
            <Text
              style={
                this.record === eachQuickRecord ? (
                  styles.QuickRecordsCardsWeight
                ) : (
                  styles.QuickRecordsCardsWeightOpacity
                )
              }
            >
              {records[eachQuickRecord]}
            </Text>
            <Text
              numberOfLines={2}
              style={
                this.record === eachQuickRecord ? (
                  styles.QuickRecordsCardsText
                ) : (
                  styles.QuickRecordsCardsTextOpacity
                )
              }
            >
              {/* hai hello welcome good morning dude how r u? */}
              {/* {eachQuickRecord === "BLOOD_PRESSURE" ? "BP" : eachQuickRecord} */}
              {eachQuickRecord}
            </Text>
          </View>
        </TouchableOpacity>
      );
    });
  };
  render() {
    let {
      name,
      address,
      pic,
      expected_date,
      patient_attributes,
      children,
      patient_id
    } = PatientProfileStore.patientDetails;
    if (__DEV__) {
      console.log(PatientProfileUpdateStore.patientDetails, "patientDetails");
    }
    if (__DEV__) {
      console.log(
        PatientProfileUpdateStore.periodicalAttributes,
        "periodicalAttributes"
      );
    }
    // let quickRecords = this.checkAttributes(patient_attributes, children)
    let { total, documents } = PatientProfileStore.patientDocuments;
    if (
      PatientProfileStore.getDocumentsStatus === 200 &&
      PatientProfileStore.patientDocuments.total > 0
    ) {
      documents.map((doc, index) => {
        s.add(doc.document_url);
      });
    }
    let imgsArr = Array.from(s);
    if (__DEV__) {
      console.log(imgsArr, this.shown, this.curIndex);
    }

    return (
      <View style={styles.MainScreen}>
        <View style={styles.DashboardMainView}>
          {children && children.length > 0 ? (
            <Text style={styles.PatientProfileDashboardText}>
              {DASHBOARDCONSTANTS.CHANGE_DASHBOARD_TO_FIELD_TEXT}
            </Text>
          ) : null}
        </View>
        {children && children.length > 0 ? (
          <View style={styles.PatientProfileDashboardView}>
            <TouchableHighlight
              underlayColor="transparent"
              onPress={() => this.changeSelection(0, "Parent")}
              style={
                this.selectedPerson === "Parent" ? (
                  styles.ActiveDashboardProfile
                ) : (
                  styles.DeactiveDashboardProfileView
                )
              }
            >
              <Text
                style={
                  this.selectedPerson === "Parent" ? (
                    styles.ActiveDashboardProfileText
                  ) : (
                    styles.DeactiveDashboardProfileViewText
                  )
                }
              >
                {name + "'s"}
              </Text>
            </TouchableHighlight>
            {this.displayDashboardOptions()}
          </View>
        ) : null}
        <View style={styles.PatientProfilePregnancyTracker}>
          <Text style={styles.PatientProfilePregnancyTrackerText}>
            {Object.keys(this.quickRecords).length > 0 &&
            this.selectedPerson === "Parent" ? (
              "Pregnancy"
            ) : null}{" "}
            {Object.keys(this.quickRecords).length > 0 &&
              `${this.record} Tracker`}
          </Text>
        </View>
        {Object.keys(this.quickRecords).length > 0 ? (
          <View style={styles.PatientProfileGraph}>
            <Graph
              selectedPerson={this.selectedPerson}
              selectedChild={this.selectedChild}
              selectedParent={patient_id}
              record={this.record}
              recordValueKeys={this.recordValueKeys}
            />
          </View>
        ) : this.selectedPerson === "Parent" ? (
          <TouchableOpacity
            style={styles.createProfileContainer}
            onPress={() =>
              NavigationActions.PatientWelcomeScreen({
                selectedPerson: this.selectedPerson,
                selectedChild: this.selectedChild,
                selectedParent: patient_id,
                selectedRecord: this.record
              })}
          >
            <Text style={styles.createProfileTextStyles}>
              Create your profile
            </Text>
          </TouchableOpacity>
        ) : (
          <View style={styles.noDataViewContainer}>
            <NoDataView />
          </View>
        )}
        <View style={styles.recordsHeaderContainer}>
          <Text style={styles.headerTextStyles}>
            {DASHBOARDCONSTANTS.QUICK_RECORD_FIELD_TEXT}
          </Text>
          {Object.keys(this.quickRecords).length > 0 && (
            <TouchableOpacity
              style={styles.addUpdateBtnContainer}
              onPress={() => {
                this.navigateToDataUpdate(patient_id);
              }}
            >
              <Text style={styles.addUpdateTextStyles}>
                {DASHBOARDCONSTANTS.ADD_UPDATE_BUTTON_TEXT}
              </Text>
            </TouchableOpacity>
          )}
        </View>

        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={styles.QuickRecordsContainer}>
            <TouchableOpacity onPress={this.navigateToAddNewAttribute}>
              <View style={styles.Cards}>
                <Image
                  style={styles.addRecordImage}
                  resizeMode="contain"
                  source={Images.addRecord}
                />
                <Text style={styles.addRecordTextStyles}>
                  {DASHBOARDCONSTANTS.CREATE_RECORD_FIELD_TEXT}
                </Text>
              </View>
            </TouchableOpacity>
            {this.displayQuickRecordKeys(this.quickRecords)}
          </View>
        </ScrollView>
      </View>
    );
  }
}
