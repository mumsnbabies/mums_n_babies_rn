checkAttributes = (attributes, children) => {
  let attribs = ["BMI", "WEIGHT", "HEIGHT", "SUGAR"]
  let returningAttribs = { BMI: "", WEIGHT: 0, HEIGHT: "", SUGAR: "" }
  if (this.selectedPerson === "Parent") {
    if (attributes) {
      attributes.map((eachAttrib, index) => {
        if (attribs.indexOf(eachAttrib.attr_key) > -1) {
          returningAttribs[eachAttrib.attr_key] = eachAttrib.attr_value
        }
      })
      if (__DEV__) {
        console.log(returningAttribs)
      }
    }
  } else {
    if (children.length > 0) {
      children.map((eachChild, index) => {
        if (eachChild.child_id === this.selectedChild) {
          if (eachChild.child_attributes) {
            eachChild.child_attributes.map((eachAttrib, index) => {
              if (attribs.indexOf(eachAttrib.attr_key) > -1) {
                returningAttribs[eachAttrib.attr_key] = eachAttrib.attr_value
              }
            })
          }
          if (__DEV__) {
            console.log(returningAttribs)
          }
        }
      })
    }
  }
  return returningAttribs
}
