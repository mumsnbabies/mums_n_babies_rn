import { StyleSheet, Platform, PixelRatio } from "react-native";
import { Fonts, Colors } from "../../../../../../Themes/";

export default StyleSheet.create({
  //Title
  MedicalRecordsHeader: {
    justifyContent: "center",
    alignItems: "center"
  },
  MedicalRecordsHeaderText: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  //Main Styles
  medicalRecordHeaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Colors.secondaryColor,
    maxWidth: 100,
    height: 40,
    borderRadius: 10
  },
  medicalRecordBodyContainer: {
    justifyContent: "center",
    flex: 1
  },
  medicalRecordsHeaderText: {
    color: Colors.whiteColor,
    fontFamily: Fonts.family.fontFamilyBold,
    fontSize: Fonts.sizes.large
  },
  headerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  yourRecordsContainer: {
    flexDirection: "column",
    flex: 1,
    marginLeft: 30,
    marginRight: 30
  },

  yourRecordsText: {
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.large,
    fontFamily: Fonts.family.fontFamilyBold
  },
  floatButton: {
    borderRadius: 40,
    backgroundColor: Colors.whiteColor,
    height: 30,
    width: 100,
    borderWidth: 0.7,
    borderColor: Colors.darkColor,
    justifyContent: "center",
    alignItems: "center"
  },
  addupdatebtn: {
    fontFamily: Fonts.family.fontfamily,
    color: Colors.blackColor
  },
  noDataViewContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 200
  },
  imageContainer: {
    alignItems: "center",
    marginTop: 10,
    elevation: 2,
    width: 50
  },
  attachedImage: {
    width: 50,
    height: 50
  },
  footerButtonContainer: {
    alignItems: "center",
    justifyContent: "center",
    height: 60,
    elevation: 2,
    backgroundColor: Colors.requestButtom
  },
  btnTextStyles: {
    fontFamily: Fonts.family.fontFamilyBold,
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.smallheading
  },
  loaderContainer: {
    flex: 1,
    height: 200,
    justifyContent: "center",
    alignItems: "center"
  },
  PatientProfileDashboardText: {
    color: Colors.whiteColor,
    fontSize: Fonts.sizes.large,
    fontFamily: Fonts.family.fontfamily
  },
  PatientProfileDashboardView: {
    flexDirection: "row",
    // flex: 1,
    marginLeft: 30,
    marginTop: 10,
    marginRight: 30,
    marginBottom: 10,
    height: 30
  },
  ActiveDashboardProfile: {
    flex: 1,
    backgroundColor: Colors.selectedTab,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 40,
    borderWidth: 0.7,
    borderColor: Colors.selectedTab,
    marginRight: 5
  },
  ActiveDashboardProfileText: {
    fontSize: Fonts.sizes.mediumsmall,
    fontFamily: Fonts.family.fontfamily,
    color: Colors.secondaryColor
  },
  DeactiveDashboardProfileView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginRight: 5,
    backgroundColor: Colors.unselectedTab,
    borderRadius: 40,
    borderWidth: 0.7,
    borderColor: Colors.unselectedTab
  },
  DeactiveDashboardProfileViewText: {
    fontSize: Fonts.sizes.mediumsmall,
    color: Colors.secondaryColor,
    fontFamily: Fonts.family.fontfamily,
    opacity: 0.6
  }
});
