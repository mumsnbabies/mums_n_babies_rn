import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Platform,
  StyleSheet,
  Modal
} from "react-native";
import { Fonts, Colors } from "../../../../../../Themes/";
import { DOCUMENTDELETEMODELCONSTANTS } from "../../../../../../Constants/Patient";
export default class DocumentDeleteModal extends Component {
  constructor(props) {
    super(props);
  }

  handleSubmit = () => {
    const { toggleModalVisible, handleDeleteDocumentPress } = this.props;
    toggleModalVisible();
    handleDeleteDocumentPress();
  };

  render() {
    return (
      <Modal
        animationType={"fade"}
        transparent={true}
        visible={this.props.modalVisible}
        onRequestClose={() => {
          this.props.toggleModalVisible();
        }}
      >
        <View style={styles.modalMainContainer}>
          <View style={styles.modalContainer}>
            <View style={styles.headerMainContainer}>
              <View style={styles.headerContainer}>
                <Text style={styles.headerTextStyles}>
                  {DOCUMENTDELETEMODELCONSTANTS.DELETE_DOCUMENT_TEXT}
                </Text>
              </View>
              <View style={styles.closeBtnPostionContainer}>
                <TouchableOpacity
                  onPress={() => this.props.toggleModalVisible()}
                >
                  <View style={styles.closeBtnContainer}>
                    <Text style={styles.closeBtnTextStyles}>X</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.footerButtonContainer}>
              <TouchableOpacity
                onPress={() => {
                  this.handleSubmit();
                }}
              >
                <View style={styles.proceedBtnStyles}>
                  <Text style={styles.btnTextStyles}>
                    {DOCUMENTDELETEMODELCONSTANTS.PROCEED_BUTTON_TEXT}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modalMainContainer: {
    backgroundColor: "rgba(0, 0, 0, 0.75)",
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  modalContainer: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: Colors.whiteColor,
    borderRadius: 5,
    borderColor: "#dcdcdc",
    borderWidth: 0.7,
    position: "relative",
    width: 240
  },
  headerContainer: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    flex: 12
  },
  headerMainContainer: {
    flexDirection: "row",
    marginVertical: 10,
    marginHorizontal: 10,
    justifyContent: "space-between"
  },
  headerTextStyles: {
    fontSize: Fonts.size.input,
    color: Colors.blackColor,
    fontFamily: Fonts.family.fontFamilyBold
  },
  closeBtnPostionContainer: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "center"
  },
  closeBtnContainer: {
    width: 20,
    height: 20,
    borderRadius: 10,
    borderColor: Colors.blackColor,
    borderWidth: 1,
    paddingLeft: 5
  },
  closeBtnTextStyles: {
    color: Colors.blackColor,
    fontSize: Fonts.size.small,
    fontWeight: "600"
  },
  headerTextStyles: {
    fontSize: Fonts.size.input
  },

  bodyContainer: {},

  btnTextStyles: {
    fontSize: Fonts.size.input,
    color: Colors.whiteColor,
    fontFamily: Fonts.family.fontFamilyBold
  },

  footerButtonContainer: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.secondaryColor,
    borderRadius: 5,
    height: 40,
    elevation: 1,
    marginBottom: 10,
    width: 120
  }
});
