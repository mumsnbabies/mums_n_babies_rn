// import Stores
import PatientProfileStore from "../../../../../../stores/PatientProfileStore"

export const handleDeleteDocumentPress = doctorObject => {
  let requestObject = {
    description: doctorObject.documentName,
    document_url: doctorObject.documentUrl,
    document_type: doctorObject.documentType,
    document_date: doctorObject.documentDate,
    to_delete: true
  }
  PatientProfileStore.deleteDocumentAPI(requestObject, doctorObject.documentId)
}
