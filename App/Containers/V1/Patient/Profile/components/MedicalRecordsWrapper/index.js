import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  TouchableHighlight,
  Platform,
  Picker
} from "react-native";
import {
  API_INITIAL,
  API_FETCHING,
  API_SUCCESS,
  INIT,
  API_PAGINATION_FINISHED
} from "../../../../../../Utils/AppAPIConstants";
import { DASHBOARDCONSTANTS } from "../../../../../../Constants/Patient";
import { Icon } from "react-native-elements";
import { Actions as NavigationActions } from "react-native-router-flux";
import { observable, toJS } from "mobx";
import { observer } from "mobx-react";
import Loader from "../../../../../../Components/Loader";
import Document from "../../../../../../Components/Document";
import NoDataView from "../../../../../../Components/NoDataView";

// import Stores
import PatientProfileStore from "../../../../../../stores/PatientProfileStore";

import DocumentDeleteModal from "./DocumentDeleteModal";
import { MEDICALRECORTSWRAPPERCONSTANTS } from "../../../../../../Constants/Patient";
// Styles
import styles from "./styles";

import { handleDeleteDocumentPress } from "./Utils";

@observer
export default class MedicalRecords extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDeleteModalOpen: false,
      activeDocumentObject: null
    };
  }
  componentWillMount() {
    let requestObject = {
      search_q: "",
      limit: 20,
      filters: {},
      offset: 0
    };
    PatientProfileStore.getAllDocumentsList(requestObject);
  }

  updateDocumentScreen = documentId => {
    NavigationActions.UpdateDocument({
      documentId: documentId,
      type: "replace"
    });
  };

  handlePopupMenu = (eventName, index, documentObject) => {
    if (index === 0) {
      NavigationActions.UpdateDocument({
        documentId: documentObject.documentId,
        type: "replace"
      });
    } else if (index === 1) {
      this.handleDeleteDocumentPopup(documentObject);
    }

    // else if (index === 2) {

    //  NavigationActions.ShareDocument()

    // }
  };

  handleDeleteDocumentPopup = documentObject => {
    this.setState({
      isDeleteModalOpen: true,
      activeDocumentObject: documentObject
    });
  };
  renderChildRecordsView = () => {
    const allDocuments = PatientProfileStore.getUpdatedAllDocuments;

    if (allDocuments.slice().length > 0) {
      return PatientProfileStore.getUpdatedAllDocuments.map(documentObject => {
        if (
          documentObject.entityId === PatientProfileStore.selectedChild &&
          documentObject.entityType === "CHILD"
        ) {
          return (
            <Document
              key={documentObject.documentId}
              documentObject={documentObject}
              updateDocument={this.updateDocumentScreen}
              handlePopupMenu={this.handlePopupMenu}
            />
          );
        }
      });
    }
    return allDocuments;
  };
  renderRecordsView = () => {
    const allDocuments = PatientProfileStore.getUpdatedAllDocuments;
    if (allDocuments.slice().length > 0) {
      return PatientProfileStore.getUpdatedAllDocuments.map(documentObject => {
        if (__DEV__) {
          console.log("documentObject", documentObject);
        }
        if (documentObject.entityType !== "CHILD") {
          return (
            <Document
              key={documentObject.documentId}
              documentObject={documentObject}
              updateDocument={this.updateDocumentScreen}
              handlePopupMenu={this.handlePopupMenu}
            />
          );
        }
      });
    }
    return allDocuments;
  };

  toggleDeleteModalVisible = () => {
    this.setState({ isDeleteModalOpen: !this.state.isDeleteModalOpen });
  };
  renderDocumentDeleteModal = () => {
    if (this.state.isDeleteModalOpen) {
      return (
        <DocumentDeleteModal
          modalVisible={this.state.isDeleteModalOpen}
          toggleModalVisible={this.toggleDeleteModalVisible}
          presentationStyle={"overFullScreen"}
          documentObject={this.state.activeDocumentObject}
          handleDeleteDocumentPress={() =>
            handleDeleteDocumentPress(this.state.activeDocumentObject)
          }
        />
      );
    }
    return null;
  };
  changeSelection = (child_id, type) => {
    PatientProfileStore.storeSelectedValues(child_id, type);
  };
  displayDashboardOptions = () => {
    let { name, children } = PatientProfileStore.patientDetails;
    if (children) {
      return children.map((eachChild, index) => {
        return (
          <TouchableHighlight
            key={index}
            underlayColor="transparent"
            onPress={() => this.changeSelection(eachChild.child_id, "Child")}
            style={
              eachChild.child_id === PatientProfileStore.selectedChild
                ? styles.ActiveDashboardProfile
                : styles.DeactiveDashboardProfileView
            }
          >
            <Text
              style={
                eachChild.child_id === PatientProfileStore.selectedChild
                  ? styles.ActiveDashboardProfileText
                  : styles.DeactiveDashboardProfileViewText
              }
            >
              {eachChild.name}
            </Text>
          </TouchableHighlight>
        );
      });
    }
    return null;
  };

  navigateToAddMedicalDocument = () => {
    NavigationActions.AddMedicalDocument({ type: "replace" });
  };
  render() {
    let { name, children } = PatientProfileStore.patientDetails;
    let selectedPerson = PatientProfileStore.selectedPerson;
    return (
      <View style={styles.yourRecordsContainer}>
        {this.renderDocumentDeleteModal()}
        <View style={styles.DashboardMainView}>
          {children && children.length > 0 ? (
            <Text style={styles.PatientProfileDashboardText}>
              Change Medical Records to
            </Text>
          ) : null}
        </View>
        {children && children.length > 0 ? (
          <View style={styles.PatientProfileDashboardView}>
            <TouchableHighlight
              underlayColor="transparent"
              onPress={() => this.changeSelection(0, "Parent")}
              style={
                selectedPerson === "Parent"
                  ? styles.ActiveDashboardProfile
                  : styles.DeactiveDashboardProfileView
              }
            >
              <Text
                style={
                  selectedPerson === "Parent"
                    ? styles.ActiveDashboardProfileText
                    : styles.DeactiveDashboardProfileViewText
                }
              >
                {name + "'s"}
              </Text>
            </TouchableHighlight>
            {this.displayDashboardOptions()}
          </View>
        ) : null}
        <View style={styles.headerContainer}>
          <Text style={styles.yourRecordsText}>
            {MEDICALRECORTSWRAPPERCONSTANTS.YOUR_RECORDS_TEXT}
          </Text>
          <TouchableOpacity
            style={styles.floatButton}
            onPress={this.navigateToAddMedicalDocument}
          >
            <Text style={styles.addupdatebtn}>
              {MEDICALRECORTSWRAPPERCONSTANTS.ADD_UPDATE_BUTTON_TEXT}
            </Text>
          </TouchableOpacity>
        </View>
        {PatientProfileStore.getAllDocumentsListLoadingStatus ===
        API_SUCCESS ? (
          PatientProfileStore.allDocumentsLength === 0 ? (
            <View style={styles.noDataViewContainer}>
              <NoDataView />
            </View>
          ) : selectedPerson === "Parent" ? (
            this.renderRecordsView()
          ) : (
            this.renderChildRecordsView()
          )
        ) : (
          <View style={styles.loaderContainer}>
            <Loader animating={true} color={"white"} />
          </View>
        )}
      </View>
    );
  }
}
