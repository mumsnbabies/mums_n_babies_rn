import React, { Component } from "react";
import {
  Text,
  View,
  Platform,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { Icon } from "react-native-elements";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../../Components/Loader";
import { observable } from "mobx";
import { observer } from "mobx-react";

// import Stores
import PatientProfileStore from "../../../../stores/PatientProfileStore";
import PatientProfileUpdateStore from "../../../../stores/PatientProfileUpdateStore";
import AuthStore from "../../../../stores/AuthStore";

//Import Common Components

//Import Current Screen Components
import Dashboard from "./components/Dashboard";
import ProfileNavBar from "./components/ProfileNavBar";

import styles from "./styles";
@observer
export default class PatientProfileHome extends Component {
  @observable showFloatButton: Boolean;
  constructor(props) {
    super(props);
    this.showFloatButton =
      this.props.activeComponentText === "Medical Records" ? true : false;
  }
  componentDidMount() {
    if (__DEV__) {
      console.log("DID MOUNT");
    }
    let requestObject = {
      patient_id: AuthStore.loginResponse.patient_id
    };
    PatientProfileStore.getPatientProfile(requestObject);
    requestObject = {
      entity_id: AuthStore.loginResponse.patient_id,
      entity_type: "PATIENT"
    };
    if (PatientProfileUpdateStore.selectedPerson === "Child") {
      requestObject["entity_id"] = PatientProfileUpdateStore.selectedChild;
      requestObject["entity_type"] = "CHILD";
    }
    PatientProfileUpdateStore.getPeriodicalAttributes(requestObject);
  }
  handleShowFloatButton = activeComponentText => {
    if (activeComponentText === "Medical Records") {
      this.showFloatButton = true;
    } else {
      this.showFloatButton = false;
    }
  };
  changeRenderComponent = (activeComponent, activeComponentText) => {
    this.handleShowFloatButton(activeComponentText);
    PatientProfileStore.resetSelectedValues();
    const renderTitle = () => {
      return (
        <View style={styles.patientProfileHeader}>
          <Text style={styles.patientProfileHeaderText}>
            {activeComponentText}
          </Text>
        </View>
      );
    };
    if (activeComponentText === "Medical Records" || this.showFloatButton) {
      NavigationActions.refresh({
        renderTitle: renderTitle,
        activeComponent: activeComponent,
        onBack: this.navigateBack
      });
    } else {
      NavigationActions.refresh({
        renderTitle: renderTitle,
        activeComponent: activeComponent,
        onBack: this.navigateBack
      });
    }
  };

  componentWillMount() {
    PatientProfileUpdateStore.resetPeriodicalAttributes();
    PatientProfileUpdateStore.clearSelectedTab();
    NavigationActions.refresh({
      renderTitle: this.renderTitle,
      onBack: this.navigateBack
    });
  }

  renderTitle = () => {
    return (
      <View style={styles.patientProfileHeader}>
        <Text style={styles.patientProfileHeaderText}>
          {this.props.activeComponentText}
        </Text>
      </View>
    );
  };

  navigateBack = () => {
    PatientProfileStore.resetSelectedValues();
    PatientProfileUpdateStore.resetPeriodicalAttributes();
    PatientProfileUpdateStore.clearSelectedTab();
    NavigationActions.pop();
  };

  render() {
    if (
      PatientProfileStore.getProfileStatus === 100 ||
      PatientProfileUpdateStore.getPeriodicalAttributesStatus === 100
    ) {
      return <Loader animating={true} />;
    }
    const { activeComponent: ActiveComponent } = this.props;
    return (
      <View style={styles.profileMainContainer}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <ProfileNavBar
            changeRenderComponent={this.changeRenderComponent}
            selectedComponent={ActiveComponent}
          />
          <ActiveComponent />
        </ScrollView>
      </View>
    );
  }
}

PatientProfileHome.defaultProps = {
  activeComponent: Dashboard,
  activeComponentText: "Dashboard"
};
{
  /*// @observer
// export class PatientUpdateProfile extends Component {
//   constructor(props) {
//     super(props)
//   }

//   render() {
//     return <View />
//   }
// }*/
}
