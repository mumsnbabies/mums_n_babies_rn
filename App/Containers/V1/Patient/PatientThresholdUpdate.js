import React, { Component } from "react";
import {
  ScrollView,
  Text,
  Image,
  View,
  TouchableOpacity,
  StatusBar,
  TextInput,
  Alert,
  Picker,
  Keyboard
} from "react-native";
import { observable, toJS } from "mobx";
import { observer } from "mobx-react";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../Components/Loader";
import PatientProfileUpdateStore from "../../../stores/PatientProfileUpdateStore";
import PatientProfileStore from "../../../stores/PatientProfileStore";
import AuthStore from "../../../stores/AuthStore";
import moment from "moment";
import Images from "../../../Themes/Images";
// Styles
import styles from "./Styles/PatientProfileUpdateStyles";
import { PATIENTTHRESHOLDUPDATECONSTANTS } from "../../../Constants/Patient";
@observer
export default class PatientThresholdUpdate extends Component {
  @observable records: Array;
  @observable thresholds: Array;

  @observable attribute_keys: Array;
  constructor(props) {
    super(props);
    this.records = [];
    this.thresholds = [];

    this.attribute_keys = [];
  }
  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  componentDidMount() {
    if (PatientProfileUpdateStore.periodicalAttributes.length > 0) {
      PatientProfileUpdateStore.periodicalAttributes.map(
        (eachAttribute, index) => {
          this.attribute_keys.push(eachAttribute.attr_key);
        }
      );
    }
    if (this.props.selectedPerson === "Parent") {
      if (AuthStore.loginResponse.patient_attributes.length > 0) {
        AuthStore.loginResponse.patient_attributes.map(
          (eachAttribute, index) => {
            this.thresholds.push({
              attr_key: eachAttribute.attr_key,
              max_value: eachAttribute.max_value,
              min_value: eachAttribute.min_value
            });
          }
        );
      }
    } else {
      AuthStore.loginResponse.children.map((eachChild, index) => {
        if (eachChild.child_id === this.props.selectedChild) {
          if (eachChild.child_attributes.length > 0) {
            eachChild.child_attributes.map((eachAttribute, index) => {
              this.thresholds.push({
                attr_key: eachAttribute.attr_key,
                max_value: eachAttribute.max_value,
                min_value: eachAttribute.min_value
              });
            });
          }
        }
      });
    }
  }
  renderTitle = () => {
    return (
      <View style={styles.PatientYourProfileHeader}>
        <Text style={styles.PatientYourProfileHeaderText}>
          {
            PATIENTTHRESHOLDUPDATECONSTANTS.PATIENT_THRESHOLD_UPDATE_NAVBAR_HEADING
          }
        </Text>
      </View>
    );
  };
  handleDataUpdation = (value, attribute, key) => {
    this.thresholds.map((eachValue, index) => {
      if (eachValue.attr_key === attribute) {
        eachValue[key] = value;
      }
    });
  };
  getThresholds = () => {
    let thresholds = [];
    this.thresholds.map((eachValue, index) => {
      if (eachValue.min_value && eachValue.max_value) {
        thresholds.push(eachValue);
      }
    });
    return thresholds;
  };
  saveData = () => {
    let requestObject = {
      entity_id:
        this.props.selectedPerson === "Parent"
          ? this.props.selectedParent
          : this.props.selectedChild,
      thresholds: this.getThresholds(),
      entity_type: this.props.selectedPerson === "Parent" ? "PATIENT" : "CHILD"
    };
    PatientProfileUpdateStore.updateThresholdValuesAPI(requestObject, () =>
      PatientProfileUpdateStore.getPatientProfile({
        patient_id: this.props.selectedParent
      })
    );
  };
  displayMinMaxValues = attribute => {
    return this.thresholds.map((eachValue, index) => {
      if (eachValue.attr_key === attribute) {
        return (
          <View style={{ flexDirection: "row" }} key={index}>
            <View style={styles.halfScreen}>
              <Text style={styles.fieldHeadingText}>
                {PATIENTTHRESHOLDUPDATECONSTANTS.MIN_VALUE_FIELD_TEXT}{" "}
              </Text>
              <TextInput
                keyboardType="numeric"
                onChangeText={text =>
                  this.handleDataUpdation(text, attribute, "min_value")}
                style={styles.DocInputField}
                placeholder={
                  PATIENTTHRESHOLDUPDATECONSTANTS.MIN_VALUE_FIELD_PLACEHOLDER
                }
                autoCorrect={false}
                underlineColorAndroid="transparent"
                value={eachValue.min_value}
              />
            </View>
            <View style={styles.halfScreen}>
              <Text style={styles.fieldHeadingText}>
                {PATIENTTHRESHOLDUPDATECONSTANTS.MAX_VALUE_FIELD_TEXT}{" "}
              </Text>
              <TextInput
                keyboardType="numeric"
                onChangeText={text =>
                  this.handleDataUpdation(text, attribute, "max_value")}
                style={styles.DocInputField}
                placeholder={
                  PATIENTTHRESHOLDUPDATECONSTANTS.MAX_VALUE_FIELD_PLACEHOLDER
                }
                autoCorrect={false}
                underlineColorAndroid="transparent"
                value={eachValue.max_value}
              />
            </View>
          </View>
        );
      }
    });
  };
  displayAttributeFields = () => {
    return this.attribute_keys.map((eachAttribute, index) => {
      return (
        <View style={styles.InputUnderline} key={index}>
          <Text style={styles.fieldHeadingText}>{eachAttribute}</Text>
          {this.displayMinMaxValues(eachAttribute)}
        </View>
      );
    });
  };
  render() {
    if (PatientProfileUpdateStore.updateThresholdValues === 100) {
      return <Loader animating={true} />;
    } else if (PatientProfileUpdateStore.updateThresholdValues === 200) {
      Alert.alert("Success", "Successfully updated your thresholds", [
        {
          text: "OK",
          onPress: () => {
            PatientProfileUpdateStore.resetThresholdValues();
            NavigationActions.pop();
          }
        }
      ]);
      return null;
    } else if (PatientProfileUpdateStore.updateThresholdValues > 200) {
      Alert.alert("Error", PatientProfileUpdateStore.error.statusText, [
        {
          text: "OK",
          onPress: () => {
            PatientProfileUpdateStore.resetThresholdValues();
          }
        }
      ]);
      return null;
    } else {
      return (
        <ScrollView style={styles.MainScreen}>
          <StatusBar
            barStyle="light-content"
            translucent={false}
            backgroundColor="#c7c7c7"
          />
          <View style={styles.DocInput}>{this.displayAttributeFields()}</View>
          <View style={styles.SaveAndProceedSection}>
            <TouchableOpacity activeOpacity={1} onPress={this.saveData}>
              <View style={styles.SaveAndProceedView}>
                <Text style={styles.SaveAndProceedText}>
                  {PATIENTTHRESHOLDUPDATECONSTANTS.SAVE_BUTTON_TEXT}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      );
    }
  }
}
