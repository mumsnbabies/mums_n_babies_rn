import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  StatusBar,
  View,
  AsyncStorage,
  TouchableOpacity,
  WebView,
  Platform
} from "react-native";
import Mqtt from "@rn/react-native-mqtt-iot";
import PatientProfileStore from "../../../stores/PatientProfileStore";
import PopupMenu from "../../../Components/PopupMenu";
import PopupMenuIOS from "../../../Components/PopupMenuIOS";
import { Actions as NavigationActions } from "react-native-router-flux";
import Loader from "../../../Components/Loader";
import { observable } from "mobx";
import { observer } from "mobx-react";
import AuthStore from "../../../stores/AuthStore";
import Images from "../../../Themes/Images";
// Styles
import styles from "./Styles/PatientHomeScreenStyles";

import { PATIENTHOMESCREENCONSTANTS } from "../../../Constants/Patient";

@observer
export default class PatientHomeScreen extends Component {
  @observable onWeb;
  // constructor(props) {
  //   super(props);
  // }

  static onEnter = () => {
    NavigationActions.refresh({
      right: displayRightButton,
      renderTitle: displayTitle,
      left: null
    });
  };

  // renderLeftButton = () => {
  //   return <Image source={require("../../../Images/bell.png")} />;
  // };

  render() {
    if (__DEV__) {
      console.log(NavigationActions);
    }
    return (
      <View style={styles.MainScreen}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <ScrollView
          style={styles.Container}
          showsVerticalScrollIndicator={false}
        >
          <View style={styles.MainImageView}>
            <Image
              style={styles.MainImage}
              source={require("../../../Images/slide-mom-and-baby.jpg")}
            />
          </View>
          <View style={styles.SectionRowHolder}>
            <TouchableOpacity
              underlayColor="#eee"
              activeOpacity={1}
              onPress={() =>
                NavigationActions.PatientProfileHome({
                  screenTitleText: "Dashboard"
                })
              }
            >
              <View style={styles.SectionView}>
                <View style={styles.SectionIconView}>
                  <Image
                    style={styles.SectionImage}
                    resizeMode="contain"
                    source={Images.patientProfilePic}
                  />
                </View>
                <Text style={styles.SectionText}>
                  {PATIENTHOMESCREENCONSTANTS.PATIENT_PROFILE_TAB_TEXT}
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              underlayColor="#eee"
              activeOpacity={1}
              onPress={() => NavigationActions.DocConnect()}
            >
              <View style={styles.SectionView}>
                <View style={styles.SectionIconView}>
                  <Image
                    style={styles.SectionImage}
                    resizeMode="contain"
                    source={require("../../../Images/doc.png")}
                  />
                </View>
                <Text style={styles.SectionText}>
                  {PATIENTHOMESCREENCONSTANTS.PATIENT_DOCTOR_CONNECT_TAB_TEXT}
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.SectionRowHolder}>
            <TouchableOpacity
              activeOpacity={1}
              underlayColor="#eee"
              onPress={() => NavigationActions.PatientShop()}
            >
              <View style={styles.SectionView}>
                <View style={styles.SectionIconView}>
                  <Image
                    style={styles.SectionImage}
                    resizeMode="contain"
                    source={require("../../../Images/shop.png")}
                  />
                </View>
                <Text style={styles.SectionText}>
                  {PATIENTHOMESCREENCONSTANTS.PATIENT_SHOPPING_TAB_TEXT}
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={1}
              underlayColor="#eee"
              onPress={() => {
                NavigationActions.PatientLearn();
              }}
            >
              <View style={styles.SectionView}>
                <View style={styles.SectionIconView}>
                  <Image
                    style={styles.SectionImage}
                    resizeMode="contain"
                    source={require("../../../Images/learn.png")}
                  />
                </View>
                <Text style={styles.SectionText}>
                  {PATIENTHOMESCREENCONSTANTS.PATIENT_LEARN_TAB_TEXT}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const displayTitle = () => {
  return (
    <View style={styles.MainAppName}>
      <Text style={styles.MainAppNameText}>
        {PATIENTHOMESCREENCONSTANTS.PATIENT_HOME_SCREEN_NAVBAR_HEADING}
      </Text>
    </View>
  );
};
const displayRightButton = () => {
  if (Platform.OS === "ios") {
    return (
      <View style={styles.DropDownButton}>
        <PopupMenuIOS
          actions={["Change Password", "Update Profile", "Log Out", "Cancel"]}
        />
      </View>
    );
  }
  return (
    <PopupMenu
      actions={["Change Password", "Update Profile", "Log Out"]}
      onPress={onPopupEvent}
    />
  );
};

const onPopupEvent = (eventName, index) => {
  if (index === 0) {
    if (__DEV__) {
      console.log(NavigationActions);
    }
    NavigationActions.ChangePassword();
  } else if (index === 1) {
    NavigationActions.PatientProfileUpdate();
  } else if (index === 2) {
    AsyncStorage.multiRemove(
      ["AccessToken", "patient_id"],
      async function() {
        AuthStore.resetState();
        await Mqtt.logoutChatService();
        NavigationActions.Initial({ type: "reset" });
      }.bind(this)
    );
    // AsyncStorage.removeItem(
    //   "AccessToken",
    //   function() {
    //     AuthStore.resetState()
    //     NavigationActions.Initial({ type: "reset" })
    //   }.bind(this)
    // )
  }
};
