import React from "react";
import { View, StyleSheet, WebView, StatusBar } from "react-native";
// import MyProfilePage from "./PatientProfileUpdate";
// import DateTimePicker from "react-native-modal-datetime-picker";
import { Metrics } from "../../../Themes";

export default class PatientShop extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          barStyle="light-content"
          translucent={false}
          backgroundColor="#c7c7c7"
        />
        <WebView source={{ uri: "http://www.mumsnbabies.com/" }} />
      </View>
    );
  }
}
