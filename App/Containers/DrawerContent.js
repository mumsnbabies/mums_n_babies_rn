import React, { Component } from "react";
import {
  ScrollView,
  Image,
  BackHandler,
  Text,
  Button,
  View,
  TouchableHighlight
} from "react-native";
import styles from "./Styles/DrawerContentStyles";
import { Images } from "../Themes";
import { Actions as NavigationActions } from "react-native-router-flux";

class DrawerContent extends Component {
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", () => {
      if (this.context.drawer.props.open) {
        this.toggleDrawer();
        return true;
      }
      return false;
    });
  }

  toggleDrawer() {
    this.context.drawer.toggle();
  }

  render() {
    return (
      <View>
        <ScrollView
          style={styles.container}
          showsVerticalScrollIndicator={false}
        >
          <View style={styles.NavigationView} />
        </ScrollView>
      </View>
    );
  }
}

export default DrawerContent;
