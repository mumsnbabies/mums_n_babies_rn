import { StyleSheet, Platform } from "react-native"
import Fonts from "../../Themes/Fonts"

export default StyleSheet.create({
  MedicalRecordsHeader: {
    marginTop: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  MedicalRecordsHeaderText: {
    fontSize: 18,
    color: "black",
    fontFamily: Fonts.family.fontFamilyBold
  },
  AddingMedicalRecordText: {
    color: "white",
    fontFamily: Fonts.family.fontFamilyBold,
    fontSize: 20,
    marginLeft: 30
  },
  MedicalRecordsInput: {
    marginLeft: 10,
    marginRight: 10,
    borderColor: "gray",
    backgroundColor: "white",
    marginTop: 20,
    borderWidth: 0.3
  },
  MedicalRecordsInputField: {
    color: "gray",
    marginLeft: 20,
    height: 50,
    fontFamily: Fonts.family.fontfamily
  },
  AttachAddBtns: {
    marginTop: 10,
    marginRight: 10,
    alignItems: "flex-end"
  },
  AttachFilesBtns: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    width: 120,
    height: 20,
    borderRadius: 10,
    backgroundColor: "#662a48"
  },
  AttachFilesText: {
    color: "white",
    fontSize: 11,
    fontFamily: Fonts.family.fontfamily
  },
  AddRecordBtns: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    width: 120,
    height: 20,
    borderRadius: 10,
    backgroundColor: "#662a48"
  },
  AddRecordText: {
    color: "white",
    fontSize: 11,
    fontFamily: Fonts.family.fontfamily
  },
  YourRecords: {
    marginTop: 10,
    paddingBottom: 40,
    flexDirection: "column"
  },
  YourRecordsText: {
    color: "white",
    fontSize: 20,
    fontFamily: Fonts.family.fontFamilyBold,
    marginLeft: 30,
    marginBottom: 30
  }
})
