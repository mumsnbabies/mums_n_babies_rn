import { StyleSheet, Platform } from "react-native"
import Fonts from "../../Themes/Fonts"

export default StyleSheet.create({
  AppointmentsHeader: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15
  },
  AppointmentsHeaderText: {
    fontSize: 18,
    color: "black",
    fontFamily: Fonts.family.fontFamilyBold
  },
  MainScreen: {
    height: "100%",
    backgroundColor: "#ff69b4",
    paddingTop: 50,
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: "center"
  },
  AppointmentHeading: {
    marginTop: 40,
    marginLeft: 30
  },
  AppointmentHeadingText: {
    color: "white",
    fontSize: 25,
    fontFamily: Fonts.family.fontFamilyBold
  },
  AppointmentAccepting: {
    marginTop: 10
  },
  AppointmentAcceptingView: {
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#662a48",
    borderRadius: 50
  },
  AppointmentAcceptingText: {
    color: "white",
    fontFamily: Fonts.family.fontfamily
  },
  WishListScrollView: {
    marginTop: 10
  },
  TodayAppointmentsView: {
    marginBottom: 50,
    marginTop: 20
  },
  TodayAppointments: {
    marginTop: 5,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ffffff"
  },
  TodayAppointmentsText: {
    paddingTop: 15,
    paddingBottom: 15,
    fontFamily: Fonts.family.fontfamily
  }
})
