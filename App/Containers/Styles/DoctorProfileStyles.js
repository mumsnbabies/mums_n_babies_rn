import { StyleSheet, Platform } from "react-native"
import Fonts from "../../Themes/Fonts"

export default StyleSheet.create({
  MainScreen: {
    backgroundColor: "#ffffff",
    paddingTop: 50
  },
  DoctorProfileHeader: {
    marginTop: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  DoctorProfileHeaderText: {
    fontSize: 18,
    color: "black",
    fontFamily: Fonts.family.fontFamilyBold
  },
  DoctorProfilePic: {
    marginTop: 20,
    alignItems: "center",
    justifyContent: "center"
  },
  ProfilePic: {
    height: 130,
    width: 130
  },
  DoctorDetails: {
    marginTop: 20,
    alignItems: "center",
    justifyContent: "center"
  },
  DoctorName: {
    color: "black",
    fontFamily: Fonts.family.fontfamily,
    fontSize: 13,
    marginBottom: 6
  },
  SpecialistIn: {
    marginBottom: 6,
    fontSize: 15,
    fontFamily: Fonts.family.fontfamily
  },
  OpenTimings: {
    flexDirection: "row"
  },
  OpenOrNot: {
    color: "#33cc33",
    fontFamily: Fonts.family.fontFamilyBold,
    fontSize: 14
  },
  Timings: {
    fontSize: 14,
    marginLeft: 5,
    fontFamily: Fonts.family.fontFamilyBold
  },
  BookingnNetworkBtns: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10
  },
  BookAppointmentView: {
    marginRight: 5,
    width: "48%",
    backgroundColor: "#eee",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  BookAppointmentText: {
    fontFamily: Fonts.family.fontfamily,
    color: "black",
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 12
  },
  AddNetworkView: {
    marginLeft: 5,
    width: "48%",
    backgroundColor: "#eee",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  AddNetworkText: {
    fontFamily: Fonts.family.fontfamily,
    color: "black",
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 12
  },
  AddressnNavigation: {
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    paddingBottom: 30,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  Address: {
    width: "50%"
  },
  AddressHeading: {
    fontSize: 16,
    marginBottom: 10,
    fontFamily: Fonts.family.fontFamilyBold,
    color: "black"
  },
  StreetName: {
    fontSize: 13,
    marginBottom: 4,
    fontFamily: Fonts.family.fontfamily
  },
  AreaName: {
    fontSize: 13,
    marginBottom: 4,
    fontFamily: Fonts.family.fontfamily
  },
  CityName: {
    fontSize: 13,
    marginBottom: 4,
    fontFamily: Fonts.family.fontfamily
  },
  Pincode: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  PincodeHeading: {
    fontFamily: Fonts.family.fontfamily,
    fontSize: 13
  },
  PincodeNumber: {
    fontFamily: Fonts.family.fontfamily,
    fontSize: 13
  },
  NavigationImage: {
    width: "40%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 30
  },
  NavigationMap: {
    width: 80,
    height: 60
  },
  NavigationText: {
    color: "#ff0000",
    fontFamily: Fonts.family.fontFamilyBold,
    paddingTop: 10,
    textAlign: "center"
  },
  DoctorTimings: {
    borderTopWidth: 2,
    borderColor: "#eee"
  },
  TimingsView: {
    marginLeft: 20,
    marginTop: 10,
    marginBottom: 30
  },
  TimingsHead: {
    fontSize: 16,
    marginTop: 20,
    marginLeft: 20,
    color: "black",
    fontFamily: Fonts.family.fontFamilyBold
  },
  EachDay: {
    flexDirection: "row"
  },
  DayName: {
    fontFamily: Fonts.family.fontFamilyBold,
    color: "black"
  },
  DayTimings: {
    marginLeft: 10,
    fontFamily: Fonts.family.fontFamilyBold
  }
})
