import { StyleSheet, Platform } from "react-native"
import Fonts from "../../Themes/Fonts"

export default StyleSheet.create({
  MainScreen: {
    // backgroundColor: "#ffffff",
    paddingTop: 50
  },
  DoctorProfileHeader: {
    marginTop: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  DoctorProfileHeaderText: {
    fontSize: 18,
    color: "black",
    fontFamily: Fonts.family.fontFamilyBold
  },
  DashboardMainView: {
    marginTop: 15,
    marginLeft: 30
  },
  PatientProfileMainName: {
    color: "white",
    fontSize: 24,
    fontFamily: Fonts.family.fontfamily
  },
  PatientProfileDashboardText: {
    color: "white",
    fontSize: 12,
    marginTop: 15,
    fontFamily: Fonts.family.fontfamily
  },
  PatientProfileDashboardView: {
    flexDirection: "row",
    // flex: 1,
    marginLeft: 10,
    marginTop: 10,
    marginRight: 10,
    height: 30
  },
  ActiveDashboardProfile: {
    backgroundColor: "white",
    padding: 5,
    flex: 3,
    alignItems: "center",
    justifyContent: "center"
  },
  ActiveDashboardProfileText: {
    fontSize: 13,
    fontFamily: Fonts.family.fontfamily
  },
  DeactiveDashboardProfileView: {
    flex: 3,
    backgroundColor: "#662a48",
    padding: 5,
    alignItems: "center"
  },
  DeactiveDashboardProfileViewText: {
    color: "white",
    fontFamily: Fonts.family.fontfamily
  },
  PatientProfileDayCount: {
    justifyContent: "flex-end",
    alignItems: "flex-end",
    marginTop: 10,
    marginRight: 10
    // marginBottom: 20
  },
  DayCountText: {
    backgroundColor: "white",
    borderRadius: 30,
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 35,
    paddingRight: 35,
    color: "black",
    fontFamily: Fonts.family.fontfamily
  },
  PatientProfileGraph: {
    paddingTop: 20
  },
  PatientProfilePregnancyTracker: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 25
  },
  PatientProfilePregnancyTrackerText: {
    color: "white",
    fontFamily: Fonts.family.fontFamilyBold
  },
  QuickRecordsText: {
    color: "white",
    fontSize: 18,
    marginTop: 35,
    marginLeft: 30,
    fontFamily: Fonts.family.fontFamilyBold
  },
  QuickRecordsContainer: {
    flexDirection: "row",
    height: 100,
    marginLeft: 20,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10,
    elevation: 2,
    justifyContent: "center"
  },
  Cards: {
    justifyContent: "center",
    alignItems: "center",
    height: 90,
    width: 120,
    backgroundColor: "white",
    borderRadius: 5,
    elevation: 2,
    marginRight: 5
  },
  QuickRecordsCardsWeight: {
    fontSize: 18,
    color: "black"
  },
  QuickRecordsCardsText: {
    marginTop: 5,
    fontSize: 18,
    color: "black",
    fontFamily: Fonts.family.fontfamily
  },
  QuickRecordsCardsEdit: {
    fontSize: 10,
    color: "black",
    fontFamily: Fonts.family.fontfamily
  }
})
