import { StyleSheet, Platform } from "react-native";
import Fonts from "../../Themes/Fonts";

export default StyleSheet.create({
  MainScreen: {
    flex: 1,
    backgroundColor: "#ffffff",
    marginTop: 80
  },
  PatientConnectHeader: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15
  },
  PatientConnectHeaderText: {
    fontSize: 18,
    color: "black",
    fontFamily: Fonts.family.fontFamilyBold
  },
  Container: {
    flex: 1
  },
  //chat component styles///
  ChatListSection: {
    height: 85,
    flexDirection: "row",
    padding: 5,
    paddingTop: 0,
    margin: 10,
    marginTop: 0,
    marginBottom: 15
  },
  ChatLeftTopSection: {
    width: 50,
    height: 50
  },
  PersonImage: {
    width: 48,
    height: 48,
    // margin: 5,
    // padding: 5,
    backgroundColor: "#eee"
  },
  PersonName: {
    color: "black",
    fontSize: 13,
    fontFamily: Fonts.family.fontfamily,
    marginLeft: 15
  },
  PersonDetails: {
    color: "gray",
    fontSize: 14,
    fontFamily: Fonts.family.fontfamily,
    marginLeft: 15
  },
  ActiveDot: {
    width: 10,
    height: 10,
    backgroundColor: "green",
    borderRadius: 5,
    marginTop: 10,
    marginRight: 18
  },
  InactiveDot: {
    width: 10,
    marginTop: 10,
    height: 10,
    marginRight: 18,
    backgroundColor: "white",
    borderRadius: 5
  },
  ChatRightSection: {
    flex: 1,
    flexDirection: "column",
    borderBottomWidth: 0.8,
    borderBottomColor: "#d3d3d3",
    marginTop: 8
  },
  ChatRightTopSection: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  ChatRightBottomSection: {
    marginTop: 10
  },
  LatestMsgViewText: {
    color: "black",
    fontFamily: Fonts.family.fontfamily,
    marginLeft: 15,
    marginBottom: 10
  }
  //chat component styles end//
});
