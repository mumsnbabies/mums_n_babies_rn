import { StyleSheet, Platform } from "react-native"
import Fonts from "../../Themes/Fonts"

export default StyleSheet.create({
  MainScreen: {
    flex: 1,
    backgroundColor: "#eeeeee",
    justifyContent: "center",
    alignItems: "center",
    position: "relative"
  },
  PatientConnectHeader: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15
  },
  PatientConnectHeaderText: {
    fontSize: 18,
    color: "black",
    fontFamily: Fonts.family.fontFamilyBold
  }
})
