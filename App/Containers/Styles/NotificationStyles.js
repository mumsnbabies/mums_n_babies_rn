import { StyleSheet,Platform } from 'react-native'

export default StyleSheet.create({
    MainScreen: {
        flex: 1,
        marginTop: 60,
        backgroundColor: '#ff69b4',
    },
    inputField: {
        height: 45,
        width: 355,
        color: 'gray',
        marginLeft: 20
    },
    input: {
        marginLeft: 10,
        width: 355,
        borderColor: 'gray',
        borderWidth: 0.5,
        borderRadius: 5,
    },
    PrimaryButton: {
        backgroundColor: '#ff69b4',
        height: 40,
        borderRadius: 40,
        justifyContent: 'center',
        alignItems: 'center',
        width: 230,
        marginLeft: 70,
        marginTop: 20
    },
    forgotTextView: {
        marginTop: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ButtonText: {
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white'
    },
})
