import { StyleSheet, Platform } from "react-native"
import Fonts from "../../Themes/Fonts"

export default StyleSheet.create({
  DashboardHeader: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15
  },
  DashboardHeaderText: {
    fontSize: 18,
    color: "black",
    fontFamily: Fonts.family.fontFamilyBold
  },
  MainScreen: {
    flex: 1,
    backgroundColor: "#ff69b4",
    position: "relative"
  },
  DashboardMainText: {
    marginTop: 50
  },
  DashboardHeadingView: {
    marginTop: 60,
    marginLeft: 40
  },
  DashboardHeadingText: {
    color: "white",
    fontSize: 25,
    fontFamily: Fonts.family.fontFamilyBold
  },
  DashboardHeadingTagView: {
    marginTop: 30,
    marginLeft: 40
  },
  DashboardHeadingTagText: {
    color: "white",
    fontFamily: Fonts.family.fontFamilyBold
  },
  DashboardTabs: {
    flexDirection: "row",
    height: 30,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10
  },
  TotalPatientsTabView: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#ffffff"
  },
  TotalPatientsTabText: {
    fontFamily: Fonts.family.fontfamily
  },
  TotalViewsTabView: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#662a48"
  },
  TotalViewsTabText: {
    color: "white",
    fontFamily: Fonts.family.fontfamily
  },
  DashboardGraph: {
    marginTop: 40,
    marginBottom: 20
  },
  TotalProfileViews: {
    marginTop: 10,
    marginBottom: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  TotalProfileViewsText: {
    color: "white",
    fontSize: 18,
    fontFamily: Fonts.family.fontfamily
  }
})
