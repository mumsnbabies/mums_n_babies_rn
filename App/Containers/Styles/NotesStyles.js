import { StyleSheet, Platform } from "react-native"
import Fonts from "../../Themes/Fonts"

export default StyleSheet.create({
  NotesHeader: {
    marginTop: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  NotesHeaderText: {
    fontSize: 18,
    color: "black",
    fontFamily: Fonts.family.fontFamilyBold
  },
  AddingNoteText: {
    color: "white",
    fontFamily: Fonts.family.fontFamilyBold,
    fontSize: 20,
    marginLeft: 30
  },
  NotesInput: {
    marginLeft: 10,
    marginRight: 10,
    borderColor: "gray",
    backgroundColor: "white",
    marginTop: 20,
    borderWidth: 0.3
  },
  NotesInputField: {
    color: "gray",
    marginLeft: 20,
    height: 50,
    fontFamily: Fonts.family.fontfamily
  },
  AttachAddBtns: {
    marginTop: 10,
    marginRight: 10,
    alignItems: "flex-end"
  },
  RemindBtn: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    width: 190,
    height: 20,
    borderRadius: 10,
    backgroundColor: "#662a48"
  },
  RemindBtnText: {
    color: "white",
    fontSize: 10,
    fontFamily: Fonts.family.fontfamily
  },
  AddEventBtns: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    width: 100,
    height: 20,
    borderRadius: 10,
    backgroundColor: "#662a48"
  },
  AddEventText: {
    color: "white",
    fontSize: 10,
    fontFamily: Fonts.family.fontfamily
  },
  YourNotes: {
    marginTop: 10,
    paddingBottom: 40,
    flexDirection: "column"
  },
  YourNoteText: {
    color: "white",
    fontSize: 20,
    fontFamily: Fonts.family.fontFamilyBold,
    marginLeft: 30,
    marginBottom: 30
  }
})
