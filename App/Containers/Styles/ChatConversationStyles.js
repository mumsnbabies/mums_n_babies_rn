import { StyleSheet,Platform } from "react-native";

export default StyleSheet.create({
  MainScreen: {
    flex: 1,
    marginTop: 65,
    backgroundColor: "#ffffff",
    position: "relative"
  },
  welcometext: {
    top: 250
  },
  welcome: {
    marginLeft: 15,
    color: "black",
    fontSize: 25
  },
  welcome2: {
    marginLeft: 15,
    color: "black",
    fontSize: 15,
    marginBottom: 15
  },
  input: {
    flexDirection: "row",
    bottom: 0,
    height: 40,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: "#eee",
    alignItems: "center",
    borderColor: "gray",
    borderWidth: 0.5
  },
  inputtext: {
    fontSize: 16,
    color: "#387ef5",
    fontWeight: "600"
  },
  inputField: {
    height: 40,
    marginLeft: 10,
    flex: 1
    // color: 'gray'
  },
  inputview: {
    flex: 2,
    borderColor: "gray",
    borderWidth: 0.5,
    borderRadius: 5,
    // color: 'gray',
    marginTop: 3,
    marginLeft: 10,
    marginRight: 5,
    backgroundColor: "white"
  }
});
