import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import Record from "../Components/Record";
import { Actions as NavigationActions } from "react-native-router-flux";
import _ from "lodash";
import { Actions } from "jumpstate";
import Loader from "../Components/Loader";

// Styles
import styles from "./Styles/NotificationStyles";
export default class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: false
    };
  }

  componentWillMount() {
    NavigationActions.refresh({
      renderRightButton: this.ItemDetailsRightButton,
      renderTitle: this.WishListTitle
    });
  }
  WishListTitle = () => {
    return (
      <View
        style={{
          alignItems: "center",
          justifyContent: "center",
          marginTop: Platform.OS === "ios" ? 28 : 15
        }}
      >
        <Text style={{ fontSize: 18, color: "black", fontWeight: "500" }}>
          Notifications
        </Text>
      </View>
    );
  };
  ItemDetailsRightButton = () => {
    return (
      <View style={styles.rightNavView}>
        <TouchableOpacity>
          <View style={styles.addView}>
            <Image
              style={styles.searchImg}
              resizeMode="contain"
              source={require("../Images/white_search.png")}
            />
          </View>
        </TouchableOpacity>

        {/*<TouchableOpacity onPress={()=>NavigationActions.createGroupScreen({org_vehicles:this.props.groupsinfo.org_vehicles})}>
          <View style={styles.addView} >
              <Image style={styles.moreImg} resizeMode="contain" source={require('../Images/filter.png')}/>
          </View>
        </TouchableOpacity>*/}
        {/*<View style={styles.filterView} >
            <Image style={styles.filterViewImg}  source={require('../Images/filter.png')}/>
        </View>*/}
      </View>
    );
  };
  start = () => {
    return null;
  };
  render() {
    return (
      <View style={styles.MainScreen}>
        <ScrollView
          style={{ marginTop: 20 }}
          showsVerticalScrollIndicator={false}
        >
          <Record />
          <Record />
          <Record />
          <Record />
          <Record />
          <Record />
          <Record />
          <Record />
        </ScrollView>
      </View>
    );
  }
}
