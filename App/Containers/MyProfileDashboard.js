import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";
import _ from "lodash";
import { Actions } from "jumpstate";
import Loader from "../Components/Loader";
import Graph from "../Components/Graph";

// Styles
import styles from "./Styles/MyProfileDashboardStyles";
export default class MyProfileDashboard extends Component {
  constructor(props) {
    super(props);
    this.value;
  }

  componentWillMount() {
    NavigationActions.refresh({
      renderTitle: this.renderTitle
    });
  }
  renderTitle = () => {
    return (
      <View style={styles.DoctorProfileHeader}>
        <Text style={styles.DoctorProfileHeaderText}>
          Dashboard
        </Text>
      </View>
    );
  };
  start = () => {
    return null;
  };
  render() {
    return (
      <View style={styles.MainScreen}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.DashboardMainView}>
            <Text style={styles.PatientProfileMainName}>
              Your Dashboard
            </Text>
            <Text style={styles.PatientProfileDashboardText}>
              Change Dashboard to:
            </Text>
          </View>
          <ScrollView horizontal style={styles.PatientProfileDashboardView}>
            <TouchableOpacity>
              <View style={styles.ActiveDashboardProfile}>
                <Text style={styles.ActiveDashboardProfileText}>
                  Your's
                </Text>
              </View>
            </TouchableOpacity>
          </ScrollView>
          <View style={styles.PatientProfileDayCount}>
            <Text style={styles.DayCountText}>
              102 days for 👶
            </Text>
          </View>
          <View style={styles.PatientProfileGraph}>
            <Graph
            // selectedPerson={this.selectedPerson}
            // selectedChild={this.selectedChild}
            // selectedParent={patient_id}
            // record={this.record}
            />
          </View>
          <View style={styles.PatientProfilePregnancyTracker}>
            <Text style={styles.PatientProfilePregnancyTrackerText}>
              Pregnancy BMI Tracker
            </Text>
          </View>
          <Text style={styles.QuickRecordsText}>Quick Records</Text>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={styles.QuickRecordsContainer}>
              <TouchableOpacity>
                <View style={styles.Cards}>
                  <Text style={styles.QuickRecordsCardsWeight}>
                    75 Kg
                  </Text>
                  <Text style={styles.QuickRecordsCardsText}>Weight</Text>
                  <Text style={styles.QuickRecordsCardsEdit}>Tap to edit</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={styles.Cards}>
                  <Text style={styles.QuickRecordsCardsWeight}>
                    5.4
                  </Text>
                  <Text style={styles.QuickRecordsCardsText}>Height</Text>
                  <Text style={styles.QuickRecordsCardsEdit}>Tap to edit</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={styles.Cards}>
                  <Text style={styles.QuickRecordsCardsWeight}>
                    5.4
                  </Text>
                  <Text style={styles.QuickRecordsCardsText}>Height</Text>
                  <Text style={styles.QuickRecordsCardsEdit}>Tap to edit</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={styles.Cards}>
                  <Text style={styles.QuickRecordsCardsWeight}>
                    5.4
                  </Text>
                  <Text style={styles.QuickRecordsCardsText}>Height</Text>
                  <Text style={styles.QuickRecordsCardsEdit}>Tap to edit</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={styles.Cards}>
                  <Text style={styles.QuickRecordsCardsWeight}>
                    5.4
                  </Text>
                  <Text style={styles.QuickRecordsCardsText}>Height</Text>
                  <Text style={styles.QuickRecordsCardsEdit}>Tap to edit</Text>
                </View>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </ScrollView>
      </View>
    );
  }
}
