import React, { Component } from "react";
import { View, StatusBar } from "react-native";
import NavigationRouter from "../Navigation/NavigationRouter";

// Styles
import styles from "./Styles/RootContainerStyles";

export default class RootContainer extends Component {
  render() {
    return (
      <View style={styles.applicationView}>
        <StatusBar barStyle="light-content" backgroundColor="#c7c7c7" />
        <NavigationRouter />
      </View>
    );
  }
}
