import React, { Component } from "react";
import {
  ScrollView,
  Image,
  Text,
  TextInput,
  Button,
  View,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from "react-native";
import { Actions as NavigationActions } from "react-native-router-flux";
import _ from "lodash";
import { Actions } from "jumpstate";
import Loader from "../Components/Loader";
// Styles
import styles from "./Styles/ChatConversationStyles";

export default class ChatConversation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: false,
      text: "  Message",
      messages: [],
      value: "",
      url: ""
    };
  }

  componentWillMount() {
    NavigationActions.refresh({
      renderRightButton: this.ItemDetailsRightButton,
      renderTitle: this.WishListTitle
    });
  }
  WishListTitle = () => {
    return (
      <View
        style={{
          alignItems: "center",
          justifyContent: "center",
          marginTop: Platform.OS === "ios" ? 28 : 15
        }}
      >
        <Text style={{ fontSize: 18, color: "black", fontWeight: "500" }}>
          {this.props.item.name}
        </Text>
      </View>
    );
  };
  ItemDetailsRightButton = () => {
    return (
      <View style={styles.rightNavView}>
        <TouchableOpacity>
          <View style={styles.addView}>
            <Image
              style={styles.searchImg}
              resizeMode="contain"
              source={require("../Images/white_search.png")}
            />
          </View>
        </TouchableOpacity>

        {/*<TouchableOpacity onPress={()=>NavigationActions.createGroupScreen({org_vehicles:this.props.groupsinfo.org_vehicles})}>
          <View style={styles.addView} >
              <Image style={styles.moreImg} resizeMode="contain" source={require('../Images/filter.png')}/>
          </View>
        </TouchableOpacity>*/}
        {/*<View style={styles.filterView} >
            <Image style={styles.filterViewImg}  source={require('../Images/filter.png')}/>
        </View>*/}
      </View>
    );
  };
  start = () => {
    return null;
  };
  handleText = () => {
    var messages = this.state.messages.slice();
    messages.push(this.state.value);
    this.setState({
      messages: messages,
      url:
        "https://cdn.pixabay.com/photo/2017/01/31/15/12/avatar-2024924_960_720.png"
    });
  };
  showMessages = () => {
    let x = this.state.messages.map((key, value) => {
      return (
        <View
          key={key}
          style={{ marginLeft: 20, flexDirection: "row", marginBottom: 10 }}
        >
          <Image
            style={{ width: 40, height: 40, marginRight: 20 }}
            source={{ uri: this.state.url }}
          />
          <Text style={{ marginTop: 10 }}>{key}</Text>
        </View>
      );
    });
    return x;
  };
  render() {
    return (
      <View style={styles.MainScreen}>
        <ScrollView
          style={styles.WishListScrollView}
          showsVerticalScrollIndicator={false}
        >
          <View style={{ flexDirection: "row", flex: 1 }}>
            <TouchableOpacity>
              <View
                style={{
                  width: 140,
                  height: 30,
                  backgroundColor: "blue",
                  borderRadius: 20,
                  margin: 5,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text style={{ color: "white", fontSize: 13 }}>
                  Last seen: 12th Mar
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                NavigationActions.PatientProfile({ item: this.props.item });
              }}
            >
              <View
                style={{
                  width: 100,
                  height: 30,
                  backgroundColor: "darkblue",
                  borderRadius: 20,
                  margin: 5,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text style={{ color: "white" }}>Profile</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View
                style={{
                  width: 100,
                  height: 30,
                  backgroundColor: "white",
                  borderColor: "red",
                  borderWidth: 0.5,
                  borderRadius: 20,
                  margin: 5,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text style={{ color: "red" }}>Mute Chat</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.welcometext}>
            <Text style={styles.welcome}>Welcome!</Text>
            <Text style={styles.welcome2}>
              You can ask your queries easily to{" "}
              <Text style={{ fontWeight: "600" }}>
                {this.props.item.name}{" "}
              </Text>in this section
            </Text>
            <View
              style={{
                flex: 1,
                borderBottomWidth: 0.5,
                borderBottomColor: "gray",
                marginBottom: 10
              }}
            />
            {this.showMessages()}
          </View>
        </ScrollView>
        <View style={styles.input}>
          <View style={{ height: 25, width: 25 }}>
            <TouchableOpacity>
              <Image
                style={{ width: 25, height: 25 }}
                source={{
                  uri:
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTtYvNyuO66dj8kRbe0TkqF8g82D50z7wI73AJX3q6d4oTNUwssHA"
                }}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.inputview}>
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              style={styles.inputField}
              placeholder="Message"
              autoCorrect={false}
              underlineColorAndroid="transparent"
              onChangeText={value => this.setState({ value })}
            />
          </View>
          <View>
            <TouchableOpacity
              onPress={() => {
                this.handleText();
              }}
            >
              <Text style={styles.inputtext}>Send</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
