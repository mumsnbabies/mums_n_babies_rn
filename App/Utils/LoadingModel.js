import { API_FETCHING } from "./AppAPIConstants"

export default class LoadingModel {
  totalNoOfAPICalls = 1
  noOfAPICallsCompleted = 0

  updateNoOfAPICallsCompleted = (status: string) => {
    if (status !== API_FETCHING) {
      this.noOfAPICallsCompleted += 1
    }
  }

  setTotalNoOfAPICalls = (totalNoOfAPICalls: number) => {
    this.totalNoOfAPICalls = totalNoOfAPICalls
  }

  setNoOfAPICallsCompleted = (noOfAPICallsCompleted: number) => {
    this.noOfAPICallsCompleted = noOfAPICallsCompleted
  }
}
