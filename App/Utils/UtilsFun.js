export const Generate64LengthString = () => {
  let generatedString = ""

  let charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

  for (let i = 0; i < 64; i++) {
    generatedString += charset.charAt(
      Math.floor(Math.random() * charset.length)
    )
  }
  return generatedString
}
