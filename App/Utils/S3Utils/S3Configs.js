/*
 * @author: Chinmaya
 * @flow
 */

export type AWSConfig = {
  s3Region: string,
  cognitoRegion: string,
  identityPoolId: string,
  bucketName: string
};

export const PatientBucketConfig = {
  s3Region: "ap-south-1",
  cognitoRegion: "ap-south-1",
  identityPoolId: "ap-south-1:6c1e7e83-c3ea-4527-b735-ac6ea16b25cd",
  bucketName: "mnb-backend-media-static-mumbai"
};

//  s3Region: 'ap-southeast-1',
//   cognitoRegion: 'us-east-1',
//   identityPoolId: 'us-east-1:b64aced5-59fb-4cac-ba90-0e1279ce7333',
//   bucketName: 'ib-users-backend-media-static',
//   downloadKeyName: 'INSERTDOWNLOADKEY',
//   uploadKeyName: 'INSERTUPLOADKEY'

export default {
  PatientBucketConfig
};
