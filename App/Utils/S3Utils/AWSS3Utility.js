/*
 * @author: Chinmaya
 * @flow
 */

// import { AWSCognitoCredentials } from 'aws-sdk-react-native-core'
// import { AWSS3TransferUtility } from 'aws-sdk-react-native-transfer-utility'
import AWS from "aws-sdk/dist/aws-sdk-react-native";
import RNFS from "react-native-fs";

import type { AWSConfig } from "./S3Configs";

const Buffer = require("buffer").Buffer;

type Request = {};

type UploadOptions = {
  ACL?:
    | "private"
    | "public-read"
    | "public-read-write"
    | "authenticated-read"
    | "aws-exec-read"
    | "bucket-owner-read"
    | "bucket-owner-full-control",
  Body: string | Stream | Blob | TypedArray | ReadableStream | Buffer,
  Bucket: string,
  ContentType?: MimeType,
  Key: string,
  Metadata?: Map<string, any>,
  subscribe: boolean,
  completionhandler: boolean
};

type DataKeys = "Location" | "ETag" | "Bucket" | "Key";

type UploadCallback = (error: Error, value: Map<DataKeys, string>) => mixed;

export default class AWSS3Utility {
  options: Object;

  requests: Array<Request>;

  constructor(config: AWSConfig) {
    this.requests = [];
    this.congnitoCredentials = new AWS.CognitoIdentityCredentials(
      {
        IdentityPoolId: config.identityPoolId
      },
      {
        region: config.cognitoRegion
      }
    );
    AWS.config.update({
      region: config.s3Region,
      credentials: this.congnitoCredentials
    });

    if (__DEV__) {
      AWS.config.update({
        logger: console
      });
    }
    this.options = {
      region: config.s3Region,
      apiVersion: "latest",
      credentials: this.congnitoCredentials,
      params: { Bucket: config.bucketName }
    };
    this.initS3();
  }

  initS3(): void {
    this.s3 = new AWS.S3(this.options);
  }

  upload(options: UploadOptions, callback: UploadCallback): any {
    const uploadOptions = {
      ...options
    };

    if (this.s3) {
      return this.s3.upload(uploadOptions, callback);
    } else {
      throw Error("Initialize s3 before requesting an upload");
    }
  }

  uploadImage(options: UploadOptions, callback: UploadCallback) {
    let body = options.Body;
    if (typeof options.Body === "string") {
      body = new Buffer(options.Body, "base64");
    }
    return this.upload(
      { ContentType: "image/jpeg", ...options, Body: body },
      callback
    );
  }

  async uploadImageFromURI(options: UploadOptions, callback: UploadCallback) {
    if (RNFS === undefined) {
      throw Error(
        "Install react-native-fs package before using uploadImageFromURI"
      );
    }
    if (options.path === undefined || options.path === null) {
      throw Error("options.path cannot be null or undefined");
    }
    const body = await RNFS.readFile(options.path, "base64");

    return this.uploadImage({ ...options, Body: body }, callback);
  }

  pause(requestId: number): void {
    // AWSS3TransferUtility.editEventt({ config: 'pause', request: requestId })
  }

  resume(requestId: number): void {
    // AWSS3TransferUtility.editEvent({ config: 'resume', request: requestId })
  }

  cancel(requestId: number): void {
    // AWSS3TransferUtility.editEvent({ config: 'cancel', request: requestId })
  }
}
