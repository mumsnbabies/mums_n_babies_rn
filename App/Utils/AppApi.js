import { AsyncStorage } from "react-native"
import { create } from "apisauce"
import { action } from "mobx"

import { API_FETCHING, API_SUCCESS, API_FAILED } from "./AppAPIConstants"

const handleSuccessResponse = action(
  (response, onSuccess, handleLoadingFunction) => {
    onSuccess(response)
  }
)

export function requestWrapper(requestObject) {
  let formattedrequestObject = JSON.stringify(JSON.stringify(requestObject))
  let dataRequestObject = JSON.stringify({
    data: formattedrequestObject,
    clientKeyDetailsId: 1
  })
  return dataRequestObject
}

export async function getAccessToken() {
  let access_token = await AsyncStorage.getItem("AccessToken")
  if (__DEV__) {
    console.log(access_token)
  }
  return access_token
}

export async function genericApiCall(
  apiEndpoint,
  requestType,
  requestObject,
  onSuccess,
  onFailure,
  handleLoadingFunction = () => {}
) {
  handleLoadingFunction(API_FETCHING)
  let bodyData = requestWrapper(requestObject)
  if (!requestType) {
    requestType = "POST"
  }
  let accessToken = await getAccessToken()
  let headers = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + accessToken
  }
  let options = {
    method: requestType,
    headers: headers,
    body: bodyData
  }
  if (requestType === "GET") {
    delete options["body"]
  }

  fetch(apiEndpoint, options).then(function(response) {
    if (response.ok) {
      handleLoadingFunction(API_SUCCESS)
      response.json().then(
        jsonResponse => {
          onSuccess(jsonResponse)
        },
        () => {
          onSuccess({})
        }
      )
    } else {
      response.json().then(errors => {
        let errorResponse = {
          errors: errors,
          statusText: errors.response
        }
        handleLoadingFunction(API_FAILED)
        onFailure(errorResponse)
      })
    }
  })
}

export async function networkCallWithApisauce(
  url,
  requestObject,
  onSuccess,
  onFailure,
  type = "POST"
) {
  let bodyData = requestWrapper(requestObject)
  let accessToken = await getAccessToken()
  let headers = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + accessToken
  }
  const api = create({
    baseURL: url,
    headers: headers
  })
  let response = await api.post("", bodyData)
  response = response.data
  if (__DEV__) console.log("In networkCallWithApisauce response: ", response)
  return response
}
