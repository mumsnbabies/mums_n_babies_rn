export const BASE_URL =
  "https://mnb-backend-prod.mums-n-babies.com/api/mnb_doctor/";

export const BASE_URL_AUTH =
  "https://mnb-backend-prod.mums-n-babies.com/api/ib_users/";

export const BASE_URL_PATIENT =
  "https://mnb-backend-prod.mums-n-babies.com/api/mnb_patient/";

export const BASE_URL_AGENDA =
  "https://mnb-backend-prod.mums-n-babies.com/api/ib_agenda/";

export const BASE_URL_DOCTOR_GAMMA =
  "https://mnb-backend-prod.mums-n-babies.com/api/mnb_doctor/";

export const BASE_URL_CHAT =
  "https://mnb-backend-prod.mums-n-babies.com/api/ib_chat_wrapper/";
