const colors = {
  darkColor: "#ff69b4",
  primaryColor: "#ff69b4",
  whiteColor: "#fff",
  blackColor: "#000",
  graycolor: "#D3D3D3",
  lightblack: "#999999",
  secondaryColor: "#662a48",
  bluecolor: "#4d94ff",
  greencolor: "#33cc33",
  whiteblack: "#ff0000",
  background: "#3B5998",
  background1: "#ff4dac",
  bottom: "#A9A9A9",
  requestButtom: "#387ef5",
  genericcardborder: "#00bfff",
  selectedTab: "rgba(255,255,255,1)",
  unselectedTab: "rgba(255,255,255,0.4)",
  lightwhite: "#f5f5f5"
};

export default colors;
