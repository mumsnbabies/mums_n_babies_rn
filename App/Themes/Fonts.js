import { Platform } from "react-native"

const type = {
  base: "Avenir-Book",
  bold: "Avenir-Black",
  emphasis: "HelveticaNeue-Italic"
}

const family = {
  fontfamily: Platform.OS === "ios" ? "Kreon-Regular" : "OpenKreonRegular",
  fontFamilyBold: Platform.OS === "ios" ? "Kreon-Bold" : "OpenKreonBold"
}
const sizes = {
  large: 20,
  heading: 34,
  mediumsmall: 13,
  normal: 15,
  smallheading: 24
}
const size = {
  h1: 38,
  h2: 34,
  h3: 30,
  h4: 26,
  h5: 20,
  h6: 19,
  input: 18,
  regular: 17,
  medium: 14,
  small: 12,
  mini: 10,
  minitiny: 11,
  tiny: 8.5
}

const style = {
  h1: {
    fontFamily: type.base,
    fontSize: size.h1
  },
  h2: {
    fontWeight: "bold",
    fontSize: size.h2
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium
  }
}

export default {
  type,
  size,
  style,
  sizes,
  family
}
