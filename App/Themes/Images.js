// leave off @2x/@3x
const images = {
  // logo: require('../Images/ir.png'),
  // clearLogo: require('../Images/top_logo.png'),
  // launch: require('../Images/launch-icon.png'),
  // ready: require('../Images/your-app.png'),
  // ignite: require('../Images/ignite_logo.png'),
  // igniteClear: require('../Images/ignite-logo-transparent.png'),
  // tileBg: require('../Images/tile_bg.png'),
  // background: require('../Images/BG.png'),
  // buttonBackground: require('../Images/button-bg.png'),
  // api: require('../Images/Icons/icon-api-testing.png'),
  // components: require('../Images/Icons/icon-components.png'),
  // deviceInfo: require('../Images/Icons/icon-device-information.png'),
  // faq: require('../Images/Icons/faq-icon.png'),
  // home: require('../Images/Icons/icon-home.png'),
  // theme: require('../Images/Icons/icon-theme.png'),
  // usageExamples: require('../Images/Icons/icon-usage-examples.png'),
  // chevronRight: require('../Images/Icons/chevron-right.png'),
  // hamburger: require('../Images/Icons/hamburger.png'),
  // backButton: require('../Images/Icons/back-button.png'),
  // closeButton: require('../Images/Icons/close-button.png')
  patientProfilePic: require("../Images/profilePics/patient/patient.png"),
  bellPic: require("../Images/icons/bell/bell.png"),
  arrowDropDown: require("../Images/icons/arrowdropdown/arrow_drop_down.png"),
  checkBoxOff: require("../Images/icons/checkbox/checkbox_off.png"),
  // chevron: require("../Images/icons/chevron/chevron_right.png"),
  maleDoctorPic: require("../Images/profilePics/maledoctor/male_doctor.png"),
  femaleDoctorPic: require("../Images/profilePics/femaledoctor/female_doctor.png"),
  searchIcon: require("../Images/profilePics/searchicon/search_icon.png"),
  pinLeft: require("../Images/icons/pinleft/pin_left.png"),
  menuHorizontal: require("../Images/icons/menuhorizontal/menu_copy.png"),
  addRecord: require("../Images/add_record.png"),
  alertShield: require("../Images/icons/alert_shield.png"),
  profileEdit: require("../Images/icons/camera/profile_edit.png")
};

export default images;
