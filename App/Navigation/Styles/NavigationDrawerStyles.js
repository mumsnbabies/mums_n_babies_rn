import {Colors} from '../../Themes/'

export default {
  drawer: {
    backgroundColor: '#fafafa',
    elevation: 5,
    alignItems: 'center',
    shadowColor: '#000000'
  },
  main: {
    backgroundColor: Colors.clear
  }
}
