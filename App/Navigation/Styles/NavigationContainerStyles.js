import { Colors } from "../../Themes/";

export default {
  container: {
    flex: 1
  },
  navBar: {
    backgroundColor: "#ffffff"
  },
  title: {
    color: Colors.snow
  },
  leftButton: {
    tintColor: "#000",
    height: 16
  },
  rightButton: {
    color: Colors.snow
  },
  backButton: {
    height: 30,
    width: 30
  }
};
