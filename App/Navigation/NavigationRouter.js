import React, { Component } from "react";
import { Scene, Stack, Router } from "react-native-router-flux";

// screens identified by the router

import PatientShop from "../Containers/V1/Patient/PatientShop";
import PatientLearn from "../Containers/V1/Patient/PatientLearn";
import Initial from "../Containers/V1/Common/Initial";
import LogIn from "../Containers/V1/Common/LogIn";
import SignUp from "../Containers/V1/Common/SignUp";
import SignUpVerify from "../Containers/V1/Common/SignUpVerify";
import RecoverPassword from "../Containers/V1/Common/RecoverPassword";
import ResetPassword from "../Containers/V1/Common/ResetPassword";
import DoctorHomeScreen from "../Containers/V1/Doctor/DoctorHomeScreen";
import ChatRoom from "../Containers/Chat/containers/ChatHomeScreen";
import ChatsList from "../Containers/V1/Doctor/ChatsList";
import PatientConnect from "../Containers/PatientConnect";
import ChangePassword from "../Containers/V1/Common/ChangePassword";
import Requests from "../Containers/V1/Doctor/Requests";
import PatientHomeScreen from "../Containers/V1/Patient/PatientHomeScreen";
import DoctorWelcomeScreen from "../Containers/V1/Doctor/DoctorWelcomeScreen";
import PatientWelcomeScreen from "../Containers/V1/Patient/PatientWelcomeScreen";
import PatientPreWelcomeScreen from "../Containers/V1/Patient/PatientPreWelcomeScreen";
import PatientProfile from "../Containers/V1/Common/PatientProfile";
import DoctorUpdateProfile from "../Containers/V1/Doctor/DoctorUpdateProfile";
import DoctorUpdateTimings from "../Containers/V1/Doctor/DoctorUpdateTimings";
import DoctorTiming from "../Containers/V1/Doctor/DoctorTiming";
import PatientProfileUpdate from "../Containers/V1/Patient/PatientProfileUpdate";
import PatientDataUpdate from "../Containers/V1/Patient/PatientDataUpdate";
import PatientProfileDetails from "../Containers/V1/Patient/PatientProfileDetails";
import DoctorProfile from "../Containers/V1/Patient/DoctorProfile";
import PatientProfileHome from "../Containers/V1/Patient/Profile";
import PatientThresholdUpdate from "../Containers/V1/Patient/PatientThresholdUpdate";
import DocConnectScenes from "../Containers/V1/Patient/DocConnect/scenes";
import PatientProfileScenes from "../Containers/V1/Patient/Profile/scenes";
/***************************
 * Documentation: https://github.com/aksonov/react-native-router-flux
 ***************************/
import codePush from "react-native-code-push";

class NavigationRouter extends Component {
  componentDidMount() {
    codePush.sync();
  }

  render() {
    return (
      <Router>
        <Stack>
          <Scene
            hideNavBar={false}
            key="PatientShop"
            component={PatientShop}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="PdoctorProfile"
            component={DoctorProfile}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="PatientLearn"
            component={PatientLearn}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            initial
            hideNavBar={true}
            key="Initial"
            component={Initial}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="PatientProfileUpdate"
            component={PatientProfileUpdate}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="PatientDataUpdate"
            component={PatientDataUpdate}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="thresholdUpdate"
            component={PatientThresholdUpdate}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="LogIn"
            component={LogIn}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="SignUp"
            component={SignUp}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="SignUpVerify"
            component={SignUpVerify}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="RecoverPassword"
            component={RecoverPassword}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="ResetPassword"
            component={ResetPassword}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="PatientProfileDetails"
            component={PatientProfileDetails}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="ChangePassword"
            component={ChangePassword}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="DoctorUpdateProfile"
            component={DoctorUpdateProfile}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="DoctorUpdatetimings"
            component={DoctorUpdateTimings}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            key="DoctorHomeScreen"
            hideNavBar={false}
            component={DoctorHomeScreen}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="ChatsList"
            component={ChatsList}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="ChatRoom"
            component={ChatRoom}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="Requests"
            component={Requests}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="PatientProfile"
            component={PatientProfile}
            navigationBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="DoctorTiming"
            component={DoctorTiming}
            navigatonBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="PatientHomeScreen"
            component={PatientHomeScreen}
            navigatonBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={true}
            key="PatientPreWelcomeScreen"
            component={PatientPreWelcomeScreen}
            navigatonBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={false}
            key="PatientWelcomeScreen"
            component={PatientWelcomeScreen}
            navigatonBarStyle={{ backgroundColor: "#ffffff" }}
          />
          <Scene
            hideNavBar={true}
            key="DoctorWelcomeScreen"
            component={DoctorWelcomeScreen}
            navigatonBarStyle={{ backgroundColor: "#ffffff" }}
          />
          {DocConnectScenes.map(scene => scene)}
          {PatientProfileScenes.map(scene => scene)}
        </Stack>
      </Router>
    );
  }
}

export default codePush(NavigationRouter);
