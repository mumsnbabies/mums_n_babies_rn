import React, { Component } from "react"
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from "react-native"

import { observable } from "mobx"
import { observer } from "mobx-react"
import _ from "lodash"

//Stores
import FormStore from "./store/FormStore"

//Components
import FormField from "./components/FormField"
import FormParameter from "./components/FormParameter"

//Styles
import styles from "./styles"

@observer
export default class FormFactory extends Component {
  constructor(props) {
    super(props)
  }

  createFormField = formField => {
    if (formField.type === "PARAMETER") {
      return <FormParameter parameterFieldSpec={formField} key={formField.id} />
    }
    if (formField.type === "FIELD") {
      return <FormField fieldSpec={formField} key={formField.id} />
    }
  }

  componentDidMount() {
    if (Object.keys(this.props.specFile).length > 0) {
      FormStore.createFormFields(this.props.specFile)
    }
    if (Object.keys(this.props.specFile).length === 0) {
      FormStore.createFormFields("EmptyObject")
    }
  }
  componentWillReceiveProps(nextProps) {
    if (Object.keys(nextProps.specFile).length > 0) {
      FormStore.createFormFields(nextProps.specFile)
    }
    if (Object.keys(nextProps.specFile).length === 0) {
      FormStore.createFormFields("EmptyObject")
    }
  }
  render() {
    if (FormStore.formFields.length === 0) {
      return null
    }
    return (
      <ScrollView style={styles.formContainer}>
        <View style={styles.formContentContainer}>
          {FormStore.formFields.map(this.createFormField)}
        </View>
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={() => this.props.onSubmit(FormStore.formFields)}
        >
          <Text style={styles.buttonTextStyles}>Submit</Text>
        </TouchableOpacity>
      </ScrollView>
    )
  }
}
