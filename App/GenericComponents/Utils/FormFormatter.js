export default (FormFormatter = (
  patientAttributes,
  selectedAttribute = {},
  operation = "add"
) => {
  if (__DEV__) console.log(patientAttributes, "patientAttributes")
  if (operation === "add") {
    let form = {}
    switch (selectedAttribute) {
      case "WEIGHT":
        form = {
          id: 0,
          name: "FormInstance",
          fields: [
            {
              id: 0,
              name: "WEIGHT",
              type: "FIELD",
              label: "Weight",
              character: "TEXTINPUT",
              required: true,
              placeholder: "Weight",
              regex: "",
              errorText: "Please give valid name",
              showLabel: true,
              returnKeyLabel: "next",
              keyboardType: "numeric",
              unit: "kg"
            },
            {
              id: 1,
              name: "blood_pressure",
              type: "PARAMETER",
              label: "BLOOD PRESSURE",
              unit: "mg/L",
              fields: [
                {
                  id: 0,
                  name: "systolic_pressure",
                  type: "FIELD",
                  label: "Systolic Pressure",
                  character: "TEXTINPUT",
                  required: true,
                  placeholder: "Systolic Pressure",
                  errorText: "Please give valid details",
                  showLabel: true,
                  keyboardType: "numeric",
                  returnKeyLabel: "next"
                },
                {
                  id: 1,
                  name: "normal_pressure",
                  type: "FIELD",
                  label: "Diastolic Pressure",
                  required: true,
                  character: "TEXTINPUT",
                  placeholder: "Diastolic Pressure",
                  errorText: "Please give valid details",
                  showLabel: false,
                  keyboardType: "numeric"
                }
              ]
            }
          ]
        }
        return form

      case "BLOODPRESSURE":
        form = {
          id: 0,
          name: "FormInstance",
          fields: [
            {
              id: 0,
              name: "blood_pressure",
              type: "PARAMETER",
              label: "BLOOD PRESSURE",
              unit: "mg/L",
              fields: [
                {
                  id: 0,
                  name: "systolic_pressure",
                  type: "FIELD",
                  label: "Systolic Pressure",
                  character: "TEXTINPUT",
                  required: true,
                  placeholder: "Systolic Pressure",
                  errorText: "Please give valid details",
                  showLabel: true,
                  keyboardType: "numeric",
                  returnKeyLabel: "next"
                },
                {
                  id: 1,
                  name: "normal_pressure",
                  type: "FIELD",
                  label: "Diastolic Pressure",
                  required: true,
                  character: "TEXTINPUT",
                  placeholder: "Diastolic Pressure",
                  errorText: "Please give valid details",
                  showLabel: false,
                  keyboardType: "numeric"
                }
              ]
            }
          ]
        }
        return form
    }
  }
})
