import { StyleSheet, Platform } from "react-native"
import Colors from "../Themes/Colors"
import Fonts from "../Themes/Fonts"

export default StyleSheet.create({
  formContainer: {
    flex: 1,
    marginHorizontal: 20,
    flexDirection: "column"
  },
  formContentContainer: {
    elevation: 1,
    shadowOffset: { width: 10, height: 10 },
    marginTop: 20
  },
  buttonContainer: {
    backgroundColor: Colors.primaryColor,
    marginTop: 30,
    height: 50,
    borderRadius: 40,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    width: 150
  },
  buttonTextStyles: {
    justifyContent: "center",
    alignItems: "center",
    color: Colors.whiteColor,
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.family.fontfamily
  }
})
