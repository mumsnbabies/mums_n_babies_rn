import React, { Component } from "react"
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from "react-native"

import { observable } from "mobx"
import { observer } from "mobx-react"
import _ from "lodash"

//Styles
import styles from "./styles"
//Components
import FormFactory from "../index"

@observer
export default class Example extends Component {
  @observable spec_file: Object
  constructor(props) {
    super(props)
    this.spec_file = {
      form: {
        id: 0,
        name: "NewForm",
        fields: [
          {
            id: 0,
            name: "first_name",
            type: "FIELD",
            label: "FirstName",
            character: "TEXTINPUT",
            required: true,
            placeholder: "FirstName",
            regex: "^[a-zA-Z ]{2,30}$",
            errorText: "Please give valid name",
            showLabel: true,
            returnKeyLabel: "next"
          },
          {
            id: 1,
            name: "blood_pressure",
            type: "PARAMETER",
            label: "BLOOD PRESSURE",
            unit: "mg/L",
            fields: [
              {
                id: 0,
                name: "systolic_pressure",
                type: "FIELD",
                label: "Systolic Pressure",
                character: "TEXTINPUT",
                required: true,
                placeholder: "Systolic Pressure",
                errorText: "Please give valid details",
                showLabel: true,
                keyboardType: "numeric",
                returnKeyLabel: "next"
              },
              {
                id: 1,
                name: "normal_pressure",
                type: "FIELD",
                label: "Diastolic Pressure",
                required: true,
                character: "TEXTINPUT",
                placeholder: "Diastolic Pressure",
                errorText: "Please give valid details",
                showLabel: false,
                keyboardType: "numeric"
              }
            ]
          }
        ]
      }
    }
  }

  createParameterRequestObject = parameterField => {
    let formattedAttributedValues = []
    parameterField.parameterFieldList.map(field => {
      formattedAttributedValues.push({
        value_key: field.name,
        value: field.value
      })
    })
    let finalObject = {
      attr_key: parameterField.name,
      description: parameterField.unit,
      attr_values: formattedAttributedValues,
      to_add: true
    }
    return finalObject
  }
  createFieldRequestObject = field => {
    let formattedAttributedrequestObject = {
      attr_key: field.name,
      description: field.unit,
      attr_values: [
        {
          value_key: field.name,
          value: field.value
        }
      ],
      to_add: true
    }
    return formattedAttributedrequestObject
  }
  onSubmit = formFields => {
    let parameterRequestObjects = []
    let fieldRequestObjects = []
    formFields.map(formField => {
      if (formField.type === "PARAMETER") {
        let formattedObject = this.createParameterRequestObject(formField)
        parameterRequestObjects.push(formattedObject)
      }
      if (formField.type === "FIELD") {
        let formattedObject = this.createFieldRequestObject(formField)
        fieldRequestObjects.push(formattedObject)
      }
    })
    let requestObject = {
      attributes: [...fieldRequestObjects, ...parameterRequestObjects],
      entity_id: 0,
      activity_required: true,
      entity_type: "string"
    }

    //Make the network call
  }
  render() {
    return (
      <View style={styles.mainContainer}>
        <FormFactory specFile={this.spec_file.form} onSubmit={this.onSubmit} />
      </View>
    )
  }
}

/**
 * 
 */
