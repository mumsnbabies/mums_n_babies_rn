import { StyleSheet, Platform } from "react-native"

export default StyleSheet.create({
  mainContainer: {
    flex: 1
  }
})
