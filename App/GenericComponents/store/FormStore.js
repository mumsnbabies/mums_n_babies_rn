import { observable, computed, action } from "mobx"

import FormField from "./models/FormField"
import FormParameter from "./models/FormParameter"

class FormStore {
  @observable formId
  @observable onSubmit: Function
  @observable formName: String
  @observable formFields: Array
  constructor() {
    this.formFields = observable([])
  }

  @action("CREATE_FORM_FIELDS_PARAMETERS")
  createFormFields = formSpec => {
    if (formSpec === "EmptyObject") {
      this.formFields = observable([])
    }
    if (__DEV__) console.log("formSpec", formSpec)
    if (formSpec != "EmptyObject") {
      this.formId = formSpec.id
      this.formName = formSpec.name
      this.formFields = formSpec.fields.map(specField => {
        if (specField.type === "PARAMETER") {
          return new FormParameter(this, specField)
        }
        if (specField.type === "FIELD") {
          return new FormField(this, specField)
        }
      })
    }
  }
}

const formStore = new FormStore()
export default formStore
