import { observable, computed, action } from "mobx"

import FormField from "./FormField"

class FormParameter {
  formStore
  className
  unit
  @observable id
  @observable name
  @observable type
  @observable label
  @observable parameterFieldList
  constructor(store, formParameter) {
    this.formStore = store
    this.name = formParameter.name
    this.type = formParameter.type
    this.id = formParameter.id
    this.label = formParameter.label
    this.parameterFieldList = formParameter.fields.map(
      this.createParameterFields
    )
    this.unit = formParameter.unit || ""
  }

  @action("CREATE_FORM_FIELDS_PARAMETERS")
  createParameterFields = field => {
    return new FormField(this, field)
  }
}

export default FormParameter
