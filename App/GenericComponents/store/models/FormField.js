import { observable, computed, action } from "mobx"

class FormField {
  creatorStore
  unit
  keyboardType
  @observable id
  @observable type
  @observable character
  @observable name
  @observable label
  @observable placeholder
  @observable regex
  @observable required
  @observable returnKeyLabel
  @observable errorText
  @observable showLabel
  @observable isFocus

  @observable isTouched
  @observable value
  @observable isError

  constructor(store, field) {
    this.creatorStore = store
    this.id = field.id
    this.type = field.type
    this.character = field.character
    this.name = field.name
    this.label = field.label
    this.placeholder = field.placeholder || "Please Fill"
    this.regex = field.regex || "//g"
    this.required = field.required || false
    this.errorText = field.errorText
    this.showLabel = field.showLabel || false
    this.returnKeyLabel = field.returnKeyLabel || "next"
    this.value = field.value || null
    this.isError = false
    this.isFocus = false
    this.keyboardType = field.keyboardType || "default"
    this.unit = field.unit || ""
  }

  @action("HANDLE_FORM_FIELD_CHANGE")
  handleValueChange = value => {
    this.value = value
    this.validateField()
  }

  @action("HANDLE_FORM_FIELD_FOCUS")
  handleFocus = () => {
    this.isFocus = true
  }

  @action("HANDLE_FORM_FIELD_BLUR")
  handleBlur = () => {
    this.isFocus = false
  }

  @action("HANDLE_FORM_FIELD_FOCUS")
  validateField = () => {
    if (this.value === "") {
      this.isError = true
      return
    }
    // let theRegex = new RegExp(this.regex)
    // if (!theRegex.test(this.value)) {
    //   this.isError = true
    //   return
    // }
    this.isError = false
  }
}

export default FormField
