import React, { Component } from "react"
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from "react-native"

import { observable } from "mobx"
import { observer } from "mobx-react"
import _ from "lodash"

//Styles
import styles from "./styles"

import Images from "../../../Themes/Images"
@observer
export default class FormField extends Component {
  @observable regex
  constructor(props) {
    super(props)
    this.regex = this.props.fieldSpec.regex
  }

  renderContainerClassName = () => {
    const { isFocus, isError } = this.props.fieldSpec

    if (!isFocus && !isError) {
      return [styles.formFieldContainer]
    }

    if (!isFocus && isError) {
      return [styles.formFieldContainer, styles.errorFormFieldContainer]
    }

    if (isFocus) {
      return [styles.formFieldContainer, styles.focussedFormFieldContainer]
    }
  }
  renderTextInputClassName = () => {
    const { isFocus } = this.props.fieldSpec
    if (!isFocus) {
      return this.props.fieldSpec.className || styles.formFieldStyles
    }
    if (isFocus) {
      return [
        this.props.fieldSpec.className || styles.formFieldStyles,
        styles.focussedFormFieldStyles
      ]
    }
  }
  renderLabelClassName = () => {
    const { isFocus } = this.props.fieldSpec
    if (!isFocus) {
      return styles.labelTextStyles
    }
    if (isFocus) {
      return [styles.labelTextStyles, styles.focussedLabelTextStyles]
    }
  }
  createFormField = formField => {
    if (formField.character === "TEXTINPUT") {
      return (
        <TextInput
          autoCorrect={false}
          placeholder={`${formField.placeholder}${formField.required
            ? "*"
            : ""}`}
          style={this.renderTextInputClassName()}
          returnKeyLabel={formField.returnKeyLabel}
          underlineColorAndroid="transparent"
          onChangeText={formField.handleValueChange}
          value={formField.value}
          onFocus={formField.handleFocus}
          onBlur={formField.handleBlur}
        />
      )
    }
  }
  createFormFieldLabel = formField => {
    if (
      formField.showLabel &&
      formField.value != "" &&
      formField.value != null
    ) {
      return (
        <View style={styles.fieldLabelContainer}>
          <Text
            style={styles.labelTextStyles}
          >{`${formField.label}${formField.required ? "*" : ""}`}</Text>
        </View>
      )
    }
    return null
  }
  render() {
    const { fieldSpec } = this.props
    return (
      <View style={styles.mainContainer}>
        {this.createFormFieldLabel(fieldSpec)}
        <View style={{ flexDirection: "row", paddingLeft: 20 }}>
          <View style={styles.textInputContainer}>
            <TextInput
              autoCorrect={false}
              style={styles.inputFieldStyles}
              placeholder={`${fieldSpec.placeholder}${fieldSpec.required
                ? "*"
                : ""}`}
              underlineColorAndroid="transparent"
              returnKeyLabel={fieldSpec.returnKeyLabel}
              onChangeText={fieldSpec.handleValueChange}
              value={fieldSpec.value}
              keyboardType={fieldSpec.keyboardType}
            />
          </View>
          {fieldSpec.isError && (
            <View style={styles.imageContainer}>
              <Image style={styles.requiredField} source={Images.alertShield} />
            </View>
          )}
        </View>
      </View>
    )
  }
}
