import { StyleSheet, Platform } from "react-native"
import Colors from "../../../Themes/Colors"
import Fonts from "../../../Themes/Fonts"
export default StyleSheet.create({
  formFieldContainer: {
    flexDirection: "column",
    borderWidth: 1,
    borderColor: "rgba(216, 216, 216, 0.5)",
    borderRadius: 2,
    marginVertical: 5,
    height: 60,
    paddingVertical: 2
  },
  fieldLabelContainer: {
    marginTop: 2,
    marginLeft: 4
  },
  labelTextStyles: {
    fontSize: 14
  },
  focussedLabelTextStyles: {
    color: "#0287CC"
  },
  formFieldStyles: {
    flex: 4,
    paddingTop: 5,
    color: "#B1B1B1",
    paddingLeft: 20,
    fontSize: 15
  },
  focussedFormFieldContainer: {
    borderColor: "#0287CC"
  },
  focussedFormFieldStyles: {
    color: "#0287CC"
  },
  errorFormFieldContainer: {
    borderColor: "red"
  },

  mainContainer: {
    marginTop: 10,
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: Colors.lightblack
  },
  textInputContainer: {
    flex: 10
  },
  inputFieldStyles: {
    color: Colors.blackColor,
    borderRadius: 5,
    fontFamily: Fonts.family.fontfamily
  },
  imageContainer: {
    paddingRight: 10,
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  requiredField: {
    width: 20,
    height: 20
  },
  underline: {
    borderBottomWidth: 0.8,
    borderColor: Colors.lightblack
  }
})
