import { StyleSheet, Platform } from "react-native"
import Colors from "../../../Themes/Colors"
import Fonts from "../../../Themes/Fonts"

export default StyleSheet.create({
  formParameterContainer: {
    marginTop: 15,
    marginBottom: 5
  },
  formParameterLabelStyles: {
    fontSize: Fonts.sizes.normal,
    fontFamily: Fonts.family.fontfamily
  }
})
