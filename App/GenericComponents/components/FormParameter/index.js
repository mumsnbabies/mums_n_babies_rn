import React, { Component } from "react"
import {
  ScrollView,
  Image,
  Text,
  Button,
  View,
  TextInput,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from "react-native"

import { observable } from "mobx"
import { observer } from "mobx-react"
import _ from "lodash"

//Components
import FormField from "../FormField"

//Styles
import styles from "./styles"

@observer
export default class FormParameter extends Component {
  constructor(props) {
    super(props)
  }

  createFormField = formField => {
    if (formField.type === "FIELD" && formField.character === "TEXTINPUT") {
      return (
        <View style={styles.parameterFieldsContainer} key={formField.id}>
          <FormField fieldSpec={formField} />
        </View>
      )
    }
  }

  render() {
    const { parameterFieldSpec } = this.props
    if (__DEV__) console.log(parameterFieldSpec, "parameterFieldSpec")
    return (
      <View style={styles.formParameterContainer}>
        <Text style={styles.formParameterLabelStyles}>
          {parameterFieldSpec.label}
        </Text>
        {parameterFieldSpec.parameterFieldList.map(this.createFormField)}
      </View>
    )
  }
}
