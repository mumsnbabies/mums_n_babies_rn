package com.mums_n_babies;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import com.microsoft.codepush.react.CodePush;

import com.facebook.react.ReactApplication;
import com.rnfs.RNFSPackage;
import co.ibhubs.ibchatrn.MqttIoTManagerPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.wix.autogrowtextinput.AutoGrowTextInputPackage;
import com.github.wuxudong.rncharts.MPAndroidChartPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.imagepicker.ImagePickerPackage;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import com.horcrux.svg.SvgPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends MultiDexApplication implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

    @Override
    protected String getJSBundleFile() {
      return CodePush.getJSBundleFile();
    }

    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new RNFSPackage(),
            new MqttIoTManagerPackage(),
            new RNDeviceInfo(),
            new AutoGrowTextInputPackage(),
            new MPAndroidChartPackage(),
            new VectorIconsPackage(),
            new ImagePickerPackage(),
            new ReactNativeConfigPackage(),
            new SvgPackage(),
            new CodePush("MbGNGWZ-zv0Q0GfhJwV4Cc56UG5cc43eb530-b653-4e0f-b588-bb470b9b9a7d", MainApplication.this, BuildConfig.DEBUG)
      );
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
